package mobile.android.emvl2test.activity;

public class EmvTermParam
{
  public static String IFD;
  public static String TerminalCountry;
  public static byte ucTermType;
  public static String TermCapa;
  public static String AdducTermCapa;
  public static String MerchantNameLocation;
  public static String MerchantCode;
  public static String MerchantID;
  public static String AcquirerID;
  public static String TermID;
  public static byte ucTranRefCurrExp;
  public static String TranRefCurr;
  public static byte ucTranCurrExp;
  public static String TranCurrCode;
  public static byte ucTermSMSupportIndicator;
  public static byte ucTermFLmtFlg;
  public static byte ucRFTxnLmtFlg;
  public static byte ucRFFLmtFlg;
  public static byte ucRFCVMLmtFlg;
  public static byte ucRFStatusCheckFlg;
  public static byte ucRFZeroAmtNoAllowed;
  public static byte ucPrintfDebugInfo;
  public static byte ucUseFangba;
  public static byte ucEmvTest;
  public static String emvParamFilePath;
  
  public EmvTermParam() {}
}
