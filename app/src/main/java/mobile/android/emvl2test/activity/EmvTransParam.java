package mobile.android.emvl2test.activity;

public class EmvTransParam
{
  public byte ucTransKernalType;
  public byte ucEcTerminalSupportIndicator;
  public String ReaderTTQ;
  public String TransNo;
  public String TransDate;
  public String TransTime;
  public String AmountAuth;
  public String AmountOther;
  public byte ucTransType;
  
  public EmvTransParam() {}
}
