package mobile.android.emvl2test.activity;

public abstract interface TransAPDU
{
  public abstract byte[] onTransmitApdu(byte[] paramArrayOfByte);
  
  public abstract String onGetDataPath();
}
