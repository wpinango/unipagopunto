package mobile.android.emvl2test.activity;

import android.util.Log;
import com.android.citic.lib.utils.DateUtil;
import com.imagpay.beans.EMVApp;
import com.imagpay.beans.EMVCapk;
import com.imagpay.beans.EMVRevoc;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;






















public class EmvCoreJNI
{
  public static final int KERNAL_EMV_PBOC = 0;
  public static final int KERNAL_CONTACTLESS_ENTRY_POINT = 1;
  public static final int RD_CVM_NO = 0;
  public static final int RD_CVM_SIG = 16;
  public static final int RD_CVM_ONLINE_PIN = 17;
  public static final int APPROVE_M = 64;
  public static final int DECLINE_M = 0;
  public static final int ONLINE_M = 128;
  private static ArrayList<TransAPDU> tras = new ArrayList();
  public static final int STATIC_TRANS_INIT = 0;
  public static final int STATIC_EMV_CORE_INIT = 1;
  
  public void addTransMethod(TransAPDU tra) { if (tras.contains(tra))
      return;
    tras.clear();
    tras.add(tra);
  }
  
  public String exists() {
    /*try { InputStream io = getClass().getResourceAsStream("/data/AidRec.data");
      File dir = new File("/sdcard/AidRec.data");
      if (!dir.exists()) {
        FileOutputStream out = new FileOutputStream("/sdcard/AidRec.data");
        int length = io.available();
        byte[] buffer = new byte[length];
        io.read(buffer);
        out.write(buffer);
        out.close();
      }
      io.close();
      InputStream io2 = getClass().getResourceAsStream("/data/CapkRec.data");
      File dir2 = new File("/sdcard/CapkRec.data");
      if (!dir2.exists()) {
        FileOutputStream out2 = new FileOutputStream("/sdcard/CapkRec.data");
        int length2 = io2.available();
        byte[] buffer2 = new byte[length2];
        io2.read(buffer2);
        out2.write(buffer2);
        out2.close();
      }
      io2.close();
    }
    catch (IOException e) {
      e.printStackTrace();
    }*/
    return "/sdcard/";
  }
  
  public static final int STATIC_QTRANS_PRO_PROEC = 2;
  public static final int STATIC_INPUT_AMOUNT = 3;
  public static final int STATIC_QTRANS_INIT = 4;
  public static final int STATIC_SWIPE_CARD = 5;
  public static final int STATIC_INPUT_PIN = 7;
  public static final int STATIC_ONLINE = 8;
  public byte[] EmvCbExchangeApdu(byte[] send)
  {
    try {
      if (tras.size() > 0) {
        TransAPDU tra = (TransAPDU)tras.get(0);
        return tra.onTransmitApdu(send);
      }
      return null;
    } catch (Exception e) {}
    return null;
  }
  
  public String EmvCbGetEmvFilePath() {
    try {
      String path = new String();
      if (tras.size() > 0) {
        TransAPDU tra = (TransAPDU)tras.get(0);
        path = tra.onGetDataPath();
        
        Log.d("Debug", path);
        return path;
      }
      return null;
    }
    catch (Exception localException) {}
    return null;
  }
  
  static
  {
    System.loadLibrary("EmvCoreJNI");
  }
  


  public static final int STATIC_TRANS_END = 9;
  

  public static final int STATIC_QTRANS = 16;
  

  public static final int STATIC_BALANCE_QUERY = 17;
  

  public static final int STATIC_READ_CARD_NO = 18;
  

  public static final int STATIC_INPUT_AMOUNT_END = 32;
  
  public static final int STATIC_INPUT_PIN_END = 33;
  
  public static final int STATIC_SWIPE_CARD_END = 34;
  
  public static final int STATIC_DETECTED_CARD = 19;
  
  public EmvCoreJNI()
  {
    EmvTermParam.IFD = "12345678";
    EmvTermParam.TerminalCountry = "0156";
    EmvTermParam.ucTermType = 34;
    
    EmvTermParam.TermCapa = "E0F8C8";
    



    EmvTermParam.AdducTermCapa = "6000F0A001";
    
    EmvTermParam.MerchantNameLocation = "SZZCS";
    EmvTermParam.MerchantCode = "3031";
    EmvTermParam.MerchantID = "123456789012345";
    EmvTermParam.AcquirerID = "12345678912";
    EmvTermParam.TermID = "12345678";
    EmvTermParam.ucTranRefCurrExp = 2;
    EmvTermParam.TranRefCurr = "0156";
    EmvTermParam.ucTranCurrExp = 2;
    EmvTermParam.TranCurrCode = "0156";
    
    EmvTermParam.ucTermSMSupportIndicator = 0;
    EmvTermParam.ucTermFLmtFlg = 1;
    EmvTermParam.ucRFTxnLmtFlg = 1;
    EmvTermParam.ucRFFLmtFlg = 1;
    EmvTermParam.ucRFCVMLmtFlg = 1;
    
    EmvTermParam.ucRFStatusCheckFlg = 0;
    EmvTermParam.ucRFZeroAmtNoAllowed = 1;
    
    EmvTermParam.ucPrintfDebugInfo = 1;
    EmvTermParam.ucUseFangba = 0;
    EmvTermParam.ucEmvTest = 1;
    
    EmvTermParam.emvParamFilePath = EmvCbGetEmvFilePath();
    EmvCoreInit();
  }
  

  public void EmvQTransParamInit(String AmountAuth)
  {
    EmvTransParam emvTransParam = new EmvTransParam();
    
   /* ucTransKernalType = 1;
    ucEcTerminalSupportIndicator = 0;
    ReaderTTQ = "26000080";
    TransDate = DateUtil.DateToStr(new Date(), "yyMMdd");
    TransTime = DateUtil.DateToStr(new Date(), "HHmmss");
    AmountAuth = AmountAuth;
    AmountOther = "000000000000";
    TransNo = "00000001";
    ucTransType = 0;*/
    
    EmvTransParamInit(emvTransParam);
  }
  



  public String packAuthorisationRequest()
  {
    int[] tags = { 130, 40758, 40742, 40743, 40756, 40734, 
      40720, 40755, 40757, 149, 40759, 40705, 40706, 40707, 
      24357, 24356, 90, 24372, 40725, 40726, 40730, 40732, 87, 24362, 154, 40737, 156, 
      153, 40761, 40740 };
    return getIcField(tags);
  }
  


  public String packRequest()
  {
    int[] tags = { 130, 40758, 40711, 40742, 40743, 142, 40756, 40734, 40717, 
      40718, 40719, 40720, 40755, 40757, 149, 40759, 40705, 40706, 40707, 
      24357, 24356, 90, 24372, 24360, 40725, 40726, 40730, 40732, 87, 24362, 154, 40737, 156, 
      153, 40761, 40740 };
    return getIcField(tags);
  }
  


  public String packConfirmation()
  {
    int[] tags = { 40758, 40742, 40755, 40757, 149, 40759, 40705, 40706, 40707, 
      24357, 24356, 90, 156, 138, 57137, 113, 114, 40732, 40769, 155, 40761, 40740 };
    return getIcField(tags);
  }
  


  public String packReversal()
  {
    int[] tags = { 130, 40758, 40734, 40720, 40755, 40757, 149, 40705, 24356, 
      90, 24372, 40725, 40726, 40730, 24362, 154, 40737, 156, 87, 
      138, 57137, 113, 114, 40732, 40769, 155, 40761, 40740 };
    return getIcField(tags);
  }
  


  public String packTransResult()
  {
    int[] tags = { 57137, 149, 155 };
    return getIcField(tags);
  }
  


  public String getIcField(int[] tags)
  {
    StringBuffer sb = new StringBuffer();
    for (int i : tags) {
      String res = EmvGetTLVData(i);
      if (res != null) {
        sb.append(getHexString(i));
        sb.append(getHexString(res.length() / 2));
        sb.append(res);
      }
    }
    
    return sb.toString();
  }
  
  public static String getHexString(int i) {
    String tmp = Integer.toHexString(i);
    if (tmp.length() == 1) {
      return "0" + tmp;
    }
    return tmp;
  }
  
  public native void EmvCoreInit();
  
  public native void EmvTransParamInit(EmvTransParam paramEmvTransParam);
  
  public native int addApp(EMVApp paramEMVApp);
  
  public native int addCapk(EMVCapk paramEMVCapk);
  
  public native int addRevoc(EMVRevoc paramEMVRevoc);
  
  public native int EmvQTrans(byte[] paramArrayOfByte1, byte[] paramArrayOfByte2, byte[] paramArrayOfByte3);
  
  public native int EmvQBalanceQuery(byte[] paramArrayOfByte1, byte[] paramArrayOfByte2);
  
  public native int EmvReadPANProc(int paramInt, byte[] paramArrayOfByte, String[] paramArrayOfString1, String[] paramArrayOfString2);
  
  public native String EmvGetTLVData(int paramInt);
  
  public native int EmvGetTrack2AndPAN(String[] paramArrayOfString1, String[] paramArrayOfString2);
}
