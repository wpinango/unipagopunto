package com.ivsign.android.IDCReader;

import android.graphics.Bitmap;



public class UserIDCardInfo
{
  private String uName;
  private String uSex;
  private String uNation;
  private String uBirthday;
  private String uAddress;
  private String uID;
  private String uAuthor;
  private String uExpDateStart;
  private String uExpDateEnd;
  private String uRemark;
  private Bitmap uImage;
  
  public UserIDCardInfo() {}
  
  public String getuName()
  {
    return uName;
  }
  
  public void setuName(String uName) {
    this.uName = uName;
  }
  
  public String getuSex() {
    return uSex;
  }
  
  public void setuSex(String uSex) {
    this.uSex = uSex;
  }
  
  public String getuNation() {
    return uNation;
  }
  
  public void setuNation(String uNation) {
    this.uNation = uNation;
  }
  
  public String getuBirthday() {
    return uBirthday;
  }
  
  public void setuBirthday(String uBirthday) {
    this.uBirthday = uBirthday;
  }
  
  public String getuAddress() {
    return uAddress;
  }
  
  public void setuAddress(String uAddress) {
    this.uAddress = uAddress;
  }
  
  public String getuID() {
    return uID;
  }
  
  public void setuID(String uID) {
    this.uID = uID;
  }
  
  public String getuAuthor() {
    return uAuthor;
  }
  
  public void setuAuthor(String uAuthor) {
    this.uAuthor = uAuthor;
  }
  
  public String getuExpDateStart() {
    return uExpDateStart;
  }
  
  public void setuExpDateStart(String uExpDateStart) {
    this.uExpDateStart = uExpDateStart;
  }
  
  public String getuExpDateEnd() {
    return uExpDateEnd;
  }
  
  public void setuExpDateEnd(String uExpDateEnd) {
    this.uExpDateEnd = uExpDateEnd;
  }
  
  public String getuRemark() {
    return uRemark;
  }
  
  public void setuRemark(String uRemark) {
    this.uRemark = uRemark;
  }
  
  public Bitmap getuImage() {
    return uImage;
  }
  
  public void setuImage(Bitmap uImage) {
    this.uImage = uImage;
  }
}
