package com.ivsign.android.IDCReader;

import android.os.Environment;





public class IDCReaderSDK
{
  private static final String TAG = "unpack";
  
  public static int Init()
  {
    return InitWithPath(Environment.getExternalStorageDirectory().toString());
  }
  


  public static int InitWithPath(String path)
  {
    return wltInit(path + "/wltlib");
  }
  
  public static int unpack(byte[] wltdata, byte[] licdata) {
    return wltGetBMP(wltdata, licdata);
  }
  







  static
  {
    System.loadLibrary("wltdecode");
  }
  
  public IDCReaderSDK() {}
  
  public static native int wltInit(String paramString);
  
  public static native int wltGetBMP(byte[] paramArrayOfByte1, byte[] paramArrayOfByte2);
}
