package com.android.citic.lib.utils;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;






public class StringUtil
{
  public StringUtil() {}
  
  public static boolean isNullWithTrim(String str)
  {
    return (str == null) || (str.trim().equals("")) || (str.trim().equals("null"));
  }
  






  public static String getSecurityNum(String cardNo, int prefix, int suffix)
  {
    StringBuffer cardNoBuffer = new StringBuffer();
    int len = prefix + suffix;
    if (cardNo.length() > len) {
      cardNoBuffer.append(cardNo.substring(0, prefix));
      for (int i = 0; i < cardNo.length() - len; i++) {
        cardNoBuffer.append("*");
      }
      cardNoBuffer.append(cardNo.substring(cardNo.length() - suffix, cardNo.length()));
    }
    return cardNoBuffer.toString();
  }
  
  public static String TwoWei(double s) {
    DecimalFormat df = new DecimalFormat("0.00");
    return df.format(s);
  }
  




  public static String TwoWei(String amount)
  {
    DecimalFormat df = new DecimalFormat("0.00");
    double d = 0.0D;
    if (!isNullWithTrim(amount))
      d = Double.parseDouble(amount) / 100.0D;
    return df.format(d);
  }
  







  public static String StringPattern(String date, String oldPattern, String newPattern)
  {
    if ((date == null) || (oldPattern == null) || (newPattern == null))
      return "";
    SimpleDateFormat sdf1 = new SimpleDateFormat(oldPattern);
    SimpleDateFormat sdf2 = new SimpleDateFormat(newPattern);
    Date d = null;
    try {
      d = sdf1.parse(date);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return sdf2.format(d);
  }
}
