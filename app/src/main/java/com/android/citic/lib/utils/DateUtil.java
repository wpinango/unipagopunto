package com.android.citic.lib.utils;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil
{
  public DateUtil() {}
  
  public static String getNetworkTime() throws Exception
  {
    URL url = new URL("http://www.bjtime.cn");
    java.net.URLConnection uc = url.openConnection();
    uc.connect();
    long ld = uc.getDate();
    Date date = new Date(ld);
    
    return date.getYear() + "-" + date.getMonth() + "-" + date.getDay() + "  " + 
      date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
  }
  




  public static int getNetype(android.content.Context context)
  {
    int netType = -1;
    ConnectivityManager connMgr = (ConnectivityManager)context.getSystemService("connectivity");
    NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
    if (networkInfo == null) {
      return netType;
    }
    int nType = networkInfo.getType();
    if (nType == 0) {
      if (networkInfo.getExtraInfo().toLowerCase().equals("cmnet")) {
        netType = 3;
      } else {
        netType = 2;
      }
    } else if (nType == 1) {
      netType = 1;
    }
    return netType;
  }
  
  public static String getSysTime() {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    return sdf.format(new Date());
  }
  
  public static String getHMS() {
    Calendar calendar = Calendar.getInstance();
    return str2int(calendar.get(11)) + 
      str2int(calendar.get(12)) + 
      str2int(calendar.get(13));
  }
  
  public static String getYMD() {
    Calendar calendar = Calendar.getInstance();
    return str2int(calendar.get(1)) + 
      str2int(calendar.get(2)) + 
      str2int(calendar.get(13));
  }
  
  public static String str2int(int date) {
    String temp = String.valueOf(date);
    if (temp.length() == 1) {
      return "0" + temp;
    }
    return temp;
  }
  
  public static String strToDateFormat(String str, String format) {
    return DateToStr(StrToDate(str), format);
  }
  




  public static Date StrToDate(String str)
  {
    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    Date date = null;
    try {
      date = format.parse(str);
    } catch (ParseException e) {
      e.printStackTrace();
    }
    return date;
  }
  
  public static Date StrToDate(String str, String formatString) {
    SimpleDateFormat format = new SimpleDateFormat(formatString);
    Date date = null;
    try {
      date = format.parse(str);
    } catch (ParseException e) {
      e.printStackTrace();
    }
    return date;
  }
  





  public static String DateToStr(Date date, String formatString)
  {
    String str = null;
    try {
      SimpleDateFormat format = new SimpleDateFormat(formatString);
      str = format.format(date);
    }
    catch (Exception localException) {}
    return str;
  }
  



  public static int getYear()
  {
    Calendar c = Calendar.getInstance();
    int year = c.get(1);
    return year;
  }
  





  public static String printStr(String date, String time)
  {
    String newdate = "";
    if ((!StringUtil.isNullWithTrim(date)) && (!StringUtil.isNullWithTrim(time))) {
      if (time.length() == 5) {
        newdate = 
          date.substring(0, 4) + "/" + date.substring(4, 6) + "/" + date.substring(6, 8) + "  " + "0" + time.substring(0, 1) + ":" + time.substring(1, 3);
      } else {
        newdate = 
          date.substring(0, 4) + "/" + date.substring(4, 6) + "/" + date.substring(6, 8) + "  " + time.substring(0, 2) + ":" + time.substring(2, 4);
      }
      return newdate; }
    return "    ";
  }
}
