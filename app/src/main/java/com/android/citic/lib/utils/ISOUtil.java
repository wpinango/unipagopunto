package com.android.citic.lib.utils;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.BitSet;
import java.util.StringTokenizer;







































public class ISOUtil
{
  public static final String[] hexStrings = new String['Ā'];
  static { for (int i = 0; i < 256; i++) {
      StringBuilder d = new StringBuilder(2);
      char ch = Character.forDigit((byte)i >> 4 & 0xF, 16);
      d.append(Character.toUpperCase(ch));
      ch = Character.forDigit((byte)i & 0xF, 16);
      d.append(Character.toUpperCase(ch));
      hexStrings[i] = d.toString();
    }
  }
  



  public static final Charset CHARSET = Charset.forName("UTF-8");
  

  public static final byte STX = 2;
  

  public static final byte FS = 28;
  

  public static final byte US = 31;
  
  public static final byte RS = 29;
  
  public static final byte GS = 30;
  
  public static final byte ETX = 3;
  

  public ISOUtil() {}
  

  public static String padleft(String s, int len, char c)
  {
    s = s.trim();
    if (s.length() > len)
      return null;
    StringBuilder d = new StringBuilder(len);
    int fill = len - s.length();
    while (fill-- > 0)
      d.append(c);
    d.append(s);
    return d.toString();
  }
  










  public static String padright(String s, int len, char c)
  {
    s = s.trim();
    StringBuilder d = new StringBuilder(len);
    int fill = len - s.length();
    d.append(s);
    while (fill-- > 0)
      d.append(c);
    return d.toString();
  }
  






  public static String trim(String s)
  {
    return s != null ? s.trim() : null;
  }
  










  public static String zeropad(String s, int len)
  {
    return padleft(s, len, '0');
  }
  









  public static String zeropad(long l, int len)
  {
    return padleft(Double.toString((l % Math.pow(10.0D, len))), len, '0');
  }
  








  public static String strpad(String s, int len)
  {
    StringBuilder d = new StringBuilder(s);
    while (d.length() < len)
      d.append(' ');
    return d.toString();
  }
  
  public static String zeropadRight(String s, int len) {
    StringBuilder d = new StringBuilder(s);
    while (d.length() < len)
      d.append('0');
    return d.toString();
  }
  













  public static byte[] str2bcd(String s, boolean padLeft, byte[] d, int offset)
  {
    int len = s.length();
    int start = ((len & 0x1) == 1) && (padLeft) ? 1 : 0;
    for (int i = start; i < len + start; i++) {
      char c = s.charAt(i - start);
      if ((c >= '0') && (c <= '?')) {
        c = (char)(c - '0');
      } else {
        c = (char)(c & 0xFFFFFFDF);
        c = (char)(c - '7');
      }
      int tmp91_90 = (offset + (i >> 1)); byte[] tmp91_84 = d;tmp91_84[tmp91_90] = ((byte)(tmp91_84[tmp91_90] | c << ((i & 0x1) == 1 ? 0 : '\004')));
    }
    return d;
  }
  








  public static byte[] str2bcd(String s, boolean padLeft)
  {
    if (s == null)
      return null;
    int len = s.length();
    byte[] d = new byte[len + 1 >> 1];
    return str2bcd(s, padLeft, d, 0);
  }
  











  public static byte[] str2bcd(String s, boolean padLeft, byte fill)
  {
    int len = s.length();
    byte[] d = new byte[len + 1 >> 1];
    Arrays.fill(d, fill);
    int start = ((len & 0x1) == 1) && (padLeft) ? 1 : 0;
    for (int i = start; i < len + start; i++) {
      char c = s.charAt(i - start);
      if ((c >= '0') && (c <= '?')) {
        c = (char)(c - '0');
      } else {
        c = (char)(c & 0xFFFFFFDF);
        c = (char)(c - '7');
      }
      int tmp97_96 = (i >> 1); byte[] tmp97_91 = d;tmp97_91[tmp97_96] = ((byte)(tmp97_91[tmp97_96] | c << ((i & 0x1) == 1 ? 0 : '\004')));
    }
    
    return d;
  }
  












  public static String bcd2str(byte[] b, int offset, int len, boolean padLeft)
  {
    StringBuilder d = new StringBuilder(len);
    int start = ((len & 0x1) == 1) && (padLeft) ? 1 : 0;
    for (int i = start; i < len + start; i++) {
      int shift = (i & 0x1) == 1 ? 0 : 4;
      char c = Character.forDigit(b[(offset + (i >> 1))] >> shift & 0xF, 16);
      if (c == 'd')
        c = '=';
      d.append(Character.toUpperCase(c));
    }
    return d.toString();
  }
  







  public static String hexString(byte[] b)
  {
    StringBuilder d = new StringBuilder(b.length * 2);
    byte[] arrayOfByte = b;int j = b.length; for (int i = 0; i < j; i++) { byte aB = arrayOfByte[i];
      d.append(hexStrings[(aB & 0xFF)]);
    }
    return d.toString();
  }
  











  public static String hexString(byte[] b, int offset, int len)
  {
    StringBuilder d = new StringBuilder(len * 2);
    len += offset;
    for (int i = offset; i < len; i++) {
      d.append(hexStrings[(b[i] & 0xFF)]);
    }
    return d.toString();
  }
  






  public static String bitSet2String(BitSet b)
  {
    int len = b.size();
    len = len > 128 ? 128 : len;
    StringBuilder d = new StringBuilder(len);
    for (int i = 0; i < len; i++)
      d.append(b.get(i) ? '1' : '0');
    return d.toString();
  }
  






  public static byte[] bitSet2byte(BitSet b)
  {
    int len = b.length() + 62 >> 6 << 6;
    byte[] d = new byte[len >> 3];
    for (int i = 0; i < len; i++)
      if (b.get(i + 1)) {
        int tmp39_38 = (i >> 3); byte[] tmp39_35 = d;tmp39_35[tmp39_38] = ((byte)(tmp39_35[tmp39_38] | 128 >> i % 8)); }
    if (len > 64) {
      int tmp68_67 = 0; byte[] tmp68_66 = d;tmp68_66[tmp68_67] = ((byte)(tmp68_66[tmp68_67] | 0x80)); }
    if (len > 128) {
      byte[] tmp86_83 = d;tmp86_83[8] = ((byte)(tmp86_83[8] | 0x80)); }
    return d;
  }
  








  public static byte[] bitSet2byte(BitSet b, int bytes)
  {
    int len = bytes * 8;
    byte[] d = new byte[bytes];
    for (int i = 0; i < len; i++)
      if (b.get(i + 1)) {
        int tmp31_30 = (i >> 3); byte[] tmp31_26 = d;tmp31_26[tmp31_30] = ((byte)(tmp31_26[tmp31_30] | 128 >> i % 8));
      }
    if (len > 64) {
      int tmp62_61 = 0; byte[] tmp62_60 = d;tmp62_60[tmp62_61] = ((byte)(tmp62_60[tmp62_61] | 0x80)); }
    if (len > 128) {
      byte[] tmp80_77 = d;tmp80_77[8] = ((byte)(tmp80_77[8] | 0x80)); }
    return d;
  }
  


  public static int bitSet2Int(BitSet bs)
  {
    int total = 0;
    int b = bs.length() - 1;
    if (b > 0) {
      int value = (int)Math.pow(2.0D, b);
      for (int i = 0; i <= b; i++) {
        if (bs.get(i))
          total += value;
        value >>= 1;
      }
    }
    
    return total;
  }
  



  public static BitSet int2BitSet(int value)
  {
    return int2BitSet(value, 0);
  }
  



  public static BitSet int2BitSet(int value, int offset)
  {
    BitSet bs = new BitSet();
    
    String hex = Integer.toHexString(value);
    hex2BitSet(bs, hex.getBytes(), offset);
    
    return bs;
  }
  











  public static BitSet byte2BitSet(byte[] b, int offset, boolean bitZeroMeansExtended)
  {
    int len = bitZeroMeansExtended ? 64 : (b[offset] & 0x80) == 128 ? 128 : 
      64;
    BitSet bmap = new BitSet(len);
    for (int i = 0; i < len; i++)
      if ((b[(offset + (i >> 3))] & 128 >> i % 8) > 0)
        bmap.set(i + 1);
    return bmap;
  }
  










  public static BitSet byte2BitSet(byte[] b, int offset, int maxBits)
  {
    int len = maxBits > 64 ? 64 : (b[offset] & 0x80) == 128 ? 128 : 
      maxBits;
    
    if ((maxBits > 128) && (b.length > offset + 8) && 
      ((b[(offset + 8)] & 0x80) == 128)) {
      len = 192;
    }
    BitSet bmap = new BitSet(len);
    for (int i = 0; i < len; i++)
      if ((b[(offset + (i >> 3))] & 128 >> i % 8) > 0)
        bmap.set(i + 1);
    return bmap;
  }
  










  public static BitSet byte2BitSet(BitSet bmap, byte[] b, int bitOffset)
  {
    int len = b.length << 3;
    for (int i = 0; i < len; i++)
      if ((b[(i >> 3)] & 128 >> i % 8) > 0)
        bmap.set(bitOffset + i + 1);
    return bmap;
  }
  











  public static BitSet hex2BitSet(byte[] b, int offset, boolean bitZeroMeansExtended)
  {
    int len = bitZeroMeansExtended ? 
      64 : (Character.digit((char)b[offset], 16) & 0x8) == 8 ? 128 : 
      64;
    BitSet bmap = new BitSet(len);
    for (int i = 0; i < len; i++) {
      int digit = Character.digit((char)b[(offset + (i >> 2))], 16);
      if ((digit & 8 >> i % 4) > 0)
        bmap.set(i + 1);
    }
    return bmap;
  }
  











  public static BitSet hex2BitSet(byte[] b, int offset, int maxBits)
  {
    int len = maxBits > 64 ? 
      64 : (Character.digit((char)b[offset], 16) & 0x8) == 8 ? 128 : 
      maxBits;
    if ((len > 64) && (maxBits > 128) && (b.length > offset + 16) && 
      ((Character.digit((char)b[(offset + 16)], 16) & 0x8) == 8)) {
      len = 192;
    }
    BitSet bmap = new BitSet(len);
    for (int i = 0; i < len; i++) {
      int digit = Character.digit((char)b[(offset + (i >> 2))], 16);
      if ((digit & 8 >> i % 4) > 0) {
        bmap.set(i + 1);
        if ((i == 65) && (maxBits > 128))
          len = 192;
      }
    }
    return bmap;
  }
  










  public static BitSet hex2BitSet(BitSet bmap, byte[] b, int bitOffset)
  {
    int len = b.length << 2;
    for (int i = 0; i < len; i++) {
      int digit = Character.digit((char)b[(i >> 2)], 16);
      if ((digit & 8 >> i % 4) > 0)
        bmap.set(bitOffset + i + 1);
    }
    return bmap;
  }
  








  public static byte[] hex2byte(byte[] b, int offset, int len)
  {
    byte[] d = new byte[len];
    for (int i = 0; i < len * 2; i++)
    {

      int shift = i % 2 == 1 ? 0 : 4; int 
        tmp30_29 = (i >> 1); byte[] tmp30_25 = d;tmp30_25[tmp30_29] = ((byte)(tmp30_25[tmp30_29] | Character.digit((char)b[(offset + i)], 16) << shift));
    }
    return d;
  }
  






  public static byte[] hex2byte(String s)
  {
    if (s.length() % 2 == 0) {
      return hex2byte(s.getBytes(), 0, s.length() >> 1);
    }
    
    return hex2byte("0" + s);
  }
  







  public static String byte2hex(byte[] bs)
  {
    return byte2hex(bs, 0, bs.length);
  }
  





  public static byte[] int2byte(int value)
  {
    if (value < 0)
      return new byte[] { (byte)(value >>> 24 & 0xFF), 
        (byte)(value >>> 16 & 0xFF), (byte)(value >>> 8 & 0xFF), 
        (byte)(value & 0xFF) };
    if (value <= 255)
      return new byte[] { (byte)(value & 0xFF) };
    if (value <= 65535)
      return new byte[] { (byte)(value >>> 8 & 0xFF), 
        (byte)(value & 0xFF) };
    if (value <= 16777215) {
      return new byte[] { (byte)(value >>> 16 & 0xFF), 
        (byte)(value >>> 8 & 0xFF), (byte)(value & 0xFF) };
    }
    return new byte[] { (byte)(value >>> 24 & 0xFF), 
      (byte)(value >>> 16 & 0xFF), (byte)(value >>> 8 & 0xFF), 
      (byte)(value & 0xFF) };
  }
  






  public static int byte2int(byte[] bytes)
  {
    if ((bytes == null) || (bytes.length == 0)) {
      return 0;
    }
    ByteBuffer byteBuffer = ByteBuffer.allocate(4);
    for (int i = 0; i < 4 - bytes.length; i++) {
      byteBuffer.put((byte)0);
    }
    for (int i = 0; i < bytes.length; i++) {
      byteBuffer.put(bytes[i]);
    }
    byteBuffer.position(0);
    return byteBuffer.getInt();
  }
  










  public static String byte2hex(byte[] bs, int off, int length)
  {
    if ((bs.length <= off) || (bs.length < off + length))
      throw new IllegalArgumentException();
    StringBuilder sb = new StringBuilder(length * 2);
    byte2hexAppend(bs, off, length, sb);
    return sb.toString().toUpperCase();
  }
  
  private static void byte2hexAppend(byte[] bs, int off, int length, StringBuilder sb)
  {
    if ((bs.length <= off) || (bs.length < off + length))
      throw new IllegalArgumentException();
    sb.ensureCapacity(sb.length() + length * 2);
    for (int i = off; i < off + length; i++) {
      sb.append(Character.forDigit(bs[i] >>> 4 & 0xF, 16));
      sb.append(Character.forDigit(bs[i] & 0xF, 16));
    }
  }
  








  public static String formatAmount(long l, int len)
    throws Exception
  {
    String buf = Long.toString(l);
    if (l < 100L)
      buf = zeropad(buf, 3);
    StringBuilder s = new StringBuilder(padleft(buf, len - 1, ' '));
    s.insert(len - 3, '.');
    return s.toString();
  }
  








  public static String normalize(String s, boolean canonical)
  {
    StringBuilder str = new StringBuilder();
    
    int len = s != null ? s.length() : 0;
    for (int i = 0; i < len; i++) {
      char ch = s.charAt(i);
      switch (ch) {
      case '<': 
        str.append("&lt;");
        break;
      case '>': 
        str.append("&gt;");
        break;
      case '&': 
        str.append("&amp;");
        break;
      case '"': 
        str.append("&quot;");
        break;
      case '\n': 
      case '\r': 
        if (canonical) {
          str.append("&#");
          str.append(Integer.toString(ch & 0xFF));
          str.append(';'); }
        break;
      }
      
      
      if (ch < ' ') {
        str.append("&#");
        str.append(Integer.toString(ch & 0xFF));
        str.append(';');
      } else if (ch > 65280) {
        str.append((char)(ch & 0xFF));
      } else {
        str.append(ch);
      }
    }
    return str.toString();
  }
  






  public static String normalize(String s)
  {
    return normalize(s, true);
  }
  
  public static int[] toIntArray(String s) {
    StringTokenizer st = new StringTokenizer(s);
    int[] array = new int[st.countTokens()];
    for (int i = 0; st.hasMoreTokens(); i++)
      array[i] = Integer.parseInt(st.nextToken());
    return array;
  }
  
  public static String[] toStringArray(String s) {
    StringTokenizer st = new StringTokenizer(s);
    String[] array = new String[st.countTokens()];
    for (int i = 0; st.hasMoreTokens(); i++)
      array[i] = st.nextToken();
    return array;
  }
  



  public static byte[] xor(byte[] op1, byte[] op2)
  {
    byte[] result;
    


    int length;
    


    if (op2.length > op1.length) {
      result = new byte[op2.length];
      length = op1.length;
      System.arraycopy(op2, 0, result, 0, op2.length);
    } else {
      result = new byte[op1.length];
      length = op2.length;
      System.arraycopy(op1, 0, result, 0, op1.length);
    }
    
    for (int i = 0; i < length; i++) {
      result[i] = ((byte)(op1[i] ^ op2[i]));
    }
    
    return result;
  }
  








  public static String hexor(String op1, String op2)
  {
    byte[] xor = xor(hex2byte(op1), hex2byte(op2));
    return hexString(xor);
  }
  








  public static byte[] trim(byte[] array, int length)
  {
    byte[] trimmedArray = new byte[length];
    System.arraycopy(array, 0, trimmedArray, 0, length);
    return trimmedArray;
  }
  








  public static byte[] concat(byte[] array1, byte[] array2)
  {
    byte[] concatArray = new byte[array1.length + array2.length];
    System.arraycopy(array1, 0, concatArray, 0, array1.length);
    System.arraycopy(array2, 0, concatArray, array1.length, array2.length);
    return concatArray;
  }
  

















  public static byte[] concat(byte[] array1, int beginIndex1, int length1, byte[] array2, int beginIndex2, int length2)
  {
    byte[] concatArray = new byte[length1 + length2];
    System.arraycopy(array1, beginIndex1, concatArray, 0, length1);
    System.arraycopy(array2, beginIndex2, concatArray, length1, length2);
    return concatArray;
  }
  




  public static String zeroUnPad(String s)
  {
    return unPadLeft(s, '0');
  }
  




  public static String blankUnPad(String s)
  {
    return unPadRight(s, ' ');
  }
  








  public static String unPadRight(String s, char c)
  {
    int end = s.length();
    if (end == 0)
      return s;
    while ((end > 0) && (s.charAt(end - 1) == c))
      end--;
    return end > 0 ? s.substring(0, end) : s.substring(0, 1);
  }
  








  public static String unPadLeft(String s, char c)
  {
    int fill = 0;int end = s.length();
    if (end == 0)
      return s;
    while ((fill < end) && (s.charAt(fill) == c))
      fill++;
    return fill < end ? s.substring(fill, end) : s.substring(fill - 1, end);
  }
  



  public static boolean isZero(String s)
  {
    int i = 0;int len = s.length();
    while ((i < len) && (s.charAt(i) == '0')) {
      i++;
    }
    return i >= len;
  }
  


  public static boolean isBlank(String s)
  {
    return s.trim().length() == 0;
  }
  



  public static boolean isAlphaNumeric(String s)
  {
    int i = 0;int len = s.length();
    while (((i < len) && (
      (Character.isLetterOrDigit(s.charAt(i))) || 
      (s.charAt(i) == ' ') || (s.charAt(i) == '.') || 
      (s.charAt(i) == '-') || (s.charAt(i) == '_'))) || 
      (s.charAt(i) == '?')) {
      i++;
    }
    return i >= len;
  }
  




  public static boolean isNumeric(String s, int radix)
  {
    int i = 0;int len = s.length();
    while ((i < len) && (Character.digit(s.charAt(i), radix) > -1)) {
      i++;
    }
    return (i >= len) && (len > 0);
  }
  








  public static byte[] bitSet2extendedByte(BitSet b)
  {
    int len = 128;
    byte[] d = new byte[len >> 3];
    for (int i = 0; i < len; i++)
      if (b.get(i + 1)) {
        int tmp29_28 = (i >> 3); byte[] tmp29_25 = d;tmp29_25[tmp29_28] = ((byte)(tmp29_25[tmp29_28] | 128 >> i % 8)); }
    int tmp52_51 = 0; byte[] tmp52_50 = d;tmp52_50[tmp52_51] = ((byte)(tmp52_50[tmp52_51] | 0x80));
    return d;
  }
  












  public static int parseInt(String s, int radix)
    throws NumberFormatException
  {
    int length = s.length();
    if (length > 9) {
      throw new NumberFormatException("Number can have maximum 9 digits");
    }
    int index = 0;
    int digit = Character.digit(s.charAt(index++), radix);
    if (digit == -1)
      throw new NumberFormatException("String contains non-digit");
    int result = digit;
    while (index < length) {
      result *= radix;
      digit = Character.digit(s.charAt(index++), radix);
      if (digit == -1)
        throw new NumberFormatException("String contains non-digit");
      result += digit;
    }
    return result;
  }
  









  public static int parseInt(String s)
    throws NumberFormatException
  {
    return parseInt(s, 10);
  }
  












  public static int parseInt(char[] cArray, int radix)
    throws NumberFormatException
  {
    int length = cArray.length;
    if (length > 9) {
      throw new NumberFormatException("Number can have maximum 9 digits");
    }
    int index = 0;
    int digit = Character.digit(cArray[(index++)], radix);
    if (digit == -1)
      throw new NumberFormatException("Char array contains non-digit");
    int result = digit;
    while (index < length) {
      result *= radix;
      digit = Character.digit(cArray[(index++)], radix);
      if (digit == -1)
        throw new NumberFormatException("Char array contains non-digit");
      result += digit;
    }
    return result;
  }
  









  public static int parseInt(char[] cArray)
    throws NumberFormatException
  {
    return parseInt(cArray, 10);
  }
  












  public static int parseInt(byte[] bArray, int radix)
    throws NumberFormatException
  {
    int length = bArray.length;
    if (length > 9) {
      throw new NumberFormatException("Number can have maximum 9 digits");
    }
    int index = 0;
    int digit = Character.digit((char)bArray[(index++)], radix);
    if (digit == -1)
      throw new NumberFormatException("Byte array contains non-digit");
    int result = digit;
    while (index < length) {
      result *= radix;
      digit = Character.digit((char)bArray[(index++)], radix);
      if (digit == -1)
        throw new NumberFormatException("Byte array contains non-digit");
      result += digit;
    }
    return result;
  }
  









  public static int parseInt(byte[] bArray)
    throws NumberFormatException
  {
    return parseInt(bArray, 10);
  }
  
  private static String hexOffset(int i) {
    i = i >> 4 << 4;
    int w = i > 65535 ? 8 : 4;
    return zeropad(Integer.toString(i, 16), w);
  }
  




  public static String hexdump(byte[] b)
  {
    return hexdump(b, 0, b.length);
  }
  





  public static String hexdump(byte[] b, int offset)
  {
    return hexdump(b, offset, b.length - offset);
  }
  








  public static String hexdump(byte[] b, int offset, int len)
  {
    StringBuilder sb = new StringBuilder();
    StringBuilder hex = new StringBuilder();
    StringBuilder ascii = new StringBuilder();
    String sep = "  ";
    String lineSep = System.getProperty("line.separator");
    len = offset + len;
    
    for (int i = offset; i < len; i++) {
      hex.append(hexStrings[(b[i] & 0xFF)]);
      hex.append(' ');
      char c = (char)b[i];
      ascii.append((c >= ' ') && (c < '') ? c : '.');
      
      int j = i % 16;
      switch (j) {
      case 7: 
        hex.append(' ');
        break;
      case 15: 
        sb.append(hexOffset(i));
        sb.append(sep);
        sb.append(hex.toString());
        sb.append(' ');
        sb.append(ascii.toString());
        sb.append(lineSep);
        hex = new StringBuilder();
        ascii = new StringBuilder();
      }
      
    }
    if (hex.length() > 0) {
      while (hex.length() < 49) {
        hex.append(' ');
      }
      sb.append(hexOffset(len));
      sb.append(sep);
      sb.append(hex.toString());
      sb.append(' ');
      sb.append(ascii.toString());
      sb.append(lineSep);
    }
    return sb.toString();
  }
  








  public static String strpadf(String s, int len)
  {
    StringBuilder d = new StringBuilder(s);
    while (d.length() < len)
      d.append('F');
    return d.toString();
  }
  






  public static String trimf(String s)
  {
    if (s != null) {
      int l = s.length();
      if (l > 0) {
        do {
          if ((s.charAt(l) != 'F') && (s.charAt(l) != 'f')) {
            break;
          }
          l--; } while (l >= 0);
        


        s = l == 0 ? "" : s.substring(0, l + 1);
      }
    }
    return s;
  }
  









  public static String takeLastN(String s, int n)
    throws Exception
  {
    if (s.length() > n) {
      return s.substring(s.length() - n);
    }
    if (s.length() < n) {
      return zeropad(s, n);
    }
    return s;
  }
  











  public static String takeFirstN(String s, int n)
    throws Exception
  {
    if (s.length() > n) {
      return s.substring(0, n);
    }
    if (s.length() < n) {
      return zeropad(s, n);
    }
    return s;
  }
  

  public static String millisToString(long millis)
  {
    StringBuilder sb = new StringBuilder();
    if (millis < 0L) {
      millis = -millis;
      sb.append('-');
    }
    int ms = (int)(millis % 1000L);
    millis /= 1000L;
    int dd = (int)(millis / 86400L);
    millis -= dd * 86400;
    int hh = (int)(millis / 3600L);
    millis -= hh * 3600;
    int mm = (int)(millis / 60L);
    millis -= mm * 60;
    int ss = (int)millis;
    if (dd > 0) {
      sb.append(Long.toString(dd));
      sb.append("d ");
    }
    sb.append(zeropad(hh, 2));
    sb.append(':');
    sb.append(zeropad(mm, 2));
    sb.append(':');
    sb.append(zeropad(ss, 2));
    sb.append('.');
    sb.append(zeropad(ms, 3));
    return sb.toString();
  }
  








  public static byte[] int2bcd(int data, int len)
  {
    byte[] bb = null;
    if (len == 1) {
      data %= 100;
      bb = new byte[1];
      bb[0] = ((byte)((data / 10 << 4) + data % 10));
      return bb; }
    if (len == 2) {
      bb = new byte[2];
      bb[0] = ((byte)(data / 100));
      
      bb[1] = ((byte)((data / 10 % 10 << 4) + data % 10));
      return bb;
    }
    return null;
  }
  






  public static int byte2int(byte[] bb, int offset, int len)
  {
    byte[] temp = new byte[len];
    System.arraycopy(bb, offset, temp, 0, len);
    return byte2int(temp);
  }
  





  public static int bcd2int(byte bb)
  {
    return (bb >> 4 & 0xF) * 10 + (bb & 0xF);
  }
  






  public static int bcd2int(byte[] bb, int offset, int len)
  {
    int result = 0;
    for (int i = 0; i < len; i++) {
      result = result * 100 + bcd2int(bb[(offset + i)]);
    }
    return result;
  }
  



  public static boolean memcmp(byte[] b1, int offset1, byte[] b2, int offset2, int len)
  {
    for (int i = 0; i < len; i++) {
      if (b1[(offset1 + i)] != b2[(offset2 + i)])
        return false;
    }
    return true;
  }
  





  public static String[] subStrByLen(String str, int len)
  {
    if ((str.length() < len) || (str.length() % len != 0)) {
      return null;
    }
    String[] sb = new String[str.length() / len];
    for (int i = 0; i < str.length() / len; i++) {
      sb[i] = str.substring(i * len, (i + 1) * len).trim();
    }
    return sb;
  }
}
