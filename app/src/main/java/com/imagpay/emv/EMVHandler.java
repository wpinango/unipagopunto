package com.imagpay.emv;

import android.content.Context;
import android.util.Log;

import com.corpagos.corpunto.interfaces.CardInterface;
import com.google.gson.Gson;
import com.imagpay.Constants;
import com.imagpay.Settings;
import com.imagpay.SwipeHandler;
import com.imagpay.utils.StringUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.json.JSONObject;








public class EMVHandler extends SwipeHandler {
  private static final String TAG = "EMVHandler";
  private static final String CHAR_HEX = "0123456789ABCDEF";
  private List<EMVListener> _listeners = new ArrayList();
  private Settings _settings;
  private EMVParam param;
  private long counter = 0L;
  private boolean whileFlag = false;
  private boolean sendFlag = false;
  private boolean cancelFlag = false;
  private boolean showFlag = false;
  private boolean verfityFlag = false;
  
  private boolean isAutoRead = false;
  private String icRandom;
  private String encrypedField55;
  
  static {
    try {
      System.loadLibrary("JNIEMV");
    } catch (Exception e) {
      Log.d("EMVHandler", "Load library Error!");
    }
  }
  
  public EMVHandler(Context context) {
    super(context);
    _settings = new Settings(this);
  }
  





  public void addEMVListener(EMVListener listener)
  {
    if (_listeners.contains(listener))
      return;
    _listeners.add(listener);
  }
  





  public void removeEMVListener(EMVListener listener)
  {
    _listeners.remove(listener);
  }
  
  public void setShowAPDU(boolean show) {
    showFlag = show;
  }
  
  public boolean isShowAPDU() {
    return showFlag;
  }
  
  public boolean isRunning() {
    return whileFlag;
  }
  
  public boolean isAutoRead() {
    return isAutoRead;
  }
  
  public void setAutoRead(boolean isAutoRead) {
    this.isAutoRead = isAutoRead;
  }
  
  public void cancel() {
    cancelFlag = true;
    finishExchangeAPDU();
  }
  



  public int getStatus()
  {
    return status;
  }
  
  public String getMessage() {
    return msg;
  }
  
  public void kernelInit(EMVParam param)
  {
    checkSignature();
    this.param = param;
    
    kernelInit(param.getEMVParam(), param.getEMVTransData());
    
    for (EMVApp app : param.getApps()) {
      addApp(app);
    }
    msg = "";
    cancelFlag = false;
  }
  
  public String icReset() {
    msg = "";
    cancelFlag = false;
    String resp = null;
    










    if (isShowAPDU())
      Log.d("EMVHandler", "GET COUNT");
    resp = _settings.tposGetCount();
    counter += 1L;
    if ((resp != null) && (resp.length() > 8)) {
      String[] tmps = resp.replaceAll("..", "$0 ").trim().split(" ");
      counter = 
      

        (Integer.parseInt(tmps[0], 16) + Integer.parseInt(tmps[1], 16) * 255 + Integer.parseInt(tmps[2], 16) * 65535 + Integer.parseInt(tmps[3], 16) * 16777215);
    } else {
      counter += 1L;
    }
    return resp;
  }
  


  public int process(CardInterface cardInterface)
  {
    if (checkSignatureStatus() == 0) {
      Log.d("EMVHandler", "EMV Kernel Signature error...");
      return EMVConstants.EMV_SIG_ERR;
    }
    while (whileFlag) {}
    
    whileFlag = true;
    msg = "";
    cancelFlag = false;
    verfityFlag = false;
    







    exchangeAPDU();
    
    if (isShowAPDU()) {
      Log.d("EMVHandler", "App Slt...");
    }
    List<String> apps = appSelect(param.getSlot());
    if (cancelFlag) {
      if (isShowAPDU())
        Log.d("EMVHandler", "Cancel the process");
      finishProcess();
      msg = "Trans Terminated!";
      return EMVConstants.EMV_USER_CANCEL;
    }
    if ((status == EMVConstants.STATUS_OK) && (apps != null) && (!apps.isEmpty())) {
      if (isShowAPDU())
        Log.d("EMVHandler", "App Slt Ok!");
    } else {
      if (isShowAPDU())
        Log.d("EMVHandler", "App Slt Fail!");
      finishProcess();
      msg = "Trans Terminated!";
      return status;
    }
    

    if (isShowAPDU()) {
      Log.d("EMVHandler", "Final Slt...");
    }
    List<String> as = null;
    for (;;) {
      int idx = -1;
      
      if (apps.size() == 1) {
        try
        {
          if (isShowAPDU())
            Log.d("EMVHandler", "App: " + (String)apps.get(0));
          System.out.println("valores card : " + apps.get(0));
          cardInterface.onCardReading(apps.get(0));
          JSONObject json = new JSONObject((String)apps.get(0));
          


          if ((json.getInt("priorityExistFlg") == 0) || 
            ((json.getInt("priority") & 0x80) == 0))
            idx = 0;
        } catch (Exception e) {
          if (isShowAPDU()) {
            Log.d("EMVHandler", "Final Slt Fail[" + e.getLocalizedMessage() + 
              "]");
          }
        }
      }
      if ((idx < 0) && (apps.size() > 0)) { Gson gson;
      String content = null;
      if (as == null)
        {
          gson = new Gson();
          List<AppBean> beans = new ArrayList();
          for (String app : apps) {
            if (isShowAPDU())
              Log.d("EMVHandler", "App: " + app);
            AppBean bean = (AppBean)gson.fromJson(app, AppBean.class);
            content = app;
            beans.add(bean);
          }
          Collections.sort(beans);
          
          as = new ArrayList();
          for (AppBean bn : beans) {
            as.add(content);
          }
          beans.clear();
          beans = null;
        }
        if (!isAutoRead) {
          for (EMVListener listener : _listeners) {
            idx = listener.onSelectApp(as);
            if ((idx >= 0) && (idx < as.size()))
              break;
          }
        } else {
          idx = 0;
        }
        if ((idx >= 0) && (idx < as.size())) {
          idx = apps.indexOf(as.get(idx));
        } else
          idx = -1;
        if (isShowAPDU())
          Log.d("EMVHandler", "Sel Idx " + idx);
      }
      if (cancelFlag) {
        if (isShowAPDU())
          Log.d("EMVHandler", "Cancel the process");
        finishProcess();
        msg = "Trans Terminated!";
        return EMVConstants.EMV_USER_CANCEL;
      }
      if ((idx < 0) || (idx >= apps.size())) {
        if (isShowAPDU())
          Log.d("EMVHandler", "Final Slt Cancel");
        finishProcess();
        msg = "Trans Terminated!";
        return EMVConstants.EMV_USER_CANCEL;
      }
      finalSelect(idx);
      if (cancelFlag) {
        if (isShowAPDU())
          Log.d("EMVHandler", "Cancel the process");
        finishProcess();
        msg = "Trans Terminated!";
        return EMVConstants.EMV_USER_CANCEL;
      }
      if (status == EMVConstants.EMV_RESELECT_APP) {
        if (isShowAPDU())
          Log.d("EMVHandler", "ReSel App");
        String tmp = (String)apps.remove(idx);
        if (as != null) {
          as.remove(tmp);
        }
      } else {
        if (status != EMVConstants.STATUS_OK) {
          if (isShowAPDU())
            Log.d("EMVHandler", "Final Slt Fail");
          finishProcess();
          msg = "Trans Terminated!";
          return status;
        }
        

        if (isShowAPDU())
          Log.d("EMVHandler", "Get Final Slt");
        getFinalApp();
        if (cancelFlag) {
          if (isShowAPDU())
            Log.d("EMVHandler", "Cancel the process");
          finishProcess();
          msg = "Trans Terminated!";
          return EMVConstants.EMV_USER_CANCEL;
        }
        if (status != EMVConstants.STATUS_OK) {
          if (isShowAPDU())
            Log.d("EMVHandler", "GetFinalApp Fail!");
          finishProcess();
          msg = "Trans Terminated!";
          return status;
        }
        
        if (isShowAPDU()) {
          Log.d("EMVHandler", "ProviderAccount Slt...");
        }
        if (isShowAPDU())
          Log.d("EMVHandler", "GPO Proccess...");
        getProcessOption();
        if (cancelFlag) {
          if (isShowAPDU())
            Log.d("EMVHandler", "Cancel the process");
          finishProcess();
          msg = "Trans Terminated!";
          return EMVConstants.EMV_USER_CANCEL;
        }
        if (status == EMVConstants.STATUS_OK) {
          break;
        }
        
        if ((status != EMVConstants.ICC_RSP_6985) && 
          (status != EMVConstants.EMV_RSP_ERR)) {
          if (isShowAPDU())
            Log.d("EMVHandler", "GPO Proc Fail!");
          finishProcess();
          msg = "Trans Terminated!";
          return status;
        }
        if (isShowAPDU())
          Log.d("EMVHandler", "ReSel App");
        String tmp = (String)apps.remove(idx);
        if (as != null)
          as.remove(tmp); } }
    int idx;
    if (as != null) {
      as.clear();
      as = null;
    }
    

    if (isShowAPDU())
      Log.d("EMVHandler", "Read Data...");
    readAppData();
    if (cancelFlag) {
      if (isShowAPDU())
        Log.d("EMVHandler", "Cancel the process");
      finishProcess();
      msg = "Trans Terminated!";
      return EMVConstants.EMV_USER_CANCEL;
    }
    if (status != EMVConstants.STATUS_OK) {
      if (isShowAPDU())
        Log.d("EMVHandler", "Read Data Fail!");
      finishProcess();
      msg = "Trans Terminated!";
      return status;
    }
    
    StringBuffer strCounter = new StringBuffer();
    strCounter.append(Long.toString(counter % 10000L));
    int len = 4 - strCounter.length();
    for (int i = 0; i < len; i++)
      strCounter.insert(0, "0");
    setTLVData(40769, strCounter.toString());
    setTLVData(40761, "05");
    
    if (isShowAPDU()) {
      Log.d("EMVHandler", "Read Data OK!");
      Log.d("EMVHandler", "Terminal Capabilities: " + getTLVDataByTag(40755));
      Log.d("EMVHandler", "Additional Terminal Capabilities: " + 
        getTLVDataByTag(40768));
      Log.d("EMVHandler", "Card No: " + getTLVDataByTag(90));
      String holder = getTLVDataByTag(24352);
      if ((holder == null) || ("".equals(holder))) {
        Log.d("EMVHandler", "Card Holder: ");
      } else
        Log.d("EMVHandler", 
          "Card Holder: " + 
          new String(
          StringUtils.convertHexToBytes(holder)));
      Log.d("EMVHandler", "Expiration Date: " + getTLVDataByTag(24356));
      Log.d("EMVHandler", "Service Code: " + getTLVDataByTag(24368));
    }
    if (param.isReadOnly()) {
      finishProcess();
      return EMVConstants.STATUS_OK;
    }
    
    if (!display()) {
      if (isShowAPDU())
        Log.d("EMVHandler", "Cancel the process");
      finishProcess();
      msg = "Trans Terminated!";
      return EMVConstants.EMV_USER_CANCEL;
    }
    

    if (isShowAPDU())
      Log.d("EMVHandler", "Add CAPK...");
    String keyId = getTLVDataByTag(143);
    if ((status == EMVConstants.STATUS_OK) && (keyId != null)) {
      String rid = getTLVDataByTag(40710);
      if ((status == EMVConstants.STATUS_OK) && (rid != null)) {
        addCapk(keyId, rid);
        if (status != EMVConstants.STATUS_OK) {
          if (isShowAPDU())
            Log.d("EMVHandler", "Add CAPK Fail!");
          finishProcess();
          msg = "Add CAPK Fail, Trans Terminated!";
          return status;
        }
        if (isShowAPDU())
          Log.d("EMVHandler", "Add Revoc...");
        addRevoc(keyId, rid);
        if (status != EMVConstants.STATUS_OK) {
          if (isShowAPDU())
            Log.d("EMVHandler", "Add Revoc Fail!");
          finishProcess();
          msg = "Add Revoc Fail, Trans Terminated!";
          return status;
        }
      }
    }
    if (cancelFlag) {
      if (isShowAPDU())
        Log.d("EMVHandler", "Cancel the process");
      finishProcess();
      msg = "Trans Terminated!";
      return EMVConstants.EMV_USER_CANCEL;
    }
    
    if (isShowAPDU())
      Log.d("EMVHandler", "Card Auth...");
    cardAuth();
    if (cancelFlag) {
      if (isShowAPDU())
        Log.d("EMVHandler", "Cancel the process");
      finishProcess();
      msg = "Trans Terminated!";
      return EMVConstants.EMV_USER_CANCEL;
    }
    if (status != EMVConstants.STATUS_OK) {
      if (isShowAPDU())
        Log.d("EMVHandler", "Card Auth Fail!");
      finishProcess();
      msg = "Trans Terminated!";
      return status;
    }
    
    if (isShowAPDU())
      Log.d("EMVHandler", "TransProcess...");
    int tryCnt = transProcess();
    if (cancelFlag) {
      if (isShowAPDU())
        Log.d("EMVHandler", "Cancel the process");
      finishProcess();
      msg = "Trans Terminated!";
      return EMVConstants.EMV_USER_CANCEL;
    }
    if (status < EMVConstants.STATUS_OK) {
      if (isShowAPDU())
        Log.d("EMVHandler", "TransProcess Fail!");
      finishProcess();
      msg = "Trans Terminated!";
      return status;
    }
    if ((status > EMVConstants.STATUS_OK) && (
      ((status != EMVConstants.EMV_CVM_OFF_PLAIN_PIN) && (status != EMVConstants.EMV_CVM_OFF_ENCRYPT_PIN)) || ((tryCnt > 0) || ((status == EMVConstants.EMV_CVM_ONLINE_PIN) && (tryCnt >= 0)))))
    {


      if (isShowAPDU())
        Log.d("EMVHandler", "PINEntryProc...");
      verfityFlag = true;
      pinEntryProcess(tryCnt);
      verfityFlag = false;
      if (cancelFlag) {
        if (isShowAPDU())
          Log.d("EMVHandler", "Cancel the process");
        finishProcess();
        msg = "Trans Terminated!";
        return EMVConstants.EMV_USER_CANCEL;
      }
      if ((status != EMVConstants.STATUS_OK) && 
        (status != EMVConstants.EMV_USER_CANCEL)) {
        if (isShowAPDU())
          Log.d("EMVHandler", "PINEntryProc Fail!");
        finishProcess();
        msg = "Trans Terminated!";
        return status;
      }
    }
    
    if (isShowAPDU())
      Log.d("EMVHandler", "ActionAnalyse...");
    int ucACType = actionAnalyse();
    if (cancelFlag) {
      if (isShowAPDU())
        Log.d("EMVHandler", "Cancel the process");
      finishProcess();
      msg = "Trans Terminated!";
      return EMVConstants.EMV_USER_CANCEL;
    }
    if (status == EMVConstants.EMV_DENIAL) {
      if (isShowAPDU())
        Log.d("EMVHandler", "Trans Declined!");
      finishProcess();
      msg = "Trans Declined!";
      return status;
    }
    if (status == EMVConstants.EMV_NOT_ACCEPT) {
      if (isShowAPDU())
        Log.d("EMVHandler", "Not Accept!");
      finishProcess();
      msg = "Not Accept!";
      return status;
    }
    if (status != EMVConstants.STATUS_OK) {
      if (isShowAPDU())
        Log.d("EMVHandler", "Trans Terminated!");
      finishProcess();
      msg = "Trans Terminated!";
      return status;
    }
    
    if (ucACType == EMVConstants.EMV_AC_TC) {
      if (isShowAPDU())
        Log.d("EMVHandler", "Trans Approved!");
      confirmMsgProcess();
      finishProcess();
      msg = "Trans Approved!";
      return EMVConstants.STATUS_OK;
    }
    

    if (isShowAPDU())
      Log.d("EMVHandler", "Trans Online...");
    EMVResponse response = onlineMsgProcess();
    if (cancelFlag) {
      if (isShowAPDU())
        Log.d("EMVHandler", "Cancel the process");
      finishProcess();
      msg = "Trans Terminated!";
      return EMVConstants.EMV_USER_CANCEL;
    }
    
    boolean onlineApprove = false;
    boolean onlineFailed = false;
    boolean onlineError = false;
    if (response != null) {
      int commuStatus = EMVConstants.ONLINE_DENIAL;
      if (response.getStatus() == EMVConstants.ONLINE_APPROVE) {
        onlineApprove = true;
        commuStatus = EMVConstants.ONLINE_APPROVE;
      }
      
      if ((response.getARC() != null) && 
        (response.getARC().trim().length() == 4)) {
        setTLVData(138, response.getARC().trim());
      } else {
        onlineApprove = false;
        onlineError = true;
        commuStatus = EMVConstants.ONLINE_FAILED;
      }
      
      if ((response.getIAD() != null) && 
        (response.getIAD().trim().length() >= 16) && 
        (response.getIAD().trim().length() <= 32)) {
        setTLVData(145, response.getIAD().trim());
      }
      
      onlineDataProcess(response.getScript() == null ? "" : response
        .getScript().trim(), ucACType, commuStatus);
    } else {
      onlineFailed = true;
      onlineDataProcess("", ucACType, EMVConstants.ONLINE_FAILED);
    }
    String result = getScriptResult();
    if (result != null) {
      setTLVData(57137, result);
    }
    if (cancelFlag) {
      if (isShowAPDU())
        Log.d("EMVHandler", "Cancel the process");
      finishProcess();
      msg = "Trans Terminated!";
      return EMVConstants.EMV_USER_CANCEL;
    }
    if (status == EMVConstants.STATUS_OK) {
      if (onlineApprove) {
        if (isShowAPDU())
          Log.d("EMVHandler", "Txn Online Approved!");
        msg = "Txn Online Approved!";
      } else {
        reversalMsgProcess();
        if (isShowAPDU())
          Log.d("EMVHandler", "Trans Approved!");
        msg = "Trans Approved!";
      }
      confirmMsgProcess();
    } else if (status == EMVConstants.EMV_DENIAL) {
      if (isShowAPDU())
        Log.d("EMVHandler", "Txn Online Declined!");
      if ((onlineApprove) || (onlineFailed) || (onlineError))
        reversalMsgProcess();
      msg = "Txn Online Declined!";
    } else {
      if (status == EMVConstants.EMV_NOT_ACCEPT) {
        if (isShowAPDU())
          Log.d("EMVHandler", "Not Accept!");
        msg = "Not Accept!";
      } else {
        if (isShowAPDU())
          Log.d("EMVHandler", "Trans Terminated!");
        msg = "Trans Terminated!";
      }
      if ((onlineApprove) || (onlineFailed))
        reversalMsgProcess();
    }
    finishProcess();
    return status;
  }
  


  public String mPoscheckConfig()
  {
    return null;
  }
  
  public void icOff() {
    while (whileFlag) {}
    

    while (sendFlag) {}
    
    if (isShowAPDU())
      Log.d("EMVHandler", "IC Off");
    String resp = _settings.icOff();
    if (isShowAPDU())
      Log.d("EMVHandler", "Response <= " + resp);
    if ((isShowAPDU()) && (msg != null) && (!"".equals(msg)))
      Log.d("EMVHandler", "MSG = " + msg);
  }
  
  private void checkSignature() {
    String random = getShakeHand();
    String resp = getDataWithCipherCode(Constants.SYS_SHAKE + random.trim());
    if ((resp == null) || (resp.length() == 2))
      return;
    setShakeHand(StringUtils.convertHexToBytes(resp));
  }
  




























































































































  private void exchangeAPDU() { new Thread(new Runnable()
    {
      public void run()
      {
        while ((whileFlag) && (!cancelFlag)) {
          Apdu_Send as = EMVHandler.this.getApduCommand();
          if (as == null) {
            try {
              Thread.sleep(4L);
            }
            catch (Exception localException1) {}
          }
          else {
            short sendLen = as.getLC();
            byte[] commands = as.getCommand();
            if ((commands[0] != 0) || (commands[1] != 0) || 
              (commands[2] != 0) || (commands[3] != 0))
            {

              byte[] data = new byte[sendLen];
              if (sendLen > 0) {
                System.arraycopy(as.getDataIn(), 0, data, 0, sendLen);
              }
              String apdustr;
              
              if (sendLen > 0) {
                apdustr = 
                



                  EMVHandler.this.bytesToHexString(commands, 4) + EMVHandler.this.bytesToHexString(new byte[] { (byte)sendLen }, 1) + EMVHandler.this.bytesToHexString(data, sendLen) + EMVHandler.this.bytesToHexString(
                  new byte[] { (byte)as.getLE() }, 
                  1);
              } else
                apdustr = 
                  EMVHandler.this.bytesToHexString(commands, 4) + EMVHandler.this.bytesToHexString(
                  new byte[] { (byte)sendLen }, 
                  1);
              if (isShowAPDU()) {
                Log.d("EMVHandler", "send => " + apdustr);
              }
              














              sendFlag = true;
              




              com.imagpay.Apdu_Resp arsp = new com.imagpay.Apdu_Resp();
              String resp = _settings.getDataWithAPDUForStr(param.getSlot(), 
                apdustr);
              sendFlag = false;
              if (resp == null) {
                if (isShowAPDU())
                  Log.d("EMVHandler", "Response <= [null] (Timeout)");
                status = EMVConstants.EMV_TIME_OUT;
                EMVHandler.this.finishProcess();
                break;
              }
              if (isShowAPDU())
                Log.d("EMVHandler", "Response <= " + resp);
              if (!EMVHandler.this.checkResponse(resp)) {
                status = EMVConstants.EMV_TIME_OUT;
                EMVHandler.this.finishProcess();
                break;
              }
              
              String[] ss = resp.replaceAll("..", "$0 ").trim()
                .split(" ");
              int _totalLen = ss.length;
              int _len = Integer.parseInt(ss[0], 16) * 256 + 
                Integer.parseInt(ss[1], 16);
              

              if (_len + 4 != _totalLen) {
                EMVHandler.this.finishProcess();
                break;
              }
              
              arsp.setLenOut((short)_len);
              if (_len != 0) {
                try {
                  String _tmp = resp.substring(4, 4 + _len * 2);
                  arsp.setDataOut(StringUtils.convertHexToBytes(_tmp));
                } catch (Exception e) {
                  Log.d("EMVHandler", e.getMessage());
                }
                
              } else {
                arsp.setDataOut(new byte['Ȁ']);
              }
              arsp.setSWA(
                StringUtils.convertHexToBytes(ss[(_totalLen - 2)])[0]);
              arsp.setSWB(
                StringUtils.convertHexToBytes(ss[(_totalLen - 1)])[0]);
              
              Apdu_Resp ar = new Apdu_Resp();
              ar.setDataOut(arsp.getDataOut());
              ar.setLenOut(arsp.getLenOut());
              ar.setSWA(arsp.getSWA());
              ar.setSWB(arsp.getSWB());
              EMVHandler.this.setApduResult(ar);
            }
          }
        }
      }
    })
    
























































































































      .start(); }
  
  private boolean checkResponse(String resp) {
    if (resp == null)
      return false;
    if (resp.startsWith("ff3f00000000"))
      return false;
    if (resp.startsWith("323f00000000"))
      return false;
    if (resp.startsWith("333f00000000"))
      return false;
    if (resp.equals("00")) {
      return false;
    }
    return true;
  }
  
  private boolean display() {
    if (!isAutoRead) {
      for (EMVListener listener : _listeners) {
        if (listener.onReadData())
          return true;
      }
      return false;
    }
    return true;
  }
  
  private void addCapk(String keyId, String rid)
  {
    byte kd = (byte)Integer.parseInt(keyId.trim(), 16);
    for (EMVCapk capk : param.getCapks()) {
      if ((kd == capk.getKeyID()) && 
        (rid.toLowerCase().startsWith(capk.getRID().toLowerCase()))) {
        if ((capk.getCheckSum() == null) || 
          ("".equals(capk.getCheckSum())))
          capk.setCheckSum("0000000000000000000000000000000000000000");
        if (isShowAPDU())
          Log.d("EMVHandler", 
            "Add CAPK[" + capk.getKeyID() + "," + 
            capk.getRID() + "," + 
            capk.getExponent() + "," + 
            capk.getModul() + "," + 
            capk.getCheckSum() + "]");
        addCapk(capk);
        break;
      }
    }
  }
  
  private void addRevoc(String keyId, String rid)
  {
    byte kd = (byte)Integer.parseInt(keyId.trim(), 16);
    for (EMVRevoc revoc : param.getRevocs()) {
      if (kd == revoc.getUCIndex())
      {
        if (rid.toLowerCase().startsWith(revoc.getUCRID().toLowerCase())) {
          if (isShowAPDU())
            Log.d("EMVHandler", "Add Revoc[" + revoc.getUCIndex() + "," + 
              revoc.getUCRID() + "," + revoc.getUCCertSn() + 
              "]");
          addRevoc(revoc);
          break;
        }
      }
    }
  }
  
  private void pinEntryProcess(int ucPinTryCnt) {
    String pin = null;
    int nPINEntryPath = status;
    int ucTryCnt = ucPinTryCnt;
    if (isShowAPDU()) {
      Log.d("EMVHandler", "Start Pin Verfity Pin Type=" + nPINEntryPath + 
        " Try Cnt=" + ucTryCnt);
    }
    while ((nPINEntryPath != EMVConstants.STATUS_OK) && (
      ((nPINEntryPath == EMVConstants.EMV_CVM_OFF_PLAIN_PIN) || (nPINEntryPath == EMVConstants.EMV_CVM_OFF_ENCRYPT_PIN)) && ((ucTryCnt > 0) || ((nPINEntryPath == EMVConstants.EMV_CVM_ONLINE_PIN) && (ucPinTryCnt >= 0))))) {
      if (isShowAPDU()) {
        Log.d("EMVHandler", "Pin Verfity Pin Type=" + nPINEntryPath + 
          " Try Cnt=" + ucTryCnt);
      }
      if (nPINEntryPath == EMVConstants.EMV_CVM_ONLINE_PIN)
      {
        pin = readPin(EMVConstants.EMV_CVM_ONLINE_PIN, ucTryCnt);
        if ((pin != null) && (pin.length() >= 4)) {
          pinVerify(EMVConstants.EMV_CVM_ONLINE_PIN, pin);
        } else {
          pinVerify(EMVConstants.EMV_CVM_ONLINE_PIN, "");
          if (status == EMVConstants.EMV_CVM_ONLINE_PIN)
            status = EMVConstants.STATUS_OK;
        }
        ucTryCnt--;
        if (isShowAPDU()) {
          Log.d("EMVHandler", "EMV_CVM_ONLINE_PIN Pin Len=" + (
            pin == null ? 0 : pin.length()) + " Status=" + 
            status);
        }
      } else if (nPINEntryPath == EMVConstants.EMV_CVM_OFF_PLAIN_PIN)
      {
        pin = readPin(EMVConstants.EMV_CVM_OFF_PLAIN_PIN, ucTryCnt);
        if ((pin != null) && (pin.length() >= 4)) {
          pinVerify(EMVConstants.EMV_CVM_OFF_PLAIN_PIN, pin);
        } else {
          pinVerify(EMVConstants.EMV_CVM_OFF_PLAIN_PIN, "");
          if (status == EMVConstants.EMV_CVM_ONLINE_PIN)
            status = EMVConstants.STATUS_OK;
        }
        ucTryCnt--;
        if (isShowAPDU()) {
          Log.d("EMVHandler", "EMV_CVM_OFF_PLAIN_PIN Pin Len=" + (
            pin == null ? 0 : pin.length()) + " Status=" + 
            status);
        }
      } else if (nPINEntryPath == EMVConstants.EMV_CVM_OFF_ENCRYPT_PIN)
      {
        pin = readPin(EMVConstants.EMV_CVM_OFF_ENCRYPT_PIN, ucTryCnt);
        if ((pin != null) && (pin.length() >= 4)) {
          pinVerify(EMVConstants.EMV_CVM_OFF_ENCRYPT_PIN, pin);
        } else {
          pinVerify(EMVConstants.EMV_CVM_OFF_ENCRYPT_PIN, "");
          if (status == EMVConstants.EMV_CVM_ONLINE_PIN)
            status = EMVConstants.STATUS_OK;
        }
        ucTryCnt--;
        if (isShowAPDU()) {
          Log.d("EMVHandler", "EMV_CVM_OFF_ENCRYPT_PIN Pin Len=" + (
            pin == null ? 0 : pin.length()) + " Status=" + 
            status);
        }
      }
      else {
        if (isShowAPDU()) {
          Log.d("EMVHandler", "EMV_CVM_CMD_CANCEL");
        }
        return;
      }
      if ((status == EMVConstants.STATUS_OK) || 
        (status == EMVConstants.EMV_USER_CANCEL) || (ucTryCnt <= 0)) {
        return;
      }
      nPINEntryPath = status;
    }
  }
  
  private String readPin(int type, int ucPinTryCnt)
  {
    String pin = null;
    for (EMVListener listener : _listeners) {
      pin = listener.onReadPin(type, ucPinTryCnt);
      if (pin != null) {
        return pin;
      }
    }
    return null;
  }
  

  private EMVResponse onlineMsgProcess()
  {
    if (!isAutoRead) {
      EMVResponse response = null;
      for (EMVListener listener : _listeners) {
        response = listener.onSubmitData();
        if (response != null) {
          System.out.println("valores resp");
          return response;
        }
      }
      return null;
    }
    return null;
  }
  
  private void confirmMsgProcess() {
    if (!isAutoRead) {
      for (EMVListener listener : _listeners) {
        listener.onConfirmData();
      }
    }
  }
  
  private void reversalMsgProcess() {
    if (!isAutoRead) {
      for (EMVListener listener : _listeners) {
        listener.onReversalData();
      }
    }
  }
  
  private void finishProcess() {
    finishExchangeAPDU();
    whileFlag = false;
  }
  
  private byte charToByte(char c) {
    return (byte)"0123456789abcdef".indexOf(c);
  }
  
  private String bytesToHexString(byte[] src, int len) {
    StringBuilder stringBuilder = new StringBuilder("");
    if ((src == null) || (src.length <= 0) || (len > src.length)) {
      return null;
    }
    for (int i = 0; i < len; i++) {
      int v = src[i] & 0xFF;
      String hv = Integer.toHexString(v);
      if (hv.length() < 2) {
        stringBuilder.append(0);
      }
      stringBuilder.append(hv);
    }
    return stringBuilder.toString();
  }
  
  public class AppBean implements Comparable<AppBean>
  {
    private String appPreName;
    private String appLabel;
    private String AID;
    private int priority;
    private String content;
    
    private AppBean() {}

    public String getAppLabel() {
        return appLabel;
    }

    public String getAID(){
      return AID;
    }

    public int compareTo(AppBean bean) {
      return new Integer(priority & 0xF).compareTo(new Integer(
        priority & 0xF));
    }
    
    public String toString() {
      if ((appPreName != null) && (!"".equals(appPreName)))
        return "appPreName: " + appPreName;
      if ((appLabel != null) && (!"".equals(appLabel)))
        return "appLabel: " + appLabel;
      if ((AID != null) && (!"".equals(AID)))
        return "AID: " + AID;
      return content;
    }
  }
  


  public String getIcField55()
  {
    int[] tags = { 40742, 40743, 40720, 40759, 40758, 149, 
      154, 156, 40706, 24362, 130, 40730, 40707, 40755, 
      40756, 40757, 40734, 132, 40713, 40769, 40803 };
    return getIcField55(tags);
  }
  


  public String getIcField55(int[] tags)
  {
    StringBuffer sb = new StringBuffer();
    for (int i : tags) {
      String res = getTLVDataByTag(i);
      System.out.println("valores : card " + i + " " + res);
      if (res != null) {
        sb.append(Settings.getHexString(i));
        sb.append(Settings.getHexString(res.length() / 2));
        sb.append(res);
      } else if (i == 40707) {
        sb.append("9F03");
        sb.append("06");
        sb.append("000000000000");
      }
    }
    return sb.toString().toUpperCase();
  }
  


  public String getIcSeq()
  {
    return "0" + getTLVDataByTag(24372);
  }
  


  public String getIcPan()
  {
    String pan = getTLVDataByTag(90);
    if ((pan != null) && (pan.endsWith("f"))) {
      return pan.substring(0, pan.length() - 1);
    }
    return pan;
  }
  


  public String getICTrack2Data()
  {
    String track = getTLVDataByTag(87);
    if ((track != null) && (track.endsWith("f"))) {
      return track.substring(0, track.length() - 1);
    }
    return track;
  }
  




  public String getICRandom()
  {
    return icRandom;
  }
  
  public String getIcExpDate()
  {
    return getTLVDataByTag(24356);
  }
  
  public String getIcEffDate()
  {
    return getTLVDataByTag(24357);
  }
  
  protected void resetKernel() {
    kernelInit(new EMV_Param(), new EMV_TransData());
  }
  

  private int status = 255;
  private String msg = "";
  
  private native void kernelInit(EMV_Param paramEMV_Param, EMV_TransData paramEMV_TransData);
  
  private native void addApp(EMVApp paramEMVApp);
  
  private native Apdu_Send getApduCommand();
  
  private native void setApduResult(Apdu_Resp paramApdu_Resp);
  
  private native List<String> appSelect(int paramInt);
  
  private native void finalSelect(int paramInt);
  
  private native void getFinalApp();
  
  private native void getProcessOption();
  
  private native void readAppData();
  
  public native String getTLVDataByTag(int paramInt);
  
  public native void setTLVData(int paramInt, String paramString);
  
  private native void addCapk(EMVCapk paramEMVCapk);
  
  private native void addRevoc(EMVRevoc paramEMVRevoc);
  
  private native void cardAuth();
  
  private native int transProcess();
  
  private native void pinVerify(int paramInt, String paramString);
  
  private native int actionAnalyse();
  
  private native void onlineDataProcess(String paramString, int paramInt1, int paramInt2);
  
  private native void finishExchangeAPDU();
  
  public native String getScriptResult();
  
  public native String getCapkCheckSum(EMVCapk paramEMVCapk);
  
  private native String getShakeHand();
  
  private native void setShakeHand(byte[] paramArrayOfByte);
  
  private native int checkSignatureStatus();
}
