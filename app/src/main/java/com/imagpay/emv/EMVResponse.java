package com.imagpay.emv;




public class EMVResponse
{
  public static int ONLINE_APPROVE = EMVConstants.ONLINE_APPROVE;
  public static int ONLINE_DENIAL = EMVConstants.ONLINE_DENIAL;
  public static int ONLINE_FAILED = EMVConstants.ONLINE_FAILED;
  
  private String arc;
  private String iad;
  private String script;
  private int status;
  
  public EMVResponse() {}
  
  public EMVResponse(String arc, String iad, String script)
  {
    this.arc = arc;
    this.iad = iad;
    this.script = script;
  }
  
  public String getARC() {
    return arc;
  }
  
  public void setARC(String arc) {
    this.arc = arc;
  }
  
  public String getIAD() {
    return iad;
  }
  
  public void setIAD(String iad) {
    this.iad = iad;
  }
  
  public String getScript() {
    return script;
  }
  
  public void setScript(String script) {
    this.script = script;
  }
  
  public int getStatus() {
    return status;
  }
  
  public void setStatus(int status) {
    this.status = status;
  }
}
