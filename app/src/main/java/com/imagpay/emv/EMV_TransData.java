package com.imagpay.emv;

import java.util.Random;

















public class EMV_TransData
{
  private int authAmnt;
  private int otherAmnt;
  private String transDate;
  private String transTime;
  private String transTermUN;
  
  public EMV_TransData()
  {
    transTermUN = generateTermUN();
  }
  
  public EMV_TransData(int authAmnt, int otherAmnt, String transDate, String transTime)
  {
    this.authAmnt = authAmnt;
    this.otherAmnt = otherAmnt;
    this.transDate = transDate;
    this.transTime = transTime;
    

    transTermUN = generateTermUN();
  }
  
  private String generateTermUN() {
    Random random = new Random(System.currentTimeMillis());
    Long tu = Long.valueOf(random.nextLong());
    StringBuffer sbf = new StringBuffer();
    sbf.append(Long.toHexString(tu.longValue()));
    int length = sbf.length();
    if (length < 8) {
      for (int i = 0; i < 8 - length; i++)
        sbf.insert(0, "0");
    } else if (length > 8) {
      for (int i = 0; i < length - 8; i++)
        sbf.deleteCharAt(0);
    }
    return sbf.toString();
  }
  
  public int getAuthAmnt() {
    return authAmnt;
  }
  
  public void setAuthAmnt(int authAmnt) {
    this.authAmnt = authAmnt;
  }
  
  public int getOtherAmnt() {
    return otherAmnt;
  }
  
  public void setOtherAmnt(int otherAmnt) {
    this.otherAmnt = otherAmnt;
  }
  
  public String getTransDate() {
    return transDate;
  }
  
  public void setTransDate(String transDate) {
    this.transDate = transDate;
  }
  
  public String getTransTime() {
    return transTime;
  }
  
  public void setTransTime(String transTime) {
    this.transTime = transTime;
  }
  







  public String getTransTermUN()
  {
    return transTermUN;
  }
}
