package com.imagpay.emv;



public class EMVApp
{
  private String appName;
  

  private String aid;
  

  private byte selFlag;
  

  private byte priority;
  

  private byte targetPer;
  

  private byte maxTargetPer;
  

  private byte floorLimitCheck;
  

  private int floorLimit;
  

  private int threshold;
  
  private String tacDenial;
  
  private String tacOnline;
  
  private String tacDefault;
  
  private String acquierId;
  
  private String dDOL;
  
  private String tDOL;
  
  private String version;
  

  public EMVApp() {}
  

  public EMVApp(String appName, String aid, byte selFlag, byte priority, byte targetPer, byte maxTargetPer, byte floorLimitCheck, int floorLimit, int threshold, String tacDenial, String tacOnline, String tacDefault, String acquierId, String dDOL, String tDOL, String version)
  {
    this.appName = appName;
    this.aid = aid;
    
    this.selFlag = selFlag;
    this.priority = priority;
    
    this.targetPer = targetPer;
    this.maxTargetPer = maxTargetPer;
    this.floorLimitCheck = floorLimitCheck;
    

    this.floorLimit = floorLimit;
    this.threshold = threshold;
    this.tacDenial = tacDenial;
    this.tacOnline = tacOnline;
    this.tacDefault = tacDefault;
    this.acquierId = acquierId;
    this.dDOL = dDOL;
    this.tDOL = tDOL;
    this.version = version;
  }
  
  public String getAppName() {
    return appName;
  }
  
  public void setAppName(String appName) {
    this.appName = appName;
  }
  
  public String getAID() {
    return aid;
  }
  
  public void setAID(String aid) {
    this.aid = aid;
  }
  







  public byte getSelFlag()
  {
    return selFlag;
  }
  
  public void setSelFlag(byte selFlag) {
    this.selFlag = selFlag;
  }
  
  public byte getPriority() {
    return priority;
  }
  
  public void setPriority(byte priority) {
    this.priority = priority;
  }
  







  public byte getTargetPer()
  {
    return targetPer;
  }
  
  public void setTargetPer(byte targetPer) {
    this.targetPer = targetPer;
  }
  
  public byte getMaxTargetPer() {
    return maxTargetPer;
  }
  
  public void setMaxTargetPer(byte maxTargetPer) {
    this.maxTargetPer = maxTargetPer;
  }
  
  public byte getFloorLimitCheck() {
    return floorLimitCheck;
  }
  
  public void setFloorLimitCheck(byte floorLimitCheck) {
    this.floorLimitCheck = floorLimitCheck;
  }
  















  public int getFloorLimit()
  {
    return floorLimit;
  }
  
  public void setFloorLimit(int floorLimit) {
    this.floorLimit = floorLimit;
  }
  
  public int getThreshold() {
    return threshold;
  }
  
  public void setThreshold(int threshold) {
    this.threshold = threshold;
  }
  
  public String getTACDenial() {
    return tacDenial;
  }
  
  public void setTACDenial(String tacDenial) {
    this.tacDenial = tacDenial;
  }
  
  public String getTACOnline() {
    return tacOnline;
  }
  
  public void setTACOnline(String tacOnline) {
    this.tacOnline = tacOnline;
  }
  
  public String getTACDefault() {
    return tacDefault;
  }
  
  public void setTACDefault(String tacDefault) {
    this.tacDefault = tacDefault;
  }
  
  public String getAcquierId() {
    return acquierId;
  }
  
  public void setAcquierId(String acquierId) {
    this.acquierId = acquierId;
  }
  
  public String getDDOL() {
    return dDOL;
  }
  
  public void setDDOL(String dDOL) {
    this.dDOL = dDOL;
  }
  
  public String getTDOL() {
    return tDOL;
  }
  
  public void setTDOL(String tDOL) {
    this.tDOL = tDOL;
  }
  
  public String getVersion() {
    return version;
  }
  
  public void setVersion(String version) {
    this.version = version;
  }
}
