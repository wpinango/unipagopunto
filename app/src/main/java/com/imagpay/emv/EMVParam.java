package com.imagpay.emv;

import java.util.ArrayList;
import java.util.List;


















public class EMVParam
{
  private byte slot = 0;
  private boolean readOnly = false;
  



  private List<EMVApp> apps = new ArrayList();
  


  private List<EMVCapk> capks = new ArrayList();
  


  private List<EMVRevoc> revocs = new ArrayList();
  


  private EMV_Param param;
  

  private EMV_TransData transData;
  


  public EMVParam()
  {
    param = new EMV_Param();
    transData = new EMV_TransData();
  }
  
  protected EMV_Param getEMVParam() {
    return param;
  }
  
  protected EMV_TransData getEMVTransData() {
    return transData;
  }
  
  public String getMerchName() {
    return param.getMerchName();
  }
  
  public void setMerchName(String merchName) {
    param.setMerchName(merchName);
  }
  
  public String getMerchCateCode() {
    return param.getMerchCateCode();
  }
  
  public void setMerchCateCode(String merchCateCode) {
    param.setMerchCateCode(merchCateCode);
  }
  
  public String getMerchId() {
    return param.getMerchId();
  }
  
  public void setMerchId(String merchId) {
    param.setMerchId(merchId);
  }
  
  public String getTermId() {
    return param.getTermId();
  }
  
  public void setTermId(String termId) {
    param.setTermId(termId);
  }
  
  public byte getTerminalType() {
    return param.getTerminalType();
  }
  
  public void setTerminalType(byte terminalType) {
    param.setTerminalType(terminalType);
  }
  
  public String getCapability() {
    return param.getCapability();
  }
  
  public void setCapability(String capability) {
    param.setCapability(capability);
  }
  
  public String getExCapability() {
    return param.getExCapability();
  }
  
  public void setExCapability(String exCapability) {
    param.setExCapability(exCapability);
  }
  
  public byte getTransCurrExp() {
    return param.getTransCurrExp();
  }
  
  public void setTransCurrExp(byte transCurrExp) {
    param.setTransCurrExp(transCurrExp);
  }
  
  public String getCountryCode() {
    return param.getCountryCode();
  }
  
  public void setCountryCode(String countryCode) {
    param.setCountryCode(countryCode);
  }
  
  public String getTransCurrCode() {
    return param.getTransCurrCode();
  }
  
  public void setTransCurrCode(String transCurrCode) {
    param.setTransCurrCode(transCurrCode);
  }
  
  public byte getTransType() {
    return param.getTransType();
  }
  
  public void setTransType(byte transType) {
    param.setTransType(transType);
  }
  















  public String getTermIFDSn()
  {
    return param.getTermIFDSn();
  }
  
  public void setTermIFDSn(String termIFDSn) {
    param.setTermIFDSn(termIFDSn);
  }
  























  public int getAuthAmnt()
  {
    return transData.getAuthAmnt();
  }
  
  public void setAuthAmnt(int authAmnt) {
    transData.setAuthAmnt(authAmnt);
  }
  
  public int getOtherAmnt() {
    return transData.getOtherAmnt();
  }
  
  public void setOtherAmnt(int otherAmnt) {
    transData.setOtherAmnt(otherAmnt);
  }
  
  public String getTransDate() {
    return transData.getTransDate();
  }
  
  public void setTransDate(String transDate) {
    transData.setTransDate(transDate);
  }
  
  public String getTransTime() {
    return transData.getTransTime();
  }
  
  public void setTransTime(String transTime) {
    transData.setTransTime(transTime);
  }
  


















  public void addApp(EMVApp app)
  {
    apps.add(app);
  }
  


  public void delApp(EMVApp app)
  {
    apps.remove(app);
  }
  


  public List<EMVApp> getApps()
  {
    return apps;
  }
  


  public void addCapk(EMVCapk capk)
  {
    capks.add(capk);
  }
  


  public void delCapk(EMVCapk capk)
  {
    capks.remove(capk);
  }
  


  public List<EMVCapk> getCapks()
  {
    return capks;
  }
  


  public void addRecov(EMVRevoc revoc)
  {
    revocs.add(revoc);
  }
  


  public void delRecov(EMVRevoc revoc)
  {
    revocs.remove(revoc);
  }
  


  public List<EMVRevoc> getRevocs()
  {
    return revocs;
  }
  


  public byte getSlot()
  {
    return slot;
  }
  


  public void setSlot(byte slot)
  {
    this.slot = slot;
  }
  


  public void setReadOnly(boolean readOnly)
  {
    this.readOnly = readOnly;
  }
  


  public boolean isReadOnly()
  {
    return readOnly;
  }
}
