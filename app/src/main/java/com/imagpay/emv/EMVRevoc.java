package com.imagpay.emv;





public class EMVRevoc
{
  private String ucRID;
  



  private byte ucIndex;
  


  private String ucCertSn;
  



  public EMVRevoc() {}
  



  public EMVRevoc(String ucRID, byte ucIndex, String ucCertSn)
  {
    this.ucRID = ucRID;
    this.ucIndex = ucIndex;
    this.ucCertSn = ucCertSn;
  }
  
  public String getUCRID() {
    return ucRID;
  }
  
  public void setUCRID(String ucRID) {
    this.ucRID = ucRID;
  }
  
  public byte getUCIndex() {
    return ucIndex;
  }
  
  public void setUCIndex(byte ucIndex) {
    this.ucIndex = ucIndex;
  }
  
  public String getUCCertSn() {
    return ucCertSn;
  }
  
  public void setUCCertSn(String ucCertSn) {
    this.ucCertSn = ucCertSn;
  }
}
