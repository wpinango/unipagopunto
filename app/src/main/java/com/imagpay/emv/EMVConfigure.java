package com.imagpay.emv;

import android.annotation.SuppressLint;
import com.imagpay.utils.StringUtils;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
















public class EMVConfigure
{
  private byte slot = 0;
  


  private boolean isReadOnly = false;
  


  private String merchName = "China";
  


  private String merchCateCode = "0001";
  


  private String merchId = "123456789012345";
  


  private String termId = "12345678";
  


  private byte terminalType = 34;
  


  private String capability = "E0F8C8";
  


  private String exCapability = "F00000A001";
  


  private byte transCurrExp = 2;
  


  private String countryCode = "0156";
  


  private String transCurrCode = "0156";
  


  private byte transType = 0;
  


  private String termIFDSn = "88888888";
  


  private int authAmnt = 0;
  


  private int otherAmnt = 0;
  


  private String transDate;
  


  private String transTime;
  


  private List<EMVApp> _aids = new ArrayList();
  


  private List<EMVCapk> _capks = new ArrayList();
  
  public void setSlot(byte slot) {
    this.slot = slot;
  }
  
  public void setReadOnly(boolean isReadOnly) {
    this.isReadOnly = isReadOnly;
  }
  
  public void setMerchName(String merchName) {
    this.merchName = merchName;
  }
  
  public void setMerchCateCode(String merchCateCode) {
    this.merchCateCode = merchCateCode;
  }
  
  public void setMerchId(String merchId) {
    this.merchId = merchId;
  }
  
  public void setTermId(String termId) {
    this.termId = termId;
  }
  
  public void setTerminalType(byte terminalType) {
    this.terminalType = terminalType;
  }
  
  public void setCapability(String capability) {
    this.capability = capability;
  }
  
  public void setExCapability(String exCapability) {
    this.exCapability = exCapability;
  }
  
  public void setTransCurrExp(byte transCurrExp) {
    this.transCurrExp = transCurrExp;
  }
  
  public void setCountryCode(String countryCode) {
    this.countryCode = countryCode;
  }
  
  public void setTransCurrCode(String transCurrCode) {
    this.transCurrCode = transCurrCode;
  }
  
  public void setTransType(byte transType) {
    this.transType = transType;
  }
  
  public void setTermIFDSn(String termIFDSn) {
    this.termIFDSn = termIFDSn;
  }
  
  public void setAuthAmnt(int authAmnt) {
    this.authAmnt = authAmnt;
  }
  
  public void setOtherAmnt(int otherAmnt) {
    this.otherAmnt = otherAmnt;
  }
  
  public void setTransDate(String transDate) {
    this.transDate = transDate;
  }
  
  public void setTransTime(String transTime) {
    this.transTime = transTime;
  }
  
  public void addAID(EMVApp aid) {
    EMVApp tmp = new EMVApp();
    for (EMVApp app : _aids) {
      if (app.getAID().equals(aid.getAID()))
      {
        tmp = app;
      }
    }
    if (tmp != null) {
      _aids.remove(tmp);
    }
    _aids.add(aid);
  }
  


  public void addCAPKs(EMVCapk capk)
  {
    EMVCapk tmp = new EMVCapk();
    for (EMVCapk ca : _capks) {
      if ((ca.getRID().equals(capk.getRID())) && 
        (ca.getKeyID() == capk.getKeyID())) {
        tmp = ca;
      }
    }
    if (tmp != null) {
      _capks.remove(tmp);
    }
    _capks.add(capk);
  }
  



  public EMVConfigure()
  {
    loadAIDs();
    loadCapks();
  }
  





  @SuppressLint({"SimpleDateFormat"})
  public EMVConfigure(int authAmnt)
  {
    Date date = new Date();
    DateFormat sdf = new SimpleDateFormat("yyMMdd");
    String _date = sdf.format(date);
    sdf = new SimpleDateFormat("HHmmss");
    String _time = sdf.format(date);
    initParameters(authAmnt, _date, _time);
  }
  









  public EMVConfigure(int authAmnt, String transDate, String transTime)
  {
    initParameters(authAmnt, transDate, transTime);
  }
  
  private void initParameters(int authAmnt, String transDate, String transTime) {
    this.authAmnt = authAmnt;
    this.transDate = transDate;
    this.transTime = transTime;
    loadAIDs();
    loadCapks();
  }
  
  public EMVParam getEmvConfig() {
    EMVParam param = new EMVParam();
    param.setSlot(slot);
    param.setReadOnly(isReadOnly);
    param.setMerchName(BCDToHex(merchName));
    param.setMerchCateCode(merchCateCode);
    param.setMerchId(BCDToHex(merchId));
    param.setTermId(BCDToHex(termId));
    param.setTerminalType(terminalType);
    param.setCapability(capability);
    
    param.setExCapability(exCapability);
    param.setTransCurrExp(transCurrExp);
    param.setCountryCode(countryCode);
    param.setTransCurrCode(transCurrCode);
    param.setTransType(transType);
    param.setTermIFDSn(BCDToHex(termIFDSn));
    param.setAuthAmnt(authAmnt);
    param.setOtherAmnt(otherAmnt);
    param.setTransDate(transDate);
    param.setTransTime(transTime);
    
    for (EMVApp app : _aids) {
      param.addApp(app);
    }
    
    for (EMVCapk capk : _capks) {
      param.addCapk(capk);
    }
    return param;
  }
  
  private String BCDToHex(String str) {
    return StringUtils.convertBytesToHex(str.getBytes());
  }
  

  private void loadAIDs()
  {
    EMVApp ea = new EMVApp();
    ea.setAppName("");
    ea.setAID("A0000003330101");
    ea.setSelFlag(EMVConstants.PART_MATCH);
    ea.setPriority((byte)0);
    ea.setTargetPer((byte)14);
    ea.setMaxTargetPer((byte)32);
    ea.setFloorLimitCheck((byte)1);
    ea.setFloorLimit(5);
    ea.setThreshold(28);
    ea.setTACDenial("0010000000");
    ea.setTACOnline("FC78FCF8F0");
    ea.setTACDefault("FC78FCF8F0");
    ea.setAcquierId("000000123456");
    ea.setDDOL("039F3704");
    ea.setTDOL("");
    ea.setVersion("0020");
    _aids.add(ea);
    
    ea = new EMVApp();
    ea.setAppName("");
    ea.setAID("A0000003330102");
    ea.setSelFlag(EMVConstants.PART_MATCH);
    ea.setPriority((byte)0);
    ea.setTargetPer((byte)14);
    ea.setMaxTargetPer((byte)32);
    ea.setFloorLimitCheck((byte)1);
    ea.setFloorLimit(5);
    ea.setThreshold(28);
    ea.setTACDenial("0010000000");
    ea.setTACOnline("FC78FCF8F0");
    ea.setTACDefault("FC78FCF8F0");
    ea.setAcquierId("000000123456");
    ea.setDDOL("039F3704");
    ea.setTDOL("");
    ea.setVersion("0020");
    _aids.add(ea);
  }
  
  private void loadCapks() {
    EMVCapk ec = new EMVCapk();
    ec.setRID("A000000333");
    ec.setKeyID((byte)1);
    ec.setModul("BBE9066D2517511D239C7BFA77884144AE20C7372F515147E8CE6537C54C0A6A4D45F8CA4D290870CDA59F1344EF71D17D3F35D92F3F06778D0D511EC2A7DC4FFEADF4FB1253CE37A7B2B5A3741227BEF72524DA7A2B7B1CB426BEE27BC513B0CB11AB99BC1BC61DF5AC6CC4D831D0848788CD74F6D543AD37C5A2B4C5D5A93B");
    


    ec.setExponent("000003");
    ec.setExpDate("091231");
    ec.setCheckSum("E881E390675D44C2DD81234DCE29C3F5AB2297A0");
    _capks.add(ec);
    
    ec = new EMVCapk();
    ec.setRID("A000000333");
    ec.setKeyID((byte)2);
    ec.setModul("A3767ABD1B6AA69D7F3FBF28C092DE9ED1E658BA5F0909AF7A1CCD907373B7210FDEB16287BA8E78E1529F443976FD27F991EC67D95E5F4E96B127CAB2396A94D6E45CDA44CA4C4867570D6B07542F8D4BF9FF97975DB9891515E66F525D2B3CBEB6D662BFB6C3F338E93B02142BFC44173A3764C56AADD202075B26DC2F9F7D7AE74BD7D00FD05EE430032663D27A57");
    


    ec.setExponent("000003");
    ec.setExpDate("141231");
    ec.setCheckSum("03BB335A8549A03B87AB089D006F60852E4B8060");
    _capks.add(ec);
    
    ec = new EMVCapk();
    ec.setRID("A000000333");
    ec.setKeyID((byte)3);
    ec.setModul("B0627DEE87864F9C18C13B9A1F025448BF13C58380C91F4CEBA9F9BCB214FF8414E9B59D6ABA10F941C7331768F47B2127907D857FA39AAF8CE02045DD01619D689EE731C551159BE7EB2D51A372FF56B556E5CB2FDE36E23073A44CA215D6C26CA68847B388E39520E0026E62294B557D6470440CA0AEFC9438C923AEC9B2098D6D3A1AF5E8B1DE36F4B53040109D89B77CAFAF70C26C601ABDF59EEC0FDC8A99089140CD2E817E335175B03B7AA33D");
    



    ec.setExponent("000003");
    ec.setExpDate("171231");
    ec.setCheckSum("87F0CD7C0E86F38F89A66F8C47071A8B88586F26");
    _capks.add(ec);
    
    ec = new EMVCapk();
    ec.setRID("A000000333");
    ec.setKeyID((byte)4);
    ec.setModul("BC853E6B5365E89E7EE9317C94B02D0ABB0DBD91C05A224A2554AA29ED9FCB9D86EB9CCBB322A57811F86188AAC7351C72BD9EF196C5A01ACEF7A4EB0D2AD63D9E6AC2E7836547CB1595C68BCBAFD0F6728760F3A7CA7B97301B7E0220184EFC4F653008D93CE098C0D93B45201096D1ADFF4CF1F9FC02AF759DA27CD6DFD6D789B099F16F378B6100334E63F3D35F3251A5EC78693731F5233519CDB380F5AB8C0F02728E91D469ABD0EAE0D93B1CC66CE127B29C7D77441A49D09FCA5D6D9762FC74C31BB506C8BAE3C79AD6C2578775B95956B5370D1D0519E37906B384736233251E8F09AD79DFBE2C6ABFADAC8E4D8624318C27DAF1");
    




    ec.setExponent("000003");
    ec.setExpDate("171231");
    ec.setCheckSum("F527081CF371DD7E1FD4FA414A665036E0F5E6E5");
    _capks.add(ec);
  }
}
