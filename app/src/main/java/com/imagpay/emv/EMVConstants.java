package com.imagpay.emv;




















public class EMVConstants
{
  public static byte APP_ONLINE_REFER = 4;
  public static byte APP_ONLINE_ABORT = 4;
  
  public static byte PART_MATCH = 0;
  public static byte FULL_MATCH = 1;
  
  public static int TRANS_TYPE_CASH = 1;
  public static int TRANS_TYPE_GOODS = 0;
  public static int TRANS_TYPE_CASHBACK = 9;
  
  public static int STATUS_OK = 0;
  public static int EMV_RESELECT_APP = -1;
  public static int ICC_CMD_ERR = -2;
  public static int ICC_BLOCK = -3;
  public static int EMV_RSP_ERR = -4;
  public static int EMV_APP_BLOCK = -5;
  public static int EMV_NO_APP = -6;
  public static int EMV_USER_CANCEL = -7;
  public static int EMV_TIME_OUT = -8;
  public static int EMV_DATA_ERR = -9;
  public static int EMV_NOT_ACCEPT = -10;
  public static int EMV_DENIAL = -11;
  public static int EMV_KEY_EXP = -12;
  
  public static int EMV_NO_PINPAD = -13;
  public static int EMV_OVERFLOW = -18;
  public static int EMV_NO_PASSWORD = -14;
  public static int EMV_SUM_ERR = -15;
  public static int EMV_NOT_FOUND = -16;
  public static int EMV_NO_DATA = -17;
  public static int EMV_DATA_EXIST = -20;
  public static int EMV_FILE_ERR = -21;
  public static int EMV_PIN_BLOCK = -22;
  public static int ICC_RSP_6985 = -23;
  public static int EMV_PARAM_ERR = -24;
  public static int EMV_CID_ERR = -25;
  public static int EMV_ENCR_CONTROL = -26;
  
  public static int EMV_PED_TIMEOUT = 1;
  public static int EMV_PED_WAIT = 2;
  public static int EMV_PED_FAIL = 3;
  
  public static int ONLINE_APPROVE = 0;
  public static int ONLINE_DENIAL = 1;
  public static int ONLINE_FAILED = 2;
  
  public static int EMV_AC_AAC = 0;
  public static int EMV_AC_TC = 1;
  public static int EMV_AC_ARQC = 2;
  
  public static int EMV_CVM_OFF_PLAIN_PIN = 1;
  public static int EMV_CVM_OFF_ENCRYPT_PIN = 2;
  public static int EMV_CVM_ONLINE_PIN = 3;
  
  public static int EMV_CVM_CMD_CANCEL = 1;
  public static int EMV_CVM_CMD_FINISHED = 2;
  public static int EMV_CVM_CMD_FAILED = 3;
  

  public static int EMV_ERROR = 255;
  public static int EMV_CHECK = 239;
  public static int EMV_SIG_ERR = 238;
  
  public EMVConstants() {}
}
