package com.imagpay.emv;




public class EMV_Param
{
  private String merchName;
  


  private String merchCateCode;
  

  private String merchId;
  

  private String termId;
  

  private byte terminalType;
  

  private String capability;
  

  private String exCapability;
  

  private byte transCurrExp;
  

  private String countryCode;
  

  private String transCurrCode;
  

  private byte transType;
  

  private String termIFDSn;
  


  public EMV_Param() {}
  


  public EMV_Param(String merchName, String merchCateCode, String merchId, String termId, byte terminalType, String capability, String exCapability, byte transCurrExp, String countryCode, String transCurrCode, byte transType, String termIFDSn)
  {
    this.merchName = merchName;
    this.merchCateCode = merchCateCode;
    this.merchId = merchId;
    this.termId = termId;
    this.terminalType = terminalType;
    this.capability = capability;
    this.exCapability = exCapability;
    this.transCurrExp = transCurrExp;
    this.countryCode = countryCode;
    this.transCurrCode = transCurrCode;
    this.transType = transType;
    

    this.termIFDSn = termIFDSn;
  }
  


  public String getMerchName()
  {
    return merchName;
  }
  
  public void setMerchName(String merchName) {
    this.merchName = merchName;
  }
  
  public String getMerchCateCode() {
    return merchCateCode;
  }
  
  public void setMerchCateCode(String merchCateCode) {
    this.merchCateCode = merchCateCode;
  }
  
  public String getMerchId() {
    return merchId;
  }
  
  public void setMerchId(String merchId) {
    this.merchId = merchId;
  }
  
  public String getTermId() {
    return termId;
  }
  
  public void setTermId(String termId) {
    this.termId = termId;
  }
  
  public byte getTerminalType() {
    return terminalType;
  }
  
  public void setTerminalType(byte terminalType) {
    this.terminalType = terminalType;
  }
  
  public String getCapability() {
    return capability;
  }
  
  public void setCapability(String capability) {
    this.capability = capability;
  }
  
  public String getExCapability() {
    return exCapability;
  }
  
  public void setExCapability(String exCapability) {
    this.exCapability = exCapability;
  }
  
  public byte getTransCurrExp() {
    return transCurrExp;
  }
  
  public void setTransCurrExp(byte transCurrExp) {
    this.transCurrExp = transCurrExp;
  }
  
  public String getCountryCode() {
    return countryCode;
  }
  
  public void setCountryCode(String countryCode) {
    this.countryCode = countryCode;
  }
  
  public String getTransCurrCode() {
    return transCurrCode;
  }
  
  public void setTransCurrCode(String transCurrCode) {
    this.transCurrCode = transCurrCode;
  }
  
  public byte getTransType() {
    return transType;
  }
  
  public void setTransType(byte transType) {
    this.transType = transType;
  }
  















  public String getTermIFDSn()
  {
    return termIFDSn;
  }
  
  public void setTermIFDSn(String termIFDSn) {
    this.termIFDSn = termIFDSn;
  }
}
