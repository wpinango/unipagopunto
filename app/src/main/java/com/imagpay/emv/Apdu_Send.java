package com.imagpay.emv;




public class Apdu_Send
{
  private byte[] command;
  


  private short lc;
  


  private byte[] dataIn;
  


  private short le;
  



  public Apdu_Send() {}
  



  public Apdu_Send(byte[] command, short lc, byte[] data, short le)
  {
    this.command = command;
    this.lc = lc;
    dataIn = data;
    this.le = le;
  }
  
  public byte[] getCommand() {
    return command;
  }
  
  public void setCommand(byte[] command) {
    this.command = command;
  }
  
  public short getLC() {
    return lc;
  }
  
  public void setLC(short lc) {
    this.lc = lc;
  }
  
  public byte[] getDataIn() {
    return dataIn;
  }
  
  public void setDataIn(byte[] dataIn) {
    this.dataIn = dataIn;
  }
  
  public short getLE() {
    return le;
  }
  
  public void setLE(short le) {
    this.le = le;
  }
}
