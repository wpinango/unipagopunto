package com.imagpay.emv;

import java.util.List;

public abstract interface EMVListener
{
  public abstract int onSelectApp(List<String> paramList);
  
  public abstract boolean onReadData();
  
  public abstract String onReadPin(int paramInt1, int paramInt2);
  
  public abstract EMVResponse onSubmitData();
  
  public abstract void onConfirmData();
  
  public abstract void onReversalData();
}
