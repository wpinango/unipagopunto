package com.imagpay.beans;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import java.util.List;












@XStreamAlias("capks")
public class EMVCapks
{
  @XStreamImplicit(itemFieldName="capk")
  private List<EMVCapk> capk;
  
  public EMVCapks() {}
  
  public List<EMVCapk> getCapkList()
  {
    return capk;
  }
  
  public void setCapkList(List<EMVCapk> capk) {
    this.capk = capk;
  }
}
