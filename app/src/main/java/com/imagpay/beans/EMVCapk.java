package com.imagpay.beans;

import com.thoughtworks.xstream.annotations.XStreamAlias;

























@XStreamAlias("capk")
public class EMVCapk
{
  @XStreamAlias("rid")
  private String rid;
  @XStreamAlias("keyID")
  private byte keyID;
  @XStreamAlias("modul")
  private String modul;
  @XStreamAlias("exponent")
  private String exponent;
  @XStreamAlias("expDate")
  private String expDate;
  @XStreamAlias("checkSum")
  private String checkSum;
  
  public EMVCapk() {}
  
  public EMVCapk(String rid, byte keyID, String modul, String exponent, String expDate, String checkSum)
  {
    this.rid = rid;
    this.keyID = keyID;
    


    this.modul = modul;
    
    this.exponent = exponent;
    this.expDate = expDate;
    this.checkSum = checkSum;
  }
  
  public String getRID() {
    return rid;
  }
  
  public void setRID(String rid) {
    this.rid = rid;
  }
  
  public byte getKeyID() {
    return keyID;
  }
  
  public void setKeyID(byte keyID) {
    this.keyID = keyID;
  }
  























  public String getModul()
  {
    return modul;
  }
  
  public void setModul(String modul) {
    this.modul = modul;
  }
  







  public String getExponent()
  {
    return exponent;
  }
  
  public void setExponent(String exponent) {
    this.exponent = exponent;
  }
  
  public String getExpDate() {
    return expDate;
  }
  
  public void setExpDate(String expDate) {
    this.expDate = expDate;
  }
  
  public String getCheckSum() {
    return checkSum;
  }
  
  public void setCheckSum(String checkSum) {
    this.checkSum = checkSum;
  }
}
