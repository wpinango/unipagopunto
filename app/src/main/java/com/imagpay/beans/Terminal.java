package com.imagpay.beans;

import com.thoughtworks.xstream.annotations.XStreamAlias;











@XStreamAlias("terminal")
public class Terminal
{
  @XStreamAlias("dataCapture")
  private String dataCapture;
  @XStreamAlias("termType")
  private String termType;
  @XStreamAlias("termCapabilities")
  private String termCapabilities;
  @XStreamAlias("addTermCapabilities")
  private String addTermCapabilities;
  @XStreamAlias("termCountryCode")
  private String termCountryCode;
  @XStreamAlias("IFD")
  private String IFD;
  @XStreamAlias("transDate")
  private String transDate;
  @XStreamAlias("transTime")
  private String transTime;
  
  public Terminal() {}
  
  public String getDataCapture()
  {
    return dataCapture;
  }
  
  public void setDataCapture(String dataCapture) {
    this.dataCapture = dataCapture;
  }
  
  public String getTermType() {
    return termType;
  }
  
  public void setTermType(String termType) {
    this.termType = termType;
  }
  
  public String getTermCapabilities() {
    return termCapabilities;
  }
  
  public void setTermCapabilities(String termCapabilities) {
    this.termCapabilities = termCapabilities;
  }
  
  public String getAddTermCapabilities() {
    return addTermCapabilities;
  }
  
  public void setAddTermCapabilities(String addTermCapabilities) {
    this.addTermCapabilities = addTermCapabilities;
  }
  
  public String getTermCountryCode() {
    return termCountryCode;
  }
  
  public void setTermCountryCode(String termCountryCode) {
    this.termCountryCode = termCountryCode;
  }
  
  public String getIFD() {
    return IFD;
  }
  
  public void setIFD(String iFD) {
    IFD = iFD;
  }
  
  public String getTransDate() {
    return transDate;
  }
  
  public void setTransDate(String transDate) {
    this.transDate = transDate;
  }
  
  public String getTransTime() {
    return transTime;
  }
  
  public void setTransTime(String transTime) {
    this.transTime = transTime;
  }
}
