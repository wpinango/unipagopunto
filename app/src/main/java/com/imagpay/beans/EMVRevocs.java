package com.imagpay.beans;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import java.util.List;












@XStreamAlias("revocs")
public class EMVRevocs
{
  @XStreamImplicit(itemFieldName="revoc")
  private List<EMVRevoc> revoc;
  
  public EMVRevocs() {}
  
  public List<EMVRevoc> getRevocList()
  {
    return revoc;
  }
  
  public void setRevocList(List<EMVRevoc> revoc) {
    this.revoc = revoc;
  }
}
