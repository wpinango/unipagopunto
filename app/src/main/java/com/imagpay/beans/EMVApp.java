package com.imagpay.beans;

import com.thoughtworks.xstream.annotations.XStreamAlias;


































@XStreamAlias("app")
public class EMVApp
{
  @XStreamAlias("appName")
  private String appName;
  @XStreamAlias("aid")
  private String aid;
  @XStreamAlias("selFlag")
  private byte selFlag;
  @XStreamAlias("priority")
  private byte priority;
  @XStreamAlias("targetPer")
  private byte targetPer;
  @XStreamAlias("maxTargetPer")
  private byte maxTargetPer;
  @XStreamAlias("floorLimitCheck")
  private byte floorLimitCheck;
  @XStreamAlias("floorLimit")
  private int floorLimit;
  @XStreamAlias("threshold")
  private int threshold;
  @XStreamAlias("tacDenial")
  private String tacDenial;
  @XStreamAlias("tacOnline")
  private String tacOnline;
  @XStreamAlias("tacDefault")
  private String tacDefault;
  @XStreamAlias("acquierId")
  private String acquierId;
  @XStreamAlias("dDOL")
  private String dDOL;
  @XStreamAlias("tDOL")
  private String tDOL;
  @XStreamAlias("appVersion")
  private String version;
  @XStreamAlias("merchCateCode")
  private String merchCateCode;
  @XStreamAlias("merchId")
  private String merchId;
  @XStreamAlias("merName")
  private String merName;
  @XStreamAlias("termId")
  private String termId;
  @XStreamAlias("transCurrCode")
  private String transCurrCode;
  @XStreamAlias("transCurrExp")
  private byte transCurrExp;
  @XStreamAlias("transRefCode")
  private String transRefCode;
  @XStreamAlias("transRefExp")
  private byte transRefExp;
  @XStreamAlias("terRisk")
  private String terRisk;
  @XStreamAlias("ecTTLVal")
  private String ecTTLVal;
  @XStreamAlias("CLOfflineLimit")
  private String CLOfflineLimit;
  @XStreamAlias("CLTransLimit")
  private String CLTransLimit;
  @XStreamAlias("CLCVMLimit")
  private String CLCVMLimit;
  
  public EMVApp() {}
  
  public EMVApp(String appName, String aid, byte selFlag, byte priority, byte targetPer, byte maxTargetPer, byte floorLimitCheck, int floorLimit, int threshold, String tacDenial, String tacOnline, String tacDefault, String acquierId, String dDOL, String tDOL, String version, String merchCateCode, String merchId, String merName, String termId, String transCurrCode, byte transCurrExp, String transRefCode, byte transRefExp, String terRisk, String ecTTLVal, String CLOfflineLimit, String CLTransLimit, String CLCVMLimit)
  {
    this.appName = appName;
    this.aid = aid;
    
    this.selFlag = selFlag;
    this.priority = priority;
    
    this.targetPer = targetPer;
    this.maxTargetPer = maxTargetPer;
    this.floorLimitCheck = floorLimitCheck;
    

    this.floorLimit = floorLimit;
    this.threshold = threshold;
    this.tacDenial = tacDenial;
    this.tacOnline = tacOnline;
    this.tacDefault = tacDefault;
    this.acquierId = acquierId;
    this.dDOL = dDOL;
    this.tDOL = tDOL;
    this.version = version;
    
    this.merchCateCode = merchCateCode;
    this.merchId = merchId;
    this.merName = merName;
    this.termId = termId;
    this.transCurrCode = transCurrCode;
    this.transCurrExp = transCurrExp;
    this.transRefCode = transRefCode;
    this.transRefExp = transRefExp;
    this.terRisk = terRisk;
    this.ecTTLVal = ecTTLVal;
    this.CLOfflineLimit = CLOfflineLimit;
    this.CLTransLimit = CLTransLimit;
    this.CLCVMLimit = CLCVMLimit;
  }
  
  public String getAppName() {
    return appName;
  }
  
  public void setAppName(String appName) {
    this.appName = appName;
  }
  
  public String getAID() {
    return aid;
  }
  
  public void setAID(String aid) {
    this.aid = aid;
  }
  







  public byte getSelFlag()
  {
    return selFlag;
  }
  
  public void setSelFlag(byte selFlag) {
    this.selFlag = selFlag;
  }
  
  public byte getPriority() {
    return priority;
  }
  
  public void setPriority(byte priority) {
    this.priority = priority;
  }
  







  public byte getTargetPer()
  {
    return targetPer;
  }
  
  public void setTargetPer(byte targetPer) {
    this.targetPer = targetPer;
  }
  
  public byte getMaxTargetPer() {
    return maxTargetPer;
  }
  
  public void setMaxTargetPer(byte maxTargetPer) {
    this.maxTargetPer = maxTargetPer;
  }
  
  public byte getFloorLimitCheck() {
    return floorLimitCheck;
  }
  
  public void setFloorLimitCheck(byte floorLimitCheck) {
    this.floorLimitCheck = floorLimitCheck;
  }
  















  public int getFloorLimit()
  {
    return floorLimit;
  }
  
  public void setFloorLimit(int floorLimit) {
    this.floorLimit = floorLimit;
  }
  
  public int getThreshold() {
    return threshold;
  }
  
  public void setThreshold(int threshold) {
    this.threshold = threshold;
  }
  
  public String getTACDenial() {
    return tacDenial;
  }
  
  public void setTACDenial(String tacDenial) {
    this.tacDenial = tacDenial;
  }
  
  public String getTACOnline() {
    return tacOnline;
  }
  
  public void setTACOnline(String tacOnline) {
    this.tacOnline = tacOnline;
  }
  
  public String getTACDefault() {
    return tacDefault;
  }
  
  public void setTACDefault(String tacDefault) {
    this.tacDefault = tacDefault;
  }
  
  public String getAcquierId() {
    return acquierId;
  }
  
  public void setAcquierId(String acquierId) {
    this.acquierId = acquierId;
  }
  
  public String getDDOL() {
    return dDOL;
  }
  
  public void setDDOL(String dDOL) {
    this.dDOL = dDOL;
  }
  
  public String getTDOL() {
    return tDOL;
  }
  
  public void setTDOL(String tDOL) {
    this.tDOL = tDOL;
  }
  
  public String getVersion() {
    return version;
  }
  
  public void setVersion(String version) {
    this.version = version;
  }
  
  public String getMerchCateCode() {
    return merchCateCode;
  }
  
  public void setMerchCateCode(String merchCateCode) {
    this.merchCateCode = merchCateCode;
  }
  
  public String getMerchId() {
    return merchId;
  }
  
  public void setMerchId(String merchId) {
    this.merchId = merchId;
  }
  
  public String getMerName() {
    return merName;
  }
  
  public void setMerName(String merName) {
    this.merName = merName;
  }
  
  public String getTermId() {
    return termId;
  }
  
  public void setTermId(String termId) {
    this.termId = termId;
  }
  
  public String getTransCurrCode() {
    return transCurrCode;
  }
  
  public void setTransCurrCode(String transCurrCode) {
    this.transCurrCode = transCurrCode;
  }
  
  public byte getTransCurrExp() {
    return transCurrExp;
  }
  
  public void setTransCurrExp(byte transCurrExp) {
    this.transCurrExp = transCurrExp;
  }
  
  public String getTransRefCode() {
    return transRefCode;
  }
  
  public void setTransRefCode(String transRefCode) {
    this.transRefCode = transRefCode;
  }
  
  public byte getTransRefExp() {
    return transRefExp;
  }
  
  public void setTransRefExp(byte transRefExp) {
    this.transRefExp = transRefExp;
  }
  
  public String getTerRisk() {
    return terRisk;
  }
  
  public void setTerRisk(String terRisk) {
    this.terRisk = terRisk;
  }
  
  public String getEcTTLVal() {
    return ecTTLVal;
  }
  
  public void setEcTTLVal(String ecTTLVal) {
    this.ecTTLVal = ecTTLVal;
  }
  
  public String getCLOfflineLimit() {
    return ecTTLVal;
  }
  
  public void setCLOfflineLimit(String CLOfflineLimit) {
    this.CLOfflineLimit = CLOfflineLimit;
  }
  
  public String getCLTransLimit() {
    return CLTransLimit;
  }
  
  public void setCLTransLimit(String CLTransLimit) {
    this.CLTransLimit = CLTransLimit;
  }
  
  public String getCLCVMLimit() {
    return CLCVMLimit;
  }
  
  public void setCLCVMLimit(String CLCVMLimit) {
    this.CLCVMLimit = CLCVMLimit;
  }
}
