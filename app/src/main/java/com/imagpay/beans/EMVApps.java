package com.imagpay.beans;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import java.util.List;












@XStreamAlias("apps")
public class EMVApps
{
  @XStreamImplicit(itemFieldName="app")
  private List<EMVApp> app;
  
  public EMVApps() {}
  
  public List<EMVApp> getAid()
  {
    return app;
  }
  
  public void setAid(List<EMVApp> aid) {
    app = aid;
  }
}
