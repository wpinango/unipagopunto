package com.imagpay.beans;

import com.thoughtworks.xstream.annotations.XStreamAlias;



















@XStreamAlias("revoc")
public class EMVRevoc
{
  @XStreamAlias("rid")
  private String ucRID;
  @XStreamAlias("index")
  private String ucIndex;
  @XStreamAlias("sn")
  private String ucCertSn;
  
  public EMVRevoc() {}
  
  public EMVRevoc(String ucRID, String ucIndex, String ucCertSn)
  {
    this.ucRID = ucRID;
    this.ucIndex = ucIndex;
    this.ucCertSn = ucCertSn;
  }
  
  public String getUCRID() {
    return ucRID;
  }
  
  public void setUCRID(String ucRID) {
    this.ucRID = ucRID;
  }
  
  public String getUCIndex() {
    return ucIndex;
  }
  
  public void setUCIndex(String ucIndex) {
    this.ucIndex = ucIndex;
  }
  
  public String getUCCertSn() {
    return ucCertSn;
  }
  
  public void setUCCertSn(String ucCertSn) {
    this.ucCertSn = ucCertSn;
  }
}
