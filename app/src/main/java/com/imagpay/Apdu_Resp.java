package com.imagpay;


public class Apdu_Resp
{
  private short lenOut;
  
  private byte[] dataOut;
  
  private byte swA;
  
  private byte swB;
  
  public Apdu_Resp() {}
  
  public Apdu_Resp(short len, byte[] data, byte swA, byte swB)
  {
    lenOut = len;
    dataOut = data;
    this.swA = swA;
    this.swB = swB;
  }
  
  public short getLenOut() {
    return lenOut;
  }
  
  public void setLenOut(short lenOut) {
    this.lenOut = lenOut;
  }
  
  public byte[] getDataOut() {
    return dataOut;
  }
  
  public void setDataOut(byte[] dataOut) {
    this.dataOut = dataOut;
  }
  
  public byte getSWA() {
    return swA;
  }
  
  public void setSWA(byte swA) {
    this.swA = swA;
  }
  
  public byte getSWB() {
    return swB;
  }
  
  public void setSWB(byte swB) {
    this.swB = swB;
  }
}
