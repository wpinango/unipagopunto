package com.imagpay.ssc;

import android.util.Log;
import com.imagpay.Settings;
import com.imagpay.utils.StringUtils;
import com.ivsign.android.IDCReader.IDUtil;
import java.util.ArrayList;
import java.util.List;













public class SSCard
{
  private static String TAG = "SSCard";
  
  private Settings _settings;
  private List<Tlv> datas = new ArrayList();
  
  public SSCard(Settings settings) {
    _settings = settings;
  }
  




  public String ssReset()
  {
    return _settings.icReset();
  }
  


  public String getIdentificationCode()
  {
    return getTLVDataByTag("01");
  }
  


  public String getCategory()
  {
    return getTLVDataByTag("02");
  }
  


  public String getSpecVer()
  {
    return StringUtils.covertHexToASCII(getTLVDataByTag("03"));
  }
  


  public String getInitCode()
  {
    return getTLVDataByTag("04");
  }
  



  public String getIssueDate()
  {
    return getTLVDataByTag("05");
  }
  



  public String getExpDate()
  {
    return getTLVDataByTag("06");
  }
  


  public String getCardNo()
  {
    return StringUtils.covertHexToASCII(getTLVDataByTag("07"));
  }
  


  public String getSSNo()
  {
    return StringUtils.covertHexToASCII(getTLVDataByTag("08"));
  }
  


  public String getUserName()
  {
    return StringUtils.hexToStringGBK(getTLVDataByTag("09"));
  }
  


  public String getUserSex()
  {
    String tmp = getTLVDataByTag("0a");
    if (tmp != null) {
      if (tmp.equals("31"))
        return "男";
      if (tmp.equals("32")) {
        return "女";
      }
      return "其它";
    }
    return null;
  }
  


  public String getUserNation()
  {
    String tmp = getTLVDataByTag("0b");
    return IDUtil.decodeNation(Integer.parseInt(tmp, 16));
  }
  


  public String getUserBirthAddress()
  {
    return getTLVDataByTag("0c");
  }
  


  public String getUserBirthDay()
  {
    return getTLVDataByTag("0d");
  }
  





  public int ssReadProcess()
  {
    String res = _settings.getDataWithAPDU(SSCommands.getAppMF());
    if (!checkRes(res)) {
      return SSConstants.SSC_MF_ERR;
    }
    res = _settings.getDataWithAPDU(SSCommands.getAppDDF());
    if (!checkRes(res)) {
      return SSConstants.SSC_DDF_ERR;
    }
    res = _settings.getDataWithAPDU(SSCommands.getAppEF("EF05"));
    if (!checkRes(res))
      return SSConstants.SSC_EF05_ERR;
    res = _settings.getDataWithAPDU(SSCommands.readAllRecord());
    
    if (!checkRes(res))
      return SSConstants.SSC_EF05_RECORD;
    setTLVData(res.substring(4));
    
    res = _settings.getDataWithAPDU(SSCommands.getAppEF("EF06"));
    if (!checkRes(res))
      return SSConstants.SSC_EF06_ERR;
    res = _settings.getDataWithAPDU(SSCommands.readAllRecord());
    if (!checkRes(res))
      return SSConstants.SSC_EF06_RECORD;
    setTLVData(res.substring(4));
    return SSConstants.STATUS_OK;
  }
  
  private boolean checkRes(String res) {
    if ((res == null) || (!res.startsWith("00")) || (!res.endsWith("9000")))
      return false;
    return true;
  }
  





  public String getTLVDataByTag(String tag)
  {
    for (Tlv obj : datas) {
      if (obj.getTag().equals(tag))
        return obj.getValue();
    }
    return null;
  }
  
  private void setTLVData(String res)
  {
    try {
      String tmp = res.substring(0, res.length() - 4);
      List<Tlv> list = TlvUtils.builderTlvList(tmp);
      for (Tlv obj : list) {
        datas.add(obj);
      }
    } catch (Exception e) {
      Log.d(TAG, e.getMessage());
    }
  }
}
