package com.imagpay.ssc;





public class Tlv
{
  private String tag;
  


  private int length;
  


  private String value;
  



  public Tlv(String tag, int length, String value)
  {
    this.length = length;
    this.tag = tag;
    this.value = value;
  }
  
  public String getTag() {
    return tag;
  }
  
  public void setTag(String tag) {
    this.tag = tag;
  }
  
  public int getLength() {
    return length;
  }
  
  public void setLength(int length) {
    this.length = length;
  }
  
  public String getValue() {
    return value;
  }
  
  public void setValue(String value) {
    this.value = value;
  }
  
  public String toString()
  {
    return 
      "tag=[" + tag + "]," + "length=[" + length + "]," + "value=[" + value + "]";
  }
}
