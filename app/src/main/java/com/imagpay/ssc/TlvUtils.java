package com.imagpay.ssc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;


















public class TlvUtils
{
  public TlvUtils() {}
  
  public static List<Tlv> builderTlvList(String hexString)
  {
    List<Tlv> tlvs = new ArrayList();
    
    int position = 0;
    while (position != StringUtils.length(hexString)) {
      String _hexTag = getTag(hexString, position);
      position += _hexTag.length();
      
      LPosition l_position = getLengthAndPosition(hexString, position);
      int _vl = l_position.get_vL();
      
      position = l_position.get_position();
      
      String _value = StringUtils.substring(hexString, position, position + 
        _vl * 2);
      
      position += _value.length();
      
      tlvs.add(new Tlv(_hexTag, _vl, _value));
    }
    return tlvs;
  }
  






  public static Map<String, Tlv> builderTlvMap(String hexString)
  {
    Map<String, Tlv> tlvs = new HashMap();
    
    int position = 0;
    while (position != hexString.length()) {
      String _hexTag = getTag(hexString, position);
      
      position += _hexTag.length();
      
      LPosition l_position = getLengthAndPosition(hexString, position);
      
      int _vl = l_position.get_vL();
      position = l_position.get_position();
      String _value = hexString.substring(position, position + _vl * 2);
      position += _value.length();
      
      tlvs.put(_hexTag, new Tlv(_hexTag, _vl, _value));
    }
    return tlvs;
  }
  






  private static LPosition getLengthAndPosition(String hexString, int position)
  {
    String firstByteString = hexString.substring(position, position + 2);
    int i = Integer.parseInt(firstByteString, 16);
    String hexLength = "";
    
    if ((i >>> 7 & 0x1) == 0) {
      hexLength = hexString.substring(position, position + 2);
      position += 2;
    }
    else {
      int _L_Len = i & 0x7F;
      position += 2;
      hexLength = hexString.substring(position, position + _L_Len * 2);
      
      position += _L_Len * 2;
    }
    return new LPosition(Integer.parseInt(hexLength, 16), position);
  }
  







  private static String getTag(String hexString, int position)
  {
    String firstByte = StringUtils.substring(hexString, position, 
      position + 2);
    int i = Integer.parseInt(firstByte, 16);
    if ((i & 0x1F) == 31) {
      return hexString.substring(position, position + 4);
    }
    
    return hexString.substring(position, position + 2);
  }
}
