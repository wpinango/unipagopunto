package com.imagpay.ssc;

import com.imagpay.Apdu_Send;
import com.imagpay.utils.StringUtils;





public class SSCommands
  extends SSConstants
{
  private static Apdu_Send aSend;
  
  public SSCommands() {}
  
  public static Apdu_Send getAppMF()
  {
    aSend = new Apdu_Send();
    aSend.setCommand(StringUtils.convertHexToBytes("00A40000"));
    aSend.setLC((short)2);
    aSend.setDataIn(StringUtils.convertHexToBytes("3F00"));
    return aSend;
  }
  
  public static Apdu_Send getAppDDF() {
    aSend = new Apdu_Send();
    aSend.setCommand(StringUtils.convertHexToBytes("00A40000"));
    aSend.setLC((short)2);
    aSend.setDataIn(StringUtils.convertHexToBytes("DDF1"));
    return aSend;
  }
  
  public static Apdu_Send getAppEF(String fileName) {
    aSend = new Apdu_Send();
    aSend.setCommand(StringUtils.convertHexToBytes("00A40000"));
    aSend.setLC((short)2);
    aSend.setDataIn(StringUtils.convertHexToBytes(fileName));
    return aSend;
  }
  
  public static Apdu_Send getCardUserNo()
  {
    return readRecord(SSCNO, 14);
  }
  
  public static Apdu_Send getUserName()
  {
    return readRecord(USERNAME, 20);
  }
  
  protected Apdu_Send getCardType()
  {
    return readRecord(TYPE, 3);
  }
  
  public static Apdu_Send getCardIssuingDate()
  {
    return readRecord(ISSUEDATE, 4);
  }
  
  public static Apdu_Send getCardNo()
  {
    return readRecord(CARDNO, 9);
  }
  
  private static Apdu_Send readRecord(String fileName, int le)
  {
    aSend = new Apdu_Send();
    aSend.setCommand(
      StringUtils.convertHexToBytes("00B2" + fileName + "04"));
    aSend.setLE((short)le);
    return aSend;
  }
  
  public static Apdu_Send readAllRecord()
  {
    aSend = new Apdu_Send();
    aSend.setCommand(StringUtils.convertHexToBytes("00B20105"));
    return aSend;
  }
}
