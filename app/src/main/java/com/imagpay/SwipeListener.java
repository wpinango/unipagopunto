package com.imagpay;

import com.imagpay.enums.CardDetected;
import com.imagpay.enums.EmvStatus;
import com.imagpay.enums.PrintStatus;

public abstract interface SwipeListener
{
  public abstract void onDisconnected(SwipeEvent paramSwipeEvent);
  
  public abstract void onConnected(SwipeEvent paramSwipeEvent);
  
  public abstract void onStarted(SwipeEvent paramSwipeEvent);
  
  public abstract void onStopped(SwipeEvent paramSwipeEvent);
  
  public abstract void onReadData(SwipeEvent paramSwipeEvent);
  
  public abstract void onParseData(SwipeEvent paramSwipeEvent);
  
  public abstract void onPermission(SwipeEvent paramSwipeEvent);
  
  public abstract void onCardDetect(CardDetected paramCardDetected);
  
  public abstract void onPrintStatus(PrintStatus paramPrintStatus);
  
  public abstract void onEmvStatus(EmvStatus paramEmvStatus);

}
