package com.imagpay.enums;

public enum PrintStatus
{
  ENTER,  EXIT,  IMAGES,  NO_PAPER,  LACK_PAPER;
}
