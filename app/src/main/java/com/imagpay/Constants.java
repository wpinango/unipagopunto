package com.imagpay;










public final class Constants
{
  public static String SDK_VERSION = "POS2.7.0.180129_R";
  


  public static String POS_Z90 = "Z90";
  


  public static String POS_Z91 = "Z91";
  
  public static String ERROR_TIMEOUT = "ff 3f 00 00 00 00 00 00";
  public static String ERROR_COMMAND = "33 3f 00 00 00 00 00 00";
  public static String ERROR_DATA = "32 3f 00 00 00 00 00 00";
  
  public static String SYS_VER = "0c01";
  public static String SYS_RSN = "0c02";
  public static String SYS_BEEP = "0c03";
  public static String SYS_WSN = "0c04";
  public static String SYS_PLAIN = "0c05";
  public static String SYS_3DES = "0c06";
  public static String SYS_3DES_KEY = "0c18";
  public static String SYS_BLE_NAME = "0c21";
  public static String SYS_BLE_PASS = "0c22";
  public static String SYS_R_TIME = "0c23";
  public static String SYS_W_TIME = "0c24";
  
  public static String SYS_MAG_DEBUG = "0c00";
  public static String SYS_SHAKE = "0901";
  
  public static String MAG_OPEN = "0201";
  public static String MAG_CLOSE = "0202";
  public static String MAG_SWIPE = "0203";
  public static String MAG_READ = "0204";
  public static String MAG_RESET = "0205";
  
  public static String IC_RESET = "0101";
  public static String IC_CLOSE = "0102";
  public static String IC_APDU = "0104";
  public static String IC_APDU_TW = "0154";
  public static String IC_DETECT = "0105";
  
  public static String M1_REQUEST = "0D01";
  public static String M1_SELECT = "0D02";
  public static String M1_AUTH = "0D03";
  public static String M1_READ_BLOCK = "0D04";
  public static String M1_WRITE_BLOCK = "0D05";
  public static String M1_HALT = "0D07";
  public static String M1_READ_SEC = "0D0E";
  public static String M1_WRITE_SEC = "0D0F";
  public static String M1_WRITE_PASS = "0D10";
  public static String M1_PLUS_AMNT = "0D0601";
  public static String M1_MINUS_AMNT = "0D0602";
  
  public static String ID_RESET = "100101";
  public static String ID_READ = "100201";
  public static String ID_OFF = "100301";
  

  public static String NEWID_OPEN = "1000";
  public static String NEWID_RESET = "1001";
  public static String NEWID_READ = "1002";
  public static String NEWID_CLOSE = "1003";
  public static String NEWID_OFF = "1004";
  
  public static String PRINT_INIT = "0401";
  public static String PRINT_FONT = "0402";
  public static String PRINT_SPACE = "0403";
  public static String PRINT_LEFT = "0404";
  public static String PRINT_STEP = "0405";
  public static String PRINT_LOGO = "0407";
  public static String PRINT_STR = "0408";
  public static String PRINT_STATUS = "0409";
  
  public static String PPINPUT = "0507";
  public static String PPGETPWD = "0508";
  public static String PPWRITEPKEY = "0500";
  public static String PPWRITEEKEY = "0501";
  public static String PPDELKEY = "0502";
  public static String PPDUKPT = "050b";
  public static String PPKSN = "050c";
  public static String PPGETKEY = "0506";
  public static String PPCLOSE = "0509";
  public static String PPEINPUT = "0505";
  
  public static String PPMKEY = "0520";
  public static String PPSIGN = "0521";
  public static String PPMAC = "0523";
  public static String PPMLOGIN = "0524";
  public static String PPMWPASS = "0525";
  public static String PPMADDU = "0528";
  public static String PPMDELU = "0529";
  public static String PPULOGIN = "0526";
  public static String PPUWPASS = "0527";
  public static String PPSIGNSTATUS = "052a";
  public static String PPEXIT = "0522";
  public static String PPLOAD = "052b";
  

  public static String LCD_CLEAR = "2001";
  public static String LCD_LIGHT = "2002";
  public static String LCD_SHOW = "2003";
  
  public static String DF_SELECT = "0e01";
  public static String DF_RESET = "0e07";
  public static String DF_PSE = "0e03";
  public static String DF_CREAT_AID = "0e05";
  public static String DF_MODIFY_KEY = "0e08";
  public static String DF_ADD_STD = "0e09";
  public static String DF_ADD_VALUE = "0e0a";
  public static String DF_ADD_BACKUP = "0e17";
  public static String DF_ADD_LINEAR = "0e18";
  public static String DF_ADD_CYCLIC = "0e19";
  public static String DF_DEL_FILE = "0e1a";
  public static String DF_W_STD = "0e0b";
  public static String DF_PLUS_VALUE = "0e1c";
  public static String DF_MINUS_VALUE = "0e1d";
  public static String DF_R_VALUE = "0e1b";
  public static String DF_R_STD = "0e0c";
  public static String DF_W_RECORD = "0e22";
  public static String DF_R_RECORD = "0e23";
  public static String DF_R_KEYCONF = "0e0e";
  public static String DF_R_FILECONF = "0e15";
  public static String DF_W_FILECONF = "0e16";
  public static String DF_DEL_APP = "0e10";
  public static String DF_F_PICC = "0e12";
  public static String DF_R_KEYVER = "0e0f";
  public static String DF_R_AIDS = "0e11";
  public static String DF_R_INFO = "0e13";
  public static String DF_R_FILEIDS = "0e14";
  
  public static String FLICA_REQUEST = "0f01";
  public static String FLICA_CMDS = "0f02";
  
  public static String TB_REQUEST = "1101";
  public static String TB_ATTRIB = "1102";
  public static String TB_APDU = "1105";
  public static String TB_UID = "1103";
  public static String TB_HALT = "1104";
  
  public static String INIT_4442 = "0119";
  public static String SRD_4442 = "0120";
  public static String SWR_4442 = "0121";
  public static String CSC_4442 = "0124";
  public static String RSC_4442 = "0125";
  public static String WSC_4442 = "0126";
  public static String RSTC_4442 = "0127";
  public static String PWR_4442 = "0122";
  public static String PRD_4442 = "0123";
  
  public static String INIT_4428 = "0133";
  public static String SRD_4428 = "012B";
  public static String SWR_4428 = "012A";
  public static String CSC_4428 = "012F";
  public static String RSC_4428 = "0131";
  public static String WSC_4428 = "0130";
  public static String RSTC_4428 = "0132";
  public static String PWR_4428 = "012E";
  public static String PRD_4428 = "012C";
  
  public static String AT24_RESET = "0152";
  public static String AT24_READ = "0151";
  public static String AT24_WRITE = "0150";
  
  public static String WORK_MODE = "2301";
  public static String LOCK_MODE = "2302";
  public static String WRITE_DUKPT = "2303";
  public static String WRITE_DES = "2304";
  public static String WRITE_SN = "2305";
  public static String READ_SN = "2306";
  public static String DES_UTIL = "2307";
  public static String GET_CUR_SN = "2308";
  public static String DUKPT_UTIL = "2309";
  

  public static String T105_TMK = "0601";
  public static String T105_SINGIN = "0602";
  public static String T105_GETPIN = "0603";
  public static String T105_GETMAC = "0604";
  public static String T105_ICCOUNT = "0605";
  


  public static int PED_OK = 0;
  
  public static int PED_ERR_NO_KEY = 1;
  
  public static int PED_ERR_KEYIDX_ERR = 2;
  
  public static int PED_ERR_DERIVE_ERR = 3;
  
  public static int PED_ERR_CHECK_KEY_FAIL = 4;
  
  public static int PED_ERR_NO_PIN_INPUT = 5;
  
  public static int PED_ERR_INPUT_CANCEL = 6;
  
  public static int PED_ERR_WAIT_INTERVAL = 7;
  
  public static int PED_ERR_CHECK_MODE_ERR = 8;
  
  public static int PED_ERR_NO_RIGHT_USE = 9;
  
  public static int PED_ERR_KEY_TYPE_ERR = 10;
  
  public static int PED_ERR_EXPLEN_ERR = 11;
  
  public static int PED_ERR_DSTKEY_IDX_ERR = 12;
  


  public static int PED_ERR_SRCKEY_IDX_ERR = 13;
  
  public static int PED_ERR_KEY_LEN_ERR = 14;
  
  public static int PED_ERR_INPUT_TIMEOUT = 15;
  
  public static int PED_ERR_NO_ICC = 16;
  
  public static int PED_ERR_ICC_NO_INIT = 17;
  
  public static int PED_ERR_GROUP_IDX_ERR = 18;
  
  public static int PED_ERR_PARAM_PTR_NULL = 19;
  
  public static int PED_ERR_TAMPERED = 20;
  
  public static int PED_ERROR = 21;
  
  public static int PED_ERR_NOMORE_BUF = 22;
  
  public static int PED_ERR_NEED_ADMIN = 23;
  
  public static int PED_ERR_DUKPT_OVERFLOW = 24;
  
  public static int PED_ERR_KCV_CHECK_FAIL = 25;
  
  public static int PED_ERR_SRCKEY_TYPE_ERR = 26;
  
  public static int PED_ERR_UNSPT_CMD = 27;
  
  public static int PED_ERR_COMM_ERR = 28;
  
  public static int PED_ERR_NO_UAPUK = 29;
  
  public static int PED_ERR_ADMIN_ERR = 30;
  
  public static int PED_ERR_DOWNLOAD_INACTIVE = 31;
  
  public static int PED_ERR_KCV_ODD_CHECK_FAIL = 32;
  
  public static int PED_ERR_PED_DATA_RW_FAIL = 33;
  
  public static int PED_ERR_ICC_CMD_ERR = 34;
  
  public static int PED_ERR_KEY_VALUE_INVALID = 35;
  
  public static int PED_ERR_KEY_VALUE_EXIST = 36;
  
  public static int PED_ERR_UART_PARAM_INVALID = 37;
  
  public static int PED_ERR_KEY_INDEX_NOT_SELECT_OR_NOT_MATCH = 38;
  
  public static int PED_RET_ERR_INPUT_CLEAR = 39;
  public static int PED_ERR_LOAD_TRK_FAIL = 40;
  public static int PED_ERR_TRK_VERIFY_FAIL = 41;
  public static int PED_ERR_MSR_STATUS_INVALID = 42;
  public static int PED_ERR_NO_FREE_FLASH = 43;
  
  public static int PED_ERR_DUKPT_NEED_INC_KSN = 44;
  
  public static int PED_ERR_KCV_MODE_ERR = 45;
  
  public static int PED_ERR_DUKPT_NO_KCV = 46;
  
  public static int PED_ERR_PIN_BYPASS_BYFUNKEY = 47;
  
  public static int PED_ERR_MAC_ERR = 48;
  
  public static int PED_ERR_CRC_ERR = 49;
  
  public static int PED_ERR_PWD_FAIL = 50;
  
  public static int PED_ERR_FORMAT = 51;
  
  public static int PED_ADMIN_PWD_NEED_MODIFY = 52;
  
  public static int PED_SEK = 0;
  public static int PED_TLK = 1;
  public static int PED_TMK = 2;
  public static int PED_TPK = 3;
  public static int PED_TAK = 4;
  public static int PED_TDK = 5;
  public static int PED_TEK = 6;
  public static int PED_TIK = 7;
  public static int PED_TRK = 8;
  public static int PED_FUTUREKEY = 9;
  public static int PED_DUKPT = 10;
  public static int TYPE_DUKPT_OVERLOW_KEY = 11;
  public static int PED_HEADER = 16;
  public static int PED_TWK = 32;
  public static int PED_KEY_INVALID = 170;
  
  public static String T_RANDOM = "0C20";
  public static String T_VERIFY = "0C21";
  


  public static String POS_PRINT = "0C02";
  public static String PRINT_EXIT = "1C2F";
  
  public Constants() {}
}
