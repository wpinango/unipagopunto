package com.imagpay;

import android.content.Context;
import com.imagpay.emv.EMVParam;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;






public class SwipeHandler
{
  private List<SwipeListener> _listeners = new ArrayList();
  private boolean connected = false;
  
  private int _retryCount = 0;
  private int _timeout = 5;
  private String _lastResponse = null;
  private boolean isPrn = false;
  private boolean isExit = false;
  private boolean showFlag = false;
  private Context _context;
  
  public SwipeHandler(Context context) {
    _context = context;
  }
  





  public void addSwipeListener(SwipeListener listener)
  {
    _listeners.add(listener);
  }
  





  public void removeSwipeListener(SwipeListener listener)
  {
    _listeners.remove(listener);
  }
  





  public int getRetryCount()
  {
    return _retryCount;
  }
  




  public void setRetryCount(int retryCount)
  {
    _retryCount = retryCount;
  }
  





  public int getTimeout()
  {
    return _timeout;
  }
  




  public void setTimeout(int timeout)
  {
    _timeout = timeout;
  }
  
  public boolean isPrn() {
    return isPrn;
  }
  
  public void setPrn(boolean isPrn) {
    this.isPrn = isPrn;
  }
  
  public boolean isExit() {
    return isExit;
  }
  
  public void setExit(boolean isExit) {
    this.isExit = isExit;
  }
  
  public void setShowLog(boolean log) {
    showFlag = log;
  }
  
  public boolean isShowLog() {
    return showFlag;
  }
  






  public boolean isConnected()
  {
    return connected;
  }
  
  private void onDisconnected() {
    connected = false;
    for (SwipeListener listener : _listeners) {
      SwipeEvent event = new SwipeEvent(this, 
        SwipeEvent.TYPE_DISCONNECTED, "Device is disconnected!");
      listener.onDisconnected(event);
    }
  }
  
  private void onConnected() {
    if (connected)
      return;
    connected = true;
    for (SwipeListener listener : _listeners) {
      SwipeEvent event = new SwipeEvent(this, SwipeEvent.TYPE_CONNECTED, 
        "Device is connected!");
      listener.onConnected(event);
    }
  }
  
  protected void onParseData(String data) {
    _lastResponse = data;
    for (SwipeListener listener : _listeners) {
      SwipeEvent event = new SwipeEvent(this, SwipeEvent.TYPE_PARSEDATA, 
        data);
      listener.onParseData(event);
    }
  }
  



  public void writeCipherCode(String cipherCode) {}
  



  public void writeCipherCode(String cipherCode, String type) {}
  



  public String getDataWithCipherCode(String cipherCode)
  {
    int time = 0;
    int retryCount = _retryCount + 1;
    while (retryCount > 0) {
      time = 0;
      _lastResponse = null;
      writeCipherCode(cipherCode);
      System.out.println("ValorePalu"+cipherCode);
      long tt = System.currentTimeMillis();
      while ((_lastResponse == null) && (cipherCode != null) && (
        time < _timeout * 1000)) {
        try {
          Thread.sleep(100L);
        }
        catch (InterruptedException localInterruptedException) {}
        time += 100;
      }
      if (_lastResponse != null)
        return _lastResponse;
      System.out.println("Data: " + cipherCode + "=>" + _lastResponse + 
        " Time:" + (System.currentTimeMillis() - tt));
      try {
        Thread.sleep(100L);
      }
      catch (InterruptedException localInterruptedException1) {}
      retryCount--;
    }
    return _lastResponse;
  }
  
  public String getDataWithCipherCode(String cipherCode, String type) {
    int time = 0;
    int retryCount = _retryCount + 1;
    while (retryCount > 0) {
      time = 0;
      _lastResponse = null;
      writeCipherCode(cipherCode, type);
      long tt = System.currentTimeMillis();
      while ((_lastResponse == null) && (cipherCode != null) && (
        time < _timeout * 1000)) {
        try {
          Thread.sleep(100L);
        }
        catch (InterruptedException localInterruptedException) {}
        time += 100;
      }
      if (_lastResponse != null)
        return _lastResponse;
      System.out.println("Data: " + cipherCode + "=>" + _lastResponse + 
        " Time:" + (System.currentTimeMillis() - tt));
      try {
        Thread.sleep(100L);
      }
      catch (InterruptedException localInterruptedException1) {}
      retryCount--;
    }
    return _lastResponse;
  }
  




  public void close()
  {
    onDisconnected();
  }
  
  private String getHexString(int i) {
    String tmp = Integer.toHexString(i);
    if (tmp.length() == 1) {
      return "0" + tmp;
    }
    return tmp;
  }
  

  public void kernelInit(EMVParam param) {}
  

  public String icReset()
  {
    return null;
  }
  
  public int process() {
    return -1;
  }
  

  public void icOff() {}
  
  public String getTLVDataByTag(int tag)
  {
    return null;
  }
  
  public String getPosModel() {
    return "Z91";
  }
}
