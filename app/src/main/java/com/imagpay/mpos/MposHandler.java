package com.imagpay.mpos;

import android.content.Context;
import android.content.SharedPreferences;

import com.imagpay.Constants;
import com.imagpay.SwipeEvent;
import com.imagpay.SwipeListener;
import com.imagpay.emv.EMVHandler;
import com.imagpay.enums.CardDetected;
import com.imagpay.enums.EmvStatus;
import com.imagpay.enums.PosModel;
import com.imagpay.enums.PrintStatus;
import com.imagpay.serialport.SerialPort;
import com.imagpay.utils.StringUtils;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MposHandler
        extends EMVHandler
{
  private static final String TAG = "TTLHandler";
  private SerialPort serialPort;
  private SerialReader reader;
  private SerialWriter writer;
  private List<SwipeListener> listeners = new ArrayList();

  private int retryCount = 1;
  private int timeout = 3;
  private int cmdTime = 3;
  private String lastResponse = null;
  private boolean connected = false;
  private String devName;
  private int baudrate;
  private boolean isPrn = false;
  private boolean isExit = false;
  private boolean showFlag = false;
  private String prnFlag = "";

  private SharedPreferences _preferences = null;
  private String LAST = "lastResponse";
  private String IC_IN = "49";
  private String IC_OUT = "4f";
  private String MAG_SWIPE = "4d";

  private String PRINT_EXIT = "45584954205052494e54";

  private String PRINT_IMAG = "4249544d4150204f4b";

  private String PRINT_LACK_PAPER = "4e4f205041504552";

  private String EMV_OK = "49434f4b";

  private String EMV_ERR = "4943455252";

  private static MposHandler instance = null;

  private boolean isSending = false;
  private String _track1 = "";
  private String _track2 = "";
  private String _track3 = "";
  private String _pan = "";
  private String _exp = "";
  private String _serviceCode = "";
  private String _trackRandom = "";
  private boolean isReadMag = false;
  private String posModel = "Z91";

  private boolean _checkState;
  private boolean _modifyState;
  private String cardHolder;
  public static String MPOS_PLAIN_STATE = "Mpos_Plain_state";
  public static String MPOS_DUKPT_STATE = "Mpos_dukpt_state";
  public static String MPOS_BPOS_STATE = "Mpos_BPos_state";
  public static String MPOS_UNIONPAY_STATE = "Mpos_UnionPay_state";



  public MposHandler(Context context)
  {
    super(context);
    _preferences = context.getSharedPreferences("TTLHandler", 0);
    prnFlag = StringUtils.convertBytesToHex("ESC".getBytes());
    devName = "/dev/ttyHSL1";
    baudrate = 115200;
  }



  public MposHandler(Context context, PosModel model)
  {
    super(context);
    _preferences = context.getSharedPreferences("TTLHandler", 0);
    prnFlag = StringUtils.convertBytesToHex("ESC".getBytes());
    if (model.equals(Constants.POS_Z91)) {
      devName = "/dev/ttyHSL1";
    } else {
      devName = "/dev/ttyS2";
    }
    posModel = model.toString();
    baudrate = 115200;
  }






  public static MposHandler getInstance(Context _context, PosModel model)
  {
    if (instance == null) {
      synchronized (MposHandler.class) {
        if (instance == null) {
          instance = new MposHandler(_context, model);
        }
      }
    }
    return instance;
  }






  public static MposHandler getInstance(Context _context)
  {
    if (instance == null) {
      synchronized (MposHandler.class) {
        if (instance == null) {
          instance = new MposHandler(_context);
        }
      }
    }
    return instance;
  }






  public void addSwipeListener(SwipeListener listener)
  {
    listeners.add(listener);
  }






  public void removeSwipeListener(SwipeListener listener)
  {
    listeners.remove(listener);
  }

  public int getRetryCount() {
    return retryCount;
  }

  public void setRetryCount(int retryCount) {
    this.retryCount = retryCount;
  }

  public int getTimeout() {
    return timeout;
  }

  public void setTimeout(int timeout) {
    this.timeout = timeout;
  }

  public int getCmdTime() {
    return cmdTime;
  }

  public void setCmdTime(int cmdTime) {
    this.cmdTime = cmdTime;
  }

  public String getLastResponse() {
    return lastResponse;
  }

  public void setLastResponse(String lastResponse) {
    this.lastResponse = lastResponse;
  }

  public boolean isConnected() {
    return connected;
  }

  public void setConnected(boolean connected) {
    this.connected = connected;
  }

  public boolean isPrn() {
    return isPrn;
  }

  public void setPrn(boolean isPrn) {
    this.isPrn = isPrn;
  }

  public boolean isExit() {
    return isExit;
  }

  public void setExit(boolean isExit) {
    this.isExit = isExit;
  }

  public void setShowLog(boolean log) {
    showFlag = log;
  }

  public boolean isShowLog() {
    return showFlag;
  }

  public String getPosModel() {
    return posModel;
  }

  public void setPosModel(String posModel) {
    this.posModel = posModel;
  }








  public void setParameters(String devName, int baudrate)
  {
    this.devName = devName;
    this.baudrate = baudrate;
  }




  public synchronized boolean connect()
  {
    try
    {
      serialPort = new SerialPort(new File(devName), baudrate, 0);
      reader = new SerialReader(this, serialPort.getInputStream());
      reader.start();
      writer = new SerialWriter(this, serialPort.getOutputStream());
      writer.start();
      new Thread(new Runnable()
      {
        public void run()
        {
          try
          {
            Thread.sleep(100L);
            MposHandler.this.onConnected();
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
        }
      })











              .start();
    } catch (Exception e) {
      throw new IllegalArgumentException("TTL Connection Failed!", e);
    }
    return true;
  }

  private void onConnected() {
    if (isConnected())
      return;
    setConnected(true);
    for (SwipeListener listener : listeners) {
      SwipeEvent event = new SwipeEvent(this, SwipeEvent.TYPE_CONNECTED,
              "Device is connected!");
      listener.onConnected(event);
    }
  }

  private void onCardDected(CardDetected status)
  {
    for (SwipeListener listener : listeners) {
      listener.onCardDetect(status);
    }
  }

  public boolean isPowerOn() {
    return isConnected();
  }

  private void onDisconnected() {
    setConnected(false);
    for (SwipeListener listener : listeners) {
      SwipeEvent event = new SwipeEvent(this,
              SwipeEvent.TYPE_DISCONNECTED, "Device is disconnected!");
      listener.onDisconnected(event);
    }
  }

  public void onParseData(String data)
  {
    if (data.equals(prnFlag)) {
      setExit(true);
    }
    setLast(data);
    if (!isConnected())
      return;
    if (data.equals(IC_IN)) {
      for (SwipeListener listener : listeners) {
        listener.onCardDetect(CardDetected.INSERTED);
      }
      return; }
    if (data.equals(IC_OUT)) {
      if (isRunning()) {
        cancel();
      }
      for (SwipeListener listener : listeners) {
        listener.onCardDetect(CardDetected.REMOVED);
      }
      return;
    }

    if (data.equals(MAG_SWIPE)) {
      for (SwipeListener listener : listeners) {
        listener.onCardDetect(CardDetected.SWIPED);
      }
    } else if (data.endsWith(PRINT_EXIT)) {
      for (SwipeListener listener : listeners) {
        listener.onPrintStatus(PrintStatus.EXIT);
      }
    } else if (data.endsWith(PRINT_IMAG)) {
      for (SwipeListener listener : listeners) {
        listener.onPrintStatus(PrintStatus.IMAGES);
      }
    } else if (data.equals(PRINT_LACK_PAPER)) {
      for (SwipeListener listener : listeners) {
        listener.onPrintStatus(PrintStatus.LACK_PAPER);
      }
    } else if (isReadMag) {
      checkValueForMag(data);
    } else if (data.endsWith(EMV_OK)) {
      for (SwipeListener listener : listeners) {
        listener.onEmvStatus(EmvStatus.OK);
      }
    } else if (data.endsWith(EMV_ERR)) {
      for (SwipeListener listener : listeners) {
        listener.onEmvStatus(EmvStatus.ERR);
      }
    } else {
      for (SwipeListener listener : listeners) {
        SwipeEvent event = new SwipeEvent(this,
                SwipeEvent.TYPE_PARSEDATA, data);
        listener.onParseData(event);
      }
    }
  }






  public void writeCipherCode(String cipherCode)
  {
    if ((isConnected()) && (writer != null)) {
      if (!isPrn()) {
        String cmd = cipherCode.replaceAll(" ", "");
        int len = cmd.length();

        StringBuffer sb = new StringBuffer();
        sb.append("02");
        cmd =
                StringUtils.convertBytesToHex(new byte[] { (byte)(len / 2 / 256) }) +

                        StringUtils.convertBytesToHex(new byte[] { (byte)(len / 2 % 256) }) +
                        cmd;
        sb.append(cmd);

        int lrc = 0;
        for (int i = 0; i < len + 4; i += 2) {
          if (i == 0) {
            lrc = Integer.parseInt(cmd.substring(i, i + 2), 16);
          }
          else
            lrc = lrc ^ Integer.parseInt(cmd.substring(i, i + 2), 16);
        }
        String strLrc = Integer.toHexString(lrc);
        if (strLrc.length() != 1) {
          writer.send(sb.toString() + Integer.toHexString(lrc));
        } else {
          writer.send(sb.toString() + "0" + Integer.toHexString(lrc));
        }
      } else {
        writer.send(cipherCode.replaceAll(" ", ""));
      }
    }
  }

  public void write(String cipherCode) {
    if ((isConnected()) && (writer != null)) {
      String cmd = cipherCode.replaceAll(" ", "");
      writer.send(cmd);
    }
  }



  public void initNfcThread(){
    //singletonThread = SingletonThread.getInstance(this);

/* nfcThread = new Thread(() -> {
          while (threadStatus) {
              try {
                  writeCipherCode("010105");
                  Thread.sleep(1000);
              } catch (InterruptedException e) {
                  e.printStackTrace();
                  return;
              }
          }
      });*/

  }

    public void setNfcThread(boolean status) {


/*threadStatus = status;
      if (status && !nfcThread.isAlive()) {
        nfcThread.start();
      } else {
        if (nfcThread != null) {
          nfcThread.interrupt();
        }
      }*/


    }


  public String getDataWithCipherCode(String cipherCode)
  {
    if (isSending)
      return null;
    isSending = true;
    int time = 0;
    int rc = retryCount + 1;
    while (rc > 0) {
      time = 0;
      if (!isConnected()) {
        isSending = false;
        return null;
      }
      try {
        Thread.sleep(10L);
      }
      catch (InterruptedException localInterruptedException) {}


      setLast(null);
      if (cipherCode.equals("0204")) {
        isReadMag = true;
      } else {
        isReadMag = false;
      }
      writeCipherCode(cipherCode);
      if (isPrn) {
        isSending = false;
        return null;
      }

      while ((isConnected()) && (getLast() == null) && (time < timeout * 1000)) {
        try {
          Thread.sleep(cmdTime);
        }
        catch (InterruptedException localInterruptedException1) {}
        time += cmdTime;
      }
      if (getLast() != null) {
        isSending = false;
        return getLast();
      }
      rc--;
    }
    isSending = false;
    return getLast();
  }




  public synchronized void close()
  {
    if (isRunning()) {
      cancel();
    }
    resetKernel();
    try {
      if (reader != null) {
        reader.stop();
      }
    }
    catch (Exception localException) {}
    reader = null;
    try {
      if (writer != null) {
        writer.stop();
      }
    }
    catch (Exception localException1) {}
    writer = null;
    try {
      if (serialPort != null) {
        serialPort.close();
        serialPort = null;
      }
    }
    catch (Exception localException2) {}

    instance = null;
    onDisconnected();
  }

  public void onDestroy() {
    close();
  }

  public void setLast(String lastRes) {
    SharedPreferences.Editor editor = _preferences.edit();
    editor.putString(LAST, lastRes);
    editor.commit();
  }

  public String getLast() {
    return _preferences.getString(LAST, null);
  }



  private void checkValueForMag(String data)
  {
    resetData();
    try {
      if (data != null) {
        if (data.endsWith("4d414745")) {
          String state = data.substring(0, 2);
          _trackRandom = data.substring(2, 18);
          _track2 = data.substring(18, 66);
          if ((state.equals("06")) || (state.equals("07"))) {
            _track3 = data.substring(66, 178);
            int panLan = Integer.parseInt(data.substring(178, 180),
                    16);
            _pan = StringUtils.covertHexToASCII(data.substring(180,
                    180 + panLan * 2));
            _serviceCode = StringUtils.covertHexToASCII(data
                    .substring(180 + panLan * 2,
                            180 + panLan * 2 + 6));
            _exp = StringUtils.covertHexToASCII(data
                    .substring(180 + panLan * 2 + 6 + 2, 180 +
                            panLan * 2 + 6 + 10));
            icchip();
          } else if ((state.equals("03")) || (state.equals("02"))) {
            int panLan = Integer.parseInt(data.substring(66, 68),
                    16);
            _pan = StringUtils.covertHexToASCII(data.substring(68,
                    68 + panLan * 2));
            _serviceCode =
                    StringUtils.covertHexToASCII(data.substring(
                            68 + panLan * 2, 68 + panLan * 2 + 6));
            _exp =
                    StringUtils.covertHexToASCII(data.substring(68 + panLan *
                            2 + 6 + 2, 68 + panLan * 2 + 6 + 10));
            icchip();
          }
        } else if (data.endsWith("4d414752")) {
          String stata = data.substring(0, 2);
          if ((stata.equals("06")) || (stata.equals("07"))) {
            int track2len = Integer.parseInt(
                    data.substring(2, 4), 16);
            _track2 = data.substring(4, 4 + track2len);
            int track3len = 0;
            if (track2len % 2 == 0) {
              track3len = Integer.parseInt(data.substring(
                      4 + track2len, 4 + track2len + 2), 16);
              _track3 = data.substring(4 + track2len + 2, 4 +
                      track2len + 2 + track3len);
              int panlen = 0;
              if (track3len % 2 == 0) {
                panlen = Integer.parseInt(
                        data.substring(4 + track2len + 2 +
                                track3len, 4 + track2len + 2 +
                                track3len + 2), 16);
                _pan = StringUtils.covertHexToASCII(data
                        .substring(4 + track2len + 2 +
                                track3len + 2, 4 + track2len +
                                2 + track3len + 2 + panlen *
                                2));
                _serviceCode =
                        StringUtils.covertHexToASCII(data.substring(4 +
                                track2len + 2 + track3len + 2 +
                                panlen * 2, 4 + track2len + 2 +
                                track3len + 2 + panlen * 2 +
                                6));
                _exp = StringUtils.covertHexToASCII(data
                        .substring(4 + track2len + 2 +
                                track3len + 2 + panlen * 2 +
                                6 + 2, 4 + track2len + 2 +
                                track3len + 2 + panlen * 2 +
                                6 + 2 + 8));
                icchip();
              } else {
                panlen = Integer.parseInt(
                        data.substring(4 + track2len + 2 +
                                track3len + 1, 4 + track2len +
                                2 + track3len + 2 + 1), 16);
                _pan = StringUtils.covertHexToASCII(data
                        .substring(4 + track2len + 2 +
                                track3len + 2 + 1, 4 +
                                track2len + 2 + track3len + 2 +
                                panlen * 2 + 1));
                _serviceCode =
                        StringUtils.covertHexToASCII(data.substring(4 +
                                track2len + 2 + track3len + 2 +
                                1 + panlen * 2, 4 + track2len +
                                2 + track3len + 2 + 1 +
                                panlen * 2 + 6));
                _exp = StringUtils.covertHexToASCII(data
                        .substring(4 + track2len + 2 +
                                track3len + 2 + panlen * 2 +
                                6 + 2 + 1, 4 + track2len + 2 +
                                track3len + 2 + 1 + panlen *
                                2 + 6 + 2 + 8));
                icchip();
              }
            } else {
              track3len = Integer.parseInt(data.substring(
                      4 + track2len + 1, 4 + track2len + 2 + 1),
                      16);
              _track3 = data.substring(4 + track2len + 2 + 1, 4 +
                      track2len + 2 + track3len + 1);
              int panlen = 0;
              if (track3len % 2 == 0) {
                panlen = Integer.parseInt(
                        data.substring(4 + track2len + 2 +
                                track3len + 1, 4 + track2len +
                                2 + track3len + 2 + 1), 16);
                _pan = StringUtils.covertHexToASCII(data
                        .substring(4 + track2len + 2 +
                                track3len + 2 + 1, 4 +
                                track2len + 2 + track3len + 2 +
                                panlen * 2 + 1));
                _serviceCode =
                        StringUtils.covertHexToASCII(data.substring(4 +
                                track2len + 2 + track3len + 2 +
                                1 + panlen * 2, 4 + track2len +
                                2 + track3len + 2 + 1 +
                                panlen * 2 + 6));
                _exp = StringUtils.covertHexToASCII(data
                        .substring(4 + track2len + 2 +
                                track3len + 2 + 1 + panlen *
                                2 + 6 + 2, 4 + track2len + 2 +
                                track3len + 2 + 1 + panlen *
                                2 + 6 + 2 + 8));
                icchip();
              } else {
                panlen = Integer.parseInt(
                        data.substring(4 + track2len + 2 +
                                track3len + 1 + 1, 4 +
                                track2len + 2 + track3len + 2 +
                                1 + 1), 16);
                _pan = StringUtils.covertHexToASCII(data
                        .substring(4 + track2len + 2 +
                                track3len + 2 + 1 + 1, 4 +
                                track2len + 2 + track3len + 2 +
                                panlen * 2 + 1 + 1));
                _serviceCode =
                        StringUtils.covertHexToASCII(data.substring(4 +
                                track2len + 2 + track3len + 2 +
                                1 + 1 + panlen * 2, 4 +
                                track2len + 2 + track3len + 2 +
                                1 + 1 + panlen * 2 + 6));
                _exp = StringUtils.covertHexToASCII(data
                        .substring(4 + track2len + 2 +
                                track3len + 2 + 1 + 1 +
                                panlen * 2 + 6 + 2, 4 +
                                track2len + 2 + track3len + 2 +
                                1 + 1 + panlen * 2 + 6 + 2 +
                                8));
                icchip();
              }
            }
          } else if ((stata.equals("03")) || (stata.equals("02"))) {
            int track2len = Integer.parseInt(
                    data.substring(2, 4), 16);

            _track2 = data.substring(4, 4 + track2len);
            int panlen = 0;
            if (track2len % 2 == 0) {
              panlen = Integer.parseInt(data.substring(
                      4 + track2len, 4 + track2len + 2), 16);
              _pan = StringUtils.covertHexToASCII(data.substring(
                      4 + track2len + 2 + 2, 4 + track2len + 2 +
                              2 + panlen * 2));
              _serviceCode =
                      StringUtils.covertHexToASCII(data.substring(4 +
                              track2len + 2 + panlen * 2, 4 +
                              track2len + 2 + panlen * 2 + 6));
              _exp = StringUtils.covertHexToASCII(data.substring(
                      4 + track2len + 2 + panlen * 2 + 6 + 2, 4 +
                              track2len + 2 + panlen * 2 + 6 +
                              2 + 8));
              icchip();
            } else {
              panlen = Integer.parseInt(data.substring(
                      4 + track2len + 1, 4 + track2len + 2 + 1),
                      16);
              _pan = StringUtils.covertHexToASCII(data.substring(
                      4 + track2len + 2 + 1, 4 + track2len + 2 +
                              panlen * 2 + 1));
              _serviceCode = StringUtils.covertHexToASCII(data
                      .substring(4 + track2len + 2 + panlen * 2 +
                              1, 4 + track2len + 2 + panlen * 2 +
                              6 + 1));
              _exp = StringUtils.covertHexToASCII(data.substring(
                      4 + track2len + 2 + panlen * 2 + 6 + 2 + 1,
                      4 + track2len + 2 + panlen * 2 + 6 + 2 + 8 +
                              1));
              icchip();
            }
          }
        }
        else if (((data.startsWith("03")) || (data.startsWith("02")) ||
                (data.startsWith("06")) || (data.startsWith("07"))) &&
                (!data.endsWith("4d414745")) &&
                (!data.endsWith("4d414752"))) {
          String tmp = StringUtils.covertHexToASCII(data);
          int start = tmp.indexOf("B");
          int end = 0;
          if (start >= 0) {
            end = tmp.indexOf("?");
            int index = tmp.indexOf("^");
            _track1 = tmp.substring(start, end);
            _pan = tmp.substring(start + 1, index);
            int idx1 = tmp.indexOf("^", index + 1);
            if ((idx1 > 0) && (idx1 < tmp.length() - 4)) {
              cardHolder = tmp.substring(index + 1, idx1);
              _exp = tmp.substring(idx1 + 1, idx1 + 1 + 4);
            }
          }
          int start2 = tmp.indexOf(";", end);
          int fs = tmp.indexOf(";99");
          if ((start2 >= 0) && (start2 != fs)) {
            end = tmp.indexOf("?", start2);
            _track2 = tmp.substring(start2 + 1, end);
            if (_pan.equals("")) {
              int index = tmp.indexOf("=");
              _pan = tmp.substring(start2 + 1, index);
            }
          }
          start = fs;
          if (start >= 0) {
            end = tmp.lastIndexOf("?");
            _track3 = tmp.substring(start + 1, end - 1);
          }
          if ((_track2.equals("")) || (_track2 == null) ||
                  (!_track2.contains("="))) {
            _exp = null;
          } else {
            int index = _track2.indexOf("=") + 1;
            _exp = _track2.substring(index, index + 4);
            _serviceCode = _track2.substring(index + 4, index + 7);
            icchip();
          }
        }
      } else {
        resetData();
        onCardDected(CardDetected.MAGERR);
      }
    }
    catch (Exception localException) {}
  }

  private void icchip()
  {
    if ((_serviceCode.startsWith("2")) || (_serviceCode.startsWith("6")))
    {
      onCardDected(CardDetected.ICCHIP);
      return;
    }
  }

  private void resetData() {
    cardHolder = "";
    _track1 = "";
    _track2 = "";
    _track3 = "";
    _pan = "";
    _exp = "";
    _serviceCode = "";
    _trackRandom = "";
  }

  public String getMagPan()
  {
    return _pan;
  }

  public String getTrack1Data()
  {
    return _track1;
  }

  public String getTrack2Data()
  {
    return _track2;
  }

  public String getCardHolder()
  {
    return cardHolder;
  }

  public String getTrackRandom() {
    return _trackRandom;
  }

  public String getTrack3Data()
  {
    return _track3;
  }

  public String getTrackServiceCode()
  {
    return _serviceCode;
  }

  public String getMagExpDate()
  {
    if (!_exp.equals("")) {
      return _exp;
    }
    return null;
  }








  public String mPosmodifyConfig(String type)
  {
    if (type.equals(MPOS_PLAIN_STATE)) {
      String resp1 = getDataWithCipherCode("230100");
      String resp2 = getDataWithCipherCode("09020000");
      if ((resp1 != null) && (resp2 != null)) {
        commitPreferences(4);
        return resp1 + resp2;
      }
    } else if (type.equals(MPOS_DUKPT_STATE)) {
      String resp1 = getDataWithCipherCode("230102");
      if (resp1 != null) {
        commitPreferences(4);
        return resp1;
      }
    } else if (type.equals(MPOS_BPOS_STATE)) {
      String resp1 = getDataWithCipherCode("230103");
      String resp2 = getDataWithCipherCode("09020003");
      if ((resp1 != null) && (resp2 != null)) {
        commitPreferences(4);
        return resp1 + resp2;
      }
    } else if (type.equals(MPOS_UNIONPAY_STATE)) {
      String resp1 = getDataWithCipherCode("230104");
      String resp2 = getDataWithCipherCode("09020001");
      if ((resp1 != null) && (resp2 != null)) {
        commitPreferences(4);
        return resp1 + resp2;
      }
    }
    return "";
  }







  public String mPoscheckConfig()
  {
    _checkState = _preferences.getBoolean("checkState", false);
    _modifyState = _preferences.getBoolean("modifyState", false);

    if ((_modifyState) || (!_checkState)) {
      String resp = getDataWithCipherCode("0903");
      _checkState = true;
      commitPreferences(1);
      commitPreferences(5);
      if (resp != null) {
        commitPreferences(resp);
        return resp;
      }
      return "";
    }
    String plainState = _preferences.getString("plainState", "");
    return plainState;
  }


  private void commitPreferences(String resp)
  {
    SharedPreferences.Editor editor = _preferences.edit();
    editor.putString("plainState", resp);
    editor.commit();
  }

  private void commitPreferences(int i)
  {
    SharedPreferences.Editor editor = _preferences.edit();
    switch (i) {
      case 1:
        editor.putBoolean("checkState", true);
        break;
      case 4:
        editor.putBoolean("modifyState", true);
        break;
      case 5:
        editor.putBoolean("modifyState", false);
    }


    editor.commit();
  }
}

/*
public class MposHandler extends EMVHandler implements ThreadInterface {
  private static final String TAG = "TTLHandler";
  private SerialPort serialPort;
  private SerialReader reader;
  public SerialWriter writer;
  private List<SwipeListener> listeners = new ArrayList();

  private int retryCount = 1;
  private int timeout = 3;
  private int cmdTime = 3;
  private String lastResponse = null;
  private boolean connected = false;
  private String devName;
  private int baudrate;
  private boolean isPrn = false;
  private boolean isExit = false;
  private boolean showFlag = false;
  private String prnFlag = "";

  private SharedPreferences _preferences = null;
  private String LAST = "lastResponse";
  private String IC_IN = "49";
  private String IC_OUT = "4f";
  private String MAG_SWIPE = "4d";
  private String NFC_IN = "00087833b502410704ab6a";

  private String PRINT_EXIT = "45584954205052494e54";

  private String PRINT_IMAG = "4249544d4150204f4b";

  private String PRINT_LACK_PAPER = "4e4f205041504552";

  private String EMV_OK = "49434f4b";

  private String EMV_ERR = "4943455252";

  private static MposHandler instance = null;

  private boolean isSending = false;
  private String _track1 = "";
  private String _track2 = "";
  private String _track3 = "";
  private String _pan = "";
  private String _exp = "";
  private String _serviceCode = "";
  private String _trackRandom = "";
  private boolean isReadMag = false;
  private String posModel = "Z91";

  private boolean _checkState;
  private boolean _modifyState;
  private String cardHolder;
  public static String MPOS_PLAIN_STATE = "Mpos_Plain_state";
  public static String MPOS_DUKPT_STATE = "Mpos_dukpt_state";
  public static String MPOS_BPOS_STATE = "Mpos_BPos_state";
  public static String MPOS_UNIONPAY_STATE = "Mpos_UnionPay_state";
  private Thread nfcThread;
  private boolean threadStatus;
  private boolean flag = true;
  private Timer timer;
  private TimerTask timerTask;
  private Handler timerHandler = new Handler();
  private SingletonThread singletonThread;

  public MposHandler(Context context)
  {
    super(context);
    _preferences = context.getSharedPreferences("TTLHandler", 0);
    prnFlag = StringUtils.convertBytesToHex("ESC".getBytes());
    devName = "/dev/ttyHSL1";
    baudrate = 115200;
  }



  public MposHandler(Context context, PosModel model)
  {
    super(context);
    _preferences = context.getSharedPreferences("TTLHandler", 0);
    prnFlag = StringUtils.convertBytesToHex("ESC".getBytes());
    if (model.equals(Constants.POS_Z91)) {
      devName = "/dev/ttyHSL1";
    } else {
      devName = "/dev/ttyS2";
    }
    posModel = model.toString();
    baudrate = 115200;
  }

  public static MposHandler getInstance(Context _context, PosModel model)
  {
    if (instance == null) {
      synchronized (MposHandler.class) {
        if (instance == null) {
          instance = new MposHandler(_context, model);

        }
      }
    }
    return instance;
  }

  public static MposHandler getInstance(Context _context)
  {
    if (instance == null) {
      synchronized (MposHandler.class) {
        if (instance == null) {
          instance = new MposHandler(_context);

        }
      }
    }
    return instance;
  }






  public void addSwipeListener(SwipeListener listener)
  {
    listeners.add(listener);
  }

  public void stopTimer(){
    */
/*if(timer != null){
      timer.cancel();
      threadStatus = false;
      timer.purge();
    }*//*

  }

  public void startTimer(boolean status){
    */
/*threadStatus = status;
    System.out.println("valores algo");
    timer = new Timer();
    timerTask = new TimerTask() {
      public void run() {
        writeCipherCode("010105");
        *//*
*/
/*while (threadStatus) {
          try {
            writeCipherCode("010105");
            Thread.sleep(1000);
          } catch (InterruptedException e) {
            e.printStackTrace();
            return;
          }
        }*//*
*/
/*
        *//*
*/
/*timerHandler.post(new Runnable() {
          public void run(){
            while (threadStatus) {
              writeCipherCode("010105");
            }
          }
        });*//*
*/
/*
      }
    };

    timer.schedule(timerTask, 500);*//*

  }

  public void initNfcThread(){
    //singletonThread = SingletonThread.getInstance(this);
   */
/* nfcThread = new Thread(() -> {
          while (threadStatus) {
              try {
                  writeCipherCode("010105");
                  Thread.sleep(1000);
              } catch (InterruptedException e) {
                  e.printStackTrace();
                  return;
              }
          }
      });*//*

  }

    public void setCardDetectionThread(boolean status) {

    */
/*threadStatus = status;
      if (status && !nfcThread.isAlive()) {
        nfcThread.start();
      } else {
        if (nfcThread != null) {
          nfcThread.interrupt();
        }
      }*//*



    */
/*if (status) {
      if (!singletonThread.isThreadAlive()) {
        singletonThread.setThreadStatus(status);
        singletonThread.startThread();
      }
    } else {
      singletonThread.setThreadStatus(status);
    }*//*


      */
/*this.threadStatus = status;
      if (status) {
        threadStatus = false;
        threadStatus = true;
      }
        if (status) {
          System.out.println("valores : thread " + nfcThread.isAlive());
          if (!nfcThread.isAlive() && flag) {
            //nfcThread.start();
            flag = false;
          }
        } else {
          flag = true;
          if (nfcThread != null) {
            nfcThread.interrupt();
            }
        }*//*

    }



  public void removeSwipeListener(SwipeListener listener)
  {
    listeners.remove(listener);
  }

  public int getRetryCount() {
    return retryCount;
  }

  public void setRetryCount(int retryCount) {
    this.retryCount = retryCount;
  }

  public int getTimeout() {
    return timeout;
  }

  public void setTimeout(int timeout) {
    this.timeout = timeout;
  }

  public int getCmdTime() {
    return cmdTime;
  }

  public void setCmdTime(int cmdTime) {
    this.cmdTime = cmdTime;
  }

  public String getLastResponse() {
    return lastResponse;
  }

  public void setLastResponse(String lastResponse) {
    this.lastResponse = lastResponse;
  }

  public boolean isConnected() {
    return connected;
  }

  public void setConnected(boolean connected) {
    this.connected = connected;
  }

  public boolean isPrn() {
    return isPrn;
  }

  public void setPrn(boolean isPrn) {
    this.isPrn = isPrn;
  }

  public boolean isExit() {
    return isExit;
  }

  public void setExit(boolean isExit) {
    this.isExit = isExit;
  }

  public void setShowLog(boolean log) {
    showFlag = log;
  }

  public boolean isShowLog() {
    return showFlag;
  }

  public String getPosModel() {
    return posModel;
  }

  public void setPosModel(String posModel) {
    this.posModel = posModel;
  }








  public void setParameters(String devName, int baudrate)
  {
    this.devName = devName;
    this.baudrate = baudrate;
  }




  public synchronized boolean connect()
  {
    try
    {
      serialPort = new SerialPort(new File(devName), baudrate, 0);
      reader = new SerialReader(this, serialPort.getInputStream());
      reader.start();
      writer = new SerialWriter(this, serialPort.getOutputStream());
      writer.start();
      new Thread(new Runnable()
      {
        public void run()
        {
          try
          {
            Thread.sleep(100L);
            MposHandler.this.onConnected();
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
        }
      }).start();
    } catch (Exception e) {
      throw new IllegalArgumentException("TTL Connection Failed!", e);
    }
    return true;
  }

  private void onConnected() {
    if (isConnected())
      return;
    setConnected(true);
    for (SwipeListener listener : listeners) {
      SwipeEvent event = new SwipeEvent(this, SwipeEvent.TYPE_CONNECTED,
        "Device is connected!");
      listener.onConnected(event);
    }
  }

  private void onCardDected(CardDetected status)
  {
    for (SwipeListener listener : listeners) {
      listener.onCardDetect(status);
    }
  }

  public boolean isPowerOn() {
    return isConnected();
  }

  private void onDisconnected() {
    setConnected(false);
    for (SwipeListener listener : listeners) {
      SwipeEvent event = new SwipeEvent(this,
        SwipeEvent.TYPE_DISCONNECTED, "Device is disconnected!");
      listener.onDisconnected(event);
    }
  }

  public void onParseData(String data) {
    System.out.println("valores 1 : " + data);
    if (data.equals(prnFlag)) {
      setExit(true);
    }
    setLast(data);
    if (!isConnected())
      return;
    if (data.equals(IC_IN)) {
        setCardDetectionThread(false);
        //stopTimer();
      for (SwipeListener listener : listeners) {
        listener.onCardDetect(CardDetected.INSERTED);
      }
      return; }
    if (data.equals(IC_OUT)) {
      if (isRunning()) {
        cancel();
      }
        //initNfcThread();
        setCardDetectionThread(true);
        //startTimer(true);
      for (SwipeListener listener : listeners) {
        listener.onCardDetect(CardDetected.REMOVED);
      }
      return;
    }
    if (data.equals(NFC_IN)) {
        System.out.println("valores paso aca");
        for (SwipeListener listener : listeners) {
            listener.onCardDetect(CardDetected.NFC);
        }
    }
    if (data.equals(MAG_SWIPE)) {
      for (SwipeListener listener : listeners) {
        listener.onCardDetect(CardDetected.SWIPED);
      }
    } else if (data.endsWith(PRINT_EXIT)) {
      //initNfcThread();
      //setCardDetectionThread(true);
      System.out.println("valores paso aca");
      for (SwipeListener listener : listeners) {
        listener.onPrintStatus(PrintStatus.EXIT);
      }
    } else if (data.endsWith(PRINT_IMAG)) {
        //setCardDetectionThread(false);
      for (SwipeListener listener : listeners) {
        listener.onPrintStatus(PrintStatus.IMAGES);
      }
    } else if (data.equals(PRINT_LACK_PAPER)) {
        //nfcFlag = true;
        //initNfcThread();
        //setCardDetectionThread(true);
      for (SwipeListener listener : listeners) {
        listener.onPrintStatus(PrintStatus.LACK_PAPER);
      }
    } else if (isReadMag) {
      checkValueForMag(data);
    } else if (data.endsWith(EMV_OK)) {
      for (SwipeListener listener : listeners) {
        listener.onEmvStatus(EmvStatus.OK);
      }
    } else if (data.endsWith(EMV_ERR)) {
      for (SwipeListener listener : listeners) {
        listener.onEmvStatus(EmvStatus.ERR);
      }
    } else {
      for (SwipeListener listener : listeners) {
        SwipeEvent event = new SwipeEvent(this,
          SwipeEvent.TYPE_PARSEDATA, data);
        listener.onParseData(event);
      }
    }
  }






  public void writeCipherCode(String cipherCode)
  {
      System.out.println("valores cipher : " + cipherCode);
    if ((isConnected()) && (writer != null)) {
      if (!isPrn()) {
        String cmd = cipherCode.replaceAll(" ", "");
        int len = cmd.length();

        StringBuffer sb = new StringBuffer();
        sb.append("02");
        cmd =
          StringUtils.convertBytesToHex(new byte[] { (byte)(len / 2 / 256) }) +

          StringUtils.convertBytesToHex(new byte[] { (byte)(len / 2 % 256) }) +
          cmd;
        sb.append(cmd);

        int lrc = 0;
        for (int i = 0; i < len + 4; i += 2) {
          if (i == 0) {
            lrc = Integer.parseInt(cmd.substring(i, i + 2), 16);
          }
          else
            lrc = lrc ^ Integer.parseInt(cmd.substring(i, i + 2), 16);
        }
        String strLrc = Integer.toHexString(lrc);
        if (strLrc.length() != 1) {
          writer.send(sb.toString() + Integer.toHexString(lrc));
        } else {
          writer.send(sb.toString() + "0" + Integer.toHexString(lrc));
        }
      } else {
        writer.send(cipherCode.replaceAll(" ", ""));
      }
    }
  }

  public void write(String cipherCode) {
    if ((isConnected()) && (writer != null)) {
      String cmd = cipherCode.replaceAll(" ", "");
      writer.send(cmd);
    }
  }









  public String getDataWithCipherCode(String cipherCode)
  {
    if (isSending)
      return null;
    isSending = true;
    int time = 0;
    int rc = retryCount + 1;
    while (rc > 0) {
      time = 0;
      if (!isConnected()) {
        isSending = false;
        return null;
      }
      try {
        Thread.sleep(10L);
      }
      catch (InterruptedException localInterruptedException) {}


      setLast(null);
      if (cipherCode.equals("0204")) {
        isReadMag = true;
      } else {
        isReadMag = false;
      }
      writeCipherCode(cipherCode);
      if (isPrn) {
        isSending = false;
        return null;
      }

      while ((isConnected()) && (getLast() == null) && (time < timeout * 1000)) {
        try {
          Thread.sleep(cmdTime);
        }
        catch (InterruptedException localInterruptedException1) {}
        time += cmdTime;
      }
      if (getLast() != null) {
        isSending = false;
        return getLast();
      }
      rc--;
    }
    isSending = false;
    return getLast();
  }




  public synchronized void close()
  {
    if (isRunning()) {
      cancel();
    }
    resetKernel();
    try {
      if (reader != null) {
        reader.stop();
      }
    }
    catch (Exception localException) {}
    reader = null;
    try {
      if (writer != null) {
        writer.stop();
      }
    }
    catch (Exception localException1) {}
    writer = null;
    try {
      if (serialPort != null) {
        serialPort.close();
        serialPort = null;
      }
    }
    catch (Exception localException2) {}

    instance = null;
    onDisconnected();
  }

  public void onDestroy() {
    close();
  }

  public void setLast(String lastRes) {
    SharedPreferences.Editor editor = _preferences.edit();
    editor.putString(LAST, lastRes);
    editor.commit();
  }

  public String getLast() {
    return _preferences.getString(LAST, null);
  }



  private void checkValueForMag(String data)
  {
    resetData();
    try {
      if (data != null) {
        if (data.endsWith("4d414745")) {
          String state = data.substring(0, 2);
          _trackRandom = data.substring(2, 18);
          _track2 = data.substring(18, 66);
          if ((state.equals("06")) || (state.equals("07"))) {
            _track3 = data.substring(66, 178);
            int panLan = Integer.parseInt(data.substring(178, 180),
              16);
            _pan = StringUtils.covertHexToASCII(data.substring(180,
              180 + panLan * 2));
            _serviceCode = StringUtils.covertHexToASCII(data
              .substring(180 + panLan * 2,
              180 + panLan * 2 + 6));
            _exp = StringUtils.covertHexToASCII(data
              .substring(180 + panLan * 2 + 6 + 2, 180 +
              panLan * 2 + 6 + 10));
            icchip();
          } else if ((state.equals("03")) || (state.equals("02"))) {
            int panLan = Integer.parseInt(data.substring(66, 68),
              16);
            _pan = StringUtils.covertHexToASCII(data.substring(68,
              68 + panLan * 2));
            _serviceCode =
              StringUtils.covertHexToASCII(data.substring(
              68 + panLan * 2, 68 + panLan * 2 + 6));
            _exp =
              StringUtils.covertHexToASCII(data.substring(68 + panLan *
              2 + 6 + 2, 68 + panLan * 2 + 6 + 10));
            icchip();
          }
        } else if (data.endsWith("4d414752")) {
          String stata = data.substring(0, 2);
          if ((stata.equals("06")) || (stata.equals("07"))) {
            int track2len = Integer.parseInt(
              data.substring(2, 4), 16);
            _track2 = data.substring(4, 4 + track2len);
            int track3len = 0;
            if (track2len % 2 == 0) {
              track3len = Integer.parseInt(data.substring(
                4 + track2len, 4 + track2len + 2), 16);
              _track3 = data.substring(4 + track2len + 2, 4 +
                track2len + 2 + track3len);
              int panlen = 0;
              if (track3len % 2 == 0) {
                panlen = Integer.parseInt(
                  data.substring(4 + track2len + 2 +
                  track3len, 4 + track2len + 2 +
                  track3len + 2), 16);
                _pan = StringUtils.covertHexToASCII(data
                  .substring(4 + track2len + 2 +
                  track3len + 2, 4 + track2len +
                  2 + track3len + 2 + panlen *
                  2));
                _serviceCode =
                  StringUtils.covertHexToASCII(data.substring(4 +
                  track2len + 2 + track3len + 2 +
                  panlen * 2, 4 + track2len + 2 +
                  track3len + 2 + panlen * 2 +
                  6));
                _exp = StringUtils.covertHexToASCII(data
                  .substring(4 + track2len + 2 +
                  track3len + 2 + panlen * 2 +
                  6 + 2, 4 + track2len + 2 +
                  track3len + 2 + panlen * 2 +
                  6 + 2 + 8));
                icchip();
              } else {
                panlen = Integer.parseInt(
                  data.substring(4 + track2len + 2 +
                  track3len + 1, 4 + track2len +
                  2 + track3len + 2 + 1), 16);
                _pan = StringUtils.covertHexToASCII(data
                  .substring(4 + track2len + 2 +
                  track3len + 2 + 1, 4 +
                  track2len + 2 + track3len + 2 +
                  panlen * 2 + 1));
                _serviceCode =
                  StringUtils.covertHexToASCII(data.substring(4 +
                  track2len + 2 + track3len + 2 +
                  1 + panlen * 2, 4 + track2len +
                  2 + track3len + 2 + 1 +
                  panlen * 2 + 6));
                _exp = StringUtils.covertHexToASCII(data
                  .substring(4 + track2len + 2 +
                  track3len + 2 + panlen * 2 +
                  6 + 2 + 1, 4 + track2len + 2 +
                  track3len + 2 + 1 + panlen *
                  2 + 6 + 2 + 8));
                icchip();
              }
            } else {
              track3len = Integer.parseInt(data.substring(
                4 + track2len + 1, 4 + track2len + 2 + 1),
                16);
              _track3 = data.substring(4 + track2len + 2 + 1, 4 +
                track2len + 2 + track3len + 1);
              int panlen = 0;
              if (track3len % 2 == 0) {
                panlen = Integer.parseInt(
                  data.substring(4 + track2len + 2 +
                  track3len + 1, 4 + track2len +
                  2 + track3len + 2 + 1), 16);
                _pan = StringUtils.covertHexToASCII(data
                  .substring(4 + track2len + 2 +
                  track3len + 2 + 1, 4 +
                  track2len + 2 + track3len + 2 +
                  panlen * 2 + 1));
                _serviceCode =
                  StringUtils.covertHexToASCII(data.substring(4 +
                  track2len + 2 + track3len + 2 +
                  1 + panlen * 2, 4 + track2len +
                  2 + track3len + 2 + 1 +
                  panlen * 2 + 6));
                _exp = StringUtils.covertHexToASCII(data
                  .substring(4 + track2len + 2 +
                  track3len + 2 + 1 + panlen *
                  2 + 6 + 2, 4 + track2len + 2 +
                  track3len + 2 + 1 + panlen *
                  2 + 6 + 2 + 8));
                icchip();
              } else {
                panlen = Integer.parseInt(
                  data.substring(4 + track2len + 2 +
                  track3len + 1 + 1, 4 +
                  track2len + 2 + track3len + 2 +
                  1 + 1), 16);
                _pan = StringUtils.covertHexToASCII(data
                  .substring(4 + track2len + 2 +
                  track3len + 2 + 1 + 1, 4 +
                  track2len + 2 + track3len + 2 +
                  panlen * 2 + 1 + 1));
                _serviceCode =
                  StringUtils.covertHexToASCII(data.substring(4 +
                  track2len + 2 + track3len + 2 +
                  1 + 1 + panlen * 2, 4 +
                  track2len + 2 + track3len + 2 +
                  1 + 1 + panlen * 2 + 6));
                _exp = StringUtils.covertHexToASCII(data
                  .substring(4 + track2len + 2 +
                  track3len + 2 + 1 + 1 +
                  panlen * 2 + 6 + 2, 4 +
                  track2len + 2 + track3len + 2 +
                  1 + 1 + panlen * 2 + 6 + 2 +
                  8));
                icchip();
              }
            }
          } else if ((stata.equals("03")) || (stata.equals("02"))) {
            int track2len = Integer.parseInt(
              data.substring(2, 4), 16);

            _track2 = data.substring(4, 4 + track2len);
            int panlen = 0;
            if (track2len % 2 == 0) {
              panlen = Integer.parseInt(data.substring(
                4 + track2len, 4 + track2len + 2), 16);
              _pan = StringUtils.covertHexToASCII(data.substring(
                4 + track2len + 2 + 2, 4 + track2len + 2 +
                2 + panlen * 2));
              _serviceCode =
                StringUtils.covertHexToASCII(data.substring(4 +
                track2len + 2 + panlen * 2, 4 +
                track2len + 2 + panlen * 2 + 6));
              _exp = StringUtils.covertHexToASCII(data.substring(
                4 + track2len + 2 + panlen * 2 + 6 + 2, 4 +
                track2len + 2 + panlen * 2 + 6 +
                2 + 8));
              icchip();
            } else {
              panlen = Integer.parseInt(data.substring(
                4 + track2len + 1, 4 + track2len + 2 + 1),
                16);
              _pan = StringUtils.covertHexToASCII(data.substring(
                4 + track2len + 2 + 1, 4 + track2len + 2 +
                panlen * 2 + 1));
              _serviceCode = StringUtils.covertHexToASCII(data
                .substring(4 + track2len + 2 + panlen * 2 +
                1, 4 + track2len + 2 + panlen * 2 +
                6 + 1));
              _exp = StringUtils.covertHexToASCII(data.substring(
                4 + track2len + 2 + panlen * 2 + 6 + 2 + 1,
                4 + track2len + 2 + panlen * 2 + 6 + 2 + 8 +
                1));
              icchip();
            }
          }
        }
        else if (((data.startsWith("03")) || (data.startsWith("02")) ||
          (data.startsWith("06")) || (data.startsWith("07"))) &&
          (!data.endsWith("4d414745")) &&
          (!data.endsWith("4d414752"))) {
          String tmp = StringUtils.covertHexToASCII(data);
          int start = tmp.indexOf("B");
          int end = 0;
          if (start >= 0) {
            end = tmp.indexOf("?");
            int index = tmp.indexOf("^");
            _track1 = tmp.substring(start, end);
            _pan = tmp.substring(start + 1, index);
            int idx1 = tmp.indexOf("^", index + 1);
            if ((idx1 > 0) && (idx1 < tmp.length() - 4)) {
              cardHolder = tmp.substring(index + 1, idx1);
              _exp = tmp.substring(idx1 + 1, idx1 + 1 + 4);
            }
          }
          int start2 = tmp.indexOf(";", end);
          int fs = tmp.indexOf(";99");
          if ((start2 >= 0) && (start2 != fs)) {
            end = tmp.indexOf("?", start2);
            _track2 = tmp.substring(start2 + 1, end);
            if (_pan.equals("")) {
              int index = tmp.indexOf("=");
              _pan = tmp.substring(start2 + 1, index);
            }
          }
          start = fs;
          if (start >= 0) {
            end = tmp.lastIndexOf("?");
            _track3 = tmp.substring(start + 1, end - 1);
          }
          if ((_track2.equals("")) || (_track2 == null) ||
            (!_track2.contains("="))) {
            _exp = null;
          } else {
            int index = _track2.indexOf("=") + 1;
            _exp = _track2.substring(index, index + 4);
            _serviceCode = _track2.substring(index + 4, index + 7);
            icchip();
          }
        }
      } else {
        resetData();
        onCardDected(CardDetected.MAGERR);
      }
    }
    catch (Exception localException) {}
  }

  private void icchip()
  {
    if ((_serviceCode.startsWith("2")) || (_serviceCode.startsWith("6")))
    {
      onCardDected(CardDetected.ICCHIP);
      return;
    }
  }

  private void resetData() {
    cardHolder = "";
    _track1 = "";
    _track2 = "";
    _track3 = "";
    _pan = "";
    _exp = "";
    _serviceCode = "";
    _trackRandom = "";
  }

  public String getMagPan()
  {
    return _pan;
  }

  public String getTrack1Data()
  {
    return _track1;
  }

  public String getTrack2Data()
  {
    return _track2;
  }

  public String getCardHolder()
  {
    return cardHolder;
  }

  public String getTrackRandom() {
    return _trackRandom;
  }

  public String getTrack3Data()
  {
    return _track3;
  }

  public String getTrackServiceCode()
  {
    return _serviceCode;
  }

  public String getMagExpDate()
  {
    if (!_exp.equals("")) {
      return _exp;
    }
    return null;
  }








  public String mPosmodifyConfig(String type)
  {
    if (type.equals(MPOS_PLAIN_STATE)) {
      String resp1 = getDataWithCipherCode("230100");
      String resp2 = getDataWithCipherCode("09020000");
      if ((resp1 != null) && (resp2 != null)) {
        commitPreferences(4);
        return resp1 + resp2;
      }
    } else if (type.equals(MPOS_DUKPT_STATE)) {
      String resp1 = getDataWithCipherCode("230102");
      if (resp1 != null) {
        commitPreferences(4);
        return resp1;
      }
    } else if (type.equals(MPOS_BPOS_STATE)) {
      String resp1 = getDataWithCipherCode("230103");
      String resp2 = getDataWithCipherCode("09020003");
      if ((resp1 != null) && (resp2 != null)) {
        commitPreferences(4);
        return resp1 + resp2;
      }
    } else if (type.equals(MPOS_UNIONPAY_STATE)) {
      String resp1 = getDataWithCipherCode("230104");
      String resp2 = getDataWithCipherCode("09020001");
      if ((resp1 != null) && (resp2 != null)) {
        commitPreferences(4);
        return resp1 + resp2;
      }
    }
    return "";
  }







  public String mPoscheckConfig()
  {
    _checkState = _preferences.getBoolean("checkState", false);
    _modifyState = _preferences.getBoolean("modifyState", false);

    if ((_modifyState) || (!_checkState)) {
      String resp = getDataWithCipherCode("0903");
      _checkState = true;
      commitPreferences(1);
      commitPreferences(5);
      if (resp != null) {
        commitPreferences(resp);
        return resp;
      }
      return "";
    }
    String plainState = _preferences.getString("plainState", "");
    return plainState;
  }


  private void commitPreferences(String resp)
  {
    SharedPreferences.Editor editor = _preferences.edit();
    editor.putString("plainState", resp);
    editor.commit();
  }

  private void commitPreferences(int i)
  {
    SharedPreferences.Editor editor = _preferences.edit();
    switch (i) {
    case 1:
      editor.putBoolean("checkState", true);
      break;
    case 4:
      editor.putBoolean("modifyState", true);
      break;
    case 5:
      editor.putBoolean("modifyState", false);
    }


    editor.commit();
  }

  @Override
  public void task() {
    writeCipherCode("010105");
    System.out.println("paso por aqui ");
  }
}
*/
