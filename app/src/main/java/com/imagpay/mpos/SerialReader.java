package com.imagpay.mpos;

import android.util.Log;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;




public class SerialReader
  implements Runnable
{
  private boolean running = false;
  private MposHandler handler;
  private InputStream in;
  private StringBuffer tmp = new StringBuffer();
  
  private List<String> data = Collections.synchronizedList(new LinkedList());
  
  public SerialReader(MposHandler handler, InputStream in) {
    this.handler = handler;
    this.in = in;
  }
  
  public void start() {
    running = true;
    new Thread(this).start();
  }
  
  public void stop() {
    running = false;
    try {
      in.close();
    }
    catch (IOException localIOException) {}
    in = null;
    data.clear();
  }
  
  public boolean isRunning()
  {
    return running;
  }
  


  public void run()
  {
    byte[] buffer = new byte['Ā'];
    int len = -1;
    while (isRunning()) {
      try {
        do {

          for (int i = 0; i < len; i++) {
            //System.out.println("valorespalu" + buffer[i]);
            String str = Integer.toHexString(buffer[i]);
            if (str.length() == 1) {
              data.add("0" + str);
            } else if (str.length() == 2) {
              data.add(str);
            } else
              data.add(str.substring(str.length() - 2,
                      str.length()));
            if (!((String) data.get(0)).equals("02")) {
              data.clear();
              break;
            }
          }
          if (data.size() >= 5) {
            int totalLen = Integer.parseInt((String) data.get(1), 16) * 256 +
                    Integer.parseInt((String) data.get(2), 16);
            if (data.size() >= totalLen + 4) {
              StringBuffer sb = new StringBuffer();
              for (int i = 0; i < data.size(); i++) {
                sb.append((String) data.get(i));
              }

              for (int j = 0; j < totalLen; j++) {
                tmp.append((String) data.get(j + 3));
              }
              if ((handler.isShowLog()) && (handler.isConnected()))
                Log.d("TTL Reader:", tmp.toString());
              handler.onParseData(tmp.toString().trim());
              data.clear();
              tmp.setLength(0);
            }
          }
          if (in == null) break;
        }while ((len = in.read(buffer)) > 0);


      }
      catch (IOException e)
      {

        System.out.println("Read data fail! " + e.getMessage());
        running = false;
      }
      
      try
      {
        Thread.sleep(5L);
      }
      catch (Exception localException) {}
    }
  }
}
