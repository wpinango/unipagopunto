package com.imagpay.mpos;

import android.util.Log;
import com.imagpay.utils.StringUtils;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;




public class SerialWriter
{
  private boolean running = false;
  

  private OutputStream out;
  

  public SerialWriter(MposHandler handler, OutputStream out)
  {
    this.out = out;
  }
  
  public void start() {
    running = true;
  }
  
  public void stop()
  {
    running = false;
    


    try
    {
      out.close();
    }
    catch (IOException localIOException) {}
    out = null;
  }
  
  public boolean isRunning() {
    return running;
  }
  




  public void send(String command)
  {
    Log.d("TTL SerialWriter", "Command ==>" + command);
    try {
      if (command != null)
      {






        out.write(StringUtils.convertHexToBytes(command));
      }
      out.flush();
    } catch (Exception e) {
      System.out.println("Write data fail! " + e.getMessage());
    }
  }
}
