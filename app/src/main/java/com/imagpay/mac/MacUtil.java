package com.imagpay.mac;

import android.util.Log;






public class MacUtil
{
  static
  {
    try
    {
      System.loadLibrary("JNIMAC");
    } catch (Exception e) {
      Log.d("MacUtil", "Load library Error!");
    }
  }
  
  public MacUtil() {}
  
  public static native byte[] getKBCV(byte[] paramArrayOfByte1, byte[] paramArrayOfByte2, byte[] paramArrayOfByte3);
}
