package com.imagpay.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;












public class JsonUtil
{
  public JsonUtil() {}
  
  public static <T> T parse(String json, Class<T> type)
  {
    Gson gson = new Gson();
    T t = null;
    try {
      t = gson.fromJson(json, type);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
    
    return t;
  }
  






  public static <T> T[] parseArr(String json, Class<T[]> type)
  {
    return parse(json, type);
  }
  






  public static <T> ArrayList<T> parseList(String json, Class<T[]> type)
  {
    return new ArrayList(Arrays.asList((Object[])parse(json, type)));
  }
  






  public static String format(Object o)
  {
    Gson gson = new GsonBuilder().serializeNulls().create();
    return gson.toJson(o);
  }
  




  public static String formatURLString(Object o)
  {
    try
    {
      return URLEncoder.encode(format(o), "utf-8");
    } catch (Exception e) {}
    return null;
  }
}
