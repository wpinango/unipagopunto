package com.imagpay.utils;

import java.io.PrintStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.IvParameterSpec;






public final class MessageDigestUtils
{
  public static int TDES_ECB = 1;
  public static int TDES_CBC = 2;
  

  private static final String MD5 = "MD5";
  

  private static final String DES = "DES";
  


  public MessageDigestUtils() {}
  

  public static String encodeMD5(String data)
    throws NoSuchAlgorithmException
  {
    MessageDigest md5 = MessageDigest.getInstance("MD5");
    return bytesToHexString(md5.digest(data.getBytes()));
  }
  
















  public static final String encodeDES(String data, String key)
    throws InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException
  {
    return bytesToHexString(encodeDES(data.getBytes(), key.getBytes()));
  }
  

















  public static byte[] encodeDES(byte[] data, byte[] key)
    throws InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException
  {
    SecureRandom sr = new SecureRandom();
    DESKeySpec dks = new DESKeySpec(key);
    SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
    SecretKey securekey = keyFactory.generateSecret(dks);
    Cipher cipher = Cipher.getInstance("DES");
    cipher.init(1, securekey, sr);
    return cipher.doFinal(data);
  }
  
















  public static final String decodeDES(String data, String key)
    throws InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException
  {
    return new String(decodeDES(hexStringToBytes(data), key.getBytes()));
  }
  


















  public static byte[] decodeDES(byte[] data, byte[] key)
    throws InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException
  {
    SecureRandom sr = new SecureRandom();
    DESKeySpec dks = new DESKeySpec(key);
    SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
    SecretKey securekey = keyFactory.generateSecret(dks);
    Cipher cipher = Cipher.getInstance("DES");
    cipher.init(2, securekey, sr);
    return cipher.doFinal(data);
  }
  


  public static String encodeTripleDES(String data, String key)
    throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException, InvalidKeySpecException
  {
    return encodeTripleDES(TDES_ECB, data, key);
  }
  


  public static byte[] encodeTripleDES(byte[] data, byte[] key)
    throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException, InvalidKeySpecException
  {
    return encodeTripleDES(TDES_ECB, data, key);
  }
  


  public static String encodeTripleDES(int type, String data, String key)
    throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException, InvalidKeySpecException
  {
    return bytesToHexString(encodeTripleDES(type, hexStringToBytes(data), 
      hexStringToBytes(key)));
  }
  

  public static byte[] encodeTripleDES(int type, byte[] data, byte[] key)
    throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException, InvalidKeySpecException
  {
    byte[] keys;
    
    if (key.length == 16) {
      keys = new byte[24];
      System.arraycopy(key, 0, keys, 0, 16);
      System.arraycopy(key, 0, keys, 16, 8);
    } else {
      keys = key;
    }
    

    DESedeKeySpec spec = new DESedeKeySpec(keys);
    SecretKeyFactory factory = SecretKeyFactory.getInstance("DESede");
    Key secretKey = factory.generateSecret(spec);
    Cipher cipher = null;
    if (type == TDES_ECB) {
      cipher = Cipher.getInstance("DESede/ECB/NoPadding");
      cipher.init(1, secretKey);
    } else if (type == TDES_CBC) {
      cipher = Cipher.getInstance("DESede/CBC/NoPadding");
      IvParameterSpec iv = new IvParameterSpec(new byte[8]);
      

      cipher.init(1, secretKey, iv);
    }
    return cipher.doFinal(data);
  }
  


  public static String decodeTripleDES(String data, String key)
    throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException, InvalidKeySpecException
  {
    return decodeTripleDES(TDES_ECB, data, key);
  }
  


  public static byte[] decodeTripleDES(byte[] data, byte[] key)
    throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException, InvalidKeySpecException
  {
    return decodeTripleDES(TDES_ECB, data, key);
  }
  


  public static String decodeTripleDES(int type, String data, String key)
    throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException, InvalidKeySpecException
  {
    return bytesToHexString(decodeTripleDES(type, hexStringToBytes(data), 
      hexStringToBytes(key)));
  }
  

  public static byte[] decodeTripleDES(int type, byte[] data, byte[] key)
    throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException, InvalidKeySpecException
  {
    byte[] keys;
    
    if (key.length == 16) {
      keys = new byte[24];
      System.arraycopy(key, 0, keys, 0, 16);
      System.arraycopy(key, 0, keys, 16, 8);
    } else {
      keys = key;
    }
    

    DESedeKeySpec spec = new DESedeKeySpec(keys);
    SecretKeyFactory factory = SecretKeyFactory.getInstance("DESede");
    Key secretKey = factory.generateSecret(spec);
    Cipher cipher = null;
    if (type == TDES_ECB) {
      cipher = Cipher.getInstance("DESede/ECB/NoPadding");
      cipher.init(2, secretKey);
    } else if (type == TDES_CBC) {
      cipher = Cipher.getInstance("DESede/CBC/NoPadding");
      IvParameterSpec iv = new IvParameterSpec(new byte[8]);
      

      cipher.init(2, secretKey, iv);
    }
    return cipher.doFinal(data);
  }
  




  public static String encodeBDK()
  {
    StringBuffer sbf = new StringBuffer();
    sbf.append(RandomUtils.generateString(32, "0123456789ABCDEF"));
    return sbf.toString();
  }
  













  public static String encodeBDK(String vid, String pid, String did)
  {
    checkArguments(vid, pid, did);
    StringBuffer sbf = new StringBuffer();
    sbf.append(RandomUtils.generateString(32, "0123456789ABCDEF"));
    return sbf.toString();
  }
  










  public static String encodeKSN()
  {
    StringBuffer sbf = new StringBuffer();
    sbf.append(RandomUtils.generateString(14, "0123456789ABCDEF"));
    sbf.append("0");
    
    sbf.append("00000");
    return sbf.toString();
  }
  

















  public static String encodeKSN(String vid, String pid, String did)
  {
    checkArguments(vid, pid, did);
    
    StringBuffer sbf = new StringBuffer();
    sbf.append(RandomUtils.generateString(14 - did.length(), 
      "0123456789ABCDEF"));
    sbf.append(did);
    sbf.append("0");
    
    sbf.append("00000");
    return sbf.toString();
  }
  
  private static void checkArguments(String vid, String pid, String did) {
    if ((vid == null) || (vid.length() != 4))
      throw new IllegalArgumentException(
        "The vendor id lenght must be 4 digits");
    if ((pid == null) || (pid.length() != 4))
      throw new IllegalArgumentException(
        "The product id lenght must be 4 digits");
    if ((did == null) || (did.length() < 5) || (did.length() > 14)) {
      throw new IllegalArgumentException(
        "The device id lenght must be more than 4 digits and less than or equal to 14 digits");
    }
  }
  




























  public static String bytesToHexString(byte[] src)
  {
    StringBuilder stringBuilder = new StringBuilder("");
    for (int i = 0; i < src.length; i++) {
      int v = src[i] & 0xFF;
      String hv = Integer.toHexString(v);
      if (hv.length() < 2) {
        stringBuilder.append(0);
      }
      stringBuilder.append(hv);
    }
    return stringBuilder.toString().toLowerCase();
  }
  
  public static byte[] hexStringToBytes(String hexString) {
    hexString = hexString.toLowerCase();
    int length = hexString.length() / 2;
    char[] hexChars = hexString.toCharArray();
    byte[] d = new byte[length];
    for (int i = 0; i < length; i++) {
      int pos = i * 2;
      d[i] = ((byte)(charToByte(hexChars[pos]) << 4 | charToByte(hexChars[(pos + 1)])));
    }
    return d;
  }
  
  private static byte charToByte(char c) {
    return (byte)"0123456789abcdef".indexOf(c);
  }
  
  public static void main(String[] args) {
    try {
      System.out.println(encodeDES("abcddd", "aaaaaaaabbbbbbbbccccccccdddddddd"));
      System.out.println(decodeDES("86d990c9184772fb", "aaaaaaaabbbbbbbbccccccccdddddddd"));
      System.out.println(encodeTripleDES(TDES_ECB, "f45c3ee2666d50a8", "aaaaaaaabbbbbbbbccccccccdddddddd"));
      System.out.println(decodeTripleDES(TDES_ECB, "e94b4e62ebcc1919", "aaaaaaaabbbbbbbbccccccccdddddddd"));
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
  


  private static String encodePAN(String pan)
  {
    int len = pan.length();
    StringBuffer sbf = new StringBuffer();
    sbf.append("0000");
    if (len < 13) {
      for (int i = 0; i < 13 - len; i++)
        sbf.append("0");
      sbf.append(pan.substring(0, len - 1));
    } else {
      sbf.append(pan.substring(len - 1 - 12, len - 1));
    }
    return sbf.toString();
  }
  
  private static String encodePinBlock(String pin)
  {
    int len = pin.length();
    if ((len < 4) || (len > 12))
      throw new IllegalArgumentException("Pin length must be 4~12 digits");
    StringBuffer sbf = new StringBuffer();
    String tmp = Integer.toHexString(len);
    if (tmp.length() == 1) {
      sbf.append("0").append(tmp);
    } else
      sbf.append(tmp);
    sbf.append(pin);
    len = sbf.length();
    for (int i = 0; i < 16 - len; i++)
      sbf.append("F");
    return sbf.toString();
  }
  
  private static String decodePinBlock(String pinBlock)
  {
    int len = pinBlock.length();
    if (len != 16)
      throw new IllegalArgumentException("Pin block length must be 16 digits");
    len = Integer.parseInt(pinBlock.substring(0, 2), 16);
    return pinBlock.substring(2, 2 + len);
  }
  
  private static String encodePinBlock(String pin, String pan) {
    String pinBlock = encodePinBlock(pin);
    String anb = encodePAN(pan);
    byte[] pins = hexStringToBytes(pinBlock);
    byte[] pans = hexStringToBytes(anb);
    for (int i = 0; i < 8; i++) {
      int tmp32_30 = i; byte[] tmp32_28 = pins;tmp32_28[tmp32_30] = ((byte)(tmp32_28[tmp32_30] ^ pans[i])); }
    return bytesToHexString(pins);
  }
  
  private static String decodePinBlock(String encrypted, String pan) {
    String anb = encodePAN(pan);
    byte[] pins = hexStringToBytes(encrypted);
    byte[] pans = hexStringToBytes(anb);
    for (int i = 0; i < 8; i++) {
      int tmp25_23 = i; byte[] tmp25_22 = pins;tmp25_22[tmp25_23] = ((byte)(tmp25_22[tmp25_23] ^ pans[i])); }
    return bytesToHexString(pins);
  }
  


  public static String encodePin(String pin, String pan, String key)
    throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException, InvalidKeySpecException
  {
    String clearPinBlock = encodePinBlock(pin, pan);
    return encodeTripleDES(TDES_CBC, clearPinBlock, key);
  }
  


  public static String decodePin(String encrypted, String pan, String key)
    throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException, InvalidKeySpecException
  {
    String pinBlock = decodeTripleDES(TDES_CBC, encrypted, key);
    pinBlock = decodePinBlock(pinBlock, pan);
    return decodePinBlock(pinBlock);
  }
}
