package com.imagpay.utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build;
import android.os.Environment;
import android.os.Looper;
import android.os.Process;
import android.util.Log;
import android.widget.Toast;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;









public class CrashHandler
  implements Thread.UncaughtExceptionHandler
{
  public static final String TAG = "CrashHandler";
  private Thread.UncaughtExceptionHandler mDefaultHandler;
  private static CrashHandler INSTANCE = new CrashHandler();
  
  private Context mContext;
  
  private Map<String, String> infos = new HashMap();
  

  private DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
  

  private CrashHandler() {}
  

  public static CrashHandler getInstance()
  {
    return INSTANCE;
  }
  




  public void init(Context context)
  {
    mContext = context;
    
    mDefaultHandler = Thread.getDefaultUncaughtExceptionHandler();
    
    Thread.setDefaultUncaughtExceptionHandler(this);
  }
  



  public void uncaughtException(Thread thread, Throwable ex)
  {
    if ((!handleException(ex)) && (mDefaultHandler != null))
    {
      mDefaultHandler.uncaughtException(thread, ex);
    } else {
      try {
        Thread.sleep(3000L);
      } catch (InterruptedException e) {
        Log.e("CrashHandler", "error : ", e);
      }
      
      Process.killProcess(Process.myPid());
      System.exit(1);
    }
  }
  





  private boolean handleException(Throwable ex)
  {
    if (ex == null) {
      return false;
    }
    new Thread()
    {
      public void run()
      {
        Looper.prepare();
        Toast.makeText(mContext, "很抱歉，程序出现异常，即将退出。", Toast.LENGTH_LONG).show();
        Looper.loop();
      }
      
    }.start();
    collectDeviceInfo(mContext);
    
    saveCrashInfo2File(ex);
    return true;
  }
  



  public void collectDeviceInfo(Context ctx)
  {
    try
    {
      PackageManager pm = ctx.getPackageManager();
      PackageInfo pi = pm.getPackageInfo(ctx.getPackageName(), PackageManager.GET_ACTIVITIES);
      if (pi != null) {
        //versionName = versionName == null ? "null" : versionName;
        //versionCode = versionCode;
        //infos.put("versionName", versionName);
        //infos.put("versionCode", versionCode);
      }
    } catch (PackageManager.NameNotFoundException e) {
      Log.e("CrashHandler", "an error occured when collect package info", e);
    }
    Field[] fields = Build.class.getDeclaredFields();
    Field[] arrayOfField1;
    int versionCode = (arrayOfField1 = fields).length;
    for (int versionName = 0; versionName < versionCode; versionName++) {
      Field field = arrayOfField1[versionName];
      try {
        field.setAccessible(true);
        infos.put(field.getName(), field.get(null).toString());
        Log.d("CrashHandler", field.getName() + " : " + field.get(null));
      } catch (Exception e) {
        Log.e("CrashHandler", "an error occured when collect crash info", e);
      }
    }
  }
  





  private String saveCrashInfo2File(Throwable ex)
  {
    StringBuffer sb = new StringBuffer();
    for (Map.Entry<String, String> entry : infos.entrySet()) {
      String key = (String)entry.getKey();
      String value = (String)entry.getValue();
      sb.append(key + "=" + value + "\n");
    }
    
    Writer writer = new StringWriter();
    PrintWriter printWriter = new PrintWriter(writer);
    ex.printStackTrace(printWriter);
    Throwable cause = ex.getCause();
    while (cause != null) {
      cause.printStackTrace(printWriter);
      cause = cause.getCause();
    }
    printWriter.close();
    String result = writer.toString();
    sb.append(result);
    try {
      long timestamp = System.currentTimeMillis();
      String time = formatter.format(new Date());
      String fileName = "crash-" + time + "-" + timestamp + ".log";
      Log.d("CrashHandler", "开始记录奔溃日志...");
      
      if (Environment.getExternalStorageState().equals("mounted")) {
        String path = Storage.getExtDir() + "/";
        File dir = new File(path);
        if (!dir.exists()) {
          dir.mkdirs();
        }
        FileOutputStream fos = new FileOutputStream(path + fileName);
        fos.write(sb.toString().getBytes());
        fos.close();
      }
      return fileName;
    } catch (Exception e) {
      Log.e("CrashHandler", "an error occured while writing file...", e);
    }
    return null;
  }
  




  public String saveCrashInfo2File(String str)
  {
    try
    {
      long timestamp = System.currentTimeMillis();
      String time = formatter.format(new Date());
      String fileName = "crash-" + time + "-" + timestamp + ".log";
      Log.d("CrashHandler", "开始记录日志...");
      
      if (Environment.getExternalStorageState().equals("mounted")) {
        String path = Storage.getExtDir() + "/";
        File dir = new File(path);
        if (!dir.exists()) {
          dir.mkdirs();
        }
        FileOutputStream fos = new FileOutputStream(path + fileName);
        fos.write(str.toString().getBytes());
        fos.close();
      }
      return fileName;
    } catch (Exception e) {
      Log.e("CrashHandler", "an error occured while writing file...", e);
    }
    return null;
  }
}
