package com.imagpay.utils;

import android.content.Context;
import android.os.Environment;
import android.util.Log;
import java.io.File;
import java.io.IOException;















public class Storage
{
  public static final String DEFAULT_DIR = "MPos_Log";
  
  public Storage() {}
  
  public static boolean hasSDCard()
  {
    String status = Environment.getExternalStorageState();
    if (status.equals("mounted")) {
      return true;
    }
    return false;
  }
  





  public static File getExtDir()
  {
    File dir = null;
    File sdCardDir = new File("/mnt/sdcard/MPos_Log");
    if ((sdCardDir.exists()) || (sdCardDir.mkdirs())) {
      dir = sdCardDir;
    } else {
      File dirExt = new File(Environment.getExternalStorageDirectory()
        .getPath() + "/" + "MPos_Log");
      if ((dirExt.exists()) || (dirExt.mkdirs())) {
        dir = dirExt;
      } else {
        File dirData = new File("/data/data/MPos_Log");
        if (((dirData.exists()) || (dirData.mkdirs())) && 
          (dirData.canWrite())) {
          dir = dirData;
        } else {
          File dirCache = new File("/data/cache/MPos_Log");
          if (((dirCache.exists()) || (dirCache.mkdirs())) && 
            (dirCache.canWrite())) {
            dir = dirCache;
          }
        }
      }
    }
    
    if (dir != null) {
      File noMediaFile = new File(dir, ".nomedia");
      if (!noMediaFile.exists()) {
        try {
          noMediaFile.createNewFile();
        } catch (IOException e) {
          Log.w("Storage", "在目录(" + dir + ")中新建.nomedia文件失败。", e);
        }
      }
    }
    return dir;
  }
  






  public static File getDir(Context ctx)
  {
    File dir = getExtDir();
    
    return dir != null ? dir : ctx.getCacheDir();
  }
}
