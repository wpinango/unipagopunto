package com.imagpay.utils;







public final class StringUtils
{
  public StringUtils() {}
  






  public static String convertHexToBinary(String src)
  {
    long lg = Long.parseLong(src, 16);
    String binaryString = Long.toBinaryString(lg);
    int shouldBinaryLen = src.length() * 4;
    StringBuffer addZero = new StringBuffer();
    int addZeroNum = shouldBinaryLen - binaryString.length();
    for (int i = 1; i <= addZeroNum; i++) {
      addZero.append("0");
    }
    return addZero.toString() + binaryString;
  }
  






  public static byte[] convertHexToBytes(String src)
  {
    src = src.toUpperCase();
    int length = src.length() / 2;
    char[] hexChars = src.toCharArray();
    byte[] d = new byte[length];
    for (int i = 0; i < length; i++) {
      d[i] = ((byte)(charToByte(hexChars[(i * 2)]) << 4 | charToByte(hexChars[(i * 2 + 1)])));
    }
    return d;
  }
  
  private static byte charToByte(char c) {
    return (byte)"0123456789ABCDEF".indexOf(c);
  }
  





  public static String convertBytesToHex(byte[] src)
  {
    StringBuilder stringBuilder = new StringBuilder("");
    for (int i = 0; i < src.length; i++) {
      int v = src[i] & 0xFF;
      String hv = Integer.toHexString(v);
      if (hv.length() < 2) {
        stringBuilder.append(0);
      }
      stringBuilder.append(hv);
    }
    return stringBuilder.toString();
  }
  





  public static String convertStringToHex(String str)
  {
    char[] chars = str.toCharArray();
    StringBuffer hex = new StringBuffer();
    for (int i = 0; i < chars.length; i++) {
      hex.append(Integer.toHexString(chars[i]));
    }
    return hex.toString();
  }
  
  public static String hexToStringGBK(String s)
  {
    if (s == null)
      return "";
    byte[] baKeyword = new byte[s.length() / 2];
    for (int i = 0; i < baKeyword.length; i++) {
      try {
        baKeyword[i] = ((byte)(0xFF & Integer.parseInt(
          s.substring(i * 2, i * 2 + 2), 16)));
      } catch (Exception e) {
        e.printStackTrace();
        return "";
      }
    }
    try {
      s = new String(baKeyword, "GBK");
    } catch (Exception e1) {
      e1.printStackTrace();
      return "";
    }
    return s;
  }
  
  public static String covertHexToASCII(String str)
  {
    if (str == null)
      return null;
    try {
      String[] ss = str.replaceAll("..", "$0 ").split(" ");
      StringBuffer sb = new StringBuffer();
      for (String d : ss) {
        sb.append((char)Integer.parseInt(d, 16));
      }
      return sb.toString();
    }
    catch (Exception localException) {}
    return null;
  }
}
