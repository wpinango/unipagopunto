package com.imagpay.utils;

import java.io.PrintStream;

public class SocketSendUtil
{
  private int port = 1111;
  private String host = "192.168.0.111";
  
  private static SocketSendUtil INSTANCE = new SocketSendUtil();
  

  public SocketSendUtil() {}
  
  public static SocketSendUtil getInstance()
  {
    return INSTANCE;
  }
  
  public void init(String host, int port) {
    this.port = port;
    this.host = host;
  }
  
  public void sendData(final String str) {
    final String _host = host;
    final int _port = port;
    new Thread(new Runnable()
    {
      public void run()
      {
        java.net.Socket socket = null;
        try
        {
          socket = new java.net.Socket(_host, _port);
          PrintStream ps = new PrintStream(socket.getOutputStream(), true, 
            "UTF-8");
          ps.print(str);
          ps.close();
          socket.close();
        } catch (Exception e) {
          System.out.println(e);
        }
      }
    })
    















      .start();
  }
}
