package com.imagpay.utils;

import android.content.Context;
import android.media.AudioRecord;
import android.os.Build;
import android.os.Build.VERSION;
import android.telephony.TelephonyManager;










public final class DeviceUtils
{
  public DeviceUtils() {}
  
  public static String getManufacturer()
  {
    return Build.MANUFACTURER;
  }
  




  public static String getModel()
  {
    return Build.MODEL;
  }
  




  public static String getReleaseVersion()
  {
    return Build.VERSION.RELEASE;
  }
  





  public static int getSDKVersion()
  {
    return Build.VERSION.SDK_INT;
  }
  








  public static String getDeviceId(Context context)
  {
    TelephonyManager tm = (TelephonyManager)context
      .getSystemService("phone");
    return tm.getDeviceId();
  }
  








  public static String getDeviceSoftwareVersion(Context context)
  {
    TelephonyManager tm = (TelephonyManager)context
      .getSystemService("phone");
    return tm.getDeviceSoftwareVersion();
  }
  





  public static boolean isMICAvailable()
  {
    boolean result = false;
    try {
      int size = 
        AudioRecord.getMinBufferSize(8000, 16, 
        2);
      if (size <= 0)
        size = 2096;
      AudioRecord record = new AudioRecord(1, 8000, 
        16, 
        2, size);
      if (record.getState() == 1) {
        result = true;
      } else {
        result = false;
      }
      record.release();
      record = null;
    } catch (Exception e) {
      result = false;
    }
    return result;
  }
  






  public static boolean isEmulator(Context context)
  {
    return "000000000000000".equalsIgnoreCase(getDeviceId(context));
  }
}
