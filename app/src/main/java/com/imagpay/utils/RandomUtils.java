package com.imagpay.utils;

import java.util.Random;


























public final class RandomUtils
{
  public static final String CHAR_ALL = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
  public static final String CHAR_LETTER = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
  public static final String CHAR_NUMBER = "0123456789";
  public static final String CHAR_HEX = "0123456789ABCDEF";
  
  public RandomUtils() {}
  
  public static String generateString(int length, String data)
  {
    StringBuffer sbf = new StringBuffer();
    Random random = new Random();
    int max = data.length();
    for (int i = 0; i < length; i++) {
      sbf.append(data.charAt(random.nextInt(max)));
    }
    return sbf.toString();
  }
  






  public static String generateHexString(int length)
  {
    StringBuffer sbf = new StringBuffer();
    Random random = new Random();
    int max = "0123456789ABCDEF".length();
    for (int i = 0; i < length; i++) {
      sbf.append("0123456789ABCDEF".charAt(random.nextInt(max)));
    }
    return sbf.toString();
  }
}
