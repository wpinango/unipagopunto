package com.imagpay;








public class SwipeEvent
{
  public static int TYPE_DISCONNECTED = 1;
  
  public static int TYPE_CONNECTED = 2;
  
  public static int TYPE_STARTED = 3;
  
  public static int TYPE_STOPPED = 4;
  
  public static int TYPE_READDATA = 5;
  
  public static int TYPE_PARSEDATA = 6;
  
  public static int TYPE_PERMISSION_GRANTED = 7;
  
  public static int TYPE_PERMISSION_DENIED = 8;
  


  private int _type;
  


  private String _value;
  


  private Object _source;
  


  public SwipeEvent(Object source, int type, String value)
  {
    _source = source;
    _type = type;
    _value = value;
  }
  




  public Object getSource()
  {
    return _source;
  }
  




  public int getType()
  {
    return _type;
  }
  




  public String getValue()
  {
    return _value;
  }
}
