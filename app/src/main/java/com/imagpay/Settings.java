package com.imagpay;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.text.Layout;
import android.text.Layout.Alignment;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.text.TextUtils;
import android.util.Log;
import com.imagpay.emv.EMVApp;
import com.imagpay.emv.EMVConstants;
import com.imagpay.emv.EMVParam;
import com.imagpay.enums.Data_Mode;
import com.imagpay.enums.Work_Type;
import com.imagpay.mac.MacUtil;
import com.imagpay.utils.MessageDigestUtils;
import com.imagpay.utils.NfcCardUtils;
import com.imagpay.utils.RandomUtils;
import com.imagpay.utils.StringUtils;
import com.ivsign.android.IDCReader.IDUtil;
import com.ivsign.android.IDCReader.UserIDCardInfo;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;






@SuppressLint({"SimpleDateFormat", "DefaultLocale"})
public class Settings
{
  private static String TAG = "Settings";
  
  public static int SLOT_IC = 0;
  
  public static int SLOT_PSAM = 1;
  public static int SLOT_NFC = 5;
  
  public static String M1_TYPE_A = "0A";
  
  public static String M1_TYPE_B = "0B";
  
  public static String M1_TYPE_C = "0C";
  
  public static String M1_KEY_A = "41";
  
  public static String M1_KEY_B = "42";
  
  public static String AT_24C01 = "30";
  public static String AT_24C02 = "31";
  public static String AT_24C04 = "32";
  public static String AT_24C08 = "33";
  public static String AT_24C16 = "34";
  public static String AT_24C32 = "35";
  public static String AT_24C64 = "36";
  
  public static String PAD_ISO_FORMAT0 = "00";
  public static String PAD_ISO_FORMAT1 = "01";
  public static String PAD_ISO_FORMAT2 = "03";
  public static String PAD_KEY_TLK = "01";
  public static String PAD_KEY_TRK = "08";
  public static String PAD_KEY_TMK = "02";
  public static String PAD_KEY_TIK = "07";
  public static String PAD_KEY_TPK = "03";
  

  public static String TYPE_PLAINTEXT = "05";
  public static String TYPE_3DES = "06";
  
  private int prnFlag = 0;
  private String _sector = "00";
  
  private SwipeHandler _handler;
  
  private String _track1 = "";
  private String _track2 = "";
  private String _track3 = "";
  private String _pan = "";
  private String _serviceCode = "";
  private String _cardHolder = "";
  
  private int planlc;
  
  private int datelc;
  private int servicelc;
  private int cardholderlc;
  private int ksnlc;
  private int onelc;
  private int twolc;
  private int threeLc;
  private int qhtflc;
  private String one_length;
  private String two_length;
  private String three_length;
  private String pan;
  private String date;
  private String service_codes;
  private String cardholder;
  private String ksn;
  private String one;
  private String two;
  private String three;
  private boolean isDukpt = false;
  public static String MPOS_PLAIN_STATE = "Mpos_Plain_state";
  public static String MPOS_BPOS_STATE = "Mpos_BPos_state";
  public static String MPOS_DUKPT_STATE = "Mpos_dukpt_state";
  public static String MPOS_UNIONPAY_STATE = "Mpos_UnionPay_state";
  public static String MPOS_PRINT_ALIGN_LEFT = "Mpos_print_align_left";
  public static String MPOS_PRINT_ALIGN_CENTER = "Mpos_print_align_center";
  public static String MPOS_PRINT_ALIGN_RIGHT = "Mpos_print_align_right";
  public static String MPOS_PRINT_TEXT_NORMAL = "Mpos_print_text_normal";
  public static String MPOS_PRINT_TEXT_DOUBLE_HEIGHT = "Mpos_print_text_double_height";
  public static String MPOS_PRINT_TEXT_DOUBLE_WIDTH = "Mpos_print_text_double_width";
  public static String MPOS_PRINT_TEXT_DOUBLE_SIZE = "Mpos_print_text_double_size";
  public static String MPOS_PRINT_FONT_DEFAULT = "Mpos_print_font_default";
  public static String MPOS_PRINT_FONT_NEW = "Mpos_print_font_new";
  public static String MPOS_PRINT_FONT_LATIN = "Mpos_print_font_latin";
  private static final byte TRANS_CSU = 6;
  private static final byte TRANS_CSU_CPX = 9;
  private String defaultdata = "00000000000000000000000000000000";
  

  private static Settings instance = null;
  
  public Settings(SwipeHandler handler) {
    _handler = handler;
  }
  
  public Settings(SwipeHandler handler, Context context) {
    _handler = handler;
  }
  





  public static Settings getInstance(SwipeHandler handler)
  {
    if (instance == null) {
      synchronized (Settings.class) {
        if (instance == null) {
          instance = new Settings(handler);
        }
      }
    }
    return instance;
  }
  





  public String readVersion()
  {
    if (!checkReader())
      return null;
    String res = getResultWithData(getDataWithCipherCode(Constants.SYS_VER));
    if ((res != null) && (res.startsWith("00"))) {
      String[] ss = res.replaceAll("..", "$0 ").split(" ");
      StringBuffer sb = new StringBuffer();
      for (String str : ss) {
        sb.append((char)Integer.parseInt(str, 16));
      }
      return sb.toString();
    }
    return res;
  }
  
  public String readSN() {
    if (!checkReader())
      return null;
    String res = getDataWithCipherCode(Constants.SYS_RSN);
    if (res == null)
      return null;
    if (res.startsWith("00"))
      return res.substring(2);
    return null;
  }
  





  public boolean writeSN(String sn)
  {
    if (!checkReader())
      return false;
    String res = getDataWithCipherCode(Constants.SYS_WSN + sn);
    if (res == null)
      return false;
    if (res.startsWith("00"))
      return true;
    return false;
  }
  
  public boolean writeBeep() {
    if (!checkReader())
      return false;
    String res = getDataWithCipherCode(Constants.SYS_BEEP);
    if (res == null)
      return false;
    if (res.startsWith("00"))
      return true;
    return false;
  }
  

  public boolean write3DesKey(String oldKey, String newKey)
  {
    if (!checkReader())
      return false;
    String res = getDataWithCipherCode(Constants.SYS_3DES_KEY + oldKey + 
      newKey);
    if (res == null)
      return false;
    if (res.startsWith("00"))
      return true;
    return false;
  }
  
  public boolean writeWorkMode(String mode)
  {
    if (!checkReader())
      return false;
    String res = null;
    if (mode.equals(TYPE_3DES)) {
      res = getDataWithCipherCode(Constants.SYS_3DES);
    } else if (mode.equals(TYPE_PLAINTEXT)) {
      res = getDataWithCipherCode(Constants.SYS_PLAIN);
    } else
      return false;
    if (res == null)
      return false;
    if (res.startsWith("00"))
      return true;
    return false;
  }
  
  public boolean setBLEName(String name)
  {
    if ((!checkReader()) || (name == null) || (name.equals("")))
      return false;
    String res = getDataWithCipherCode(Constants.SYS_BLE_NAME + 
      getHexString(name.getBytes().length) + 
      StringUtils.convertBytesToHex(name.getBytes()));
    if ((res == null) || (!res.startsWith("00"))) {
      return false;
    }
    return true;
  }
  
  public boolean setBLEPass(String pass)
  {
    if ((!checkReader()) || (pass == null) || (pass.equals("")))
      return false;
    String res = getDataWithCipherCode(Constants.SYS_BLE_PASS + "04" + 
      StringUtils.convertBytesToHex(pass.getBytes()));
    if ((res == null) || (!res.startsWith("00"))) {
      return false;
    }
    return true;
  }
  
  public String getSysTime()
  {
    if (!checkReader())
      return null;
    String res = getDataWithCipherCode(Constants.SYS_R_TIME);
    if ((res == null) || (!res.startsWith("00")))
      return null;
    return res.substring(2);
  }
  
  public boolean setSysTime(String time)
  {
    if (!checkReader())
      return false;
    String res = getDataWithCipherCode(Constants.SYS_W_TIME + time);
    if ((res == null) || (!res.startsWith("00")))
      return false;
    return true;
  }
  
  public boolean magDebug()
  {
    if (!checkReader())
      return false;
    String res = getDataWithCipherCode(Constants.SYS_MAG_DEBUG);
    if (res == null)
      return false;
    if (res.length() < 2)
      return false;
    return true;
  }
  




  public String magOpen()
  {
    if (!checkReader())
      return null;
    return getDataWithCipherCode(Constants.MAG_OPEN);
  }
  




  public String magClose()
  {
    if (!checkReader())
      return null;
    return getDataWithCipherCode(Constants.MAG_CLOSE);
  }
  




  public String magSwipe()
  {
    if (!checkReader())
      return null;
    return getDataWithCipherCode(Constants.MAG_SWIPE);
  }
  




  public String magRead()
  {
    if (!checkReader())
      return null;
    _track1 = "";
    _track2 = "";
    _track3 = "";
    _pan = "";
    
    pan = "";
    service_codes = "";
    date = "";
    cardholder = "";
    ksn = "";
    onelc = 0;
    twolc = 0;
    threeLc = 0;
    one = "";
    two = "";
    three = "";
    isDukpt = false;
    String data = getDataWithCipherCode(Constants.MAG_READ);
    try {
      if ((data != null) && (
        (data.contains("51 48 54 46")) || 
        (data.contains("51485446")))) {
        isDukpt = true;
        if (TextUtils.isEmpty(data)) {
          return null;
        }
        length(data);
        hexString(data);
      }
    }
    catch (Exception localException) {}
    

    if ((data != null) && (data.length() < 42))
      return null;
    return data;
  }
  




  public String magReset()
  {
    if (!checkReader())
      return null;
    return getDataWithCipherCode(Constants.MAG_RESET);
  }
  




  public String icReset()
  {
    if (!checkReader())
      return null;
    return reset(SLOT_IC);
  }
  




  public String icOff()
  {
    if (!checkReader())
      return null;
    return off(SLOT_IC);
  }
  




  public String icDetect()
  {
    if (!checkReader())
      return null;
    return detect(SLOT_IC);
  }
  


  public String icCardNo()
  {
    return readCardNo(SLOT_IC);
  }
  


  public String nfcCardNo()
  {
    return readCardNo(SLOT_NFC);
  }
  
  private String readCardNo(int slot) {
    if (!checkReader()) {
      Log.d(TAG, "Device is disconnect!");
      return null;
    }
    try {
      String res = reset(slot);
      if (res == null) {
        Log.d(TAG, "IC reset fail!");
        return null;
      }
      Apdu_Send apdu_Send = new Apdu_Send(
        StringUtils.convertHexToBytes("00a40400"), (short)14, 
        
        StringUtils.convertHexToBytes("315041592e5359532e4444463031"), 
        (short)0);
      res = getDataWithAPDU(slot, apdu_Send);
      apdu_Send = new Apdu_Send(
        StringUtils.convertHexToBytes("00b2010c"), (short)0, 
        new byte['Ȁ'], (short)0);
      res = getDataWithAPDU(slot, apdu_Send);
      if (res != null) {
        int st = res.indexOf("4f");
        int len = Integer.parseInt(res.substring(st + 2, 16), 16);
        String aid = res.substring(st + 4, len * 2 + (st + 4));
        Log.d(TAG, "AID:" + aid);
        apdu_Send = new Apdu_Send(
          StringUtils.convertHexToBytes("00a40400"), (short)len, 
          StringUtils.convertHexToBytes(aid), (short)0);
        res = getDataWithAPDU(slot, apdu_Send);
        apdu_Send = new Apdu_Send(
          StringUtils.convertHexToBytes("00b2010c"), 
          (short)0, new byte['Ȁ'], (short)0);
        res = getDataWithAPDU(slot, apdu_Send);
        int start = res.indexOf("57");
        if (start > 0) {
          int end = res.indexOf("d");
          if (end < start) {
            end = Integer.parseInt(
              res.substring(start + 2, start + 4), 16);
            return res.substring(start + 4, start + 4 + end - 1);
          }
          return res.substring(start + 4, end);
        }
        apdu_Send = new Apdu_Send(
          StringUtils.convertHexToBytes("00b2020c"), 
          (short)0, new byte['Ȁ'], (short)0);
        res = getDataWithAPDU(slot, apdu_Send);
        start = res.indexOf("57");
        int end = res.indexOf("d");
        if (end < start) {
          end = Integer.parseInt(
            res.substring(start + 2, start + 4), 16);
          return res.substring(start + 4, start + 4 + end - 1);
        }
        return res.substring(start + 4, end);
      }
    }
    catch (Exception e) {
      Log.d(TAG, e.getMessage());
    }
    return null;
  }
  





  public String reset(int slot)
  {
    if (!checkReader())
      return null;
    return getResultWithData(getDataWithCipherCode(Constants.IC_RESET + 
      getHexString(slot)));
  }
  





  public String off(int slot)
  {
    if (!checkReader())
      return null;
    return getResultWithData(getDataWithCipherCode(Constants.IC_CLOSE + 
      getHexString(slot)));
  }
  





  public String detect(int slot)
  {
    if (!checkReader())
      return null;
    return getDataWithCipherCode(Constants.IC_DETECT + getHexString(slot));
  }
  





  public String getDataWithAPDU(Apdu_Send apdu)
  {
    return getDataWithAPDU(SLOT_IC, apdu);
  }
  






  public String getDataWithAPDU(int slot, Apdu_Send apdu)
  {
    return getDataWithAPDU(0, slot, apdu);
  }
  








  @Deprecated
  public String getDataWithAPDU(int type, int slot, Apdu_Send apdu)
  {
    return getResultWithData(_handler
      .getDataWithCipherCode(Constants.IC_APDU + 
      getAPDUString(type, slot, apdu)));
  }
  
  private String getAPDUString(int type, int slot, Apdu_Send apdu)
  {
    StringBuffer sb = new StringBuffer();
    sb.append(getHexString(slot));
    sb.append(StringUtils.convertBytesToHex(apdu.getCommand()));
    sb.append(getAPDUTotalLen(apdu.getLC()));
    byte[] data = new byte[apdu.getLC()];
    System.arraycopy(apdu.getDataIn(), 0, data, 0, apdu.getLC());
    sb.append(StringUtils.convertBytesToHex(data));
    sb.append(getAPDUTotalLen(apdu.getLE()));
    return sb.toString();
  }
  





  public String getDataWithAPDUForStr(String apdu)
  {
    return getDataWithAPDUForStr(SLOT_IC, apdu);
  }
  






  public String getDataWithAPDUForStr(int slot, String apdu)
  {
    return getDataWithAPDUForStr(0, slot, apdu);
  }
  








  @Deprecated
  public String getDataWithAPDUForStr(int type, int slot, String apdu)
  {
    return getResultWithData(_handler
      .getDataWithCipherCode(Constants.IC_APDU_TW + 
      getAPDUStringForTW(type, slot, apdu)));
  }
  
  private String getAPDUStringForTW(int type, int slot, String apdu)
  {
    StringBuffer sb = new StringBuffer();
    sb.append(getHexString(slot));
    sb.append(getHexString(apdu.length() / 2 / 255));
    sb.append(getHexString(apdu.length() / 2 % 255));
    sb.append(apdu);
    return sb.toString();
  }
  
  private String getAPDUTotalLen(int len) {
    StringBuffer sb = new StringBuffer();
    if (len <= 255) {
      sb.append("00");
      sb.append(StringUtils.convertBytesToHex(new byte[] { (byte)len }));
    } else {
      sb.append(
        StringUtils.convertBytesToHex(new byte[] { (byte)(len / 256) }));
      sb.append(
        StringUtils.convertBytesToHex(new byte[] { (byte)(len % 256) }));
    }
    return sb.toString();
  }
  




  public String m1Request()
  {
    if (!checkReader())
      return null;
    return m1Request(M1_TYPE_A);
  }
  






  public String m1Request(String type)
  {
    if (!checkReader())
      return null;
    return getResultWithData(_handler
      .getDataWithCipherCode(Constants.M1_REQUEST + type));
  }
  






  public boolean m1Select(String sn)
  {
    if (!checkReader()) {
      return false;
    }
    
    if (getResultWithData(_handler.getDataWithCipherCode(Constants.M1_SELECT + sn)).equals("00")) {
      return true;
    }
    return false;
  }
  








  public boolean m1Auth(String sector, String pass)
  {
    if (!checkReader())
      return false;
    return m1Auth(M1_TYPE_A, sector, pass);
  }
  









  public boolean m1Auth(String keyType, String sector, String pass)
  {
    if (!checkReader())
      return false;
    _sector = sector;
    int number = Integer.parseInt(sector, 16) * 4 + 
      Integer.parseInt("00", 16);
    String res = _handler.getDataWithCipherCode(Constants.M1_AUTH + keyType + 
      getHexString(number) + pass);
    if ((res != null) && (res.startsWith("00"))) {
      return true;
    }
    return false;
  }
  






  public String m1ReadBlock(String block)
  {
    if (!checkReader())
      return null;
    int number = Integer.parseInt(_sector, 16) * 4 + 
      Integer.parseInt(block, 16);
    return getResultWithData(_handler
      .getDataWithCipherCode(Constants.M1_READ_BLOCK + 
      getHexString(number)));
  }
  






  public String m1WriteBlock(String block, String data)
  {
    if ((!checkReader()) || (data == null) || (data.equals("")))
      return null;
    Log.d(TAG, "m1WriteBlock:" + data);
    int len = data.length();
    if (len < 32) {
      int _tmp = 32 - len;
      for (int i = 0; i < _tmp; i++) {
        data = data + "0";
      }
    } else if (len > 32) {
      data = data.substring(0, 32);
    }
    int number = Integer.parseInt(_sector, 16) * 4 + 
      Integer.parseInt(block, 16);
    return getResultWithData(_handler
      .getDataWithCipherCode(Constants.M1_WRITE_BLOCK + 
      getHexString(number) + data));
  }
  






  public String m1ReadSec(String pass, String sector)
  {
    if (!checkReader())
      return null;
    return m1ReadSec(M1_TYPE_A, M1_KEY_A, pass, sector);
  }
  









  public String m1ReadSec(String cardType, String keyType, String pass, String sector)
  {
    if (!checkReader())
      return null;
    if ((!cardType.equals(M1_TYPE_A)) && (!cardType.equals(M1_TYPE_B)) && 
      (!cardType.equals(M1_TYPE_C)))
      throw new IllegalArgumentException("M1 card type error");
    if ((!keyType.equals(M1_KEY_A)) && (!keyType.equals(M1_KEY_B)))
      throw new IllegalArgumentException("M1 key type error");
    if (pass.length() != 12)
      throw new IllegalArgumentException("M1 pass length error");
    m1Request(cardType);
    return getResultWithData(_handler
      .getDataWithCipherCode(Constants.M1_READ_SEC + sector + keyType + 
      pass));
  }
  







  public String m1WriteSec(String pass, String sector, String data)
  {
    if (!checkReader())
      return null;
    return m1WriteSec(M1_TYPE_A, M1_KEY_A, pass, sector, data);
  }
  










  public String m1WriteSec(String cardType, String keyType, String pass, String sector, String data)
  {
    if (!checkReader())
      return null;
    if ((!cardType.equals(M1_TYPE_A)) && (!cardType.equals(M1_TYPE_B)) && 
      (!cardType.equals(M1_TYPE_C)))
      throw new IllegalArgumentException("M1 card type error");
    if ((!keyType.equals(M1_KEY_A)) && (!keyType.equals(M1_KEY_B)))
      throw new IllegalArgumentException("M1 key type error");
    if (pass.length() != 12)
      throw new IllegalArgumentException("M1 pass length error");
    return getResultWithData(_handler
      .getDataWithCipherCode(Constants.M1_WRITE_SEC + sector + 
      keyType + pass + data));
  }
  







  public boolean m1WriteSecPass(String sector, String oldpass, String newPass)
  {
    if (!checkReader())
      return false;
    return m1WriteSecPass(sector, "0A", oldpass, "0A", newPass);
  }
  










  public boolean m1WriteSecPass(String sector, String oldKeyType, String oldpass, String newKeyType, String newPass)
  {
    String res = getDataWithCipherCode(Constants.M1_WRITE_PASS + sector + 
      oldKeyType + oldpass + newKeyType + newPass);
    if ((res != null) && (res.startsWith("00")))
      return true;
    return false;
  }
  
  public boolean m1PlusAmnt(String block, int amnt)
  {
    if (!checkReader())
      return false;
    int number = Integer.parseInt(_sector, 16) * 4 + 
      Integer.parseInt(block, 16);
    String res = getDataWithCipherCode(Constants.M1_PLUS_AMNT + 
      getHexString(number) + getHexString(number) + amntToHex(amnt));
    if ((res != null) && (res.startsWith("00")))
      return true;
    return false;
  }
  
  public boolean m1MinusAmnt(String block, int amnt)
  {
    if (!checkReader())
      return false;
    int number = Integer.parseInt(_sector, 16) * 4 + 
      Integer.parseInt(block, 16);
    String res = getDataWithCipherCode(Constants.M1_MINUS_AMNT + 
      getHexString(number) + getHexString(number) + amntToHex(amnt));
    if ((res != null) && (res.startsWith("00")))
      return true;
    return false;
  }
  
  private String amntToHex(int amnt)
  {
    StringBuffer sb = new StringBuffer();
    int len1 = amnt / 256 / 256 / 256;
    int len2 = amnt / 256 / 256;
    int len3 = amnt / 256;
    int len4 = amnt % 256;
    sb.append(getHexString(len4));
    sb.append(getHexString(len3));
    sb.append(getHexString(len2));
    sb.append(getHexString(len1));
    return sb.toString();
  }
  

  public String m1InitAmnt(String block)
  {
    String init = "00000000ffffffff0000000001fe01fe";
    return m1WriteBlock(block, init);
  }
  
  private String getResultWithData(String str) {
    if ((str == null) || (str.equals(""))) {
      Log.d(TAG, "getResultWithData:str is null");
      return null;
    }
    if ((str.startsWith("00")) && (str.length() > 2))
      return str.substring(2);
    if ((str.startsWith("00")) && (str.length() == 2)) {
      return str;
    }
    return null;
  }
  




  public String writeIDReset()
  {
    if (!checkReader())
      return null;
    return _handler.getDataWithCipherCode(Constants.ID_RESET);
  }
  





  public UserIDCardInfo writeIDRead()
  {
    return writeIDRead(null);
  }
  





  public UserIDCardInfo writeIDRead(String path)
  {
    if (!checkReader())
      return null;
    String res = _handler.getDataWithCipherCode(Constants.ID_READ);
    if (res.length() > 2590) {
      return IDUtil.getUserInfo(
        StringUtils.convertHexToBytes(res.substring(2)), path);
    }
    return null;
  }
  
  public String getIDData()
  {
    if (!checkReader())
      return null;
    String res = _handler.getDataWithCipherCode(Constants.ID_READ);
    if ((res != null) && (res.startsWith("00"))) {
      return res.substring(2);
    }
    return null;
  }
  




  public String writeIDOff()
  {
    if (!checkReader())
      return null;
    return _handler.getDataWithCipherCode(Constants.ID_OFF);
  }
  




  public String writeNewIDOpen()
  {
    if (!checkReader())
      return null;
    return _handler.getDataWithCipherCode(Constants.NEWID_OPEN);
  }
  




  public String writeNewIDReset()
  {
    if (!checkReader())
      return null;
    return _handler.getDataWithCipherCode(Constants.NEWID_RESET);
  }
  





  public UserIDCardInfo writeNewIDRead()
  {
    return writeNewIDRead(null);
  }
  





  public UserIDCardInfo writeNewIDRead(String path)
  {
    if (!checkReader())
      return null;
    String res = _handler.getDataWithCipherCode(Constants.NEWID_READ);
    if (res.length() > 2590) {
      return IDUtil.getUserInfo(
        StringUtils.convertHexToBytes(res.substring(2)), path);
    }
    return null;
  }
  
  public String getNewIDData()
  {
    if (!checkReader())
      return null;
    String res = _handler.getDataWithCipherCode(Constants.NEWID_READ);
    if ((res != null) && (res.startsWith("00"))) {
      return res.substring(2);
    }
    return null;
  }
  




  public String writeNewIDCLOSE()
  {
    if (!checkReader())
      return null;
    return _handler.getDataWithCipherCode(Constants.NEWID_CLOSE);
  }
  




  public String writeNewIDOff()
  {
    if (!checkReader())
      return null;
    return _handler.getDataWithCipherCode(Constants.NEWID_OFF);
  }
  




  public String writeGetPADData()
  {
    if (!checkReader())
      return null;
    return _handler.getDataWithCipherCode(Constants.PPINPUT + "0605");
  }
  

  public String getDataWithCipherCode(String cipherCode)
  {
    return _handler.getDataWithCipherCode(cipherCode);
  }
  
  public static String getHexString(int i) {
    String tmp = Integer.toHexString(i);
    if (tmp.length() == 1) {
      return "0" + tmp;
    }
    return tmp;
  }
  
  private boolean checkReader() {
    if ((_handler == null) || (!_handler.isConnected())) {
      return false;
    }
    return true;
  }
  
  public void prnDebug(String str)
  {
    if (!checkReader())
      return;
    if ((str == null) || (str.equals("")))
      throw new IllegalArgumentException("Print data is null!");
    _handler.setPrn(true);
    _handler.getDataWithCipherCode(unPackData(str));
    _handler.setPrn(false);
  }
  


  public boolean prnInit()
  {
    if (!checkReader())
      return false;
    String res = _handler.getDataWithCipherCode(Constants.PRINT_INIT);
    if (res == null)
      return false;
    if (res.startsWith("00")) {
      return true;
    }
    return false;
  }
  





  public boolean prnStep(int pixel)
  {
    if (!checkReader())
      return false;
    if ((pixel < 0) || (pixel > 255))
      throw new IllegalArgumentException(
        "Pixel must between at 0 and 255!");
    String res = _handler.getDataWithCipherCode(Constants.PRINT_STEP + 
      getHexString(pixel));
    if (res == null)
      return false;
    if (res.startsWith("00")) {
      return true;
    }
    return false;
  }
  


  public void prnStr(String str)
  {
    if (!checkReader())
      return;
    if ((str == null) || (str.equals("")))
      throw new IllegalArgumentException("Print data is null!");
    if (prnStart()) {
      _handler.setPrn(true);
      String ss = unPackData(str);
      
      prnStrOneByOne(ss);
      _handler.setPrn(false);
    }
  }
  
  private void prnStrOneByOne(String str)
  {
    int devil = 512;
    byte[] buffers = StringUtils.convertHexToBytes(str);
    if (buffers.length <= devil) {
      _handler.getDataWithCipherCode(str);
    }
    else {
      int m = buffers.length / devil;
      int n = buffers.length % devil;
      byte[] tmps = null;
      for (int i = 0; i < m; i++) {
        tmps = new byte[devil];
        System.arraycopy(buffers, i * devil, tmps, 0, devil);
        _handler.getDataWithCipherCode(
          StringUtils.convertBytesToHex(tmps));
        tmps = null;
        try {
          Thread.sleep(1500L);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
      tmps = new byte[n];
      System.arraycopy(buffers, m * devil, tmps, 0, n);
      _handler.getDataWithCipherCode(StringUtils.convertBytesToHex(tmps));
    }
  }
  





  public void prnImg(Bitmap bitmap)
  {
    if (!checkReader())
      return;
    if (prnStart()) {
      _handler.setPrn(true);
      printBitmap(bitmap);
      _handler.setPrn(false);
    }
  }
  
  public void prnConfig(byte[] cmds) {
    if (!checkReader())
      return;
    if (prnStart()) {
      _handler.setPrn(true);
      getDataWithCipherCode(StringUtils.convertBytesToHex(cmds));
      _handler.setPrn(false);
    }
  }
  
  private boolean prnStart()
  {
    if ((!_handler.isExit()) && (prnFlag != 0))
      return true;
    String res = _handler.getDataWithCipherCode(Constants.PRINT_STR);
    if ((res != null) && (res.startsWith("00"))) {
      prnFlag += 1;
      _handler.setExit(false);
      return true;
    }
    return false;
  }
  
  private String unPackData(String str)
  {
    long count = 0L;long j = 0L;
    StringBuffer sb = new StringBuffer();
    StringBuffer sb2 = new StringBuffer();
    try {
      byte[] bytes = str.getBytes("GBK");
      for (byte b : bytes) {
        sb2.append(StringUtils.convertBytesToHex(new byte[] { b }));
        if ((b > 128) || (b < 0)) {
          j += 1L;
          if (j > 1L) {
            count += 3L;
            j = 0L;
          }
        } else {
          count += 2L;
        }
        if ((count != 0L) && (count % 48L == 0L)) {
          sb.append(sb2.toString());
          
          sb2.setLength(0);
          count = 0L;
        }
      }
      if ((count != 0L) && (count % 48L != 0L)) {
        sb.append(sb2.toString()).append("0a");
        sb2.setLength(0);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return sb.toString();
  }
  
  private String unPackData(String str, String bianma)
  {
    long count = 0L;long j = 0L;
    StringBuffer sb = new StringBuffer();
    StringBuffer sb2 = new StringBuffer();
    try {
      byte[] bytes = str.getBytes(bianma);
      for (byte b : bytes) {
        sb2.append(StringUtils.convertBytesToHex(new byte[] { b }));
        if ((b > 128) || (b < 0)) {
          j += 1L;
          if (j > 1L) {
            count += 3L;
            j = 0L;
          }
        } else {
          count += 2L;
        }
        if ((count != 0L) && (count % 48L == 0L)) {
          sb.append(sb2.toString());
          
          sb2.setLength(0);
          count = 0L;
        }
      }
      if ((count != 0L) && (count % 48L != 0L)) {
        sb.append(sb2.toString()).append("0a");
        sb2.setLength(0);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return sb.toString();
  }
  
  private void printBitmap(Bitmap bitmap) {
    int width = bitmap.getWidth();
    int height = bitmap.getHeight();
    
    List<byte[]> datas = new ArrayList();
    byte[] command = { 29, 118, 48 };
    

    datas.add(command);
    int xl = (width % 8 == 0 ? width / 8 : width / 8 + 1) % 256;
    int xh = (width % 8 == 0 ? width / 8 : width / 8 + 1) / 256;
    int yl = height % 256;
    int yh = height / 256;
    
    datas.add(new byte[] { (byte)xl, (byte)xh, (byte)yl, (byte)yh });
    
    int zeroCount = width % 8;
    StringBuffer zero = new StringBuffer();
    if (zeroCount > 0) {
      for (int i = 0; i < 8 - zeroCount; i++) {
        zero.append("0");
      }
    }
    int idx = 0;
    byte[] data = null;
    
    StringBuffer sb = new StringBuffer();
    for (int i = 0; i < height; i++) {
      idx = 0;
      sb.setLength(0);
      data = new byte[width % 8 == 0 ? width / 8 : width / 8 + 1];
      for (int j = 0; j < width; j++) {
        int color = bitmap.getPixel(j, i);
        int r = color >> 16 & 0xFF;
        int g = color >> 8 & 0xFF;
        int b = color & 0xFF;
        

        if ((r > 200) || (g > 200) || (b > 200)) {
          sb.append("0");
        } else
          sb.append("1");
        if (sb.length() == 8) {
          data[(idx++)] = ((byte)Integer.parseInt(sb.toString(), 2));
          sb.setLength(0);
        }
      }
      if (zeroCount > 0) {
        sb.append(zero);
        data[(idx++)] = ((byte)Integer.parseInt(sb.toString(), 2));
      }
      
      datas.add(data);
    }
    


    int devil = 1024;
    byte[] buffers = sysCopy(datas);
    if (buffers.length <= devil) {
      getDataWithCipherCode(StringUtils.convertBytesToHex(buffers));
    }
    else {
      int m = buffers.length / devil;
      int n = buffers.length % devil;
      byte[] tmps = null;
      for (int i = 0; i < m; i++) {
        tmps = new byte[devil];
        System.arraycopy(buffers, i * devil, tmps, 0, devil);
        getDataWithCipherCode(StringUtils.convertBytesToHex(tmps));
        tmps = null;
        try {
          Thread.sleep(50L);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
      tmps = new byte[n];
      System.arraycopy(buffers, m * devil, tmps, 0, n);
      getDataWithCipherCode(StringUtils.convertBytesToHex(tmps));
    }
    
    try
    {
      Thread.sleep(25L);
    }
    catch (Exception localException) {}
  }
  
  private static byte[] sysCopy(List<byte[]> srcArrays) {
    int len = 0;
    for (byte[] srcArray : srcArrays) {
      len += srcArray.length;
    }
    byte[] destArray = new byte[len];
    int destLen = 0;
    for (byte[] srcArray : srcArrays) {
      System.arraycopy(srcArray, 0, destArray, destLen, srcArray.length);
      destLen += srcArray.length;
    }
    return destArray;
  }
  



  public boolean lcdClear(int startLine, int endLine)
  {
    if (!checkReader())
      return false;
    if ((startLine < 0) || (startLine > 1) || (endLine < 0) || (endLine > 1) || 
      (startLine > endLine))
      throw new IllegalArgumentException("Parameters is unvailable!");
    String res = _handler.getDataWithCipherCode(Constants.LCD_CLEAR + 
      getHexString(startLine) + getHexString(endLine));
    if (res == null)
      return false;
    if (res.startsWith("00")) {
      return true;
    }
    return false;
  }
  
  public boolean lcdBacklightSet(int type) {
    if (!checkReader())
      return false;
    if ((type < 0) || (type > 2))
      throw new IllegalArgumentException(
        "Type should between at 0 and 2!");
    String res = _handler.getDataWithCipherCode(Constants.LCD_LIGHT + 
      getHexString(type));
    if (res == null)
      return false;
    if (res.startsWith("00")) {
      return true;
    }
    return false;
  }
  
  public boolean lcdShowStr(int rowNum, int colNum, String str) {
    if (!checkReader())
      return false;
    if ((colNum < 0) || (colNum > 15) || (rowNum < 0) || (rowNum > 1)) {
      throw new IllegalArgumentException("Parameters is unvailable!");
    }
    try {
      String res = _handler.getDataWithCipherCode(Constants.LCD_SHOW + 
        getHexString(colNum) + getHexString(rowNum) + 
        StringUtils.convertBytesToHex(str.getBytes("GBK")));
      if (res == null)
        return false;
      if (res.startsWith("00")) {
        return true;
      }
      return false;
    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
    }
    return false;
  }
  



  public String dfSelect()
  {
    if (!checkReader())
      return null;
    String res = _handler.getDataWithCipherCode(Constants.DF_SELECT);
    if (res == null)
      return null;
    if (res.startsWith("00")) {
      return res.substring(2);
    }
    return res.substring(0, 2);
  }
  
  public boolean dfReset() {
    if (!checkReader())
      return false;
    String res = _handler.getDataWithCipherCode(Constants.DF_RESET);
    if (res == null)
      return false;
    if (res.startsWith("00")) {
      return true;
    }
    return false;
  }
  


  public boolean dfPSE(String aid)
  {
    if (!checkReader())
      return false;
    if ((aid == null) || (aid.length() != 6))
      throw new IllegalArgumentException(
        "AID length is error,must be 6 digits.");
    String res = _handler.getDataWithCipherCode(Constants.DF_PSE + aid);
    if (res == null)
      return false;
    byte[] tmp = StringUtils.convertHexToBytes(res);
    if ((tmp[0] == 0) && (tmp[2] == 0))
      return true;
    return false;
  }
  











  public boolean dfCreatAID(String aid, String conf, int num, String key)
  {
    if (!checkReader())
      return false;
    String res = _handler.getDataWithCipherCode(Constants.DF_CREAT_AID + 
      aid + conf + getHexString(num) + key);
    if (res == null)
      return false;
    byte[] tmp = StringUtils.convertHexToBytes(res);
    if ((tmp[0] == 0) && (tmp[2] == 0))
      return true;
    Log.d(TAG, "dfCreatAID:" + res);
    return false;
  }
  









  public boolean dfModifyKEY(String keyIndex, String newKey, String oldKey)
  {
    if (!checkReader())
      return false;
    String res = _handler.getDataWithCipherCode(Constants.DF_MODIFY_KEY + 
      keyIndex + newKey + oldKey);
    if (res == null)
      return false;
    byte[] tmp = StringUtils.convertHexToBytes(res);
    if ((tmp[0] == 0) && (tmp[2] == 0))
      return true;
    return false;
  }
  










  public boolean dfAddSTD(String config, String size, String key)
  {
    if (!checkReader())
      return false;
    String res = _handler.getDataWithCipherCode(Constants.DF_ADD_STD + 
      config + size + key);
    if (res == null)
      return false;
    byte[] tmp = StringUtils.convertHexToBytes(res);
    if ((tmp[0] == 0) && (tmp[2] == 0))
      return true;
    return false;
  }
  














  public boolean dfAddVALUE(String config, String low, String high, String init, String key)
  {
    if (!checkReader())
      return false;
    String res = _handler.getDataWithCipherCode(Constants.DF_ADD_VALUE + 
      config + low + high + init + "00" + key);
    if (res == null)
      return false;
    byte[] tmp = StringUtils.convertHexToBytes(res);
    if ((tmp[0] == 0) && (tmp[2] == 0))
      return true;
    return false;
  }
  










  public boolean dfAddBACKUP(String config, String size, String key)
  {
    if (!checkReader())
      return false;
    String res = _handler.getDataWithCipherCode(Constants.DF_ADD_BACKUP + 
      config + size + key);
    if (res == null)
      return false;
    byte[] tmp = StringUtils.convertHexToBytes(res);
    if ((tmp[0] == 0) && (tmp[2] == 0))
      return true;
    return false;
  }
  













  public boolean dfAddLINEAR(String config, String recSize, String recNum, String key)
  {
    if (!checkReader())
      return false;
    String res = _handler.getDataWithCipherCode(Constants.DF_ADD_LINEAR + 
      config + recSize + recNum + key);
    if (res == null)
      return false;
    byte[] tmp = StringUtils.convertHexToBytes(res);
    if ((tmp[0] == 0) && (tmp[2] == 0))
      return true;
    return false;
  }
  













  public boolean dfAddCYCLIC(String config, String recSize, String recNum, String key)
  {
    if (!checkReader())
      return false;
    String res = _handler.getDataWithCipherCode(Constants.DF_ADD_CYCLIC + 
      config + recSize + recNum + key);
    if (res == null)
      return false;
    byte[] tmp = StringUtils.convertHexToBytes(res);
    if ((tmp[0] == 0) && (tmp[2] == 0))
      return true;
    return false;
  }
  





  public boolean dfDelFile(String fileIndex)
  {
    if (!checkReader())
      return false;
    String res = _handler.getDataWithCipherCode(Constants.DF_DEL_FILE + 
      fileIndex);
    if (res == null)
      return false;
    byte[] tmp = StringUtils.convertHexToBytes(res);
    if ((tmp[0] == 0) && (tmp[2] == 0))
      return true;
    return false;
  }
  














  public boolean dfWSTDFile(String fileIndex, String offSet, String length, String key, String data)
  {
    if (!checkReader())
      return false;
    String res = _handler.getDataWithCipherCode(Constants.DF_W_STD + 
      fileIndex + offSet + length + key + data);
    if (res == null)
      return false;
    byte[] tmp = StringUtils.convertHexToBytes(res);
    if ((tmp[0] == 0) && (tmp[2] == 0))
      return true;
    return false;
  }
  












  public boolean dfPlusVALUE(String fileIndex, String value, String appKey, String fileKey)
  {
    if (!checkReader())
      return false;
    String res = _handler.getDataWithCipherCode(Constants.DF_PLUS_VALUE + 
      fileIndex + value + appKey + fileKey);
    if (res == null)
      return false;
    byte[] tmp = StringUtils.convertHexToBytes(res);
    if ((tmp[0] == 0) && (tmp[2] == 0))
      return true;
    return false;
  }
  












  public boolean dfMinusVALUE(String fileIndex, String value, String appKey, String fileKey)
  {
    if (!checkReader())
      return false;
    String res = _handler.getDataWithCipherCode(Constants.DF_MINUS_VALUE + 
      fileIndex + value + appKey + fileKey);
    if (res == null)
      return false;
    byte[] tmp = StringUtils.convertHexToBytes(res);
    if ((tmp[0] == 0) && (tmp[2] == 0))
      return true;
    return false;
  }
  









  public String dfReadVALUE(String fileIndex, String appKey, String fileKey)
  {
    if (!checkReader())
      return null;
    String res = _handler.getDataWithCipherCode(Constants.DF_R_VALUE + 
      fileIndex + appKey + fileKey);
    if (res == null)
      return null;
    byte[] tmp = StringUtils.convertHexToBytes(res);
    if ((tmp[0] == 0) && (tmp[2] == 0))
      return res.substring(6);
    return null;
  }
  














  public String dfReadSTD(String fileIndex, String offSet, String length, String appKey, String fileKey)
  {
    if (!checkReader())
      return null;
    String res = _handler.getDataWithCipherCode(Constants.DF_R_STD + 
      fileIndex + offSet + length + fileKey + appKey);
    if (res == null)
      return null;
    byte[] tmp = StringUtils.convertHexToBytes(res);
    if ((tmp[0] == 0) && (tmp[2] == 0))
      return res.substring(2);
    return null;
  }
  
















  public boolean dfWRecord(String fileIndex, String offSet, String length, String appKey, String fileKey, String data)
  {
    if (!checkReader())
      return false;
    String res = _handler.getDataWithCipherCode(Constants.DF_W_RECORD + 
      fileIndex + offSet + length + appKey + fileKey + data);
    if (res == null)
      return false;
    byte[] tmp = StringUtils.convertHexToBytes(res);
    if ((tmp[0] == 0) && (tmp[2] == 0))
      return true;
    return false;
  }
  














  public String dfRRecord(String fileIndex, String offSet, String length, String appKey, String fileKey)
  {
    if (!checkReader())
      return null;
    String res = _handler.getDataWithCipherCode(Constants.DF_R_STD + 
      fileIndex + offSet + length + fileKey + appKey);
    if (res == null)
      return null;
    if (res.startsWith("00"))
      return res.substring(2);
    return null;
  }
  





  public String dfRKeyConf(String appKey)
  {
    if (!checkReader())
      return null;
    String res = _handler.getDataWithCipherCode(Constants.DF_R_KEYCONF + 
      appKey);
    if (res == null)
      return null;
    byte[] tmp = StringUtils.convertHexToBytes(res);
    if ((tmp[0] == 0) && (tmp[2] == 0))
      return res.substring(6);
    return null;
  }
  







  public String dfRFileConf(String fileIndex, String appKey)
  {
    if (!checkReader())
      return null;
    String res = _handler.getDataWithCipherCode(Constants.DF_R_FILECONF + 
      fileIndex + appKey);
    if (res == null)
      return null;
    byte[] tmp = StringUtils.convertHexToBytes(res);
    if ((tmp[0] == 0) && (tmp[2] == 0)) {
      return res.substring(6);
    }
    






    return null;
  }
  









  public boolean dfWFileConf(String config, String appKey, String fileKey)
  {
    if (!checkReader())
      return false;
    String res = _handler.getDataWithCipherCode(Constants.DF_W_FILECONF + 
      config + fileKey + appKey);
    if (res == null)
      return false;
    byte[] tmp = StringUtils.convertHexToBytes(res);
    if ((tmp[0] == 0) && (tmp[2] == 0))
      return true;
    return false;
  }
  







  public boolean dfDelApp(String aid, String appKey)
  {
    if (!checkReader())
      return false;
    String res = _handler.getDataWithCipherCode(Constants.DF_DEL_APP + aid + 
      appKey);
    if (res == null)
      return false;
    byte[] tmp = StringUtils.convertHexToBytes(res);
    if ((tmp[0] == 0) && (tmp[2] == 0))
      return true;
    return false;
  }
  





  public boolean dfFormatPICC(String appKey)
  {
    if (!checkReader())
      return false;
    String res = _handler.getDataWithCipherCode(Constants.DF_F_PICC + 
      appKey);
    if (res == null)
      return false;
    byte[] tmp = StringUtils.convertHexToBytes(res);
    if ((tmp[0] == 0) && (tmp[2] == 0))
      return true;
    return false;
  }
  




  public String dfGetKeyVer(String keyNum)
  {
    if (!checkReader())
      return null;
    String res = _handler.getDataWithCipherCode(Constants.DF_R_KEYVER + 
      keyNum);
    if (res == null)
      return null;
    byte[] tmp = StringUtils.convertHexToBytes(res);
    if ((tmp[0] == 0) && (tmp[2] == 0))
      return res.substring(6);
    return null;
  }
  





  public String dfGetKeyAIDS(String appKey)
  {
    if (!checkReader())
      return null;
    String res = _handler.getDataWithCipherCode(Constants.DF_R_AIDS + 
      appKey);
    if (res == null)
      return null;
    byte[] tmp = StringUtils.convertHexToBytes(res);
    if ((tmp[0] == 0) && (tmp[2] == 0))
      return res.substring(6);
    return null;
  }
  


  public String dfGetInfo()
  {
    if (!checkReader())
      return null;
    String res = _handler.getDataWithCipherCode(Constants.DF_R_INFO);
    if (res == null)
      return null;
    if (res.startsWith("00"))
      return res.substring(2);
    return null;
  }
  





  public String dfGetFileIDS(String appKey)
  {
    if (!checkReader())
      return null;
    String res = _handler.getDataWithCipherCode(Constants.DF_R_FILEIDS + 
      appKey);
    if (res == null)
      return null;
    byte[] tmp = StringUtils.convertHexToBytes(res);
    if ((tmp[0] == 0) && (tmp[2] == 0))
      return res.substring(6);
    return null;
  }
  



  public String felicaRequest()
  {
    if (!checkReader())
      return null;
    String res = _handler.getDataWithCipherCode(Constants.FLICA_REQUEST);
    if (res == null)
      return null;
    if (res.startsWith("00"))
      return res.substring(2);
    return null;
  }
  





  public String felicaSendCmds(String cmd)
  {
    if (!checkReader())
      return null;
    String res = _handler.getDataWithCipherCode(Constants.FLICA_CMDS + cmd);
    if (res == null)
      return null;
    if (res.startsWith("00"))
      return res.substring(2);
    return null;
  }
  


  public String ulRequest()
  {
    if (!checkReader())
      return null;
    String res = _handler.getDataWithCipherCode(Constants.M1_REQUEST + 
      M1_TYPE_A);
    if ((res == null) || (!res.startsWith("00")))
      return null;
    return res.substring(2);
  }
  
  public boolean ulSelect(String sn)
  {
    if (!checkReader())
      return false;
    String res = _handler.getDataWithCipherCode(Constants.M1_SELECT + sn);
    if ((res == null) || (!res.startsWith("00")))
      return false;
    return true;
  }
  







  public boolean ulWritePage(String page, String data)
  {
    if (!checkReader())
      return false;
    String res = _handler.getDataWithCipherCode(Constants.M1_WRITE_BLOCK + 
      page + data);
    if ((res == null) || (!res.startsWith("00")))
      return false;
    return true;
  }
  
  public String ulReadPage(String page)
  {
    if (!checkReader())
      return null;
    int number = Integer.parseInt(_sector, 16) * 4 + 
      Integer.parseInt(page, 16);
    String res = _handler.getDataWithCipherCode(Constants.M1_READ_BLOCK + 
      getHexString(number));
    if ((res == null) || (!res.startsWith("00")))
      return null;
    return res.substring(2, 10);
  }
  

  public String typeBRequst()
  {
    if (!checkReader())
      return null;
    String res = _handler.getDataWithCipherCode(Constants.TB_REQUEST);
    if ((res == null) || (!res.startsWith("00")))
      return null;
    return res.substring(2);
  }
  
  public boolean typeBATTRIB()
  {
    if (!checkReader())
      return false;
    String res = _handler.getDataWithCipherCode(Constants.TB_ATTRIB);
    if ((res == null) || (!res.startsWith("00")))
      return false;
    return true;
  }
  
  public String typeBApdu(Apdu_Send apdu)
  {
    return getResultWithData(_handler
      .getDataWithCipherCode(Constants.TB_APDU + 
      getAPDUString(0, 5, apdu)));
  }
  
  public boolean typeBHalt()
  {
    if (!checkReader())
      return false;
    String res = _handler.getDataWithCipherCode(Constants.TB_HALT);
    if ((res == null) || (!res.startsWith("00")))
      return false;
    return true;
  }
  

  public boolean sle4442Init()
  {
    if (!checkReader())
      return false;
    String res = _handler.getDataWithCipherCode(Constants.INIT_4442 + "00");
    if ((res == null) || (!res.startsWith("00")))
      return false;
    return true;
  }
  
  public String sle4442SRD(int offSet, int length)
  {
    if (!checkReader())
      return null;
    String res = _handler.getDataWithCipherCode(Constants.SRD_4442 + "00" + 
      getHexString(offSet / 256) + getHexString(offSet % 256) + 
      getHexString(length / 256) + getHexString(length % 256));
    if ((res == null) || (!res.startsWith("00")))
      return null;
    return res.substring(2);
  }
  
  public boolean sle4442SWR(int offSet, int length, String data)
  {
    if (!checkReader())
      return false;
    String res = _handler.getDataWithCipherCode(Constants.SWR_4442 + "00" + 
      getHexString(offSet / 256) + getHexString(offSet % 256) + 
      getHexString(length / 256) + getHexString(length % 256) + 
      data);
    if ((res == null) || (!res.startsWith("00")))
      return false;
    return true;
  }
  
  public boolean sle4442CSC(String key)
  {
    if (!checkReader())
      return false;
    String res = _handler.getDataWithCipherCode(Constants.CSC_4442 + "00" + 
      "0003" + key);
    if ((res == null) || (!res.startsWith("00")))
      return false;
    return true;
  }
  
  public String sle4442RSC()
  {
    if (!checkReader())
      return null;
    String res = _handler.getDataWithCipherCode(Constants.RSC_4442 + "00" + 
      "0003");
    if ((res == null) || (!res.startsWith("00")))
      return null;
    return res.substring(2);
  }
  
  public boolean sle4442WSC(String key)
  {
    if (!checkReader())
      return false;
    String res = _handler.getDataWithCipherCode(Constants.WSC_4442 + "00" + 
      "0003" + key);
    if ((res == null) || (!res.startsWith("00")))
      return false;
    return true;
  }
  
  public String sle4442RSTC()
  {
    if (!checkReader())
      return null;
    String res = _handler.getDataWithCipherCode(Constants.RSTC_4442 + "00");
    if ((res == null) || (!res.startsWith("00")))
      return null;
    return res.substring(2);
  }
  
  public String sle4442PRD()
  {
    if (!checkReader())
      return null;
    String res = _handler.getDataWithCipherCode(Constants.PRD_4442 + "00" + 
      "0004");
    if ((res == null) || (!res.startsWith("00")))
      return null;
    return res.substring(2);
  }
  
  public boolean sle4442PWR(int offSet, int length, String data)
  {
    if (!checkReader())
      return false;
    String res = _handler.getDataWithCipherCode(Constants.PWR_4442 + "00" + 
      getHexString(offSet / 256) + getHexString(offSet % 256) + 
      getHexString(length / 256) + getHexString(length % 256) + 
      data);
    if ((res == null) || (!res.startsWith("00")))
      return false;
    return true;
  }
  

  public boolean sle4428Init()
  {
    if (!checkReader())
      return false;
    String res = _handler.getDataWithCipherCode(Constants.INIT_4428 + "00");
    if ((res == null) || (!res.startsWith("00")))
      return false;
    return true;
  }
  
  public String sle4428SRD(int offSet, int length)
  {
    if (!checkReader())
      return null;
    String res = _handler.getDataWithCipherCode(Constants.SRD_4428 + "00" + 
      getHexString(offSet / 256) + getHexString(offSet % 256) + 
      getHexString(length / 256) + getHexString(length % 256));
    if ((res == null) || (!res.startsWith("00")))
      return null;
    return res.substring(2);
  }
  
  public boolean sle4428SWR(int offSet, int length, String data)
  {
    if (!checkReader())
      return false;
    String res = _handler.getDataWithCipherCode(Constants.SWR_4428 + "00" + 
      getHexString(offSet / 256) + getHexString(offSet % 256) + 
      getHexString(length / 256) + getHexString(length % 256) + 
      data);
    if ((res == null) || (!res.startsWith("00")))
      return false;
    return true;
  }
  
  public boolean sle4428CSC(String key)
  {
    if (!checkReader())
      return false;
    String res = _handler.getDataWithCipherCode(Constants.CSC_4428 + "00" + 
      "0002" + key);
    if ((res == null) || (!res.startsWith("00")))
      return false;
    return true;
  }
  
  public String sle4428RSC()
  {
    if (!checkReader())
      return null;
    String res = _handler.getDataWithCipherCode(Constants.RSC_4428 + "00" + 
      "0002");
    if ((res == null) || (!res.startsWith("00")))
      return null;
    return res.substring(2);
  }
  
  public boolean sle4428WSC(String key)
  {
    if (!checkReader())
      return false;
    String res = _handler.getDataWithCipherCode(Constants.WSC_4428 + "00" + 
      "0002" + key);
    if ((res == null) || (!res.startsWith("00")))
      return false;
    return true;
  }
  
  public String sle4428RSTC()
  {
    if (!checkReader())
      return null;
    String res = _handler.getDataWithCipherCode(Constants.RSTC_4442 + "00");
    if ((res == null) || (!res.startsWith("00")))
      return null;
    return res.substring(2);
  }
  
  public String sle4428PRD(int offSet, int length)
  {
    if (!checkReader())
      return null;
    String res = _handler.getDataWithCipherCode(Constants.PRD_4428 + "00" + 
      getHexString(offSet / 256) + getHexString(offSet % 256) + 
      getHexString(length / 256) + getHexString(length % 256));
    if ((res == null) || (!res.startsWith("00")))
      return null;
    return res.substring(2);
  }
  
  public boolean sle4428PWR(int offSet, int length, String data)
  {
    if (!checkReader())
      return false;
    String res = _handler.getDataWithCipherCode(Constants.PWR_4428 + "00" + 
      getHexString(offSet / 256) + getHexString(offSet % 256) + 
      getHexString(length / 256) + getHexString(length % 256) + 
      data);
    if ((res == null) || (!res.startsWith("00")))
      return false;
    return true;
  }
  
  public boolean at24Reset()
  {
    if (!checkReader())
      return false;
    String res = _handler
      .getDataWithCipherCode(Constants.AT24_RESET + "00");
    if ((res == null) || (!res.startsWith("00")))
      return false;
    return true;
  }
  
  public String at24Read(int offSet, int length, String type) {
    if (!checkReader())
      return null;
    String res = _handler.getDataWithCipherCode(Constants.AT24_READ + "00" + 
      getHexString(offSet / 256) + getHexString(offSet % 256) + 
      getHexString(length / 256) + getHexString(length % 256) + 
      type);
    if ((res == null) || (!res.startsWith("00")))
      return null;
    return res.substring(2);
  }
  
  public boolean at24Write(int offSet, int length, String type, String data) {
    if (!checkReader())
      return false;
    String res = _handler.getDataWithCipherCode(Constants.AT24_WRITE + "00" + 
      getHexString(offSet / 256) + getHexString(offSet % 256) + 
      getHexString(length / 256) + getHexString(length % 256) + 
      type + data);
    if ((res == null) || (!res.startsWith("00")))
      return false;
    return true;
  }
  






  public String padInput(int timeOut)
  {
    if (!checkReader())
      return null;
    String res = getDataWithCipherCode(Constants.PPINPUT + 
      getHexString(timeOut));
    return res;
  }
  




  public String padEncryptInput(int pinLen, String cardNo)
  {
    if (!checkReader())
      return null;
    int len = 2;int panLen = 0;
    if ((cardNo != null) && (cardNo.length() >= 16)) {
      cardNo = 
        cardNo.substring(cardNo.length() - 13, cardNo.length() - 1);
      len = len + 12 + 1;
      panLen = 12;
    } else {
      cardNo = "";
      len++;
      panLen = 0;
    }
    len += 4;
    if (!cardNo.equals("")) {
      cardNo = cardNo.replaceAll("", "$03");
      cardNo = cardNo.substring(0, cardNo.length() - 1);
    }
    String res = getDataWithCipherCode(Constants.PPEINPUT + "01" + 
      getHexString(pinLen) + "00" + "30" + getHexString(panLen) + 
      cardNo);
    return res;
  }
  
  public String padInput()
  {
    return padInput(30);
  }
  


  public String padGetKeyValue()
  {
    if (!checkReader())
      return null;
    String res = getDataWithCipherCode(Constants.PPGETKEY);
    return res;
  }
  


  public String padClose()
  {
    if (!checkReader())
      return null;
    String res = getDataWithCipherCode(Constants.PPCLOSE);
    return res;
  }
  



  protected String padGetPWD(int keyId, int min, int max, String mode, int timeOut, int len, String cardNo)
  {
    if (!checkReader())
      return null;
    String res = getDataWithCipherCode(Constants.PPGETPWD + 
      getHexString(keyId) + getHexString(min) + getHexString(max) + 
      mode + getHexString(timeOut) + getHexString(len) + cardNo);
    return res;
  }
  
  public String padGetPWD() {
    if (!checkReader())
      return null;
    String res = getDataWithCipherCode(Constants.PPGETPWD);
    return res;
  }
  
  public boolean padDelKey()
  {
    if (!checkReader())
      return false;
    String res = getDataWithCipherCode(Constants.PPDELKEY);
    if ((res == null) || (!res.startsWith("00")))
      return false;
    return true;
  }
  
  protected String padGetKSN()
  {
    if (!checkReader())
      return null;
    String res = getDataWithCipherCode(Constants.PPKSN);
    if ((res == null) || (!res.startsWith("00")))
      return null;
    return res.substring(2);
  }
  
  protected String padEncryData(String mode, String data)
  {
    if (!checkReader())
      return null;
    String res = getDataWithCipherCode(Constants.PPDUKPT + mode + data);
    if ((res == null) || (!res.startsWith("00")))
      return null;
    return res.substring(2);
  }
  










  public boolean padWritePKey(String type, String auKey, String key)
  {
    String tmp = "";String res = "";
    try
    {
      tmp = MessageDigestUtils.encodeTripleDES("0000000000000000", key);
      if (type.equals(PAD_KEY_TLK)) {
        res = getDataWithCipherCode(Constants.PPWRITEPKEY + type + key + 
          tmp.substring(0, 8));
      } else if (type.equals(PAD_KEY_TRK))
        res = getDataWithCipherCode(Constants.PPWRITEPKEY + type + 
          auKey + key + tmp.substring(0, 8));
      if ((res == null) || (!res.startsWith("00")))
        return false;
      return true;
    } catch (Exception e) {
      e.printStackTrace();
    }
    return false;
  }
  

  public boolean padWriteEKey(String key, String srcKeyType, int srcKeyIdx, String dstKeyType, int dstkeyIdx, int dstKeyLen, String dstKey, int kcvMode, String ksn)
  {
    if (!checkReader())
      return false;
    StringBuffer sb = new StringBuffer();
    String keyhead = "";String keyblock = "";
    String tmp = "0000000000000000";
    sb.append(Constants.PPWRITEEKEY).append(srcKeyType);
    sb.append(getHexString(srcKeyIdx)).append(dstKeyType)
      .append(getHexString(dstkeyIdx));
    keyhead = srcKeyType + getHexString(srcKeyIdx) + dstKeyType + 
      getHexString(dstkeyIdx);
    
    keyblock = getHexString(dstKeyLen) + dstKey;
    if (dstKeyLen != 24) {
      keyblock = keyblock + tmp;
    }
    keyblock = keyblock + getHexString(kcvMode);
    try {
      if (kcvMode != 0) {
        String res = MessageDigestUtils.encodeTripleDES(tmp, dstKey);
        keyblock = keyblock + res.substring(0, 8);
      } else {
        keyblock = keyblock + "00000000";
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    if (dstKeyType.equals("07")) {
      keyblock = keyblock + ksn;
    } else
      keyblock = keyblock + tmp + "0000";
    byte[] result = MacUtil.getKBCV(StringUtils.convertHexToBytes(key), 
      StringUtils.convertHexToBytes(keyhead), 
      StringUtils.convertHexToBytes(keyblock));
    sb.append(StringUtils.convertBytesToHex(result));
    String res = getDataWithCipherCode(sb.toString());
    if ((res == null) || (!res.startsWith("00")))
      return false;
    return true;
  }
  

  public String mposLogin(String code, String pass)
  {
    if ((!checkReader()) || (code.equals("")) || (pass.equals("")))
      return null;
    String res = "";
    if (code.equals("00")) {
      res = getDataWithCipherCode(Constants.PPMLOGIN + 
        StringUtils.convertStringToHex(pass));
    } else
      res = getDataWithCipherCode(Constants.PPULOGIN + code + 
        StringUtils.convertStringToHex(pass));
    if (res != null)
      return res;
    return null;
  }
  
  public boolean mposWritePass(String code, String pass)
  {
    if ((!checkReader()) || (code.equals("")) || (pass.equals("")))
      return false;
    String res = "";
    if (code.equals("00")) {
      res = getDataWithCipherCode(Constants.PPMWPASS + 
        StringUtils.convertStringToHex(pass));
    } else
      res = getDataWithCipherCode(Constants.PPUWPASS + code + 
        StringUtils.convertStringToHex(pass));
    if ((res != null) && (res.startsWith("00")))
      return true;
    return false;
  }
  
  public String mposAddUser()
  {
    if (!checkReader())
      return null;
    String res = getDataWithCipherCode(Constants.PPMADDU);
    if ((res != null) && (res.startsWith("00")))
      return res.substring(2, 4);
    return null;
  }
  
  public boolean mposDelUser(String code)
  {
    if ((!checkReader()) || (code.equals("")) || (code.length() != 2))
      return false;
    String res = getDataWithCipherCode(Constants.PPMDELU + code);
    if ((res != null) && (res.startsWith("00")))
      return true;
    return false;
  }
  
  public boolean mposSignIn(String data)
  {
    if ((!checkReader()) || (data.length() != 120))
      return false;
    String res = getDataWithCipherCode(Constants.PPSIGN + data);
    if ((res != null) && (res.startsWith("00")))
      return true;
    return false;
  }
  
  public String mposSignStatus()
  {
    if (!checkReader())
      return null;
    String res = getDataWithCipherCode(Constants.PPSIGNSTATUS);
    if ((res != null) && (res.startsWith("01")))
      return res;
    return null;
  }
  
  public boolean mposSignOut()
  {
    if (!checkReader())
      return false;
    String res = getDataWithCipherCode(Constants.PPEXIT);
    if ((res != null) && (res.startsWith("00")))
      return true;
    return false;
  }
  
  public boolean mposLoadParameters(String pass)
  {
    if (!checkReader())
      return false;
    String res = getDataWithCipherCode(Constants.PPLOAD + 
      StringUtils.convertStringToHex(pass));
    if ((res != null) && (res.startsWith("00")))
      return true;
    return false;
  }
  
  public String mposGetMacBlock(String data)
  {
    if (!checkReader())
      return null;
    StringBuffer sb = new StringBuffer(data);
    if (sb.length() % 16 != 0) {
      int tmp = 16 - sb.length() % 16;
      for (int i = 0; i < tmp / 2; i++) {
        sb.append("00");
      }
    }
    byte[] datas = StringUtils.convertHexToBytes(sb.toString());
    int l = datas.length / 8;
    for (int i = 1; i < l; i++) {
      for (int j = 0; j < 8; j++) {
        int tmp93_91 = j; byte[] tmp93_90 = datas;tmp93_90[tmp93_91] = ((byte)(tmp93_90[tmp93_91] ^ datas[(j + i * 8)]));
      }
    }
    byte[] tmp = new byte[8];
    System.arraycopy(datas, 0, tmp, 0, 8);
    String res = getDataWithCipherCode(Constants.PPMAC + 
      StringUtils.convertBytesToHex(tmp));
    if ((res != null) && (res.startsWith("00")))
      return res.substring(2);
    return null;
  }
  
  public boolean mposWriteTMK(String key)
  {
    if ((!checkReader()) || (key == null) || (key.length() % 2 != 0))
      return false;
    try {
      StringBuffer sb = new StringBuffer();
      String res = MessageDigestUtils.encodeTripleDES("0000000000000000", 
        key);
      sb.append(key).append(res.substring(0, 8));
      res = _handler.getDataWithCipherCode(Constants.PPMKEY + 
        sb.toString());
      if (res.startsWith("00"))
        return true;
    } catch (Exception e) {
      e.printStackTrace();
      return false;
    }
    return false;
  }
  




  public boolean setWorkMode(Work_Type type)
  {
    if (!checkReader())
      return false;
    String res = _handler.getDataWithCipherCode(Constants.WORK_MODE + 
      getHexString(type.ordinal()));
    if ((res != null) && (res.startsWith("00")))
      return true;
    return false;
  }
  


  public boolean setLocked()
  {
    if (!checkReader())
      return false;
    String res = _handler.getDataWithCipherCode(Constants.LOCK_MODE + "01");
    if ((res != null) && (res.startsWith("00")))
      return true;
    return false;
  }
  


  public boolean setUnLocked(String key)
  {
    if (!checkReader())
      return false;
    String res = _handler.getDataWithCipherCode(Constants.LOCK_MODE + "00" + 
      key);
    if ((res != null) && (res.startsWith("00")))
      return true;
    return false;
  }
  





  public boolean set3DESKey(String key)
  {
    if (!checkReader())
      return false;
    String res = _handler.getDataWithCipherCode(Constants.WRITE_DES + key);
    if ((res != null) && (res.startsWith("00")))
      return true;
    return false;
  }
  







  public boolean setDukptKey(String bdk, String ksn)
  {
    if (!checkReader())
      return false;
    String res = _handler.getDataWithCipherCode(Constants.WRITE_DUKPT + bdk + 
      ksn);
    if ((res != null) && (res.startsWith("00")))
      return true;
    return false;
  }
  






  public boolean mposWriteSN(String sn)
  {
    if (!checkReader())
      return false;
    if ((sn == null) || (sn.equals("")) || (sn.length() < 1) || (sn.length() > 32)) {
      return false;
    }
    




    String res = _handler.getDataWithCipherCode(Constants.WRITE_SN + sn);
    if ((res != null) && (res.startsWith("00")))
      return true;
    return false;
  }
  


  public String setReadSN()
  {
    if (!checkReader())
      return null;
    String res = _handler.getDataWithCipherCode(Constants.READ_SN);
    if ((res != null) && (res.startsWith("00")))
      return res.substring(2);
    return null;
  }
  
  public String GetPID()
  {
    if (!checkReader())
      return null;
    String res = _handler.getDataWithCipherCode(Constants.GET_CUR_SN);
    
    if ((res != null) && (res.startsWith("00"))) {
      res = res.substring(2);
      return res;
    }
    return null;
  }
  
  public String DesUtil(Data_Mode mode, String data)
  {
    if (!checkReader())
      return null;
    String res = _handler.getDataWithCipherCode(Constants.DES_UTIL + 
      getHexString(mode.ordinal()) + data);
    
    if ((res != null) && (res.startsWith("00")))
      return res;
    return null;
  }
  
  public String DukptUtil(String data)
  {
    if (!checkReader())
      return null;
    String res = _handler
      .getDataWithCipherCode(Constants.DUKPT_UTIL + data);
    
    if ((res != null) && (res.startsWith("00")))
      return res;
    return null;
  }
  


  public boolean tposWriteTMK(String key)
  {
    if ((!checkReader()) || (key == null) || (key.length() % 2 != 0))
      return false;
    String res = _handler.getDataWithCipherCode(Constants.T105_TMK + key);
    if ((res != null) && (res.startsWith("00")))
      return true;
    return false;
  }
  
  public boolean tposSignIn(String data)
  {
    if ((!checkReader()) || (data.length() % 2 != 0))
      return false;
    String res = getDataWithCipherCode(Constants.T105_SINGIN + data);
    if ((res != null) && (res.startsWith("00")))
      return true;
    Log.e(TAG, res);
    return false;
  }
  
  public String tposGetCount()
  {
    if (!checkReader())
      return null;
    String res = getDataWithCipherCode(Constants.T105_ICCOUNT);
    if ((res != null) && (res.startsWith("00")))
      return res.substring(2);
    return null;
  }
  
  public String tposGetMac(String macBlock)
  {
    if ((!checkReader()) || (macBlock == null) || (macBlock.length() % 2 != 0))
      return null;
    int mac_len_h = macBlock.toString().length() / 2 / 256;
    int mac_len_l = macBlock.toString().length() / 2 % 256;
    String res = getDataWithCipherCode(Constants.T105_GETMAC + 
      getHexString(mac_len_h) + getHexString(mac_len_l) + macBlock);
    if ((res != null) && (res.startsWith("00")))
      return res.substring(2);
    return null;
  }
  
  public String tposGetPIN(String pan, String pin)
  {
    if (!checkReader())
      return null;
    int len = 2;int panLen = 0;
    if ((pan != null) && (pan.length() >= 16)) {
      pan = pan.substring(pan.length() - 13, pan.length() - 1);
      len = len + 12 + 1;
      panLen = 12;
    } else {
      pan = "";
      len++;
      panLen = 0;
    }
    len += 5;
    String res = getDataWithCipherCode(Constants.T105_GETPIN + 
      getHexString(panLen) + 
      StringUtils.convertStringToHex(new StringBuilder(String.valueOf(pan)).append(pin).toString()) + "00");
    if ((res != null) && (res.startsWith("00")))
      return res.substring(2);
    return null;
  }
  
  public String getMagTrack1Data() {
    return _track1;
  }
  
  public String getMagTrack2Data() {
    return _track2;
  }
  
  public String getMagTrack3Data() {
    return _track3;
  }
  
  public String getMagPAN() {
    return _pan;
  }
  
  public String getMagExpDate()
  {
    if ((_track2.equals("")) || (_track2 == null) || (!_track2.contains("=")))
      return null;
    int index = _track2.indexOf("=") + 1;
    return _track2.substring(index, index + 4);
  }
  
  public String getMagServiceCode() {
    return _serviceCode;
  }
  
  public String getMagCardHolder() {
    return _cardHolder;
  }
  






  private void length(String data)
  {
    String data1 = data.replace(" ", "");
    String panlcStr = data1.substring(0, 2);
    planlc = Integer.parseInt(panlcStr, 16);
    datelc = 4;
    servicelc = 3;
    cardholderlc = 26;
    ksnlc = 10;
    int ii = (0 + planlc + 1 + datelc + servicelc + cardholderlc + ksnlc) * 2;
    one_length = data1.substring(ii, ii + 2);
    onelc = lc(one_length);
    ii += (1 + onelc) * 2;
    two_length = data1.substring(ii, ii + 2);
    twolc = lc(two_length);
    ii += (1 + twolc) * 2;
    three_length = data1.substring(ii, ii + 2);
    threeLc = lc(three_length);
    qhtflc = 4;
  }
  





  private int lc(String str)
  {
    int i = Integer.parseInt(str, 16);
    int c = i % 8 == 0 ? i / 8 : i / 8 + 1;
    i = c * 8;
    return i;
  }
  



  private void hexString(String data)
  {
    String data1 = data.replace(" ", "");
    
    int start = 2;
    int end = start + planlc * 2;
    pan = data1.substring(start, end);
    
    start = end;
    end = start + datelc * 2;
    date = data1.substring(start, end);
    
    start = end;
    end = start + servicelc * 2;
    service_codes = data1.substring(start, end);
    
    start = end;
    end = start + cardholderlc * 2;
    cardholder = data1.substring(start, end);
    
    start = end;
    end = start + ksnlc * 2;
    ksn = data1.substring(start, end);
    
    start = end + 2;
    end = start + onelc * 2;
    one = data1.substring(start, end);
    
    start = end + 2;
    end = start + twolc * 2;
    two = data1.substring(start, end);
    
    start = end + 2;
    end = start + threeLc * 2;
    three = data1.substring(start, end);
    
    start = end;
    end = start + qhtflc * 2;
  }
  
  public String getGESmaskPan() {
    if (isDukpt) {
      return StringUtils.covertHexToASCII(pan);
    }
    return "";
  }
  
  public String getGESexpDate() {
    if (isDukpt) {
      return StringUtils.covertHexToASCII(date);
    }
    return "";
  }
  
  public String getGESserviceCode() {
    if (isDukpt) {
      return StringUtils.covertHexToASCII(service_codes);
    }
    return "";
  }
  
  public String getGEScardholder() {
    if (isDukpt) {
      return StringUtils.covertHexToASCII(cardholder);
    }
    return "";
  }
  
  public String getGESksn() {
    if (isDukpt) {
      return ksn;
    }
    return "";
  }
  
  public String getGESTrack1Data() {
    if (isDukpt) {
      return one;
    }
    return "";
  }
  
  public String getGESTrack2Data() {
    if (isDukpt) {
      return two;
    }
    return "";
  }
  
  public String getGESTrack3Data() {
    if (isDukpt) {
      return three;
    }
    return "";
  }
  
  public int getGESTrack1Length() {
    if (!isDukpt)
      return 0;
    int len1 = Integer.parseInt(one_length, 16);
    if (len1 > 2) {
      return len1 - 2;
    }
    return len1;
  }
  
  public int getGESTrack2Length() {
    if (!isDukpt)
      return 0;
    int len2 = Integer.parseInt(two_length, 16);
    if (len2 > 2) {
      return len2 - 2;
    }
    return len2;
  }
  
  public int getGESTrack3Length() {
    if (!isDukpt)
      return 0;
    int len3 = Integer.parseInt(three_length, 16);
    if (len3 > 2) {
      return len3 - 2;
    }
    return len3;
  }
  







  public String readBankCardAccount()
  {
    String cardNo = "";
    icReset();
    String selectAID = getDataWithAPDUForStr("00A4040008A00000017295000100");
    if ((selectAID.endsWith("9000")) && (selectAID != null)) {
      String selectEF = getDataWithAPDUForStr("00A40200021001");
      if ((selectEF != null) && (selectEF.endsWith("9000"))) {
        String allRecords = getDataWithAPDUForStr("00B2010500");
        if ((allRecords != null) && (allRecords.endsWith("9000"))) {
          String covertno = StringUtils.covertHexToASCII(allRecords
            .substring(92, 124));
          for (int i = 0; i < covertno.length(); i++) {
            String chari = String.valueOf(covertno.charAt(i));
            int value = Integer.valueOf(chari).intValue();
            if (value > 0) break;
            cardNo = covertno.substring(i + 1);
          }
        }
      }
    }
    


    return cardNo;
  }
  






  public Map<String, String> readHealthCardInfo()
  {
    Map<String, String> map = new HashMap();
    icReset();
    String selectAID = getDataWithAPDUForStr("00A4040010D158000001000000000000000000110000");
    if ((selectAID != null) && (selectAID.endsWith("9000"))) {
      String profileData = getDataWithAPDUForStr("00CA1100020000");
      if ((profileData != null) && (profileData.endsWith("9000")))
      {
        String cardNo = StringUtils.covertHexToASCII(profileData.substring(4, 
          28));
        String cardName = profileData.substring(28, 68);
        String cardID = StringUtils.covertHexToASCII(profileData.substring(68, 
          88));
        String cardBirthDay = StringUtils.covertHexToASCII(profileData
          .substring(90, 102));
        map.put("cardNo", cardNo);
        map.put("cardName", cardName);
        map.put("cardID", cardID);
        map.put("cardBirthDay", cardBirthDay);
      }
    }
    return map;
  }
  




  public Boolean mposCheckActivate()
  {
    String data = _handler.getDataWithCipherCode("0c20");
    String randnum = "";
    String desnum = "";
    if (data != null) {
      randnum = data.substring(2);
    }
    try {
      desnum = MessageDigestUtils.encodeTripleDES(randnum, 
        "20161106051010298003830131181015");
      if (desnum != null) {
        String checkresp = _handler.getDataWithCipherCode("0c21" + 
          desnum);
        if ((checkresp != null) && (checkresp.equals("00")))
          return Boolean.valueOf(true);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return Boolean.valueOf(false);
  }
  








  public Boolean mposLoadSuperKey(int mkeyCod, String mkey)
  {
    if ((mkeyCod > 20) || (mkeyCod < 0) || (mkey == null) || 
      ((mkey.length() != 32) && (mkey.length() != 16)) || 
      (mkey.length() % 2 != 0)) {
      return Boolean.valueOf(false);
    }
    String code = toHexString(mkeyCod);
    String resp = _handler.getDataWithCipherCode("2201" + code + mkey);
    if ((resp != null) && (resp.equals("00"))) {
      return Boolean.valueOf(true);
    }
    return Boolean.valueOf(false);
  }
  










  public Boolean mposLoadMainKey(int mkeycod, int wkeyCod, String wKey)
  {
    if ((mkeycod > 20) || (mkeycod < 0) || 
      (wKey == null) || 
      ((wKey.length() != 40) && (wKey.length() != 24)) || 
      (wKey.length() % 2 != 0) || (wkeyCod > 20) || (wkeyCod < 0)) {
      return Boolean.valueOf(false);
    }
    String mcode = toHexString(mkeycod);
    String wcode = toHexString(wkeyCod);
    String resp = _handler.getDataWithCipherCode("2202" + mcode + wcode + 
      wKey);
    if ((resp != null) && (resp.equals("00"))) {
      return Boolean.valueOf(true);
    }
    return Boolean.valueOf(false);
  }
  
  private String toHexString(int i)
  {
    if (i < 16) {
      return "0" + Integer.toHexString(i);
    }
    return Integer.toHexString(i);
  }
  








  public Boolean mposSetEncrypte(int part1, int part2)
  {
    if ((part1 > 3) || (part1 < 0) || (part2 > 3) || (part2 < 0)) {
      return Boolean.valueOf(false);
    }
    String p1 = "0" + Integer.toHexString(part1);
    String p2 = "0" + Integer.toHexString(part2);
    String resp = _handler.getDataWithCipherCode("220b" + p1 + p2);
    if ((resp != null) && (resp.equals("00"))) {
      return Boolean.valueOf(true);
    }
    return Boolean.valueOf(false);
  }
  








  public Boolean mposActiveKeyGroup(int mkid, int wkid)
  {
    if ((mkid > 5) || (mkid < 1) || (wkid > 16) || (wkid < 1)) {
      return Boolean.valueOf(false);
    }
    String mk = "0" + Integer.toHexString(mkid);
    String wk = "0" + 
      Integer.toHexString(wkid);
    String resp = _handler.getDataWithCipherCode("220c" + mk + wk);
    if ((resp != null) && (resp.equals("00"))) {
      return Boolean.valueOf(true);
    }
    return Boolean.valueOf(false);
  }
  






  public Boolean mposLoadAccount(String accont)
  {
    if ((accont == null) || (accont.equals("")))
      return Boolean.valueOf(false);
    if (!isInteger(accont))
      return Boolean.valueOf(false);
    if ((accont.length() < 16) || (accont.length() > 19)) {
      return Boolean.valueOf(false);
    }
    int num = 0;
    StringBuffer bf = new StringBuffer();
    for (int i = accont.length() - 2; i >= 0; i--) {
      num++;
      bf.append(accont.charAt(i));
      if (num == 12)
        break;
    }
    bf.reverse();
    String cca = StringUtils.convertStringToHex(bf.toString());
    String resp = _handler.getDataWithCipherCode("22030c" + cca);
    if ((resp != null) && (resp.equals("00"))) {
      return Boolean.valueOf(true);
    }
    return Boolean.valueOf(false);
  }
  


  private boolean isInteger(String value)
  {
    Pattern pattern = Pattern.compile("[0-9]*");
    return pattern.matcher(value).matches();
  }
  






  public String mposEncryptPIN(String data)
  {
    if (!data.matches("[0-9]{6}")) {
      return null;
    }
    String resp = _handler.getDataWithCipherCode("2209" + 
      StringUtils.convertStringToHex(data));
    if ((resp != null) && (resp.startsWith("00"))) {
      return resp.substring(2);
    }
    return null;
  }
  




  public String mposEncryptMAC(String data)
  {
    if ((data == null) || (data.equals("")))
      return null;
    int len = data.length();
    if (len % 2 != 0) {
      return null;
    }
    if (len % 8 != 0) {
      int num = 8 - len / 2 % 8;
      for (int i = 0; i < num; i++) {
        data = data + "00";
      }
    }
    String resp = _handler
      .getDataWithCipherCode("220e" + 
      
      StringUtils.convertBytesToHex(new byte[] { (byte)(len / 2 / 256) }) + 
      
      StringUtils.convertBytesToHex(new byte[] { (byte)(len / 2 % 256) }) + 
      data);
    if ((resp != null) && (resp.startsWith("00"))) {
      return resp.substring(2);
    }
    return null;
  }
  




  public String mposEntryptData(String data)
  {
    if ((data == null) || (data.equals("")))
      return null;
    int len = data.length();
    if (len % 2 != 0) {
      return null;
    }
    if (len % 8 != 0) {
      int num = 8 - len / 2 % 8;
      for (int i = 0; i < num; i++) {
        data = data + "00";
      }
    }
    String resp = _handler
      .getDataWithCipherCode("220d" + 
      
      StringUtils.convertBytesToHex(new byte[] { (byte)(len / 2 / 256) }) + 
      
      StringUtils.convertBytesToHex(new byte[] { (byte)(len / 2 % 256) }) + 
      data);
    if ((resp != null) && (resp.startsWith("00"))) {
      return resp.substring(2);
    }
    return null;
  }
  





  public boolean mPosdShake()
  {
    String key1 = "4c0139d2137683f4a12232144487134e";
    String key2 = "ac238832eeb23e7492190f1134a3e3c4";
    String ran = RandomUtils.generateHexString(16);
    String res = null;
    try {
      String tmp = MessageDigestUtils.encodeTripleDES(ran, key1);
      res = _handler.getDataWithCipherCode("4f4f" + tmp);
      if (res == null)
        return false;
      res = res.replaceAll(" ", "");
      if (res.length() == 2)
        return false;
      if (res.substring(0, 16).equalsIgnoreCase(ran))
      {
        String tmp2 = res.substring(16);
        tmp2 = MessageDigestUtils.encodeTripleDES(tmp2, key2);
        res = _handler.getDataWithCipherCode("4f4f" + tmp2);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    if ((res != null) && (res.startsWith("00"))) {
      return true;
    }
    return false;
  }
  





  public boolean mPosdType()
  {
    String res = _handler.getDataWithCipherCode("4150");
    if ((res == null) || (!res.startsWith("00")))
      return false;
    return true;
  }
  





  public boolean mPosfType()
  {
    String res = _handler.getDataWithCipherCode("4C49");
    if ((res == null) || (!res.startsWith("00")))
      return false;
    return true;
  }
  





  public boolean mPosdLength(String len)
  {
    String res = _handler.getDataWithCipherCode("4C45" + len);
    if ((res == null) || (!res.startsWith("00")))
      return false;
    return true;
  }
  





  public boolean mPosdData(String data)
  {
    if ((data == null) || (data.equals("")))
      return false;
    String res = _handler.getDataWithCipherCode("4441" + data);
    if ((res == null) || (!res.startsWith("00")))
      return false;
    return true;
  }
  






  public boolean mPosdGo2Boot()
  {
    String res = _handler.getDataWithCipherCode("4A55");
    if ((res == null) || (!res.startsWith("00")))
      return false;
    return true;
  }
  





  public boolean mPosdGo2App()
  {
    String res = _handler.getDataWithCipherCode("4558");
    if ((res == null) || (!res.startsWith("00")))
      return false;
    return true;
  }
  





  public void set80Beep(boolean boolean1)
  {
    _handler.writeCipherCode("1b61", "type_80");
    if (boolean1) {
      _handler.writeCipherCode("1b88", "type_80");
    } else {
      _handler.writeCipherCode("1b88", "type_80");
    }
  }
  



  public boolean set80HiLow(boolean boolean1)
  {
    _handler.writeCipherCode("1b61", "type_80");
    if (boolean1) {
      String msg = _handler.getDataWithCipherCode("1b78", "type_80");
      if ((msg != null) && (msg.startsWith("1b30")))
        return true;
    } else {
      String msg = _handler.getDataWithCipherCode("1b79", "type_80");
      if ((msg != null) && (msg.startsWith("1b30")))
        return true;
    }
    return false;
  }
  




  public boolean setSelcetBPI(boolean boolean1)
  {
    _handler.writeCipherCode("1b61", "type_80");
    if (boolean1) {
      String msg = _handler.getDataWithCipherCode("1b62d2", "type_80");
      if ((msg != null) && (msg.startsWith("1b30")))
        return true;
    } else {
      String msg = _handler.getDataWithCipherCode("1b624b", "type_80");
      if ((msg != null) && (msg.startsWith("1b30")))
        return true;
    }
    return false;
  }
  



  public void read80()
  {
    _handler.writeCipherCode("1b61", "type_80");
    new Thread(new Runnable()
    {
      public void run() {
        _handler.writeCipherCode("1b72", "type_80");
      }
    })
    



      .start();
  }
  










  public void write80(String track1, String track2, String track3)
  {
    if (((track1 == null) && (track2 == null) && (track3 == null)) || (
      (track1.equals("")) && (track2.equals("")) && (track3.equals("")))) {
      return;
    }
    if (track1.length() > 76) {
      return;
    }
    if (track2.length() > 37) {
      return;
    }
    if (track2.length() > 104) {
      return;
    }
    if ((track1 != null) && (track1.length() > 0)) {
      int first = Integer.parseInt("20", 16);
      int last = Integer.parseInt("5f", 16);
      for (int i = 0; i < track1.length(); i++) {
        int onechar = track1.charAt(i);
        if ((onechar < first) || (onechar > last)) {
          return;
        }
      }
    }
    if ((track2 != null) && (track2.length() > 0)) {
      String REGEX = "[\\d=><:]+";
      if (!track2.matches("[\\d=><:]+"))
        return;
    }
    if ((track3 != null) && (track3.length() > 0)) {
      String REGEX = "[\\d=><:]+";
      if (!track3.matches("[\\d=><:]+"))
        return;
    }
    _handler.writeCipherCode("1b61", "type_80");
    String hex11 = (track1 != null) && (!track1.isEmpty()) ? 
      StringUtils.convertStringToHex(track1) : "";
    String hex12 = (track2 != null) && (!track2.isEmpty()) ? 
      StringUtils.convertStringToHex(track2) : "";
    String hex13 = (track3 != null) && (!track3.isEmpty()) ? 
      StringUtils.convertStringToHex(track3) : "";
    String data = "1b731b01" + hex11 + "1b02" + hex12 + "1b03" + hex13 + 
      "3f1c";
    _handler.writeCipherCode("1b77" + data, "type_80");
  }
  










  public void erase80(boolean bl1, boolean bl2, boolean bl3)
  {
    _handler.writeCipherCode("1b61", "type_80");
    if ((bl1) && (!bl2) && (!bl3)) {
      _handler.writeCipherCode("1b6301", "type_80");
    } else if ((!bl1) && (bl2) && (!bl3)) {
      _handler.writeCipherCode("1b6302", "type_80");
    } else if ((!bl1) && (!bl2) && (bl3)) {
      _handler.writeCipherCode("1b6304", "type_80");
    } else if ((bl1) && (bl2) && (!bl3)) {
      _handler.writeCipherCode("1b6303", "type_80");
    } else if ((bl1) && (!bl2) && (bl3)) {
      _handler.writeCipherCode("1b6305", "type_80");
    } else if ((!bl1) && (bl2) && (bl3)) {
      _handler.writeCipherCode("1b6306", "type_80");
    } else if ((bl1) && (bl2) && (bl3)) {
      _handler.writeCipherCode("1b6307", "type_80");
    }
  }
  




  public boolean check80device()
  {
    _handler.writeCipherCode("1b61", "type_80");
    String resp = _handler.getDataWithCipherCode("1b65", "type_80");
    if ((resp != null) && (resp.startsWith("1b79"))) {
      return true;
    }
    return false;
  }
  





  public boolean mPosEnterPrint()
  {

    String res = _handler.getDataWithCipherCode(Constants.POS_PRINT);
    if ((res == null) || (!res.startsWith("00")))
      return false;
    return true;
  }
  




  public boolean mPosExitPrint()
  {
    _handler.setPrn(true);
    String res = _handler.getDataWithCipherCode(Constants.PRINT_EXIT);
    _handler.setPrn(false);
    if ((res == null) || (!res.startsWith("00")))
      return false;
    return true;
  }
  



  public boolean mPosConfirmExitPrint()
  {
    if (!checkReader())
      return false;
    String res = _handler.getDataWithCipherCode("0c04");
    if ((res != null) && (res.startsWith("00")))
      return true;
    return false;
  }
  


  public void mPosPrnStr(String str)
  {
    if (!checkReader())
      return;
    if ((str == null) || (str.equals("")))
      throw new IllegalArgumentException("Print data is null!");
    _handler.setPrn(true);
    String ss = unPackData(str);
    prnStrOneByOne(ss);
    _handler.setPrn(false);
  }
  


  public void mPosPrnStr(String str, String bianma)
  {
    if (!checkReader())
      return;
    if ((str == null) || (str.equals("")))
      throw new IllegalArgumentException("Print data is null!");
    _handler.setPrn(true);
    String ss = unPackData(str, bianma);
    prnStrOneByOne(ss);
    _handler.setPrn(false);
  }
  


  public void mPosPrnImg(Bitmap bit)
  {
    _handler.setPrn(true);
    mposPrintBitmap(bit);
    _handler.setPrn(false);
  }
  




  public List<byte[]> mPosPrnConvertBmp(Bitmap bitmap)
  {
    int width = bitmap.getWidth();
    int height = bitmap.getHeight();
    List<byte[]> datas = new ArrayList();
    byte[] command = { 29, 118, 48 };
    
    datas.add(command);
    int xl = (width % 8 == 0 ? width / 8 : width / 8 + 1) % 256;
    int xh = (width % 8 == 0 ? width / 8 : width / 8 + 1) / 256;
    int yl = height % 256;
    int yh = height / 256;
    datas.add(new byte[] { (byte)xl, (byte)xh, (byte)yl, (byte)yh });
    int zeroCount = width % 8;
    StringBuffer zero = new StringBuffer();
    if (zeroCount > 0) {
      for (int i = 0; i < 8 - zeroCount; i++) {
        zero.append("0");
      }
    }
    int idx = 0;
    byte[] data = null;
    StringBuffer sb = new StringBuffer();
    for (int i = 0; i < height; i++) {
      idx = 0;
      sb.setLength(0);
      data = new byte[width % 8 == 0 ? width / 8 : width / 8 + 1];
      for (int j = 0; j < width; j++) {
        int color = bitmap.getPixel(j, i);
        int r = color >> 16 & 0xFF;
        int g = color >> 8 & 0xFF;
        int b = color & 0xFF;
        if ((r > 200) || (g > 200) || (b > 200)) {
          sb.append("0");
        } else
          sb.append("1");
        if (sb.length() == 8) {
          data[(idx++)] = ((byte)Integer.parseInt(sb.toString(), 2));
          sb.setLength(0);
        }
      }
      if (zeroCount > 0) {
        sb.append(zero);
        data[(idx++)] = ((byte)Integer.parseInt(sb.toString(), 2));
      }
      datas.add(data);
    }
    return datas;
  }
  





  public List<byte[]> mPosPrnConvertStr(String text, float textSize, Layout.Alignment ali)
  {
    TextPaint textPaint = new TextPaint();
    textPaint.setColor(-16777216);
    textPaint.setTextSize(textSize);
    StaticLayout layout = new StaticLayout(text, textPaint, 380, ali, 1.0F, 
      0.0F, true);
    Bitmap bitmap = Bitmap.createBitmap(layout.getWidth() + 10, 
      layout.getHeight() - 3, Bitmap.Config.ARGB_8888);
    Canvas canvas = new Canvas(bitmap);
    canvas.translate(1.0F, 0.0F);
    canvas.drawColor(-1);
    layout.draw(canvas);
    return mPosPrnConvertBmp(bitmap);
  }
  





  public void mPosPrnImg(List<byte[]> bit)
  {
    _handler.setPrn(true);
    mposPrintBitmap(bit);
    _handler.setPrn(false);
  }
  
  private void mposPrintBitmap(Bitmap bitmap) {
    int width = bitmap.getWidth();
    int height = bitmap.getHeight();
    
    List<byte[]> datas = new ArrayList();
    byte[] command = { 29, 118, 48 };
    

    datas.add(command);
    int xl = (width % 8 == 0 ? width / 8 : width / 8 + 1) % 256;
    int xh = (width % 8 == 0 ? width / 8 : width / 8 + 1) / 256;
    int yl = height % 256;
    int yh = height / 256;
    
    datas.add(new byte[] { (byte)xl, (byte)xh, (byte)yl, (byte)yh });
    
    int zeroCount = width % 8;
    StringBuffer zero = new StringBuffer();
    if (zeroCount > 0) {
      for (int i = 0; i < 8 - zeroCount; i++) {
        zero.append("0");
      }
    }
    int idx = 0;
    byte[] data = null;
    
    StringBuffer sb = new StringBuffer();
    int color; for (int i = 0; i < height; i++) {
      idx = 0;
      sb.setLength(0);
      data = new byte[width % 8 == 0 ? width / 8 : width / 8 + 1];
      for (int j = 0; j < width; j++) {
        color = bitmap.getPixel(j, i);
        int r = color >> 16 & 0xFF;
        int g = color >> 8 & 0xFF;
        int b = color & 0xFF;
        

        if ((r > 200) || (g > 200) || (b > 200)) {
          sb.append("0");
        } else
          sb.append("1");
        if (sb.length() == 8) {
          data[(idx++)] = ((byte)Integer.parseInt(sb.toString(), 2));
          sb.setLength(0);
        }
      }
      if (zeroCount > 0) {
        sb.append(zero);
        data[(idx++)] = ((byte)Integer.parseInt(sb.toString(), 2));
      }
      
      datas.add(data);
    }
    

    StringBuffer sbS = new StringBuffer();
    for (byte[] b : datas) {
      sbS.append(StringUtils.convertBytesToHex(b));
    }
    

    int devil = 1024;
    byte[] buffers = sysCopy(datas);
    if (buffers.length <= devil) {
      getDataWithCipherCode(StringUtils.convertBytesToHex(buffers));
    }
    else {
      int m = buffers.length / devil;
      int n = buffers.length % devil;
      byte[] tmps = null;
      for (int i = 0; i < m; i++) {
        tmps = new byte[devil];
        System.arraycopy(buffers, i * devil, tmps, 0, devil);
        getDataWithCipherCode(StringUtils.convertBytesToHex(tmps));
        tmps = null;
        try {
          Thread.sleep(150L);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
      tmps = new byte[n];
      System.arraycopy(buffers, m * devil, tmps, 0, n);
      getDataWithCipherCode(StringUtils.convertBytesToHex(tmps));
    }
    
    try
    {
      Thread.sleep(25L);
    }
    catch (Exception localException) {}
  }
  



  private void mposPrintBitmap(List<byte[]> datas)
  {
    int devil = 5120;
    byte[] buffers = sysCopy(datas);
    if (buffers.length <= devil) {
      getDataWithCipherCode(StringUtils.convertBytesToHex(buffers));
    }
    else {
      int m = buffers.length / devil;
      int n = buffers.length % devil;
      byte[] tmps = null;
      for (int i = 0; i < m; i++) {
        tmps = new byte[devil];
        System.arraycopy(buffers, i * devil, tmps, 0, devil);
        getDataWithCipherCode(StringUtils.convertBytesToHex(tmps));
        tmps = null;
        try {
          Thread.sleep(10L);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
      tmps = new byte[n];
      System.arraycopy(buffers, m * devil, tmps, 0, n);
      getDataWithCipherCode(StringUtils.convertBytesToHex(tmps));
    }
    try {
      Thread.sleep(25L);
    }
    catch (Exception localException) {}
  }
  
  public void onDestroy() {
    instance = null;
  }
  
  public String ReadNfcBankCard() {
    String re = reset(SLOT_NFC);
    if ((re == null) || (re.startsWith("ff")) || (re.startsWith("32")))
      return re;
    String pan = null;
    EMVParam param = new EMVParam();
    param.setSlot((byte)5);
    param.setReadOnly(true);
    


    param.setMerchName("4368696E61");
    param.setMerchCateCode("0001");
    param.setMerchId("313233343536373839303132333435");
    param.setTermId("3132333435363738");
    param.setTerminalType((byte)34);
    param.setCapability("E0F8C8");
    
    param.setExCapability("F00000A001");
    param.setTransCurrExp((byte)2);
    param.setCountryCode("0840");
    param.setTransCurrCode("0840");
    param.setTransType((byte)0);
    param.setTermIFDSn("3838383838383838");
    param.setAuthAmnt(8000000);
    param.setOtherAmnt(0);
    Date date = new Date();
    DateFormat sdf = new SimpleDateFormat("yyMMdd");
    param.setTransDate(sdf.format(date));
    sdf = new SimpleDateFormat("HHmmss");
    param.setTransTime(sdf.format(date));
    loadMasterCardAIDs(param);
    loadVisaAIDs(param);
    _handler.kernelInit(param);
    _handler.icReset();
    if (_handler.process() == 0) {
      pan = _handler.getTLVDataByTag(90);
    }
    _handler.icOff();
    return pan;
  }
  
  private void loadMasterCardAIDs(EMVParam ep)
  {
    EMVApp ea = new EMVApp();
    ea.setAppName("");
    ea.setAID("A0000000041010");
    ea.setSelFlag(EMVConstants.PART_MATCH);
    ea.setPriority((byte)0);
    ea.setTargetPer((byte)0);
    ea.setMaxTargetPer((byte)0);
    ea.setFloorLimitCheck((byte)1);
    ea.setFloorLimit(2000);
    ea.setThreshold(0);
    ea.setTACDenial("0000000000");
    ea.setTACOnline("0000001000");
    ea.setTACDefault("0000000000");
    ea.setAcquierId("000000123456");
    ea.setDDOL("039F3704");
    ea.setTDOL("0F9F02065F2A029A039C0195059F3704");
    ea.setVersion("008C");
    ep.addApp(ea);
    

    ea = new EMVApp();
    ea.setAppName("");
    ea.setAID("A0000000043060");
    ea.setSelFlag(EMVConstants.PART_MATCH);
    ea.setPriority((byte)0);
    ea.setTargetPer((byte)0);
    ea.setMaxTargetPer((byte)0);
    ea.setFloorLimitCheck((byte)1);
    ea.setFloorLimit(2000);
    ea.setThreshold(0);
    ea.setTACDenial("0000000000");
    ea.setTACOnline("0000001000");
    ea.setTACDefault("0000000000");
    ea.setAcquierId("000000123456");
    ea.setDDOL("039F3704");
    ea.setTDOL("0F9F02065F2A029A039C0195059F3704");
    ea.setVersion("008C");
    ep.addApp(ea);
    

    ea = new EMVApp();
    ea.setAppName("");
    ea.setAID("A0000000046000");
    ea.setSelFlag(EMVConstants.PART_MATCH);
    ea.setPriority((byte)0);
    ea.setTargetPer((byte)0);
    ea.setMaxTargetPer((byte)0);
    ea.setFloorLimitCheck((byte)1);
    ea.setFloorLimit(2000);
    ea.setThreshold(0);
    ea.setTACDenial("0000000000");
    ea.setTACOnline("0000001000");
    ea.setTACDefault("0000000000");
    ea.setAcquierId("000000123456");
    ea.setDDOL("039F3704");
    ea.setTDOL("0F9F02065F2A029A039C0195059F3704");
    ea.setVersion("008C");
    ep.addApp(ea);
  }
  

  private void loadVisaAIDs(EMVParam ep)
  {
    EMVApp ea = new EMVApp();
    ea.setAppName("");
    ea.setAID("A0000000031010");
    ea.setSelFlag(EMVConstants.PART_MATCH);
    ea.setPriority((byte)0);
    ea.setTargetPer((byte)0);
    ea.setMaxTargetPer((byte)0);
    ea.setFloorLimitCheck((byte)1);
    ea.setFloorLimit(2000);
    ea.setThreshold(0);
    ea.setTACDenial("0000000000");
    ea.setTACOnline("0000001000");
    ea.setTACDefault("0000000000");
    ea.setAcquierId("000000123456");
    ea.setDDOL("039F3704");
    ea.setTDOL("0F9F02065F2A029A039C0195059F3704");
    ea.setVersion("008C");
    ep.addApp(ea);
    

    ea = new EMVApp();
    ea.setAppName("");
    ea.setAID("A0000000032010");
    ea.setSelFlag(EMVConstants.PART_MATCH);
    ea.setPriority((byte)0);
    ea.setTargetPer((byte)0);
    ea.setMaxTargetPer((byte)0);
    ea.setFloorLimitCheck((byte)1);
    ea.setFloorLimit(2000);
    ea.setThreshold(0);
    ea.setTACDenial("0000000000");
    ea.setTACOnline("0000001000");
    ea.setTACDefault("0000000000");
    ea.setAcquierId("000000123456");
    ea.setDDOL("039F3704");
    ea.setTDOL("0F9F02065F2A029A039C0195059F3704");
    ea.setVersion("008C");
    ep.addApp(ea);
    

    ea = new EMVApp();
    ea.setAppName("");
    ea.setAID("A0000000038010");
    ea.setSelFlag(EMVConstants.PART_MATCH);
    ea.setPriority((byte)0);
    ea.setTargetPer((byte)0);
    ea.setMaxTargetPer((byte)0);
    ea.setFloorLimitCheck((byte)1);
    ea.setFloorLimit(2000);
    ea.setThreshold(0);
    ea.setTACDenial("0000000000");
    ea.setTACOnline("0000001000");
    ea.setTACDefault("0000000000");
    ea.setAcquierId("000000123456");
    ea.setDDOL("039F3704");
    ea.setTDOL("0F9F02065F2A029A039C0195059F3704");
    ea.setVersion("008C");
    ep.addApp(ea);
  }
  









  public String mPosStartEmv(String date, String time, int amount)
  {
    StringBuffer sb = new StringBuffer();
    sb.append("016000");
    sb.append(date);
    sb.append(time);
    String amStr = Integer.toHexString(amount);
    StringBuffer amuontSb = new StringBuffer();
    for (int j = 0; j < 8 - amStr.length(); j++) {
      amuontSb.append("0");
    }
    amuontSb.append(amStr);
    amStr = amuontSb.toString();
    sb.append(amStr);
    
    String res = getDataWithCipherCode(sb.toString().trim());
    if ((res != null) && (res.startsWith("00")))
      return res;
    return res;
  }
  
  public String mPosGetIcField55()
  {
    return getDataWithCipherCode("016200");
  }
  
  public String mPosGetIcTrack2Data()
  {
    return getDataWithCipherCode("016300");
  }
  
  public String mPosGetIcTag(String tag)
  {
    return getDataWithCipherCode("016100" + tag);
  }
  

















  public boolean mPosSign(String pinkey, String pinCheck, String mackey, String macCheck, String tdk, String tdkCheck)
  {
    String resp = getDataWithCipherCode("240001" + pinkey + pinCheck + 
      mackey + macCheck + tdk + tdkCheck);
    if ((resp != null) && (resp.equals("00")))
      return true;
    return false;
  }
  












  public boolean mPosLoadWorkKey(String pin, String pinCheck, String mackey, String macCheck)
  {
    String resp = getDataWithCipherCode("240101" + pin + pinCheck + mackey + 
      macCheck);
    if ((resp != null) && (resp.equals("00")))
      return true;
    return false;
  }
  


  private void mPosinit()
  {
    try
    {
      FileOutputStream fdoff = new FileOutputStream("/sys/class/gpio/gpio1003/direction");
      fdoff.write("out".getBytes());
      fdoff.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
  


  public void mPosPowerOn()
  {
    if (_handler.getPosModel().equals(Constants.POS_Z91)) {
      mPosinit();
      try
      {
        FileOutputStream fdoff = new FileOutputStream("/sys/class/gpio/gpio1003/value");
        fdoff.write("1".getBytes());
        fdoff.close();
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    else {
      try {
        FileOutputStream fdon = new FileOutputStream(
          "/sys/class/input/input2/device/gpio_switch");
        fdon.write("1".getBytes());
        fdon.close();
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }
  



  public void mPosPowerOff()
  {
    if (_handler.getPosModel().equals(Constants.POS_Z91)) {
      mPosinit();
      try
      {
        FileOutputStream fdoff = new FileOutputStream("/sys/class/gpio/gpio1003/value");
        fdoff.write("0".getBytes());
        fdoff.close();
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    else {
      try {
        FileOutputStream fdoff = new FileOutputStream(
          "/sys/class/input/input2/device/gpio_switch");
        fdoff.write("0".getBytes());
        fdoff.close();
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }
  



  public String getSDKversion()
  {
    return Constants.SDK_VERSION;
  }
  







  public void mPosPrintTextSize(String size)
  {
    if (!checkReader())
      return;
    _handler.setPrn(true);
    if (size.equals(MPOS_PRINT_TEXT_NORMAL)) {
      _handler.writeCipherCode("1d2100");
    } else if (size.equals(MPOS_PRINT_TEXT_DOUBLE_HEIGHT)) {
      _handler.writeCipherCode("1d2101");
    } else if (size.equals(MPOS_PRINT_TEXT_DOUBLE_WIDTH)) {
      _handler.writeCipherCode("1d2110");
    } else if (size.equals(MPOS_PRINT_TEXT_DOUBLE_SIZE)) {
      _handler.writeCipherCode("1d2111");
    }
    _handler.setPrn(false);
  }
  




  public void mPosPrintAlign(String align)
  {
    if (!checkReader())
      return;
    _handler.setPrn(true);
    if (align.equals(MPOS_PRINT_ALIGN_LEFT)) {
      _handler.writeCipherCode("1b6100");
    } else if (align.equals(MPOS_PRINT_ALIGN_CENTER)) {
      _handler.writeCipherCode("1b6101");
    } else if (align.equals(MPOS_PRINT_ALIGN_RIGHT)) {
      _handler.writeCipherCode("1b6102");
    }
    _handler.setPrn(false);
  }
  


  public void mPosPrintLn()
  {
    if (!checkReader())
      return;
    _handler.setPrn(true);
    _handler.writeCipherCode("0a");
    _handler.setPrn(false);
  }
  





  public void mPosPrintFontSwitch(String choice)
  {
    if (!checkReader())
      return;
    _handler.setPrn(true);
    if (choice.equals(MPOS_PRINT_FONT_DEFAULT)) {
      _handler.writeCipherCode("1b7480");
    } else if (choice.equals(MPOS_PRINT_FONT_NEW)) {
      _handler.writeCipherCode("1b7481");
    } else if (choice.equals(MPOS_PRINT_FONT_LATIN)) {
      _handler.writeCipherCode("1b740a");
    }
    _handler.setPrn(false);
  }
  



  public String mPosGetSerialNumber()
  {
    if (!checkReader())
      return null;
    String resp = _handler.getDataWithCipherCode("0600");
    if ((resp != null) && (resp.length() > 2)) {
      return resp.substring(2);
    }
    return null;
  }
  




  public boolean mPosUpdateSerialNumber(String serNum)
  {
    if (!checkReader()) {
      return false;
    }
    if ((serNum != null) && (serNum.length() == 12) && (isHexNumber(serNum))) {
      String resp = _handler.getDataWithCipherCode("0601" + serNum);
      if (resp.equals("00")) {
        return true;
      }
      return false;
    }
    return false;
  }
  
  private static boolean isHexNumber(String str)
  {
    boolean flag = false;
    for (int i = 0; i < str.length(); i++) {
      char cc = str.charAt(i);
      if ((cc == '0') || (cc == '1') || (cc == '2') || (cc == '3') || (cc == '4') || 
        (cc == '5') || (cc == '6') || (cc == '7') || (cc == '8') || 
        (cc == '9') || (cc == 'A') || (cc == 'B') || (cc == 'C') || 
        (cc == 'D') || (cc == 'E') || (cc == 'F') || (cc == 'a') || 
        (cc == 'b') || (cc == 'c') || (cc == 'd') || (cc == 'e') || 
        (cc == 'f')) {
        flag = true;
      }
    }
    return flag;
  }
  



  public boolean initSzt()
  {
    int slot = SLOT_NFC;
    if (!checkReader()) {
      Log.d(TAG, "Device is disconnect!");
      return false;
    }
    try {
      String res = reset(slot);
      if (res == null) {
        Log.d(TAG, "nfc reset fail!");
        return false;
      }
      Apdu_Send apdu_Send = new Apdu_Send(
        StringUtils.convertHexToBytes("00a40400"), (short)14, 
        
        StringUtils.convertHexToBytes("315041592e5359532e4444463031"), 
        (short)0);
      res = getDataWithAPDU(slot, apdu_Send);
      if ((res == null) || (!res.startsWith("00")) || (!res.endsWith("9000")))
        return false;
      apdu_Send = new Apdu_Send(
        StringUtils.convertHexToBytes("00a40400"), (short)7, 
        StringUtils.convertHexToBytes("5041592e535a54"), 
        (short)0);
      res = getDataWithAPDU(slot, apdu_Send);
      if ((res == null) || (!res.startsWith("00")) || (!res.endsWith("9000"))) {
        return false;
      }
      return true;
    } catch (Exception e) {}
    return false;
  }
  




  public Map<String, String> readSZTinfo()
  {
    Map<String, String> info = new HashMap();
    int slot = SLOT_NFC;
    if (!checkReader()) {
      Log.d(TAG, "Device is disconnect!");
      return null;
    }
    try {
      Apdu_Send apdu_Send = new Apdu_Send(
        StringUtils.convertHexToBytes("00b09500"), (short)0, 
        new byte['Ȁ'], (short)0);
      String res = getDataWithAPDU(slot, apdu_Send);
      
      if ((res != null) && (res.startsWith("00")) && (res.endsWith("9000")))
        res = res.substring(4);
      HashMap<String, String> infor1 = parseInfo(res, 4, true);
      apdu_Send = new Apdu_Send(
        StringUtils.convertHexToBytes("805c0002"), (short)0, 
        new byte['Ȁ'], (short)4);
      String res2 = getDataWithAPDU(slot, apdu_Send);
      
      if ((res2 != null) && (res2.startsWith("00")) && (res2.endsWith("9000")))
        res2 = res2.substring(4);
      String cash = parseBalance(res2);
      info.put("serl", (String)infor1.get("serl"));
      info.put("version", (String)infor1.get("version"));
      info.put("date", (String)infor1.get("date"));
      info.put("cash", cash);
      return info;
    } catch (Exception e) {}
    return null;
  }
  

  private HashMap<String, String> parseInfo(String data, int dec, boolean bigEndian)
  {
    HashMap<String, String> info1 = new HashMap();
    
    if ((data == null) || (data.length() < 30)) { String date;
      String version; String serl = version = date = null;
      return null;
    }
    byte[] d = StringUtils.convertHexToBytes(data);
    String h = "";
    for (int i = 0; i < d.length; i++) {
      String temp = Integer.toHexString(d[i] & 0xFF);
      if (temp.length() == 1) {
        temp = "0" + temp;
      }
      h = h + " " + temp; }
    String serl;
    if ((dec < 1) || (dec > 10)) {
      serl = NfcCardUtils.toHexString(d, 10, 10);
    } else {
      int sn = bigEndian ? NfcCardUtils.toIntR(d, 19, dec) : 
        NfcCardUtils.toInt(d, 20 - dec, dec);
      serl = String.format("%d", new Object[] { Long.valueOf(0xFFFFFFFF & sn) });
    }
    String version = d[9] != 0 ? String.valueOf(d[9]) : null;
    String date = String.format("%02X%02X.%02X.%02X - %02X%02X.%02X.%02X", new Object[] { Byte.valueOf(d[20]), 
      Byte.valueOf(d[21]), Byte.valueOf(d[22]), Byte.valueOf(d[23]), Byte.valueOf(d[24]), Byte.valueOf(d[25]), Byte.valueOf(d[26]), Byte.valueOf(d[27]) });
    info1.put("serl", serl);
    info1.put("version", version);
    info1.put("date", date);
    return info1;
  }
  
  protected String parseBalance(String data)
  {
    if ((data == null) || (data.length() < 4))
      return null;
    int n = NfcCardUtils.toInt(StringUtils.convertHexToBytes(data), 0, 4);
    if ((n > 100000) || (n < -100000))
      n -= Integer.MIN_VALUE;
    String cash = NfcCardUtils.toAmountString(n / 100.0F);
    return cash;
  }
  






  public HashMap<String, String> readSZTrecrod(int i)
  {
    if ((i < 0) || (i > 10))
      return null;
    int slot = SLOT_NFC;
    if (!checkReader()) {
      Log.d(TAG, "Device is disconnect!");
      return null;
    }
    try {
      Apdu_Send apdu_Send = new Apdu_Send(
        StringUtils.convertHexToBytes("00b2" + toHexString(i) + 
        "c4"), (short)0, new byte['Ȁ'], (short)0);
      String res = getDataWithAPDU(slot, apdu_Send);
      if ((res != null) && (res.startsWith("00")) && (res.endsWith("9000"))) {
        return parseLog(res.substring(4));
      }
    }
    catch (Exception localException) {}
    return null;
  }
  



  private HashMap<String, String> parseLog(String logs)
  {
    HashMap<String, String> record = new HashMap();
    byte[] v = StringUtils.convertHexToBytes(logs);
    int cash = NfcCardUtils.toInt(v, 5, 4);
    if (cash > 0) {
      String date0 = String.format("%02X%02X.%02X.%02X %02X:%02X ", new Object[] {
        Byte.valueOf(v[16]), Byte.valueOf(v[17]), Byte.valueOf(v[18]), Byte.valueOf(v[19]), Byte.valueOf(v[20]), Byte.valueOf(v[21]), Byte.valueOf(v[22]) });
      String date = date0.substring(0, 10);
      String time = date0.substring(11);
      String t = (v[9] == 6) || (v[9] == 9) ? "消费" : 
        "充值";
      String cashamount = NfcCardUtils.toAmountString(cash / 100.0F);
      record.put("cashdate", date);
      record.put("cashtime", time);
      record.put("cashtype", t);
      record.put("cashamount", cashamount);
    }
    return record;
  }
  



  @Deprecated
  public boolean printExitDetection()
  {
    if (!checkReader())
      return false;
    String res = _handler.getDataWithCipherCode("0c04");
    if ((res != null) && (res.startsWith("00")))
      return true;
    return false;
  }
  







  public boolean mifarePlusAuthenticationSector(int sectorN, String password)
  {
    if ((sectorN > 39) || (sectorN < 0)) {
      return false;
    }
    String regex = "^[A-Fa-f0-9]{32}";
    if ((password == null) || (!password.matches(regex)))
      return false;
    int sectorAdress = 16384;
    sectorAdress += sectorN;
    String delength = getHexString(sectorAdress);
    String resp = getDataWithCipherCode("2007" + 
      delength.substring(2, delength.length()) + 
      delength.substring(0, 2) + password);
    if ((resp != null) && (resp.startsWith("00")))
      return true;
    return false;
  }
  





  public String mifarePlusReadblook(int blookN)
  {
    if ((blookN < 0) || (blookN > 255)) {
      return null;
    }
    String delength = getHexString(blookN);
    String resp = getDataWithCipherCode("200a" + delength + "00" + "01");
    return resp;
  }
  







  public boolean mifarePlusWriteBlook(int blookN, String wData)
  {
    if ((blookN > 255) || (blookN < 0)) {
      return false;
    }
    String regex = "^[A-Fa-f0-9]{32}";
    if ((wData == null) || (!wData.matches(regex)))
      return false;
    String delength = getHexString(blookN);
    String resp = getDataWithCipherCode("2007" + delength + "00" + "10" + 
      wData);
    if ((resp != null) && (resp.startsWith("00")))
      return true;
    return false;
  }
  










  public String mPosGetMacWtithMode(int wkNo, int mode, String data)
  {
    if ((!checkReader()) || (data == null) || (data.length() % 2 != 0))
      return null;
    if ((wkNo > 20) || (wkNo < 1)) {
      return null;
    }
    if (mode < 0) {
      return null;
    }
    
    if (mode > 2) {
      mode = 2;
    }
    int mac_len_h = data.toString().length() / 2 / 256;
    int mac_len_l = data.toString().length() / 2 % 256;
    String res = getDataWithCipherCode("22110102" + 
      getHexString(mac_len_h) + getHexString(mac_len_l) + data);
    if ((res != null) && (res.startsWith("00")))
      return res.substring(2).toUpperCase();
    return null;
  }
  












  public boolean mposLoadPlainKey(int mkNo, int wkNo, String spK, String wky)
  {
    boolean b1 = mposLoadSuperKey(1, spK).booleanValue();
    if (b1) {
      String mkk = "";
      String check = "";
      try {
        String resp = MessageDigestUtils.encodeTripleDES(defaultdata, 
          wky);
        mkk = MessageDigestUtils.encodeTripleDES(wky, spK);
        if ((resp != null) && (resp.length() > 8)) {
          check = resp.substring(0, 8);
        }
      } catch (Exception localException) {}
      if ((mkk != null) && (!mkk.isEmpty()) && (check.length() == 8)) {
        boolean b2 = mposLoadMainKey(1, 1, mkk + check).booleanValue();
        return b2;
      }
    }
    return false;
  }
}
