package com.imagpay.serialport;

import android.util.Log;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.Iterator;
import java.util.Vector;










public class SerialPortFinder
{
  private static final String TAG = "SerialPort";
  public SerialPortFinder() {}
  
  public class Driver
  {
    private String mDriverName;
    private String mDeviceRoot;
    
    public Driver(String name, String root)
    {
      mDriverName = name;
      mDeviceRoot = root;
    }
    

    Vector<File> mDevices = null;
    
    public Vector<File> getDevices() { if (mDevices == null) {
        mDevices = new Vector();
        File dev = new File("/dev");
        File[] files = dev.listFiles();
        
        for (int i = 0; i < files.length; i++) {
          if (files[i].getAbsolutePath().startsWith(mDeviceRoot)) {
            Log.d("SerialPort", "Found new device: " + files[i]);
            mDevices.add(files[i]);
          }
        }
      }
      return mDevices;
    }
    
    public String getName() { return mDriverName; }
  }
  



  private Vector<Driver> mDrivers = null;
  
  Vector<Driver> getDrivers() throws IOException {
    if (mDrivers == null) {
      mDrivers = new Vector();
      LineNumberReader r = new LineNumberReader(new FileReader("/proc/tty/drivers"));
      String l;
      while ((l = r.readLine()) != null)
      {
        String drivername = l.substring(0, 21).trim();
        String[] w = l.split(" +");
        if ((w.length >= 5) && (w[(w.length - 1)].equals("serial"))) {
          Log.d("SerialPort", "Found new driver " + drivername + " on " + w[(w.length - 4)]);
          mDrivers.add(new Driver(drivername, w[(w.length - 4)]));
        }
      }
      r.close();
    }
    return mDrivers;
  }
  
  public String[] getAllDevices() {
    Vector<String> devices = new Vector();
    
    try
    {
      Iterator<Driver> itdriv = getDrivers().iterator();
      Iterator<File> itdev; for (; itdriv.hasNext(); 
          

          itdev.hasNext())
      {
        Driver driver = (Driver)itdriv.next();
        itdev = driver.getDevices().iterator();
        String device = ((File)itdev.next()).getName();
        String value = String.format("%s (%s)", new Object[] { device, driver.getName() });
        devices.add(value);
        //continue;
      }
    }
    catch (IOException e) {
      e.printStackTrace();
    }
    return (String[])devices.toArray(new String[devices.size()]);
  }
  
  public String[] getAllDevicesPath() {
    Vector<String> devices = new Vector();
    
    try
    {
      Iterator<Driver> itdriv = getDrivers().iterator();
      Iterator<File> itdev; for (; itdriv.hasNext(); 
          

          itdev.hasNext())
      {
        Driver driver = (Driver)itdriv.next();
        itdev = driver.getDevices().iterator();
        //continue;
        String device = ((File)itdev.next()).getAbsolutePath();
        devices.add(device);
      }
    }
    catch (IOException e) {
      e.printStackTrace();
    }
    return (String[])devices.toArray(new String[devices.size()]);
  }
}
