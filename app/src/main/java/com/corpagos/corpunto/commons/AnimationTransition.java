package com.corpagos.corpunto.commons;

import android.app.Activity;

import com.corpagos.corpunto.R;

public class AnimationTransition {
    public static void setInActivityTransition(Activity activity) {
        activity.overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }

    public static void setOutActivityTransition(Activity activity) {
        activity.overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
    }

    public static void setInDownActivityTrantition(Activity activity){
        activity.overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
    }

    public static void setInUpActiviTrantition(Activity activity){
        activity.overridePendingTransition(R.anim.slide_in_down,R.anim.slide_out_down);
    }
}
