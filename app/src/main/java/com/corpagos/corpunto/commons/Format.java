package com.corpagos.corpunto.commons;

import android.content.Context;

import com.corpagos.corpunto.utils.Util;

import org.jetbrains.annotations.NotNull;

import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import static com.corpagos.corpunto.utils.SimDetect.getSimStatus;

public class Format {

    public static String datePattern = "dd/MM/yyyy";
    public static String dateFullPattern = "dd/MM/yyyy - HH:mm:ss";
    public static String hourPattern = "hh:mm:ss aaa";
    public static String dayPattern = "d";
    private static SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static SimpleDateFormat formatterYear = new SimpleDateFormat("MM/dd/yyyy");

    public static String getHoursWithDecimalsFormatFromSeconds(long seconds) {
        float time = ((float) seconds /60) / 60;
        return String.format("%.2f", time);
    }

    public static int getHoursFormatFromSeconds(long seconds) {
        return (int) ((seconds/60)/60);
    }

    public static String getCashFormat(int cash) {
        String pattern = "#,###.00";
        DecimalFormat format = (DecimalFormat) NumberFormat.getNumberInstance(Locale.GERMAN);
        format.applyPattern(pattern);
        return format.format(cash) + " Bs.S";
    }

    public static String getCashFormatWithoutSymbol(int cash) {
        String pattern = "#,###.00";
        DecimalFormat format = (DecimalFormat) NumberFormat.getNumberInstance(Locale.GERMAN);
        format.applyPattern(pattern);
        return format.format(cash);
    }

    public static String getCashFormat(float cash) {
        if (cash == 0.0) {
            return "0,00";
        }
        String pattern = "#,###.00";
        DecimalFormat format = (DecimalFormat) NumberFormat.getNumberInstance(Locale.GERMAN);
        format.applyPattern(pattern);
        return format.format(cash) ;
    }

    public static String getCashFormat(double cash) {
        if (cash == 0.0) {
            return "0,00";
        }
        String pattern = "#,###.00";
        DecimalFormat format = (DecimalFormat) NumberFormat.getNumberInstance(Locale.GERMAN);
        format.applyPattern(pattern);
        return format.format(cash) ;
    }

    public static String getCardNumberPrintFormat(String cardNumber) {
        char[] c = cardNumber.toCharArray();
        for (int i = 0; i < cardNumber.length(); i++) {
            if ((i > 5) && (i < cardNumber.length() - 4)) {
                c[i] = '*';
            }
        }
        cardNumber = String.copyValueOf(c);
        return cardNumber;
    }

    public static String getLastNumberFromCard(String cardNumber) {
        cardNumber = cardNumber.substring(cardNumber.length() - 4);
        return "*"+cardNumber;
    }

    public static String getMotnhForInt(int num) {
        String month = "wrong";
        DateFormatSymbols dfs =  new DateFormatSymbols();
        String[] months = dfs.getMonths();
        num = num - 1;
        if (num >= 0 && num <= 11) {
            month = months[num];
        }
        return month.substring(0,3).toUpperCase();
    }

    public static String getLocalDateFormat(Context context, String pattern, String dateToFormat) {
        String serverDateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
        String outDate;
        SimpleDateFormat formatter = new SimpleDateFormat(serverDateFormat);
        formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date value = null;
        try {
            value = formatter.parse(dateToFormat);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        TimeZone timeZone = TimeZone.getTimeZone("America/Manaus");
        SimpleDateFormat dateFormatter = new SimpleDateFormat(pattern); //this format changeable
        dateFormatter.setTimeZone(timeZone);
        outDate = dateFormatter.format(value);
        return outDate;
    }

    public static Date dateFormat(String isoDate) {
        TimeZone tz = TimeZone.getTimeZone("UTC");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        df.setTimeZone(tz);
        try {
            return df.parse(isoDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static String moneyFormat(String number) {
        Locale locale = Locale.GERMAN;
        NumberFormat numberFormat = NumberFormat.getCurrencyInstance(locale);
        return numberFormat.format(number);
    }

    public static String parseDate(@NotNull Long timeAtMiliseconds) {
        timeAtMiliseconds *= 1000L;
        if (timeAtMiliseconds == 0) {
            return "";
        }
        String result = "now";
        String dataSot = formatter.format(new Date());
        Calendar calendar = Calendar.getInstance();
        long dayagolong = timeAtMiliseconds;
        calendar.setTimeInMillis(dayagolong);
        String agoformater = formatter.format(calendar.getTime());
        Date CurrentDate = null;
        Date CreateDate = null;
        try {
            CurrentDate = formatter.parse(dataSot);
            CreateDate = formatter.parse(agoformater);

            long different = Math.abs(CurrentDate.getTime() - CreateDate.getTime());

            long secondsInMilli = 1000;
            long minutesInMilli = secondsInMilli * 60;
            long hoursInMilli = minutesInMilli * 60;
            long daysInMilli = hoursInMilli * 24;

            long elapsedDays = different / daysInMilli;
            different = different % daysInMilli;

            long elapsedHours = different / hoursInMilli;
            different = different % hoursInMilli;

            long elapsedMinutes = different / minutesInMilli;
            different = different % minutesInMilli;

            long elapsedSeconds = different / secondsInMilli;

            if (elapsedDays == 0) {
                if (elapsedHours == 0) {
                    if (elapsedMinutes == 0) {
                        if (elapsedSeconds < 0) {
                            return "0" + " s";
                        } else {
                            if (elapsedSeconds > 0 && elapsedSeconds < 59) {
                                return "now";
                            }
                        }
                    } else {
                        return String.valueOf(elapsedMinutes) + "m ago";
                    }
                } else {
                    return String.valueOf(elapsedHours) + "h ago";
                }

            } else {
                if (elapsedDays <= 29) {
                    return String.valueOf(elapsedDays) + "d ago";
                }
                if (elapsedDays > 29 && elapsedDays <= 58) {
                    return "1Mth ago";
                }
                if (elapsedDays > 58 && elapsedDays <= 87) {
                    return "2Mth ago";
                }
                if (elapsedDays > 87 && elapsedDays <= 116) {
                    return "3Mth ago";
                }
                if (elapsedDays > 116 && elapsedDays <= 145) {
                    return "4Mth ago";
                }
                if (elapsedDays > 145 && elapsedDays <= 174) {
                    return "5Mth ago";
                }
                if (elapsedDays > 174 && elapsedDays <= 203) {
                    return "6Mth ago";
                }
                if (elapsedDays > 203 && elapsedDays <= 232) {
                    return "7Mth ago";
                }
                if (elapsedDays > 232 && elapsedDays <= 261) {
                    return "8Mth ago";
                }
                if (elapsedDays > 261 && elapsedDays <= 290) {
                    return "9Mth ago";
                }
                if (elapsedDays > 290 && elapsedDays <= 319) {
                    return "10Mth ago";
                }
                if (elapsedDays > 319 && elapsedDays <= 348) {
                    return "11Mth ago";
                }
                if (elapsedDays > 348 && elapsedDays <= 360) {
                    return "12Mth ago";
                }

                if (elapsedDays > 360 && elapsedDays <= 720) {
                    return "1 year ago";
                }

                if (elapsedDays > 720) {
                    Calendar calendarYear = Calendar.getInstance();
                    calendarYear.setTimeInMillis(dayagolong);
                    return formatterYear.format(calendarYear.getTime()) + "";
                }

            }

        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        return result;
    }


}
