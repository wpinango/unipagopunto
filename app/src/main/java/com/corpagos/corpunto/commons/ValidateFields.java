package com.corpagos.corpunto.commons;

import android.text.InputFilter;
import android.text.Spanned;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

public class ValidateFields {

    public static InputFilter[] getIpformat() {
        InputFilter[] filters = new InputFilter[1];
        filters[0] = new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                if (end > start) {
                    String destTxt = dest.toString();
                    String resultingTxt = destTxt.substring(0, dstart) + source.subSequence(start, end)
                            + destTxt.substring(dend);
                    if (!resultingTxt.matches ("^\\d{1,3}(\\.(\\d{1,3}(\\.(\\d{1,3}(\\.(\\d{1,3})?)?)?)?)?)?")) {
                        return "";
                    } else {
                        String[] splits = resultingTxt.split("\\.");
                        for (String split : splits) {
                            if (Integer.valueOf(split) > 255) {
                                return "";
                            }
                        }
                    }
                }
                return null;
            }
        };
        return filters;
    }


    /*public <T> T validate(String response, T a) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(response);

            //People people= gson.fromJson(jsonObject.getJSONObject("data").toString(), People.class);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }*/

}
