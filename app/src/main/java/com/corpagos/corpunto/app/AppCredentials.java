package com.corpagos.corpunto.app;

import android.content.Context;

import com.corpagos.corpunto.databases.BackupFiles;
import com.corpagos.corpunto.models.Token;
import com.corpagos.corpunto.utils.SharedPreferencesHandler;
import com.google.gson.Gson;


public class AppCredentials {

    public static boolean isLogin(Context context) {
        boolean isLogin = false;
        if (!SharedPreferencesHandler.getSavedData(context,
                SharedPreferencesHandler.KEY_TOKEN, SharedPreferencesHandler.FILE_TOKEN).equals(
                SharedPreferencesHandler.NOT_FOUND) && !SharedPreferencesHandler
                .getSavedData(context, SharedPreferencesHandler.KEY_TOKEN,
                        SharedPreferencesHandler.FILE_TOKEN).equals("")) {
            isLogin = true;
        }
        return isLogin;
    }

    public static Token getToken(Context context) {
        Token tokenObject = new Token();
        if (AppCredentials.isLogin(context)) {
            tokenObject = new Gson().fromJson(SharedPreferencesHandler.getSavedData(
                    context, SharedPreferencesHandler.KEY_TOKEN,
                    SharedPreferencesHandler.FILE_TOKEN), Token.class);

        }
        return tokenObject;
    }

    public static Token getUserToken(Context context) {
        Token tokenObject = new Token();
        if (AppCredentials.isLogin(context)) {
            tokenObject = new Gson().fromJson(SharedPreferencesHandler.getSavedData(
                    context, SharedPreferencesHandler.KEY_USER_TOKEN,
                    SharedPreferencesHandler.KEY_USER_TOKEN_FILE), Token.class);
        }
        return tokenObject;
    }

    public static void deleteToken(Context context) {
        SharedPreferencesHandler.saveData(context,
                SharedPreferencesHandler.KEY_TOKEN,
                SharedPreferencesHandler.NOT_FOUND,
                SharedPreferencesHandler.FILE_TOKEN);
        BackupFiles.writeFiles(context, BackupFiles.configBackupFileName, "");
    }
}
