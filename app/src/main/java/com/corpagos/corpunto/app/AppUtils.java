package com.corpagos.corpunto.app;

import android.content.Context;

public class AppUtils {

    public static boolean isTimeAutomatic(Context context) {
        return android.provider.Settings.System.getInt(context.getContentResolver(),
                android.provider.Settings.System.AUTO_TIME, 0) == 1;
    }
}
