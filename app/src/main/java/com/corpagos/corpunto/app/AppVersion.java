package com.corpagos.corpunto.app;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import com.corpagos.corpunto.utils.SharedPreferencesHandler;

public class AppVersion {

    public static int versionCompare(String newVersion, String actualVersion) {
        String[] vals1 = newVersion.split("\\.");
        String[] vals2 = actualVersion.split("\\.");
        int i = 0;
        while (i < vals1.length && i < vals2.length && vals1[i].equals(vals2[i])) {
            i++;
        }
        if (i < vals1.length && i < vals2.length) {
            int diff = Integer.valueOf(vals1[i]).compareTo(Integer.valueOf(vals2[i]));
            return Integer.signum(diff);
        }
        return Integer.signum(vals1.length - vals2.length);
    }

    public static boolean checkCurrentInstalledVersion(String newVersion, String actualVersion) {
        System.out.println("version : " + versionCompare(newVersion, actualVersion));
        if (versionCompare(newVersion, actualVersion) == 1) {                                       //newVersion es mayor que actualVersion
            return false;
        } else if (versionCompare(newVersion, actualVersion) == 0) {                                //newVersion es igual que actualVersion
            return true;
        } else return versionCompare(newVersion, actualVersion) == -1;                              //newVersion es menor que actualVersion
    }

    public static String getAppVersion(Context context) {
        PackageInfo pInfo;
        try {
            pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return SharedPreferencesHandler.NOT_FOUND;
        }
        return pInfo != null ? pInfo.versionName : SharedPreferencesHandler.NOT_FOUND;
    }
}
