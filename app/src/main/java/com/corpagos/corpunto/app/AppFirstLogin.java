package com.corpagos.corpunto.app;

import android.content.Context;

import com.corpagos.corpunto.databases.BackupFiles;
import com.corpagos.corpunto.models.FirstLogin;
import com.google.gson.Gson;

public class AppFirstLogin {

    public static boolean isLogin(Context context) {
        boolean isLogin = false;
        if (BackupFiles.isFilesExist(context, BackupFiles.loginBackupFileName)) {
            FirstLogin firstLogin = new Gson().fromJson(BackupFiles.getBackupData(context,
                    BackupFiles.loginBackupFileName), FirstLogin.class);
            if (firstLogin.isLogin()) {
                isLogin = true;
            }
        }
        return isLogin;
    }

    public static FirstLogin getFirstLogin(Context context) {
        FirstLogin firstLogin = new FirstLogin();
        if (isLogin(context)) {
            firstLogin = new Gson().fromJson(BackupFiles.getBackupData(context, BackupFiles
                    .loginBackupFileName), FirstLogin.class);

        }
        return firstLogin;
    }

}
