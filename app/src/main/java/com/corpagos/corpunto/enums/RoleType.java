package com.corpagos.corpunto.enums;

public enum RoleType {
    ADMIN, OWNER, USER, TECHNICIAN
}
