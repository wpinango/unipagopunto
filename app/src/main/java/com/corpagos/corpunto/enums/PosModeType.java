package com.corpagos.corpunto.enums;

public enum PosModeType {
    SELL, PAYMENT, PREPAID_SERVICE
}
