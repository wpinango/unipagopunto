package com.corpagos.corpunto.enums;

public enum CardAccountType {
    DEBIT, CREDIT, UNDEFINED
}
