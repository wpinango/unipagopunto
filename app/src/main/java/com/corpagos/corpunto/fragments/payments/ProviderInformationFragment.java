package com.corpagos.corpunto.fragments.payments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.corpagos.corpunto.R;
import com.corpagos.corpunto.activities.PaymentActivity;
import com.corpagos.corpunto.adapters.AccountListAdapter;
import com.corpagos.corpunto.models.Provider;
import com.corpagos.corpunto.models.ProviderAccount;
import com.corpagos.corpunto.models.Transaction;
import com.corpagos.corpunto.widgets.CustomKeyboard;

import java.util.ArrayList;

import androidx.annotation.Nullable;

public class ProviderInformationFragment extends Fragment {

    private Provider provider;
    private TextView tvProviderName, tvProviderTin;
    private ArrayList<ProviderAccount> accounts = new ArrayList<>();
    private Spinner spAccount;
    private AccountListAdapter accountListAdapter;
    private EditText etAmount;
    private ArrayList<String> providerAccounts = new ArrayList<>();
    private CustomKeyboard keyboard;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_provider_information, null);
        etAmount = rootView.findViewById(R.id.et_amount_provider_payment2);
        etAmount.setShowSoftInputOnFocus(false);
        tvProviderName = rootView.findViewById(R.id.tv_provider_name);
        tvProviderTin = rootView.findViewById(R.id.tv_provider_tis);
        spAccount = rootView.findViewById(R.id.sp_provider_account);
        accountListAdapter = new AccountListAdapter(getActivity(), R.layout.spinner_dropdown_item,
                R.id.tv_provider_account, accounts);
        tvProviderTin.setText("J-" + provider.getTin());
        tvProviderName.setText(provider.getName());
        accountListAdapter.notifyDataSetChanged();
        populateArray();
        ArrayAdapter<String> accountAdapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_item,
                providerAccounts);
        accountAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        spAccount.setAdapter(accountAdapter);
        etAmount.setSelection(etAmount.getText().toString().length());
        etAmount.setOnFocusChangeListener((view, b) -> {
            if (b){
                InputConnection ic2 = etAmount.onCreateInputConnection(new EditorInfo());
                keyboard.setInputConnection(ic2);
            }
        });
        spAccount.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                etAmount.requestFocus();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        return rootView;
    }

    private void populateArray() {
        accounts = new ArrayList<>();
        providerAccounts = new ArrayList<>();
        accounts = provider.getAccounts();
        for (ProviderAccount p : accounts) {
            providerAccounts.add(p.getBank().getName() + " *" + p.getAccount().substring(
                    p.getAccount().length() - 4));
        }
    }

    public void setKeyboard(CustomKeyboard keyboard){
        this.keyboard = keyboard;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    public void selectAccount() {
        Transaction transaction = new Transaction();
        transaction.setAmount(Float.valueOf(etAmount.getText().toString().replace(
                ".", "").replace(",", ".")));
        ((PaymentActivity)getActivity()).showProviderPaymentFragment(provider,
                provider.getAccounts().get(spAccount.getSelectedItemPosition()), transaction);
    }

    public boolean validate() {
        boolean isValid = true;
        if (etAmount.getText().toString().isEmpty()) {
            isValid = false;
            etAmount.setError("Introduzca un monto valido");
        } else {
            etAmount.setError(null);
        }
        return isValid;
    }
}
