package com.corpagos.corpunto.fragments.payments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.corpagos.corpunto.R;
import com.corpagos.corpunto.activities.PaymentActivity;

import androidx.annotation.Nullable;

public class FirstFragment extends Fragment  {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_first, null);
        rootView.findViewById(R.id.btn_frec_payment).setOnClickListener(v ->
                ((PaymentActivity)getActivity()).showFindProviderInfo());
        return rootView;
    }
}
