package com.corpagos.corpunto.fragments.historicals;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;

import com.corpagos.corpunto.Asynctask;
import com.corpagos.corpunto.Global;
import com.corpagos.corpunto.R;
import com.corpagos.corpunto.activities.QRCodeReaderActivity;
import com.corpagos.corpunto.activities.RollbackActivity;
import com.corpagos.corpunto.adapters.CalendarGridAdapter;
import com.corpagos.corpunto.adapters.TransactionHistoryListAdapter;
import com.corpagos.corpunto.app.AppCredentials;
import com.corpagos.corpunto.commons.AnimationTransition;
import com.corpagos.corpunto.commons.Format;
import com.corpagos.corpunto.databases.BackupFiles;
import com.corpagos.corpunto.databases.ClosureDBHelper;
import com.corpagos.corpunto.databases.TransactionDBHelper;
import com.corpagos.corpunto.dialogs.Dialog;
import com.corpagos.corpunto.enums.PosModelType;
import com.corpagos.corpunto.interfaces.AsynctaskListener;
import com.corpagos.corpunto.interfaces.ClickInterface;
import com.corpagos.corpunto.interfaces.DialogInterface;
import com.corpagos.corpunto.interfaces.OnClick;
import com.corpagos.corpunto.interfaces.ReadCardListener;
import com.corpagos.corpunto.logs.CustomizedExceptionHandler;
import com.corpagos.corpunto.models.CardData;
import com.corpagos.corpunto.models.DailyClosure;
import com.corpagos.corpunto.models.OwnCalendar;
import com.corpagos.corpunto.models.OwnerData;
import com.corpagos.corpunto.models.Response;
import com.corpagos.corpunto.models.Summary;
import com.corpagos.corpunto.models.Token;
import com.corpagos.corpunto.models.Transaction;
import com.corpagos.corpunto.models.TransactionType;
import com.corpagos.corpunto.pos.PosDevice;
import com.corpagos.corpunto.print.PrintTicket;
import com.corpagos.corpunto.requests.Request;
import com.corpagos.corpunto.singletons.PosModel;
import com.corpagos.corpunto.utils.SharedPreferencesHandler;
import com.corpagos.corpunto.utils.Time;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.imagpay.enums.CardDetected;
import com.imagpay.enums.PrintStatus;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import static com.corpagos.corpunto.Global.chipCount;
import static com.corpagos.corpunto.Global.nfcCount;
import static com.corpagos.corpunto.Global.printCount;
import static com.corpagos.corpunto.Global.printLinesCount;
import static com.corpagos.corpunto.Global.timeCount;
import static com.corpagos.corpunto.activities.MainLauncherActivity.version;
import static com.corpagos.corpunto.activities.TransactionHistoryActivity1.rankEnd;
import static com.corpagos.corpunto.activities.TransactionHistoryActivity1.rankInit;
import static com.corpagos.corpunto.commons.Format.getHoursFormatFromSeconds;

public class SellTransactionFragment extends Fragment implements
        ClickInterface, OnClick, AsynctaskListener, ReadCardListener {

    private ArrayList<Transaction> transactionHistories = new ArrayList<>();
    private ArrayList<Transaction> transactionHistoriesTemp = new ArrayList<>();
    private ArrayList<OwnCalendar> ownCalendars = new ArrayList<>();
    private TransactionHistoryListAdapter transactionHistoryListAdapter;
    private CalendarGridAdapter calendarGridAdapter;
    private Dialog dialog;
    private int selectedPosition = -1;
    private String endDate, today, starDate;
    private TransactionDBHelper dbHelper;
    private OwnerData ownerData;
    private SearchView searchView;
    private Button btnShowTransaction, btnRollBack;
    private PosDevice posDevice;
    private PosModel posModel;
    private ProgressBar progressBar;
    private RecyclerView lvTransaction;
    boolean isLoading = false;
    private ArrayList<Transaction> transactions = new ArrayList<>();

    public TransactionHistoryListAdapter getTransactionHistoryListAdapter() {
        return transactionHistoryListAdapter;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_transaction_history, null);
        dialog = new Dialog(getActivity());
        posModel = PosModel.getInstance(getActivity());
        System.out.println("valores : " + new Gson().toJson(posModel));
        dbHelper = new TransactionDBHelper(getActivity());
        lvTransaction = rootView.findViewById(R.id.lv_transaction);
        RecyclerView rvCalendar = rootView.findViewById(R.id.rv_provider_calendar);
        ownerData = new Gson().fromJson(BackupFiles.getBackupData(getActivity(),
                BackupFiles.ownerBackupFileName), OwnerData.class);
        calendarGridAdapter = new CalendarGridAdapter(getActivity(), ownCalendars, this);
        transactionHistoryListAdapter = new TransactionHistoryListAdapter(getActivity(),
                transactionHistories, this);
        progressBar = rootView.findViewById(R.id.progressBar3);
        progressBar.setVisibility(View.INVISIBLE);
        posDevice = new PosDevice(getActivity(), this, getActivity(), false);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        rvCalendar.setLayoutManager(layoutManager);
        rvCalendar.setAdapter(calendarGridAdapter);
        rvCalendar.smoothScrollToPosition(29);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        lvTransaction.setLayoutManager(llm);
        lvTransaction.setAdapter(transactionHistoryListAdapter);
        btnRollBack = rootView.findViewById(R.id.btn_rollback_transaction);
        btnShowTransaction = rootView.findViewById(R.id.btn_show_transaction);
        populateCalendarDays();
        if (transactionHistoriesTemp.isEmpty()) {
            getTransactionStorage();
        }
        btnRollBack.setOnClickListener(v -> {
            showRollBackDialog(selectedPosition);
        });
        btnShowTransaction.setOnClickListener(v -> {
            showTransactionInformationDialog(selectedPosition);
        });
        rootView.findViewById(R.id.btn_balance_transaction).setOnClickListener(v -> {
            if (!transactionHistoriesTemp.isEmpty()) {
                dialog.showBalanceDialog(
                        "Total ingreso:\t" + Format.getCashFormat(getIncomeBalance()) +
                                "\nTotal reverso:\t" + Format.getCashFormat(getRollbackBalance()) +
                                "\nTotal balance:\t" + Format.getCashFormat(getBalance()) +
                                "\nPor credito:\t\t" + Format.getCashFormat(getCreditBalance()) +
                                "\nPor debito:\t\t" + Format.getCashFormat(getDebitBalance()) +
                                "\nOperaciones:\t" + getTransactionSize());
            } else {
                Global.showToast(getActivity(), "No se encontro operaciones en este dia",
                        1000);
            }
        });
        rootView.findViewById(R.id.btn_closure_transaction).setOnClickListener(view -> {
            requestClosing();
        });
        rootView.findViewById(R.id.btn_section_transaction).setOnClickListener(view -> {
            insertSectionInTransactions();
        });
        setButtonViewStatus(false);
        ClosureDBHelper closureDBHelper = new ClosureDBHelper(getActivity());
        System.out.println("valores : closure : " + new Gson().toJson(closureDBHelper.getDailyClosure()));
        Thread.setDefaultUncaughtExceptionHandler(new CustomizedExceptionHandler());
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    private String getTransactionSize() {
        int total = 0;
        for (Transaction t : transactionHistories) {
            if (t.getTransactionType().equals(TransactionType.SALE) ||
                    t.getTransactionType().equals(TransactionType.ROLLBACK)) {
                total += 1;
            }
        }
        return String.valueOf(total);
    }

    private double getCreditBalance() {
        double total = 0.0;
        for (Transaction t : transactionHistories) {
            if (t.getCard_type().equals("CREDIT")) {
                total += t.getAmount();
            }
        }
        return total;
    }

    private double getDebitBalance() {
        double total = 0.0;
        for (Transaction t : transactionHistories) {
            if (t.getCard_type().equals("DEBIT")) {
                total += t.getAmount();
            }
        }
        return total;
    }

    private double getBalance() {
        double total = 0.0;
        for (Transaction t : transactionHistories) {
            if (t.getTransactionType().equals(TransactionType.SALE) ||
                    t.getTransactionType().equals(TransactionType.ROLLBACK)) {
                total += t.getAmount();
            }
        }
        return total;
    }

    private double getRollbackBalance() {
        double total = 0.0;
        for (Transaction t : transactionHistories) {
            if (t.getAmount() < 0.0 && t.getTransactionType().equals(TransactionType.ROLLBACK)) {
                total += t.getAmount();
            }
        }
        return total;
    }

    private float getIncomeBalance() {
        float total = 0.0f;
        for (Transaction t : transactionHistories) {
            if (t.getAmount() > 0.0 && t.getTransactionType().equals(TransactionType.SALE)) {
                total += t.getAmount();
            }
        }
        return total;
    }

    public void getTransactionStorage() {
        transactionHistoriesTemp.clear();
        transactionHistoriesTemp.addAll(dbHelper.getTransactionList());
        if (transactionHistoriesTemp.isEmpty()) {
            Type custom = new TypeToken<ArrayList<Transaction>>() {
            }.getType();
            if (BackupFiles.isFilesExist(getActivity(), BackupFiles.transactionsBackupFileName)) {
                transactionHistoriesTemp = new Gson().fromJson(BackupFiles.readFileContent(
                        getActivity(), BackupFiles.transactionsBackupFileName), custom);
            }
        }
        checkDateInDataBase();
        //populateOperations();
        populateDayOperations(today);
        initScrollListener();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_transaction_search:
                break;
            case android.R.id.home:
                if (!searchView.isIconified()) {
                    searchView.setIconified(true);
                } //else {
                    //getfinish();
                    //AnimationTransition.setOutActivityTransition(this);
                //}
                break;
            case R.id.menu_sync:
                Global.showToast(getActivity(), "Sincronizando...", 3000);
                Request.getRequestWithToken(getActivity(), Asynctask.transactionalHost,
                        Asynctask.URL_GET_LAST_OPERATIONS, new HashMap<>(), this);
                break;
            default:
                return super.onOptionsItemSelected(item);

        }
        return true;
    }

    private void turnOffReadingPeripherals() {
        posDevice.turnOffReadingPeripherals();
    }

    /*private void initReadingPeripherals() {
        posDevice.initReadingPeripherals();
    }*/

    private void populateCalendarDays() {
        ownCalendars.clear();
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, -7);
        for (int i = rankInit; i < rankEnd; i++) {
            calendar.add(Calendar.DAY_OF_MONTH, +1);
            OwnCalendar ownCalendar = new OwnCalendar();
            int day = calendar.get(Calendar.DAY_OF_MONTH);
            int month = calendar.get(Calendar.MONTH) + 1;
            int year = calendar.get(Calendar.YEAR);
            String dateString = String.format("%d-%d-%d", year, month, day);
            Date date = null;
            try {
                date = new SimpleDateFormat("yyyy-M-d").parse(dateString);
                if (i == rankInit) {
                    starDate = dateString;
                    System.out.println();
                } else if (i == (rankEnd - 1)) {
                    endDate = dateString;
                    today = String.valueOf(day);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
            ownCalendar.setDayNumber(new SimpleDateFormat("d").format(date));
            ownCalendar.setMonthNumber(new SimpleDateFormat("M").format(date));
            ownCalendar.setDayName(new SimpleDateFormat("EEE", new Locale("es",
                    "ES")).format(date));
            ownCalendar.setMonth(new SimpleDateFormat("MMM", new Locale("es",
                    "ES")).format(date));
            ownCalendars.add(ownCalendar);
            if (i == 6) {
                ownCalendar.setSelect(true);
            }
        }
        calendarGridAdapter.notifyDataSetChanged();
    }

    @Override
    public void onclick(int position) {
        progressBar.setVisibility(View.VISIBLE);
        Handler handler = new Handler();
        handler.postDelayed(() -> {
            OwnCalendar.deselectAll(ownCalendars);
            ownCalendars.get(position).setSelect(true);
            calendarGridAdapter.notifyDataSetChanged();
            populateDayOperations(ownCalendars.get(position).getDayNumber());
            initScrollListener();
            removeFocusFromTransactionList();
            progressBar.setVisibility(View.INVISIBLE);
        }, 200);
    }

    private void populateDayOperations(String day) {
        transactionHistories.clear();
        transactions.clear();
        for (int i = 0; i < transactionHistoriesTemp.size(); i++) {
            if (new SimpleDateFormat("d").format(Format.dateFormat(
                    transactionHistoriesTemp.get(i).getCreated_at())).equals(day) &&
                    !transactionHistoriesTemp.get(i).getTransactionType().equals(TransactionType.PAYMENT) &&
                    !transactionHistoriesTemp.get(i).getTransactionType().equals(TransactionType.PREPAID_SERVICE) &&
                    !transactionHistoriesTemp.get(i).getTransactionType().equals(TransactionType.PAYMENT_PROVIDERS)) {
                transactions.add(0, transactionHistoriesTemp.get(i));
            }
        }
        int index = 0;
        for (int i = transactions.size() - 1; i >= 0; i--) {
            if (transactions.get(i).getTransactionType().equals(TransactionType.SALE) ||
                    transactions.get(i).getTransactionType().equals(TransactionType.ROLLBACK)) {
                index += 1;
                transactions.get(i).setIndex(index);
            }
        }
        transactionHistoryListAdapter.notifyDataSetChanged();
        //if (transactionHistories.isEmpty()) {
        //Global.showToast(getActivity(), "No se encontro operaciones en este dia",
        //        1000);
           /* if (today.equals(Time.getCurrentDay())) {

            }*/
        //}
    }

    private void initScrollListener() {
        int i = 0;
        while (i < 10) {
            if (transactions.size() <= i) {
                break;
            }
            transactionHistories.add(transactions.get(i));
            i++;
        }
        lvTransaction.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                if (!isLoading) {
                    if (linearLayoutManager != null && linearLayoutManager
                            .findLastCompletelyVisibleItemPosition() == transactionHistories.size() - 1
                            && transactions.size() > 10) {
                        loadMore();
                        isLoading = true;
                        progressBar.setVisibility(View.VISIBLE);
                    }
                }
            }
        });
    }

    private void loadMore() {
        Handler handler = new Handler();
        handler.postDelayed(() -> {
            int scrollPosition = transactionHistories.size();
            transactionHistoryListAdapter.notifyItemRemoved(scrollPosition);
            int currentSize = scrollPosition;
            int nextLimit = currentSize + 10;
            while (currentSize - 1 < nextLimit) {
                if (currentSize >= transactions.size()) {
                    break;
                }
                transactionHistories.add(transactions.get(currentSize));
                currentSize++;
            }
            transactionHistoryListAdapter.notifyDataSetChanged();
            isLoading = false;
            progressBar.setVisibility(View.INVISIBLE);
        }, 500);
    }

    private void checkDateInDataBase() {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-M-d");
            Date initDate = sdf.parse(starDate);
            for (int i = 0; i < transactionHistoriesTemp.size(); i++) {
                Date date = sdf.parse(sdf.format(Format.dateFormat(
                        transactionHistoriesTemp.get(i).getCreated_at())));
                if (date.compareTo(initDate) < 0) {
                    dbHelper.deleteTransaction(transactionHistoriesTemp.get(i).getReference());
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(int position) {
        if (transactionHistoryListAdapter.getFilteredData().get(position).isSelected()) {
            transactionHistoryListAdapter.getFilteredData().get(position).setSelected(false);
            selectedPosition = -1;
            setButtonViewStatus(false);
        } else {
            selectedPosition = position;
            for (Transaction t : transactionHistories) {
                t.setSelected(false);
            }
            transactionHistoryListAdapter.getFilteredData().get(position).setSelected(true);
            setButtonViewStatus(true);
        }
        transactionHistoryListAdapter.notifyDataSetChanged();
    }

    private void printTicket(ArrayList<String> items, ArrayList<String> headers, ArrayList<String>
            footers) {
        posDevice.printTicket(items, headers, footers);
    }

    @Override
    public void onAsynctaskFinished(String endPoint, String response, String headers) {
        dialog.dismissDialog();
        System.out.println("valores reverso : " + response);
        if (!response.equals("") && !response.equals(Asynctask.TIMEOUT_MESSAGE) &&
                !response.equals(Asynctask.FAILED_TO_CONNECT)) {
            Response res = new Gson().fromJson(response, Response.class);
            if (!res.isError()) {
                if (endPoint.equals(Asynctask.URL_GET_CLOSING)) {
                    onclick(ownCalendars.size() - 1);
                    System.out.println("valores cierre 0 : " + Asynctask.getDecryptedAESResponse(
                            getActivity(), new Gson().toJson(res.getData())));
                    DailyClosure dailyClosure = new Gson().fromJson(Asynctask.getDecryptedAESResponse(
                            getActivity(), new Gson().toJson(res.getData())), DailyClosure.class);
                    System.out.println("valores cierre : " + new Gson().toJson(dailyClosure));
                    if (dailyClosure == null) {
                        dialog.showErrorDialogWithListenerButton(
                                "No se puede hacer el cierre sin operaciones.", null);
                    } else {
                        try {
                            if (!dailyClosure.getPublic_key().equals("")) {
                                dialog.showProgressDialog("Configurando...");
                                Request.postRequestToken(getActivity(), Asynctask.transactionalHost,
                                        Asynctask.URL_STORE_AES, Asynctask.getEncryptedRSARequest(
                                                getActivity(), dailyClosure.getPublic_key()),
                                        new HashMap<>(), this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        Transaction transaction = new Transaction();
                        transaction.setTransactionType(TransactionType.CLOSURE);
                        transaction.setAmount(dailyClosure.getSummary().getTotal_amount());
                        transaction.setApp_version(version);
                        transaction.setCreated_at(dailyClosure.getSummary().getClose_date());
                        transaction.setDate(Time.getCurrentDate());
                        transaction.setSelected(false);
                        transaction.setLot(dailyClosure.getSummary().getLot_number());
                        transaction.setReference("1041551637202");
                        transaction.setReverse(1);
                        transaction.setStatus("ACCEPTED");
                        transaction.setCard_type("NOT_APPLY");
                        dbHelper.updateClosureStatus();
                        dbHelper.insertTransaction(transaction);
                        getTransactionStorage();
                        ClosureDBHelper closureDBHelper = new ClosureDBHelper(getActivity());
                        closureDBHelper.insertClosure(dailyClosure);
                        dialog.showDailyClosureDialog("Cierre", dailyClosure, new DialogInterface() {
                            @Override
                            public void positiveClick() {
                                if (!posModel.getPosModelType().equals(PosModelType.WPOS_MINI)) {
                                    //initReadingPeripherals();
                                    try {
                                        Thread.sleep(500);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                    printTicket(PrintTicket.getTicketClosureResume(dailyClosure,
                                            ownerData, posModel.getPosInformation().getPosSerial()),
                                            PrintTicket.getHeaders(ownerData, "REPORTE TOTAL"),
                                            null);
                                }
                            }

                            @Override
                            public void negativeClick() {
                                if (!posModel.getPosModelType().equals(PosModelType.WPOS_MINI)) {
                                    //initReadingPeripherals();
                                    try {
                                        Thread.sleep(500);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                    printTicket(PrintTicket.getTicketClosureDetails(getActivity(),
                                            dailyClosure, ownerData, posModel.getPosInformation().getPosSerial()),
                                            PrintTicket.getHeaders(ownerData, "REPORTE DETALLADO"),
                                            null);
                                }
                            }
                        }, posModel);
                        if (ownerData.getLot() >= 9999) {
                            ownerData.setLot(0);
                        } else {
                            ownerData.setLot(ownerData.getLot() + 1);
                        }
                        BackupFiles.backupData(getActivity(), BackupFiles.ownerBackupFileName,
                                new Gson().toJson(ownerData));

                    }
                } else if (endPoint.contains(Asynctask.URL_GET_LAST_OPERATIONS)) {
                    if (res.getData() == null) {
                        dialog.showErrorDialogWithListenerButton(
                                "No existen operaciones en el dia de hoy.", null);
                    } else {
                        Type custom = new TypeToken<ArrayList<Transaction>>() {
                        }.getType();
                        ArrayList<Transaction> todayTransactions = new Gson().fromJson(new Gson()
                                .toJson(res.getData()), custom);
                        ArrayList<Transaction> results = new ArrayList<>();
                        for (Transaction t : todayTransactions) {
                            boolean found = false;
                            for (Transaction f : transactionHistories) {
                                if (t.getReference().equals(f.getReference())) {
                                    found = true;
                                }
                            }
                            if (!found) {
                                results.add(t);
                            }
                            System.out.println("valores found : " + new Gson().toJson(results));
                        }
                        //transactionHistories.clear();
                        //transactionHistories.addAll(todayTransactions);
                        //transactionHistoryListAdapter.notifyDataSetChanged();
                    }
                }
            } else {
                switch (res.getMessage()) {
                    case "ERR_INVALID_OR_EXPIRED_TOKEN":
                    case Global.ERR_UNKNOWN_DEVICE_SERIAL:
                        dialog.showErrorDialogWithListenerButton(
                                "Dispositivo no registrado, registrar nuevamente",
                                sweetAlertDialog -> {
                                    dialog.dismissDialog();
                                    AppCredentials.deleteToken(getActivity());
                                    getActivity().finish();
                                });
                        break;
                    case "ERR_TRANSACTION_NOT_FOUND":
                        turnOffReadingPeripherals();
                        dialog.showErrorDialog("Transaccion no encontrada");
                        break;
                    case "ERR_INVALID_OP_PWD":
                        dialog.showErrorDialog("Clave de punto incorrecta");
                        turnOffReadingPeripherals();
                        break;
                    case "ERR_POS_AES_KEYS_NOT_CONFIGURED":
                        dialog.showErrorDialogWithListenerButton(
                                "Dispositivo no se encuentra configurado",
                                sweetAlertDialog -> {
                                    dialog.dismissDialog();
                                    SharedPreferencesHandler.saveData(getActivity(),
                                            SharedPreferencesHandler.KEY_AES_KEY,
                                            SharedPreferencesHandler.NOT_FOUND,
                                            SharedPreferencesHandler.FILE_AES_KEY
                                    );
                                });
                        break;
                    default:
                        turnOffReadingPeripherals();
                        break;
                }
            }
        } else if (response.equals(Asynctask.FAILED_TO_CONNECT)) {
            dialog.showErrorDialog("Error de conexion");
        } else {
            turnOffReadingPeripherals();
            dialog.showSomethingWrongDialog();
        }
    }

    private void insertSectionInTransactions() {
        if (!transactionHistories.isEmpty()) {
            dialog.showInfoDialogWithListenerButtonsAndText("Desea realizar corte?",
                    (materialDialog, which) -> {
                        ArrayList<Transaction> transactions = new ArrayList<>();
                        DailyClosure dailyClosure = new DailyClosure();
                        Summary summary = new Summary();
                        for (int i = 0; i < transactionHistories.size(); i++) {
                            if (transactionHistories.get(i).getReverse() == 0) {
                                if (!transactionHistories.get(i).getTransactionType().equals(
                                        TransactionType.PAYMENT)) {
                                    transactions.add(transactionHistories.get(i));
                                    transactionHistories.get(i).setReverse(1);
                                }
                            }
                        }
                        if (transactions.isEmpty()) {
                            dialog.showErrorDialog("Debe existir una operacion valida");
                        } else {
                            float amount = 0.0f;
                            for (int i = 0; i < transactions.size(); i++) {
                                amount += transactions.get(i).getAmount();
                            }
                            summary.setLot_number(transactions.get(transactions.size() - 1).getLot());
                            summary.setTotal_amount(amount);
                            summary.setClose_date(transactions.get(transactions.size() - 1).getCreated_at());
                            dailyClosure.setTransactions(transactions);
                            dailyClosure.setSummary(summary);
                            Transaction transaction = new Transaction();
                            transaction.setTransactionType(TransactionType.SECTION);
                            transaction.setAmount(dailyClosure.getSummary().getTotal_amount());
                            transaction.setApp_version(version);
                            transaction.setCreated_at(dailyClosure.getSummary().getClose_date());
                            transaction.setDate(Time.getCurrentDate());
                            transaction.setSelected(false);
                            transaction.setLot(dailyClosure.getSummary().getLot_number());
                            transaction.setReference("1041551637202");
                            transaction.setReverse(1);
                            transaction.setStatus("ACCEPTED");
                            transaction.setCard_type("NOT_APPLY");
                            dbHelper.updateClosureStatus();
                            dbHelper.insertTransaction(transaction);
                            getTransactionStorage();
                            ClosureDBHelper closureDBHelper = new ClosureDBHelper(getActivity());
                            closureDBHelper.insertClosure(dailyClosure);
                        }
                    }, (materialDialog, which) -> {
                        dialog.dismissDialog();
                    }, "Aceptar", "Cancelar");
        } else {
            Global.showToast(getActivity(), "No se encontro operaciones en este dia", 3000);
        }
    }

    private void requestClosing() {
        if (!SharedPreferencesHandler.getSavedData(getActivity(), SharedPreferencesHandler.KEY_TOKEN,
                SharedPreferencesHandler.FILE_TOKEN).equals(SharedPreferencesHandler.NOT_FOUND)) {
            dialog.showInfoDialogWithListenerButtons(
                    "Desea realizar el cierre en este momento?", (materialDialog, which) -> {
                        Token token = new Gson().fromJson(SharedPreferencesHandler
                                .getSavedData(getActivity(),
                                        SharedPreferencesHandler.KEY_TOKEN,
                                        SharedPreferencesHandler.FILE_TOKEN), Token.class);
                        Map<String, String> header = new HashMap<>();
                        header.put(Asynctask.authorization, token.getToken_type() + " " +
                                token.getAccess_token());
                        Map<String, Object> body = new HashMap<>();
                        body.put("nfc_count", nfcCount);
                        body.put("chip_count", chipCount);
                        body.put("read_count", 0);
                        body.put("print_count", printCount);
                        body.put("line_count", printLinesCount);
                        body.put("lot_number", ownerData.getLot());
                        body.put("hours_count", getHoursFormatFromSeconds(timeCount));

                        new Asynctask.PostFormMethodAsynctask(Asynctask.transactionalHost,
                                Asynctask.getEncryptedAESRequest(getActivity(), body),
                                Asynctask.URL_GET_CLOSING, header, this).execute();
                        dialog.dismissDialog();
                        dialog.showProgressDialog("Procesando ... ");
                    }, (materialDialog, which) -> {
                        dialog.dismissDialog();
                    });
        } else {
            dialog.showInfoDialogWithListenerButtonsAndText("Aun no ha iniciado sesion," +
                            " por favor escanear credenciales para utilizar el dispositivo POS",
                    (materialDialog, which) -> {
                        Intent intent = new Intent(getActivity(), QRCodeReaderActivity.class);
                        startActivity(intent);
                        dialog.dismissDialog();
                    }, (materialDialog, which) -> dialog.dismissDialog(),
                    "Ingresar", "Cancelar");
        }
        removeFocusFromTransactionList();
    }

    private void showTransactionInformationDialog(int position) {
        if (position != -1) {
            if (!transactionHistoryListAdapter.getFilteredData()
                    .get(position).getTransactionType().equals(TransactionType.CLOSURE) &&
                    !transactionHistoryListAdapter.getFilteredData()
                            .get(position).getTransactionType().equals(TransactionType.SECTION)) {
                Dialog.showTransactionHistories(getActivity(),
                        transactionHistoryListAdapter.getFilteredData().get(position),
                        new DialogInterface() {
                            @Override
                            public void positiveClick() {
                                if (!posModel.getPosModelType().equals(PosModelType.WPOS_MINI)) {
                                   // initReadingPeripherals();
                                    if (!posModel.getPosModelType().equals(PosModelType.WPOS_MINI)) {
                                        try {
                                            Thread.sleep(500);
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }
                                        printTicket(PrintTicket.getHistoricalTicketBody(getActivity(),
                                                posModel.getPosInformation().getPosSerial(),
                                                transactionHistoryListAdapter.getFilteredData()
                                                        .get(position), ownerData),
                                                PrintTicket.getHeaders(ownerData, "COMPRA"),
                                                PrintTicket.getRollBackFooter(String.valueOf(
                                                        transactionHistoryListAdapter.getFilteredData()
                                                                .get(position).getAmount())));
                                    }
                                }
                            }

                            @Override
                            public void negativeClick() {

                            }
                        }, posModel.getPosModelType().equals(PosModelType.WPOS_MINI) ? "Aceptar" : "Imprimir Ticket");
            } else {
                ClosureDBHelper closureDBHelper = new ClosureDBHelper(getActivity());
                DailyClosure dailyClosure = closureDBHelper.getDailyClosureByDate(
                        transactionHistoryListAdapter.getFilteredData().get(position).getCreated_at());
                dialog.showDailyClosureDialog(transactionHistoryListAdapter.getFilteredData()
                                .get(position).getTransactionType()
                                .equals(TransactionType.CLOSURE) ? "Cierre" : "Corte",
                        dailyClosure, new DialogInterface() {
                            @Override
                            public void positiveClick() {
                                if (!posModel.getPosModelType().equals(PosModelType.WPOS_MINI)) {
                                    //initReadingPeripherals();
                                    try {
                                        Thread.sleep(500);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                    printTicket(PrintTicket.getTicketClosureResume(dailyClosure,
                                            ownerData, posModel.getPosInformation().getPosSerial()),
                                            PrintTicket.getHeaders(ownerData, "REPORTE TOTAL"),
                                            null);
                                }
                            }

                            @Override
                            public void negativeClick() {
                                if (!posModel.getPosModelType().equals(PosModelType.WPOS_MINI)) {
                                    //initReadingPeripherals();
                                    try {
                                        Thread.sleep(500);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                    printTicket(PrintTicket.getTicketClosureDetails(getActivity(),
                                            dailyClosure, ownerData, posModel.getPosInformation().getPosSerial()),
                                            PrintTicket.getHeaders(
                                                    ownerData, "REPORTE DETALLADO"), null);
                                }
                            }
                        }, posModel);
            }
            removeFocusFromTransactionList();
        } else {
            Global.showToast(getActivity(), "Debe seleccionar una transaccion", 3000);
        }
    }

    private void showRollBackDialog(int position) {
        if (selectedPosition != -1) {
            if (transactionHistoryListAdapter.getFilteredData().get(selectedPosition).getReverse() == 0) {
                Intent intent = new Intent(getActivity(), RollbackActivity.class);
                intent.putExtra("transaction", new Gson().toJson(transactionHistoryListAdapter.
                        getFilteredData().get(position)));
                startActivity(intent);
                AnimationTransition.setInActivityTransition(getActivity());
                removeFocusFromTransactionList();
            } else {
                Global.showToast(getActivity(), "No se puede reversar", 3000);
            }
        } else {
            Global.showToast(getActivity(), "Debe seleccionar una transaccion", 3000);
        }
    }

    private void removeFocusFromTransactionList() {
        selectedPosition = -1;
        for (Transaction t : transactionHistories) {
            t.setSelected(false);
        }
        transactionHistoryListAdapter.notifyDataSetChanged();
        setButtonViewStatus(false);
    }

    private void setButtonViewStatus(boolean status) {
        btnShowTransaction.setTextColor(getResources().getColor(status ? R.color.white : R.color.icon_color));
        btnRollBack.setTextColor(getResources().getColor(status ? R.color.white : R.color.icon_color));
        Drawable drawableShowTop = getResources().getDrawable(status ? R.drawable.ic_show_white :
                R.drawable.ic_show_disabled);
        Drawable drawableRollBackTop = getResources().getDrawable(status ? R.drawable.ic_rollback_white :
                R.drawable.ic_rollback_disabled);
        btnShowTransaction.setCompoundDrawablesWithIntrinsicBounds(null, drawableShowTop,
                null, null);
        btnRollBack.setCompoundDrawablesWithIntrinsicBounds(null, drawableRollBackTop,
                null, null);
    }

    @Override
    public void onCardRead(String json) {

    }

    @Override
    public void onCardDetect(CardDetected cardDetected, Transaction transaction, CardData cardData) {

    }

    @Override
    public void onPrintStatus(PrintStatus paramPrintStatus) {
        if (paramPrintStatus.equals(PrintStatus.EXIT)) {
            posDevice.turnOffReadingPeripherals();
        }
    }

    @Override
    public void onChipError() {

    }

    @Override
    public void onProcess() {

    }

    @Override
    public void onError(String error) {
        getActivity().runOnUiThread(() -> {
            dialog.dismissDialog();
            dialog.showErrorDialogWithListenerButton(error, sweetAlertDialog -> {
                dialog.dismissDialog();
                //posDevice.initCardDetectionThread();
                //posDevice.setCardDetectionThread(true);
            });
        });
    }

    @Override
    public void onViewEnabled() {

    }
}
