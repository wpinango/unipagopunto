package com.corpagos.corpunto.fragments.historicals;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.corpagos.corpunto.Global;
import com.corpagos.corpunto.R;
import com.corpagos.corpunto.adapters.CalendarGridAdapter;
import com.corpagos.corpunto.adapters.HistoricalPrepaidServiceAdapter;
import com.corpagos.corpunto.commons.Format;
import com.corpagos.corpunto.databases.BackupFiles;
import com.corpagos.corpunto.databases.TransactionDBHelper;
import com.corpagos.corpunto.dialogs.Dialog;
import com.corpagos.corpunto.interfaces.ClickInterface;
import com.corpagos.corpunto.interfaces.DialogInterface;
import com.corpagos.corpunto.interfaces.OnClick;
import com.corpagos.corpunto.models.OwnCalendar;
import com.corpagos.corpunto.models.Transaction;
import com.corpagos.corpunto.models.TransactionType;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import static com.corpagos.corpunto.activities.TransactionHistoryActivity1.rankEnd;
import static com.corpagos.corpunto.activities.TransactionHistoryActivity1.rankInit;

public class PrepaidTransactionFragment extends Fragment implements OnClick, ClickInterface {

    private CalendarGridAdapter calendarGridAdapter;
    private ArrayList<OwnCalendar> ownCalendars = new ArrayList<>();
    private ArrayList<Transaction> transactionHistories = new ArrayList<>();
    private ArrayList<Transaction> transactionHistoriesTemp = new ArrayList<>();
    private int selectedPosition = -1;
    private String endDate, today, starDate;
    private TransactionDBHelper dbHelper;
    private Dialog dialog;
    private HistoricalPrepaidServiceAdapter historicalPrepaidServiceAdapter;

    public HistoricalPrepaidServiceAdapter getHistoricalPrepaidServiceAdapter() {
        return  this.historicalPrepaidServiceAdapter;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_payment, null);
        RecyclerView lvTransaction = rootView.findViewById(R.id.lv_payment);
        RecyclerView rvCalendar = rootView.findViewById(R.id.rv_provider_calendar);
        dialog = new Dialog(getActivity());
        dbHelper = new TransactionDBHelper(getActivity());
        calendarGridAdapter = new CalendarGridAdapter(getActivity(), ownCalendars, this);
        historicalPrepaidServiceAdapter = new HistoricalPrepaidServiceAdapter(getActivity(),
                transactionHistories, this);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        lvTransaction.setLayoutManager(llm);
        lvTransaction.setAdapter(historicalPrepaidServiceAdapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        rvCalendar.setLayoutManager(layoutManager);
        rvCalendar.setAdapter(calendarGridAdapter);
        rvCalendar.smoothScrollToPosition(29);
        populateCalendarDays();
        if (transactionHistoriesTemp.isEmpty()) {
            getTransactionStorage();
        }
        rootView.findViewById(R.id.btn_show_payment).setOnClickListener(view -> {
            showPrepaidServiceInformation(selectedPosition);
        });
        rootView.findViewById(R.id.btn_balance_payment).setOnClickListener(v -> {
            if (!transactionHistoriesTemp.isEmpty()) {
                dialog.showBalanceDialog(
                        "Total Recarga:\t"  + Format.getCashFormat(getTotal()) +
                                "\nTotal Movistar:\t" +  Format.getCashFormat(getServiceTotal("Movistar")) +
                                "\nTotal Movilnet:\t" + Format.getCashFormat(getServiceTotal("Movilnet")) +
                                "\nTotal Digitel:\t\t" + Format.getCashFormat(getServiceTotal("Digitel")) +
                                "\nTotal Directv:\t" +  Format.getCashFormat(getServiceTotal("Directv")));
                                //"\nOperaciones:\t" + getTransactionSize());
            } else {
                Global.showToast(getActivity(), "No se encontro operaciones en este dia",
                        1000);
            }
        });
        return rootView;
    }

    @Override
    public void onClick(int position) {
        if (historicalPrepaidServiceAdapter.getFilteredData().get(position).isSelected()) {
            historicalPrepaidServiceAdapter.getFilteredData().get(position).setSelected(false);
            selectedPosition = -1;
        } else {
            selectedPosition = position;
            for (Transaction t : transactionHistories) {
                t.setSelected(false);
            }
            historicalPrepaidServiceAdapter.getFilteredData().get(position).setSelected(true);
        }
        historicalPrepaidServiceAdapter.notifyDataSetChanged();
    }

    @Override
    public void onclick(int position) {
        OwnCalendar.deselectAll(ownCalendars);
        ownCalendars.get(position).setSelect(true);
        calendarGridAdapter.notifyDataSetChanged();
        populateDayOperations(ownCalendars.get(position).getDayNumber());
    }

    private double getTotal() {
        double total = 0.0;
        for (Transaction t : transactionHistories) {
            if (t.getTransactionType().equals(TransactionType.PREPAID_SERVICE)) {
                total += t.getAmount();
            }
        }
        return (total);
    }

    private double getServiceTotal(String serviceName) {
        double total = 0.0;
        for (Transaction t : transactionHistories) {
            if (t.getTransactionType().equals(TransactionType.PREPAID_SERVICE)) {
                if (t.getConcept().equals(serviceName)){
                    total += t.getAmount();
                }
            }
        }
        return (total);
    }

    private void populateDayOperations(String day) {
        transactionHistories.clear();
        for (int i = 0; i < transactionHistoriesTemp.size(); i++) {
            if (new SimpleDateFormat("d").format(Format.dateFormat(
                    transactionHistoriesTemp.get(i).getCreated_at())).equals(day) &&
                    transactionHistoriesTemp.get(i).getTransactionType().equals(
                            TransactionType.PREPAID_SERVICE)) {
                transactionHistoriesTemp.get(i).setIndex(i);
                transactionHistories.add(0, transactionHistoriesTemp.get(i));
                System.out.println("valores 10 : " + new Gson().toJson(transactionHistoriesTemp.get(i)));
            }
        }
        if (transactionHistories.isEmpty()) {
            Global.showToast(getActivity(), "No se encontro operaciones en este dia",
                    1000);
        }
        historicalPrepaidServiceAdapter.notifyDataSetChanged();
    }

    private void populateCalendarDays() {
        ownCalendars.clear();
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, -7);
        for (int i = rankInit; i < rankEnd; i++) {
            calendar.add(Calendar.DAY_OF_MONTH, +1);
            OwnCalendar ownCalendar = new OwnCalendar();
            int day = calendar.get(Calendar.DAY_OF_MONTH);
            int month = calendar.get(Calendar.MONTH) + 1;
            int year = calendar.get(Calendar.YEAR);
            String dateString = String.format("%d-%d-%d", year, month, day);
            Date date = null;
            try {
                date = new SimpleDateFormat("yyyy-M-d").parse(dateString);
                if (i == rankInit) {
                    starDate = dateString;
                    System.out.println();
                } else if (i == (rankEnd - 1)) {
                    endDate = dateString;
                    today = String.valueOf(day);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
            ownCalendar.setDayNumber(new SimpleDateFormat("d").format(date));
            ownCalendar.setDayName(new SimpleDateFormat("EEE", new Locale("es",
                    "ES")).format(date));
            ownCalendar.setMonth(new SimpleDateFormat("MMM", new Locale("es",
                    "ES")).format(date));
            ownCalendars.add(ownCalendar);
            if (i == 6) {
                ownCalendar.setSelect(true);
            }
        }
        calendarGridAdapter.notifyDataSetChanged();
    }

    private void getTransactionStorage() {
        transactionHistoriesTemp.clear();
        transactionHistoriesTemp.addAll(dbHelper.getTransactionList());
        if (transactionHistoriesTemp.isEmpty()) {
            Type custom = new TypeToken<ArrayList<Transaction>>() {
            }.getType();
            if (BackupFiles.isFilesExist(getActivity(), BackupFiles.transactionsBackupFileName)) {
                transactionHistoriesTemp = new Gson().fromJson(BackupFiles.readFileContent(
                        getActivity(), BackupFiles.transactionsBackupFileName), custom);
            }
        }
        populateDayOperations(today);
    }

    private void removeFocusFromTransactionList() {
        selectedPosition = -1;
        for (Transaction t : transactionHistories) {
            t.setSelected(false);
        }
        historicalPrepaidServiceAdapter.notifyDataSetChanged();
    }

    private void showPrepaidServiceInformation(int position) {
        if (selectedPosition != -1) {
            if (historicalPrepaidServiceAdapter.getFilteredData().get(position).getTransactionType().equals(
                    TransactionType.PREPAID_SERVICE)) {
                Dialog.showSuccessfulPrepaidServicePaymentDialog(getActivity(),
                        historicalPrepaidServiceAdapter.getFilteredData().get(position),
                        new DialogInterface() {
                            @Override
                            public void positiveClick() {
                            }

                            @Override
                            public void negativeClick() {

                            }
                        }, "");
            }
            removeFocusFromTransactionList();
        } else {
            Global.showToast(getActivity(), "Debe seleccionar una transaccion", 3000);
        }
    }
}
