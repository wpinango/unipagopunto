package com.corpagos.corpunto.fragments.transactions;

import android.app.Fragment;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.widget.EditText;
import android.widget.TextView;

import com.corpagos.corpunto.R;
import com.corpagos.corpunto.activities.MainActivity;
import com.corpagos.corpunto.commons.Format;
import com.corpagos.corpunto.enums.PaymentOptionEnum;
import com.corpagos.corpunto.enums.PosModeType;
import com.corpagos.corpunto.enums.ReadCardType;
import com.corpagos.corpunto.interfaces.MainActivityListener;
import com.corpagos.corpunto.models.Transaction;
import com.corpagos.corpunto.singletons.PosMode;
import com.corpagos.corpunto.widgets.CustomKeyboard;
import com.imagpay.enums.CardDetected;

import androidx.annotation.Nullable;

public class PinFragment extends Fragment implements MainActivityListener{

    private EditText etPass, etAmount, etDni;
    private TextView tvAmount, tvDni, tvTimer;
    private Transaction transaction;
    private CustomKeyboard keyboard;
    private PosMode posMode;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_pass, null);
        ((MainActivity)getActivity()).setMainListener(this);
        etPass = rootView.findViewById(R.id.et_pass);
        etPass.setShowSoftInputOnFocus(false);
        etAmount = rootView.findViewById(R.id.et_amount_pass);
        etAmount.setShowSoftInputOnFocus(false);
        tvAmount = rootView.findViewById(R.id.tv_amount_pass);
        etDni = rootView.findViewById(R.id.et_dni_pass);
        etDni.setShowSoftInputOnFocus(false);
        tvDni = rootView.findViewById(R.id.tv_dni_pass);
        tvTimer = rootView.findViewById(R.id.textView50);
        TextView tvPayment = rootView.findViewById(R.id.tv_payment_pass_indicator);
        TextView tvPass = rootView.findViewById(R.id.tv_payment_pass);
        tvAmount.setText(Format.getCashFormat(transaction.getAmount()));
        tvDni.setText(transaction.getDni());
        etPass.setOnFocusChangeListener((view, b) -> {
            if (b){
                keyboard.setEncryption(true, transaction.getCardNumber());
                InputConnection ic1 = etPass.onCreateInputConnection(new EditorInfo());
                keyboard.setInputConnection(ic1);
            }
        });
        etPass.requestFocus();
        posMode = PosMode.getInstance(getActivity());
        if (posMode.getPosMode().equals(PosModeType.PAYMENT)) {
            tvPayment.setGravity(Gravity.CENTER_VERTICAL);
            tvPayment.setTypeface(null, Typeface.BOLD);
            tvPayment.setTextColor(getResources().getColor(R.color.colorAccent));
            tvPayment.setText("PAGAR");
            Animation anim = new AlphaAnimation(0.0f, 1.0f);
            anim.setDuration(600);
            anim.setStartOffset(20);
            anim.setRepeatMode(Animation.REVERSE);
            anim.setRepeatCount(Animation.INFINITE);
            tvPayment.setAnimation(anim);
            tvPass.setText(getResources().getString(R.string.pos_special_op));
        } else {
            tvPayment.setVisibility(View.INVISIBLE);
        }
        return rootView;
    }

    public TextView getTimer() {
        return tvTimer;
    }

    public void setTimer(String time) {
        tvTimer.setText(time);
    }

    public void setKeyboard(CustomKeyboard keyboard) {
        this.keyboard = keyboard;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    public boolean validate() {
        boolean isValid = true;
        if (etPass.getText().toString().equals("")) {
            etPass.setError("Ingrese una clave");
            isValid = false;
        } else {
            etPass.setError(null);
        }
        return isValid;
    }

    public void populateData(Transaction transaction) {
        transaction.setCardPassword(keyboard.getEncryptedPassword());
        if (posMode.getPosMode().equals(PosModeType.PREPAID_SERVICE)) {
            ((MainActivity)getActivity()).showSuccessPrepaidServicePaymentDialog();
        } else if (posMode.getPosMode().equals(PosModeType.SELL)){
            ((MainActivity)getActivity()).requestPayment("Procesando...");
        }
    }

    public int getItemFocus() {
        View view = getActivity().getCurrentFocus();
        return view.getId();
    }

    public boolean isViewFocused() {
        return etAmount.isFocused() || etDni.isFocused();
    }

    public void editAmount() {
        cleanPass();
        etAmount.setVisibility(View.VISIBLE);
        tvAmount.setVisibility(View.INVISIBLE);
        etAmount.setText(Format.getCashFormat(transaction.getAmount()));
        etAmount.setOnFocusChangeListener((view, b) -> {
            if (b){
                keyboard.setEncryption(false, "");
                InputConnection ic1 = etAmount.onCreateInputConnection(new EditorInfo());
                keyboard.setInputConnection(ic1);
            }
        });
        etAmount.requestFocus();
    }

    public void editDni() {
        cleanPass();
        etDni.setVisibility(View.VISIBLE);
        tvDni.setVisibility(View.INVISIBLE);
        etDni.setText(transaction.getDni());
        etDni.setOnFocusChangeListener((view, b) -> {
            if (b) {
                keyboard.setEncryption(false, "");
                InputConnection ic1 = etDni.onCreateInputConnection(new EditorInfo());
                keyboard.setInputConnection(ic1);
            }
        });
        etDni.requestFocus();
        etDni.setSelection(etDni.getText().length());
    }

    public void cleanPass() {
        etPass.setText("");
        transaction.setCardPassword("");
    }

    public void finishAmountEdition(){
        tvAmount.setText(etAmount.getText().toString());
        transaction.setAmount(Float.valueOf(etAmount.getText().toString().replace(
                ".", "").replace(",", ".")));
        etAmount.setVisibility(View.INVISIBLE);
        tvAmount.setVisibility(View.VISIBLE);
        etPass.requestFocus();
    }

    public void finishDniEdition() {
        tvDni.setText(etDni.getText().toString());
        transaction.setDni(etDni.getText().toString());
        etDni.setVisibility(View.INVISIBLE);
        tvDni.setVisibility(View.VISIBLE);
        etPass.requestFocus();
    }


    @Override
    public void onCleanData() {
        etPass.setText("");
        tvAmount.setText("");
        tvDni.setText("");
    }

    @Override
    public void onCardDetected(CardDetected cardDetected) {

    }

    @Override
    public void onReadCard(ReadCardType readCardType) {
    }

    @Override
    public void onSuccessReadCard() {

    }

    @Override
    public void onFailedReadCard() {

    }

    @Override
    public void onPosMode(PosModeType posModeType) {

    }

    @Override
    public void onPaymentOption(PaymentOptionEnum paymentOptionEnum) {

    }
}
