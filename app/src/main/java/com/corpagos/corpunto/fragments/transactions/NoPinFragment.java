package com.corpagos.corpunto.fragments.transactions;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.corpagos.corpunto.R;
import com.corpagos.corpunto.activities.MainActivity;
import com.corpagos.corpunto.commons.Format;
import com.corpagos.corpunto.dialogs.Dialog;
import com.corpagos.corpunto.enums.CardAccountType;
import com.corpagos.corpunto.enums.PaymentOptionEnum;
import com.corpagos.corpunto.enums.PosModeType;
import com.corpagos.corpunto.enums.ReadCardType;
import com.corpagos.corpunto.interfaces.MainActivityListener;
import com.corpagos.corpunto.models.PrepaidService;
import com.corpagos.corpunto.models.Transaction;
import com.corpagos.corpunto.singletons.CardMode;
import com.corpagos.corpunto.singletons.PaymentOption;
import com.corpagos.corpunto.singletons.PosMode;
import com.corpagos.corpunto.utils.SharedPreferencesHandler;
import com.corpagos.corpunto.utils.Time;
import com.corpagos.corpunto.widgets.CustomKeyboard;
import com.imagpay.enums.CardDetected;

import java.util.ArrayList;
import java.util.Arrays;

import androidx.annotation.Nullable;

import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;

public class NoPinFragment extends Fragment implements MainActivityListener {

    private ArrayList<String> accounts;
    private TextView tvAccountType, tvPosMode, tvReadCard, tvTimer;
    private EditText etAmount, etDni;
    private CustomKeyboard keyboard;
    private Spinner spAccounts;
    private Dialog dialog;
    private CardMode cardMode;
    private PaymentOption paymentOption;
    private PosMode posMode;
    private boolean isRefresh = false;
    private boolean isReturn = false;
    private PrepaidService prepaidService;

    public void setRefresh(boolean isRefresh) {
        this.isRefresh = isRefresh;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_principal, null);
        posMode = PosMode.getInstance(getActivity());
        cardMode = CardMode.getInstance(getActivity());
        paymentOption = PaymentOption.getInstance(getActivity());
        dialog = new Dialog(getActivity());
        tvAccountType = rootView.findViewById(R.id.textView14);
        tvReadCard = rootView.findViewById(R.id.tv_read_card);
        tvReadCard.setVisibility(INVISIBLE);
        tvTimer = rootView.findViewById(R.id.textView49);
        accounts = new ArrayList<>(Arrays.asList(getResources().getStringArray(
                R.array.account_type)));
        ArrayAdapter arrayAdapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_item, accounts);
        arrayAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        spAccounts = rootView.findViewById(R.id.bootstrapDropDown2);
        spAccounts.setAdapter(arrayAdapter);
        etAmount = rootView.findViewById(R.id.et_amount_principal);
        etDni = rootView.findViewById(R.id.et_user_dni);
        etAmount.setShowSoftInputOnFocus(false);
        etDni.setShowSoftInputOnFocus(false);
        tvPosMode = rootView.findViewById(R.id.tv_pos_mode);
        InputConnection ic = etAmount.onCreateInputConnection(new EditorInfo());
        keyboard.setInputConnection(ic);
        etAmount.setOnFocusChangeListener((view, b) -> {
            if (b) {
                keyboard.setEncryption(false, "");
                InputConnection ic1 = etAmount.onCreateInputConnection(new EditorInfo());
                keyboard.setInputConnection(ic1);
            }
        });
        etDni.setOnFocusChangeListener((view, b) -> {
            if (b) {
                keyboard.setEncryption(false, "");
                InputConnection ic12 = etDni.onCreateInputConnection(new EditorInfo());
                keyboard.setInputConnection(ic12);
            }
        });
        spAccounts.setOnTouchListener((v, event) -> {
            if (spAccounts.isEnabled()) {
                spAccounts.setFocusable(true);
                spAccounts.setFocusableInTouchMode(true);
                spAccounts.requestFocus();
                spAccounts.performClick();
            }
            return true;
        });
        if (isReturn) {
            setViewState(true);
            setAccountsTypeVisibility(VISIBLE);
            etAmount.requestFocus();
            isReturn = false;
        } else {
            setViewState(false);
            setAccountsTypeVisibility(INVISIBLE);
        }
        ((MainActivity) getActivity()).setMainListener(this);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isRefresh) {
            System.out.println("valores : onResume12 ");
            etDni.setText("");
            etAmount.setText("0,00");
            setViewState(false);
            setAccountsTypeVisibility(INVISIBLE);
            isRefresh = false;
        }
    }

    public TextView getTimer() {
        return tvTimer;
    }

    public void setTime(String time) {
        tvTimer.setText(time);
    }

    public void setKeyboard(CustomKeyboard keyboard) {
        this.keyboard = keyboard;
    }

    public void setPrepaidService(PrepaidService prepaidService) {
        this.prepaidService = prepaidService;
    }

    public void editDni() {
        etDni.setText("");
    }

    public void editAmount() {
        etAmount.setText("");
        etAmount.requestFocus();
    }

    public String getEtAmount() {
        return etAmount.getText().toString();
    }

    public String getEtDni() {
        return etDni.getText().toString();
    }


    public int getItemFocus() {
        View view = getActivity().getCurrentFocus();
        return view.getId();
    }

    public void setEtAmountFocus() {
        etAmount.requestFocus();
    }

    public void setEtDniFocus(boolean isFocus) {
        if (isFocus) {
            etDni.requestFocus();
        } else {
            etDni.clearFocus();
        }
    }

    public void setReturn(boolean isReturn) {
        this.isReturn = isReturn;
    }

    public void setAccountsTypeVisibility(int visibility) {
        tvAccountType.setVisibility(visibility);
        spAccounts.setVisibility(visibility);
    }

    private void setViewState(boolean status) {
        etDni.setEnabled(status);
        spAccounts.setEnabled(status);
        if (posMode.getPosMode().equals(PosModeType.PREPAID_SERVICE)) {
            etAmount.setEnabled(false);
            etDni.requestFocus();
            etDni.requestFocus();
            etAmount.setText(Format.getCashFormat(prepaidService.getAmount()));
            etAmount.setEnabled(false);
            tvPosMode.setVisibility(VISIBLE);
            initializeNotificationTextView(tvPosMode, "RECARGAR");
            setTexViewAnimation(tvPosMode);
        } else if (posMode.getPosMode().equals(PosModeType.SELL)) {
            etAmount.setEnabled(status);
            keyboard.setInputConnection(null);
        } else if (posMode.getPosMode().equals(PosModeType.PAYMENT)) {
            tvPosMode.setVisibility(VISIBLE);
            initializeNotificationTextView(tvPosMode, "PAGAR");
            setTexViewAnimation(tvPosMode);
        }
    }

    public boolean validate(Transaction transaction) {
        boolean isValid = true;
        if (etAmount.getText().toString().isEmpty()) {
            isValid = false;
            etAmount.setError("Debe colocar un monto");
        } else {
            etAmount.setError(null);
        }
        if (etAmount.getText().toString().equals("0,00")) {
            isValid = false;
            etAmount.setError("Debe colocar un monto mayor a 0");
        } else {
            etAmount.setError(null);
        }
        if (etDni.getText().toString().isEmpty()) {
            isValid = false;
            etDni.setError("Debe colocar cedula");
        } else {
            etDni.setError(null);
        }
        if (paymentOption.getPaymentOptionEnum().equals(PaymentOptionEnum.CARD)) {
            if (transaction.getCardHolder() == null || transaction.getCardNumber() == null ||
                    transaction.getCardExpiration() == null) {
                dialog.showErrorDialog("Inserte nuevamente la tarjeta para continuar");
                isValid = false;
            }
        }
        System.out.println("valores : locacion " + SharedPreferencesHandler.getSavedData(
                getActivity(), SharedPreferencesHandler.KEY_LOCATION,
                SharedPreferencesHandler.KEY_LOCATION_FILE));
        return isValid;
    }

    @Override
    public void onCleanData() {
        etDni.setText("");
        etAmount.setText("0,00");
        spAccounts.setSelection(0);
        setAccountsTypeVisibility(INVISIBLE);
        tvReadCard.setVisibility(INVISIBLE);
    }

    @Override
    public void onCardDetected(CardDetected cardDetected) {
        if (CardDetected.REMOVED.equals(cardDetected)) {
            getActivity().runOnUiThread(() -> {
                tvReadCard.setVisibility(INVISIBLE);
                tvReadCard.setText("");
            });
        }
    }

    @Override
    public void onReadCard(ReadCardType readCardType) {
        tvReadCard.setVisibility(INVISIBLE);
        tvReadCard.setText("");
    }

    @Override
    public void onSuccessReadCard() {
        tvReadCard.setVisibility(View.VISIBLE);
        initializeNotificationTextView(tvReadCard, "** Leyendo Tarjeta **");
        Animation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(600);
        anim.setStartOffset(20);
        anim.setRepeatMode(Animation.REVERSE);
        anim.setRepeatCount(Animation.INFINITE);
        tvReadCard.setAnimation(anim);
        setViewState(true);
        etAmount.setSelection(etAmount.getText().toString().length());
        etDni.setError(null);
        etAmount.setError(null);
        etAmount.requestFocus();
    }

    @Override
    public void onFailedReadCard() {
        tvReadCard.setVisibility(INVISIBLE);
        tvReadCard.setText("");
        setViewState(false);
        etDni.requestFocus();
        etDni.setError(null);
        etAmount.setError(null);
        setPaymentOptionFalse();
        keyboard.setInputConnection(null);
    }

    @Override
    public void onPosMode(PosModeType posModeType) {
        if (posModeType.equals(PosModeType.PREPAID_SERVICE)) {
            etDni.requestFocus();
            etAmount.setText(Format.getCashFormat(prepaidService.getAmount()));
            etAmount.setEnabled(false);
            tvPosMode.setVisibility(VISIBLE);
            initializeNotificationTextView(tvPosMode, "RECARGAR");
            setTexViewAnimation(tvPosMode);
        } else if (posModeType.equals(PosModeType.SELL)) {
            tvPosMode.setVisibility(INVISIBLE);
        } else if (posModeType.equals(PosModeType.PAYMENT)) {
            tvPosMode.setVisibility(VISIBLE);
            initializeNotificationTextView(tvPosMode, "PAGAR");
            setTexViewAnimation(tvPosMode);
        }
    }

    @Override
    public void onPaymentOption(PaymentOptionEnum paymentOptionEnum) {
        switch (paymentOptionEnum) {
            case FINGER:
                if (!posMode.getPosMode().equals(PosModeType.PREPAID_SERVICE)) {
                    setViewState(true);
                    etAmount.requestFocus();
                }
                break;
            case CARD:
                if (!posMode.getPosMode().equals(PosModeType.PREPAID_SERVICE)) {
                    onFailedReadCard();
                }
                break;
            default:
                if (!posMode.getPosMode().equals(PosModeType.PREPAID_SERVICE)) {
                    onFailedReadCard();
                }

                break;
        }
    }

    private void setPaymentOptionFalse() {
        tvPosMode.setVisibility(INVISIBLE);
        tvPosMode.setText("");
    }

    public void populateTransactionObject(Transaction transaction) {
        dialog.dismissDialog();
        transaction.setAmount(Float.valueOf(etAmount.getText().toString().replace(
                ".", "").replace(",", ".")));
        transaction.setOperationDate(Time.getOperationDate());
        transaction.setCardAccountType(spAccounts.getSelectedItem().toString());
        transaction.setDni(etDni.getText().toString().replace(".", ""));
        tvPosMode.setVisibility(View.INVISIBLE);
        if (paymentOption.getPaymentOptionEnum().equals(PaymentOptionEnum.CARD)) {
            if (cardMode.getCard().getCardAccountType().equals(CardAccountType.CREDIT)) {
                if (posMode.getPosMode().equals(PosModeType.SELL)) {
                    ((MainActivity) getActivity()).requestPayment("Procesando...");
                } else if (posMode.getPosMode().equals(PosModeType.PREPAID_SERVICE)) {
                    ((MainActivity) getActivity()).showSuccessPrepaidServicePaymentDialog();
                }
            } else if (cardMode.getCard().getCardAccountType().equals(CardAccountType.DEBIT)) {
                ((MainActivity) getActivity()).showDebitFragment();
            }
        } else if (paymentOption.getPaymentOptionEnum().equals(PaymentOptionEnum.FINGER)) {
            ((MainActivity) getActivity()).requestPayment("Procesando...");
        }
    }

    public void expandSpinnerAccount() {
        spAccounts.setFocusableInTouchMode(true);
        spAccounts.setFocusable(true);
        spAccounts.requestFocus();
        spAccounts.performClick();
    }

    private void setTexViewAnimation(TextView textView) {
        Animation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(600);
        anim.setStartOffset(20);
        anim.setRepeatMode(Animation.REVERSE);
        anim.setRepeatCount(Animation.INFINITE);
        textView.setAnimation(anim);
    }

    private void initializeNotificationTextView(TextView textView, String text) {
        textView.setGravity(Gravity.CENTER);
        textView.setTypeface(null, Typeface.BOLD);
        textView.setTextColor(getResources().getColor(R.color.colorAccent));
        textView.setText(text);
    }
}
