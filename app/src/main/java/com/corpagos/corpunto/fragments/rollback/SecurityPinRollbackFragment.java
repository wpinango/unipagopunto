package com.corpagos.corpunto.fragments.rollback;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.widget.EditText;

import androidx.annotation.Nullable;

import com.corpagos.corpunto.R;
import com.corpagos.corpunto.widgets.CustomKeyboard;

public class SecurityPinRollbackFragment extends Fragment {

    private CustomKeyboard keyboard;
    private EditText etPin;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_security_pin, null);
        etPin = rootView.findViewById(R.id.et_owner_pass2);
        etPin.setShowSoftInputOnFocus(false);
        etPin.setOnFocusChangeListener((view, b) -> {
            if (b) {
                InputConnection ic1 = etPin.onCreateInputConnection(new EditorInfo());
                keyboard.setInputConnection(ic1);
            }
        });
        etPin.requestFocus();
        return rootView;
    }

    public boolean validate() {
        boolean isValid = false;
        if (!etPin.getText().toString().equals("")) {
            etPin.setError(null);
            isValid = true;
        } else {
            etPin.setError("Debe ingresar una clave");
        }
        return isValid;
    }

    public String getPin() {
        return etPin.getText().toString();
    }

    public void setKeyboard(CustomKeyboard keyboard) {
        this.keyboard = keyboard;
    }

}
