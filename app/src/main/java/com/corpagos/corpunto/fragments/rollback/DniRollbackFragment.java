package com.corpagos.corpunto.fragments.rollback;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.corpagos.corpunto.Asynctask;
import com.corpagos.corpunto.Global;
import com.corpagos.corpunto.R;
import com.corpagos.corpunto.app.AppCredentials;
import com.corpagos.corpunto.commons.Format;
import com.corpagos.corpunto.databases.BackupFiles;
import com.corpagos.corpunto.databases.TransactionDBHelper;
import com.corpagos.corpunto.dialogs.Dialog;
import com.corpagos.corpunto.enums.PosModelType;
import com.corpagos.corpunto.interfaces.AsynctaskListener;
import com.corpagos.corpunto.interfaces.ButtonInterface;
import com.corpagos.corpunto.interfaces.ReadCardListener;
import com.corpagos.corpunto.models.CardData;
import com.corpagos.corpunto.models.OwnerData;
import com.corpagos.corpunto.models.Response;
import com.corpagos.corpunto.models.Token;
import com.corpagos.corpunto.models.Transaction;
import com.corpagos.corpunto.models.TransactionType;
import com.corpagos.corpunto.pos.PosDevice;
import com.corpagos.corpunto.print.PrintTicket;
import com.corpagos.corpunto.singletons.PosModel;
import com.corpagos.corpunto.utils.ConsoleMessageManager;
import com.corpagos.corpunto.utils.SharedPreferencesHandler;
import com.corpagos.corpunto.widgets.CustomKeyboard;
import com.google.gson.Gson;
import com.imagpay.enums.CardDetected;
import com.imagpay.enums.PrintStatus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;


import static com.corpagos.corpunto.Global.chipCount;
import static com.corpagos.corpunto.Global.nfcCount;
import static com.corpagos.corpunto.activities.TransactionHistoryActivity1.ACTION_INTENT;
import static com.corpagos.corpunto.utils.SharedPreferencesHandler.saveCount;

public class DniRollbackFragment extends Fragment implements ReadCardListener, AsynctaskListener {

    private CustomKeyboard keyboard;
    private Transaction transaction;
    private PosDevice posDevice;
    private EditText etDni;
    private Dialog dialog;
    private boolean isFirstRequestFailed = false;
    private String reference, pinCode;
    private CardData cardData;
    private boolean isFirstPrint = true;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_dni_rollback, null);
        dialog = new Dialog(getActivity());
        posDevice = new PosDevice(getActivity(), this, getActivity(), true);
        etDni = rootView.findViewById(R.id.et_client_dni2);
        etDni.setShowSoftInputOnFocus(false);
        etDni.setOnFocusChangeListener((view, b) -> {
            if (b) {
                keyboard.setEncryption(false, "");
                InputConnection ic1 = etDni.onCreateInputConnection(new EditorInfo());
                keyboard.setInputConnection(ic1);
            }
        });
        etDni.requestFocus();
        return rootView;
    }

    public boolean validate() {
        boolean isValid = false;
        if (!etDni.getText().toString().equals("") &&
                etDni.getText().toString().length() <= 9 &&
                etDni.getText().toString().length() >= 8) {
            etDni.setError(null);
            isValid = true;
        } else {
            etDni.setError("Debe ingresar una cedula valida");
        }
        return isValid;
    }

    public String getDni() {
        return etDni.getText().toString();
    }

    public void setKeyboard(CustomKeyboard keyboard) {
        this.keyboard = keyboard;
    }

    public void setTransactionData(String reference, String pinCode) {
        this.pinCode = pinCode;
        this.reference = reference;
    }

    @Override
    public void onCardRead(String json) {

    }

    @Override
    public void onCardDetect(CardDetected cardDetected, Transaction transaction, CardData cardData) {
        dialog.dismissDialog();
        if (cardDetected == CardDetected.INSERTED) {
            this.transaction = transaction;
            this.cardData = cardData;
            iCCardTest();
            ConsoleMessageManager.consoleCardInsertedMsg();
        }
        if (cardDetected == CardDetected.REMOVED) {
            ConsoleMessageManager.consoleCardRemovedMsg();
        }
        if (cardDetected == CardDetected.NFC) {
            transaction.setCardHolder("CABRERA/JESUS");
            transaction.setCardExpiration("08/22");
            transaction.setCardNumber("5529735050007873");
            //transaction.setDeviceSerial();
            nfcCount++;
            saveCount(getActivity(), Global.keyNfcCount, nfcCount);
        }
        if (cardDetected == CardDetected.SWIPED) {

        }
    }

    @Override
    public void onPrintStatus(PrintStatus paramPrintStatus) {
        if (paramPrintStatus == PrintStatus.EXIT) {
            if (isFirstPrint) {
                getActivity().finish();
                isFirstPrint = false;
            }
        }
    }

    @Override
    public void onChipError() {
        getActivity().runOnUiThread(() -> {
            dialog.dismissDialog();
            dialog.showErrorDialog("Error de chip");
        });
    }

    @Override
    public void onProcess() {

    }

    @Override
    public void onError(String error) {
        getActivity().runOnUiThread(() -> {
            dialog.dismissDialog();
            dialog.showErrorDialogWithListenerButton(error, sweetAlertDialog -> {
                dialog.dismissDialog();
                //posDevice.initCardDetectionThread();
                //posDevice.setCardDetectionThread(true);
            });
        });
    }

    @Override
    public void onViewEnabled() {

    }

    public void showRollBackDialog() {
        posDevice.initReadingPeripherals();
        dialog.showInsertCardDialog(new ButtonInterface() {
            @Override
            public void onPositiveButtonClick(String buttonAction) {

            }

            @Override
            public void onNegativeButtonClick(String buttonAction) {

            }
        });
    }

    private synchronized void iCCardTest() {
        new Thread(() -> {
            dialog.showProgressDialog("Leyendo tarjeta... ");
            dialog.dismissDialog();
            Objects.requireNonNull(getActivity()).runOnUiThread(() -> {
                if (transaction.getCardHolder() == null || transaction.getCardNumber() == null ||
                        transaction.getCardExpiration() == null) {
                    dialog.showErrorDialogWithListenerButton(
                            "Inserte nuevamente la tarjeta para continuar",
                            sweetAlertDialog -> {
                                dialog.dismissDialog();
                            });
                } else {
                    chipCount++;
                    saveCount(getActivity(), Global.keyChipCount, chipCount);
                    posDevice.setTransaction(true);
                    dialog.showInfoDialogWithListenerButtons(
                            "Desea realizar el reverso de esta operacion?",
                            (dialog, which) -> {
                                requestRollbackOperation(transaction,
                                        "Revesando transaccion");
                            }, (dialog, which) -> {
                                posDevice.setTransaction(false);
                            });
                }
            });
        }).start();
    }

    private void requestRollbackOperation(Transaction transaction, String message) {
        Map<String, String> operation = new HashMap<>();
        operation.put("card_number", transaction.getCardNumber());
        operation.put("reference_number", reference);
        operation.put("cardholder_dni", etDni.getText().toString());
        operation.put("pin_code", pinCode);
        System.out.println("valores : " + operation);
        Token token = new Token();
        if (!SharedPreferencesHandler.getSavedData(getActivity(), SharedPreferencesHandler.KEY_TOKEN,
                SharedPreferencesHandler.FILE_TOKEN).equals(SharedPreferencesHandler.NOT_FOUND) &&
                !SharedPreferencesHandler.getSavedData(getActivity(), SharedPreferencesHandler.KEY_TOKEN,
                        SharedPreferencesHandler.FILE_TOKEN).equals("")) {
            token = new Gson().fromJson(SharedPreferencesHandler.getSavedData(
                    getActivity(), SharedPreferencesHandler.KEY_TOKEN, SharedPreferencesHandler
                            .FILE_TOKEN), Token.class);
        } else {
            token.setExpires_in(0);
            token.setAccess_token("");
            token.setToken_type("");
        }
        Map<String, String> headers = new HashMap<>();
        headers.put(Asynctask.authorization, token.getToken_type() + " " + token.getAccess_token());
        dialog.showProgressDialog(message);

        System.out.println("valores : " + operation);
        headers.put(Asynctask.authorization, token.getToken_type() + " " + token.getAccess_token());

        new Asynctask.PostFormMethodAsynctask(Asynctask.transactionalHost,
                Asynctask.getEncryptedAESRequest(getActivity(), operation),
                Asynctask.URL_TRANSACTION_REVERSE, headers, this).execute();
    }

    @Override
    public void onAsynctaskFinished(String endPoint, String response, String headers) {
        dialog.dismissDialog();
        //posDevice.setCardDetectionThread(false);
        System.out.println("valores : " + response);
        PosModel posModel = PosModel.getInstance(getActivity());
        TransactionDBHelper dbHelper = new TransactionDBHelper(getActivity());
        OwnerData ownerData = new Gson().fromJson(BackupFiles.getBackupData(getActivity(),
                BackupFiles.ownerBackupFileName), OwnerData.class);
        if (!response.equals("") && !response.equals(Asynctask.TIMEOUT_MESSAGE) &&
                !response.equals(Asynctask.FAILED_TO_CONNECT)) {
            Response res = new Gson().fromJson(response, Response.class);
            if (!res.isError()) {
                if (endPoint.equals(Asynctask.URL_TRANSACTION_REVERSE)) {
                    if (res.getMessage().equals(Asynctask.OK_RESPONSE)) {

                        Transaction transaction = new Gson().fromJson(Asynctask.getDecryptedAESResponse(
                                getActivity(), new Gson().toJson(res.getData())), Transaction.class);
                        transaction.setCard_number(Format.getLastNumberFromCard(
                                this.transaction.getCardNumber()));
                        transaction.setDate(Format.getLocalDateFormat(getActivity(), Format.datePattern,
                                transaction.getCreated_at()));
                        transaction.setDeviceSerial(posModel.getPosInformation().getPosSerial());
                        transaction.setTransactionType(TransactionType.ROLLBACK);
                        transaction.setConcept(cardData.getAID());


                        if (!posModel.getPosModelType().equals(PosModelType.WPOS_MINI)) {
                            printTicket(PrintTicket.getHistoricalTicketBody(getActivity(),
                                    posModel.getPosInformation().getPosSerial(), transaction,
                                    ownerData), PrintTicket.getHeaders(ownerData, "ANULACION"),
                                    PrintTicket.getRollBackFooter(String.valueOf(transaction.getAmount())));
                        } else {
                            dialog.showInfoDialogWithListenerButtonAndTex("Se realizo reverso",
                                    sweetAlertDialog -> {
                                        getActivity().finish();
                                    }, "Aceptar");
                        }
                        transaction.setTransactionType(TransactionType.ROLLBACK);
                        dbHelper.updateReverseStatus(transaction);
                        long trace = dbHelper.getTransactionList().size() >= 999999 ? 0 :
                                dbHelper.getTransactionList().size();
                        transaction.setTrace(trace);
                        dbHelper.insertTransaction(transaction);
                        updateRollbackView("rollback");
                    } else {
                        dialog.showErrorDialog("Algo salio mal");
                    }
                }
            } else {
                switch (res.getMessage()) {
                    case "ERR_TRANSACTION_NOT_FOUND":
                        posDevice.turnOffReadingPeripherals();
                        dialog.showErrorDialog("Transaccion no encontrada");
                        break;
                    case "ERR_INVALID_OP_PWD":
                        dialog.showErrorDialog("Clave de punto incorrecta");
                        posDevice.turnOffReadingPeripherals();
                        break;
                    case "ERR_INVALID_OR_EXPIRED_TOKEN":
                    case Global.ERR_UNKNOWN_DEVICE_SERIAL:
                        dialog.showErrorDialogWithListenerButton(
                                "Dispositivo no registrado, registrar nuevamente",
                                sweetAlertDialog -> {
                                    dialog.dismissDialog();
                                    AppCredentials.deleteToken(getActivity());
                                    getActivity().finish();
                                });
                        break;
                    default:
                        dialog.showErrorDialog("Inserto algun dato incorrecto");
                        posDevice.turnOffReadingPeripherals();
                        break;
                }
            }
        } else if (response.equals(Asynctask.TIMEOUT_MESSAGE) && !isFirstRequestFailed) {
            isFirstRequestFailed = true;
            requestRollbackOperation(transaction, "Reversando con otro servidor ...");
        } else if (response.equals(Asynctask.TIMEOUT_MESSAGE) && isFirstRequestFailed) {
            posDevice.turnOffReadingPeripherals();
            isFirstRequestFailed = false;
            dialog.showErrorDialog("Servidor no responde");
        } else if (response.equals(Asynctask.FAILED_TO_CONNECT)) {
            dialog.showErrorDialog("Error de conexion");
        } else {
            posDevice.turnOffReadingPeripherals();
            dialog.showSomethingWrongDialog();
        }
    }

    private void printTicket(ArrayList<String> items, ArrayList<String> headers, ArrayList<String>
            footers) {
        posDevice.printTicket(items, headers, footers);
    }

    private void updateRollbackView(String value) {
        Intent intent = new Intent(ACTION_INTENT);
        intent.putExtra("rollback", value);
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }
}
