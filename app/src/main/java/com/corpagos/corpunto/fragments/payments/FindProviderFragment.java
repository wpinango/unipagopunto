package com.corpagos.corpunto.fragments.payments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.widget.EditText;

import com.corpagos.corpunto.Asynctask;
import com.corpagos.corpunto.Global;
import com.corpagos.corpunto.R;
import com.corpagos.corpunto.activities.PaymentActivity;
import com.corpagos.corpunto.databases.BackupFiles;
import com.corpagos.corpunto.dialogs.Dialog;
import com.corpagos.corpunto.interfaces.AsynctaskListener;
import com.corpagos.corpunto.models.Provider;
import com.corpagos.corpunto.models.Response;
import com.corpagos.corpunto.models.Token;
import com.corpagos.corpunto.app.AppCredentials;
import com.corpagos.corpunto.utils.SharedPreferencesHandler;
import com.corpagos.corpunto.widgets.CustomKeyboard;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class FindProviderFragment extends Fragment implements AsynctaskListener {

    private CustomKeyboard keyboard;
    private EditText etTisProvider;
    private Dialog dialog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_find_provider, null);
        getActivity().setTitle("Pago");
        dialog = new Dialog(getActivity());
        etTisProvider = rootView.findViewById(R.id.et_tis_provider);
        etTisProvider.setShowSoftInputOnFocus(false);
        etTisProvider.setOnFocusChangeListener((view, b) -> {
            if (b){
                InputConnection ic1 = etTisProvider.onCreateInputConnection(new EditorInfo());
                keyboard.setInputConnection(ic1);
            }
        });
        etTisProvider.requestFocus();
        return rootView;
    }

    public void setKeyboard(CustomKeyboard keyboard) {
        this.keyboard = keyboard;
    }

    public boolean validate() {
        boolean isValid = false;
        if (!etTisProvider.getText().toString().equals("") &&
                etTisProvider.getText().toString().length() <= 9 &&
                etTisProvider.getText().toString().length() >= 8) {
            etTisProvider.setError(null);
            isValid = true;
        } else {
            etTisProvider.setError("Debe ingresar un RIF valido");
        }
        return isValid;
    }

    public void requestProviderInfo() {
        dialog.showProgressDialog("Ubicando proveedor");
        Token tokenObject = AppCredentials.getToken(getActivity());
        Map<String, String> headers = new HashMap<>();
        headers.put(Asynctask.authorization, tokenObject.getToken_type() + " " +
                tokenObject.getAccess_token());
        new Asynctask.GetMethodAsynctask(Asynctask.resourcesHost,
                Asynctask.URL_GET_FIND_PROVIDER + etTisProvider.getText().toString(), headers,
                this).execute();
    }

    @Override
    public void onAsynctaskFinished(String endPoint, String response, String headers) {
        dialog.dismissDialog();
        if (!response.equals("")) {
            Response res = new Gson().fromJson(response, Response.class);
            if (!res.isError()) {
                if (res.getMessage().equals(Asynctask.OK_RESPONSE)) {
                    if (endPoint.contains(Asynctask.URL_GET_FIND_PROVIDER)) {
                        Provider provider = new Gson().fromJson(new Gson().toJson(res.getData()),
                                Provider.class);
                        ((PaymentActivity) getActivity()).showProviderInfoFragment(provider);
                    }/* else if (endPoint.contains(Asynctask.URL_GET_PROVIDERS)) {
                        Type custom = new TypeToken<ArrayList<Provider>>() {
                        }.getType();
                        ArrayList<Provider> providers = new Gson().fromJson(new Gson().toJson(
                                res.getData()), custom);
                        for (Provider provider : providers) {
                            for (ProviderAccount p : provider.getAccounts()) {
                                providerAccounts.add(provider.getName() + " " + p.getBank().getName()
                                        + " *" + p.getAccount().substring(p.getAccount().length() - 4));
                            }
                        }
                        ArrayAdapter<String> accountAdapter = new ArrayAdapter<>(getActivity(),
                                R.layout.spinner_item, providerAccounts);
                        accountAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
                        spAccount.setAdapter(accountAdapter);
                        System.out.println("valores 2 :" + new Gson().toJson(providerAccounts)  );
                    }*/
                }
            } else {
                if (res.getMessage().equals("ERR_INVALID_OR_EXPIRED_TOKEN") ||
                        res.getMessage().equals(Global.ERR_UNKNOWN_DEVICE_SERIAL)) {
                    dialog.showErrorDialogWithListenerButton(
                            "Dispositivo no registrado",
                            sweetAlertDialog -> {
                                dialog.dismissDialog();
                                SharedPreferencesHandler.saveData(getActivity(),
                                        SharedPreferencesHandler.KEY_TOKEN,
                                        SharedPreferencesHandler.NOT_FOUND,
                                        SharedPreferencesHandler.FILE_TOKEN);
                                BackupFiles.writeFiles(getActivity(),
                                        BackupFiles.configBackupFileName, "");
                                getActivity().finish();
                            });

                } else if (res.getMessage().equals("ERR_REGISTRY_NOT_FOUND")) {
                    dialog.showErrorDialog("Rif no encontrado");
                } else {
                    dialog.showErrorDialog("Algo salio mal");
                }
            }
        }
    }
}
