package com.corpagos.corpunto.fragments.firstlogin;

import android.app.Fragment;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.widget.EditText;
import android.widget.TextView;

import com.corpagos.corpunto.Asynctask;
import com.corpagos.corpunto.Global;
import com.corpagos.corpunto.R;
import com.corpagos.corpunto.app.AppCredentials;
import com.corpagos.corpunto.databases.BackupFiles;
import com.corpagos.corpunto.dialogs.Dialog;
import com.corpagos.corpunto.interfaces.AsynctaskListener;
import com.corpagos.corpunto.models.Response;
import com.corpagos.corpunto.models.Token;
import com.corpagos.corpunto.singletons.PosModel;
import com.corpagos.corpunto.utils.SharedPreferencesHandler;
import com.corpagos.corpunto.widgets.CustomKeyboard;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

import androidx.annotation.Nullable;

public class SpecialPosCodeFragment extends Fragment implements AsynctaskListener {

    private CustomKeyboard keyboard;
    private Dialog dialog;
    private EditText etPosCode;
    private EditText etRepeatPosCode;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_pos_code, null);
        getActivity().setTitle("Crear clave especial");
        TextView tvInformation = rootView.findViewById(R.id.textView34);
        tvInformation.setText("Debe crear la clave de operaciones especiales, que " +
                "se utilizara para los pago de proveedores y personas naturales. Este es un pin de 6 digitos");
        dialog = new Dialog(getActivity());
        etPosCode = rootView.findViewById(R.id.et_pos_code2);
        etPosCode.setShowSoftInputOnFocus(false);
        InputFilter[] fArray = new InputFilter[1];
        fArray[0] = new InputFilter.LengthFilter(6);
        etPosCode.setFilters(fArray);
        etRepeatPosCode = rootView.findViewById(R.id.et_repeat_pos_code2);
        etRepeatPosCode.setShowSoftInputOnFocus(false);
        etRepeatPosCode.setFilters(fArray);
        InputConnection ic1 = etPosCode.onCreateInputConnection(new EditorInfo());
        keyboard.setInputConnection(ic1);
        etRepeatPosCode.setOnFocusChangeListener((view, b) -> {
            if (b){
                InputConnection ic2 = etRepeatPosCode.onCreateInputConnection(new EditorInfo());
                keyboard.setInputConnection(ic2);
            }
        });
        etPosCode.setOnFocusChangeListener((view, b) -> {
            if (b){
                InputConnection ic2 = etPosCode.onCreateInputConnection(new EditorInfo());
                keyboard.setInputConnection(ic2);
            }
        });
        etPosCode.requestFocus();
        return rootView;
    }

    public void setKeyboard(CustomKeyboard keyboard) {
        this.keyboard = keyboard;
    }

    public void setEtPosCodeFocus() {
        etPosCode.requestFocus();
    }

    public void setEtRepeatPosCodeFocus() {
        etRepeatPosCode.requestFocus();
    }

    public int getItemFocus() {
        View view = getActivity().getCurrentFocus();
        return view.getId();
    }

    public boolean validate() {
        boolean isValid = true;
        if (etPosCode.getText().toString().isEmpty()) {
            etPosCode.setError("Introduzca una clave");
            isValid = false;
        } else {
            etPosCode.setError(null);
        }
        if (etPosCode.getText().toString().length() < 6) {
            etPosCode.setError("Introduzca 6 digitos");
            isValid = false;
        } else {
            etPosCode.setError(null);
        }
        if (etRepeatPosCode.getText().toString().isEmpty() || etRepeatPosCode.getText().equals(
                etPosCode.getText())) {
            etRepeatPosCode.setError("Clave no coincide");
            isValid = false;
        } else {
            etRepeatPosCode.setError(null);
        }
        return isValid;
    }

    public void requestChangePosCode() {
        dialog.showProgressDialog("Procesando...");
        PosModel posModel = PosModel.getInstance(getActivity());
        Map<String, Object> body = new HashMap<>();
        body.put("password", etPosCode.getText().toString());
        body.put("serial", posModel.getPosInformation().getPosSerial());
        Map<String, String> headers = new HashMap<>();
        Token token = AppCredentials.getUserToken(getActivity());
        headers.put(Asynctask.authorization, token.getToken_type() + " " + token.getAccess_token());
        new Asynctask.PostFormMethodAsynctask(Asynctask.authHost, body,
                Asynctask.URL_CHANGE_PASSWORD + "alt_pin_code", headers, // 6 digitos
                this).execute();
    }

    @Override
    public void onAsynctaskFinished(String endPoint, String response, String headers) {
        System.out.println("valores : " + response);
        dialog.dismissDialog();
        if (!response.equals("")) {
            Response res = new Gson().fromJson(response, Response.class);
            if (!res.isError()) {
                if (res.getMessage().equals(Asynctask.OK_RESPONSE)) {
                    dialog.showSuccessDialogWithListenerButton("Se modifico exitosamente",
                            sweetAlertDialog -> {
                                dialog.dismissDialog();
                                getActivity().finish();
                            });
                }
            } else {
                switch (res.getMessage()) {
                    case "ERR_INVALID_OR_EXPIRED_TOKEN":
                        dialog.showErrorDialogWithListenerButton(
                                "Problema de autorizacion, ingrese sus datos nuevamente " +
                                        "las credenciales",
                                sweetAlertDialog -> {
                                    dialog.dismissDialog();
                                    SharedPreferencesHandler.saveData(getActivity(),
                                            SharedPreferencesHandler.KEY_TOKEN,
                                            SharedPreferencesHandler.NOT_FOUND,
                                            SharedPreferencesHandler.FILE_TOKEN);
                                    BackupFiles.writeFiles(getActivity(), BackupFiles.configBackupFileName,
                                            "");
                                });
                        break;
                    case "ERR_DEVICE_NOT_ASSIGNED":
                        dialog.showErrorDialog("Error de verificacion");
                        break;
                    case "INVALID_OP_PWD":
                        dialog.showErrorDialog("Error en las credenciales");
                        break;
                    case "ERR_FAILED":
                        dialog.showErrorDialog("Usuario inactivo");
                        break;
                    case "ERR_UNKNOWN_DEVICE":
                        dialog.showErrorDialog("Error email no validado");
                        break;
                    default:
                        dialog.showErrorDialog(Global.messageResponse(res.getMessage()));
                        break;
                }
            }
        }
    }
}
    //pin de seguridad, se utilizara para reversos y recargas de servicios.
    //clave de operaciones especiales, se utilizara para los pago de proveedores y personas naturales
