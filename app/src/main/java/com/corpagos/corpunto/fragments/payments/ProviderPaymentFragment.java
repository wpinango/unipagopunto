package com.corpagos.corpunto.fragments.payments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.widget.EditText;
import android.widget.TextView;

import com.corpagos.corpunto.Asynctask;
import com.corpagos.corpunto.R;
import com.corpagos.corpunto.commons.Format;
import com.corpagos.corpunto.databases.BackupFiles;
import com.corpagos.corpunto.databases.TransactionDBHelper;
import com.corpagos.corpunto.dialogs.Dialog;
import com.corpagos.corpunto.interfaces.AsynctaskListener;
import com.corpagos.corpunto.interfaces.DialogInterface;
import com.corpagos.corpunto.models.OwnerData;
import com.corpagos.corpunto.models.Provider;
import com.corpagos.corpunto.models.ProviderAccount;
import com.corpagos.corpunto.models.Response;
import com.corpagos.corpunto.models.Transaction;
import com.corpagos.corpunto.models.TransactionLocation;
import com.corpagos.corpunto.models.TransactionType;
import com.corpagos.corpunto.print.PrintTicket;
import com.corpagos.corpunto.requests.Request;
import com.corpagos.corpunto.utils.Keyboard;
import com.corpagos.corpunto.utils.SharedPreferencesHandler;
import com.corpagos.corpunto.widgets.CustomKeyboard;
import com.google.gson.Gson;

import androidx.annotation.Nullable;

import static com.corpagos.corpunto.activities.MainLauncherActivity.posSerial;

public class ProviderPaymentFragment extends Fragment implements AsynctaskListener {

    private Provider provider;
    private ProviderAccount providerAccount;
    private EditText etPass;
    private EditText etComment;
    private CustomKeyboard keyboard;
    private Dialog dialog;
    private TransactionLocation transactionLocation;
    private Transaction transaction;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_provider_payment, null);
        dialog = new Dialog(getActivity());
        TextView tvProviderName = rootView.findViewById(R.id.tv_provider_name_payment);
        etComment = rootView.findViewById(R.id.et_comment_provider_payment);
        TextView tvProviderAccount = rootView.findViewById(R.id.tv_provider_account);
        tvProviderAccount.setText(providerAccount.getBank().getName() + " *" +
                providerAccount.getAccount().substring(providerAccount.getAccount().length() - 4));
        etPass = rootView.findViewById(R.id.et_pass_provider_payment);
        etPass.setShowSoftInputOnFocus(false);
        etPass.requestFocus();
        tvProviderName.setText(provider.getName());
        etComment.setFocusableInTouchMode(true);
        etComment.setFocusable(true);
        etComment.requestFocus();
        Keyboard.showKeyboard(getActivity());
        InputConnection ic1 = etComment.onCreateInputConnection(new EditorInfo());
        keyboard.setInputConnection(ic1);
        etPass.setOnFocusChangeListener((view, b) -> {
            if (b){
                Keyboard.hideKeyboard(getActivity());
                InputConnection ic2 = etPass.onCreateInputConnection(new EditorInfo());
                keyboard.setInputConnection(ic2);
            }
        });
        etComment.setOnFocusChangeListener((view, b) -> {
            Keyboard.showKeyboard(getActivity());
        });
        return rootView;
    }

    public void setKeyboard(CustomKeyboard keyboard) {
        this.keyboard = keyboard;
    }

    public void setProviderInfo(Provider provider, ProviderAccount providerAccount) {
        this.provider = provider;
        this.providerAccount = providerAccount;
    }

    public void setTransactionLocation(TransactionLocation transactionLocation) {
        this.transactionLocation = transactionLocation;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    public boolean isEtAmountFocus() {
        return etComment.hasFocus();
    }

    public void setEtAmountFocus() {
        etComment.requestFocus();
    }

    public void setEtPassFocus() {
        etPass.requestFocus();
    }

    private void requestPayment(Transaction transaction) {
        dialog.showProgressDialog("Realizando pago");
        Request.requestDoProviderPayment(getActivity(), transaction, this, provider);
    }

    public boolean validate() {
        boolean isValid = true;
        if (etPass.getText().toString().equals("")) {
            etPass.setError("Introduzca una clave");
            isValid = false;
        } else {
            etPass.setError(null);
        }
        return isValid;
    }

    public void prepareTransactionData() {
        transaction.setCardNumber(providerAccount.getAccount());
        transaction.setCardPassword(etPass.getText().toString());
        transaction.setDni(provider.getTin());
        transaction.setLocation("{lat:" + transactionLocation.getLatitude() + ",long:" +
                transactionLocation.getLongitude() + "}");
        transaction.setDeviceSerial(posSerial);
        requestPayment(transaction);
    }

    @Override
    public void onAsynctaskFinished(String endPoint, String response, String headers) {
        System.out.println("valores : " + response);
        dialog.dismissDialog();
        if (!response.equals("")) {
            Response res = new Gson().fromJson(response, Response.class);
            if (!res.isError()) {
                if (res.getMessage().equals(Asynctask.OK_RESPONSE)) {

                    Transaction transaction = new Gson().fromJson(Asynctask.getDecryptedAESResponse(
                            getActivity(), new Gson().toJson(res.getData())), Transaction.class);
                    OwnerData ownerData = new Gson().fromJson(BackupFiles.getBackupData(
                            getActivity(), BackupFiles.ownerBackupFileName),
                            OwnerData.class);
                    transaction.setDate(Format.getLocalDateFormat(getActivity(),Format.datePattern,
                            transaction.getCreated_at()));
                    //transaction.setDate(new SimpleDateFormat(Format.datePattern)
                    //        .format(Format.dateFormat(transaction.getCreated_at())));
                    transaction.setCard_number(Format.getLastNumberFromCard(
                            transaction.getCard_number()));
                    transaction.setTransactionType(TransactionType.PAYMENT_PROVIDERS);
                    TransactionDBHelper dbHelper = new TransactionDBHelper(getActivity());
                    dbHelper.insertTransaction(transaction);
                    Dialog.showSuccessfulSaleDialog(getActivity(), transaction,
                            new DialogInterface() {
                                @Override
                                public void positiveClick() {
                                   /* printTicket(PrintTicket.getTicketSuccessBody(
                                                transaction, ownerData, cardData),
                                                PrintTicket.getHeaders(ownerData, "COMPRA"),
                                                PrintTicket.getDebitFooter(String.valueOf(
                                                        transaction.getAmount()), false));*/

                                    getActivity().finish();
                                }

                                @Override
                                public void negativeClick() {
                                    getActivity().finish();
                                }
                            }, "Imprimir  Ticket");
                }
            } else {
                if(res.getMessage().equals("ERR_INVALID_OP_PWD")) {
                    dialog.showErrorDialog("Error en clave");
                }  else if (response.equals(Asynctask.TIMEOUT_MESSAGE)) {
                    dialog.showErrorDialog("Servidor no responde");
                } else if (res.getMessage().equals("ERR_VALIDATION_FAILED")) {
                    dialog.showErrorDialog("Clave especial son 6 digitos");
                } else if (res.getMessage().equals("ERR_POS_AES_KEYS_NOT_CONFIGURED")) {
                    dialog.showErrorDialogWithListenerButton(
                            "Dispositivo no se encuentra configurado",
                            sweetAlertDialog -> {
                                dialog.dismissDialog();
                                SharedPreferencesHandler.saveData(getActivity(),
                                        SharedPreferencesHandler.KEY_AES_KEY,
                                        SharedPreferencesHandler.NOT_FOUND,
                                        SharedPreferencesHandler.FILE_AES_KEY
                                );
                            });
                }
            }
        }
    }
}
