package com.corpagos.corpunto.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferencesHandler {

    public static final String KEY_LOCATION_FILE = "current_location";
    public static final String NOT_FOUND = "not-found";
    public static final String KEY_LOCATION = "location";
    public static final String FILE_TOKEN = "fileToekn";
    public static final String KEY_TOKEN = "token";
    public static final String KEY_TRANSACTION = "transaction";
    public static final String KEY_TRANSACTION_FILE = "transactionFile";
    public static final String KEY_UPDATE_FILE = "update";
    public static final String KEY_UPDATE = "updateInfo";
    public static final String KEY_FB_TOKEN_FILE = "fBTokenFile";
    public static final String KEY_FB_TOKEN = "fbToken";
    public static final String KEY_USER_TOKEN = "userToken";
    public static final String KEY_USER_TOKEN_FILE = "userTokenFile";
    public static final String KEY_AES_KEY = "aesKey";
    public static final String FILE_AES_KEY = "aesFile";

    public static void saveData(Context context, String key, String value, String file) {
        SharedPreferences preferences = context.getSharedPreferences(file, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = preferences.edit();
        edit.putString(key, value);
        edit.apply();
    }

    public static String getSavedData(Context context, String key, String file) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(file, Context.MODE_PRIVATE);
        return sharedPreferences.getString(key, NOT_FOUND);
    }

    public static void saveCount(Context context, String key, int value) {
        SharedPreferences preferences = context.getSharedPreferences(key, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = preferences.edit();
        edit.putInt(key, value);
        edit.apply();
    }

    public static int getCount(Context context, String key) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(key, Context.MODE_PRIVATE);
        return sharedPreferences.getInt(key, 0);
    }

    public static void saveUseTime(Context context, String key, long value) {
        SharedPreferences preferences = context.getSharedPreferences(key, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = preferences.edit();
        edit.putLong(key, value);
        edit.apply();
    }

    public static long getUseTime(Context context, String key) {
        SharedPreferences preferences = context.getSharedPreferences(key, Context.MODE_PRIVATE);
        return preferences.getLong(key,0);
    }
}
