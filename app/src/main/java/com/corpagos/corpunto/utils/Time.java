package com.corpagos.corpunto.utils;

import android.content.Context;

import com.corpagos.corpunto.Global;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Time {

    public static String getCurrentDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        return sdf.format(new Date());
    }

    public static String getCurrentDay() {
        SimpleDateFormat sdf = new SimpleDateFormat("d");
        return sdf.format(new Date());
    }

    public static String getCurrentFullDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss aa");
        return sdf.format(new Date());
    }

    public static String getOperationDate(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(new Date());
    }

    public static long getTimestamp() {
        return System.currentTimeMillis() / 1000;
    }

    private static long getElapseTime(long elapseTime, long startTime) {
        return elapseTime + (getTimestamp() - startTime);
    }

    public static long getElapseTimeUsage(Context context, long elapseTime, long startTime ) {
        long time = SharedPreferencesHandler.getCount(context, Global.keyTimeCount);
        return getElapseTime(elapseTime, startTime) + time;
    }
}
