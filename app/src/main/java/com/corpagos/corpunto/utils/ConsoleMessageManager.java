package com.corpagos.corpunto.utils;

import com.jraska.console.Console;

public class ConsoleMessageManager {

    private static String CARDINSERTED = "card inserted\n";
    private static String CARDREMOVED = "card removed\n";
    private static String CARDSWIPE = "card swipe\n";
    private static String CONNECTED = "connected\n";
    private static String RECONNECTED = "reconnected\n";
    private static String DISCONNECTED = "disconnected\n";
    private static String LOCATION = "captured location\n";
    private static String PRINT = "print\n";
    private static String CARDDATA = "card data\n";

    private static String getTimeFormatMsg(){
        return Time.getOperationDate() + " ";
    }

    public static void consoleConnectionStatusMsg(boolean status) {
        Console.write(getTimeFormatMsg() + "connection " + status + "\n");
    }

    public static void consoleCardInsertedMsg(){
        Console.write(getTimeFormatMsg() + CARDINSERTED);
    }

    public static void consoleCardRemovedMsg() {
        Console.write(getTimeFormatMsg() + CARDREMOVED);
    }

    public static void consoleCardSwipeMsg() {
        Console.write(getTimeFormatMsg() + CARDSWIPE);
    }

    public static void consoleReconnectedMsg() {
        Console.write(getTimeFormatMsg() + RECONNECTED);
    }

    public static void consoleLocationMsg() {
        Console.write(getTimeFormatMsg() + LOCATION);
    }

    public static void consolePrintingMsg() {
        Console.write(getTimeFormatMsg() + PRINT);
    }

    public static void consoleGetDataCardMsg(){
        Console.write(getTimeFormatMsg() + CARDDATA);
    }
}
