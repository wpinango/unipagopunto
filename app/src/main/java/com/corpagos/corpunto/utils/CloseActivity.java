package com.corpagos.corpunto.utils;

import android.app.Activity;
import android.content.Intent;

import com.corpagos.corpunto.commons.AnimationTransition;

public class CloseActivity {

    public static void closeActivity(Activity contextActivity, Activity targetActivity){
        Intent intent = new Intent(contextActivity, targetActivity.getClass());
        contextActivity.startActivity(intent);
        AnimationTransition.setOutActivityTransition(contextActivity);
        contextActivity.finish();
    }

    public static void openActivity(Activity contextActivity, Activity targetActivity) {

    }
}
