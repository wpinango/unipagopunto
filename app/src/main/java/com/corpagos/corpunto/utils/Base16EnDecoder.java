package com.corpagos.corpunto.utils;

/**
 * Created by Maseo on 2015/9/20.
 */
public class Base16EnDecoder {

    public static byte[] Decode(String hexString) {
        if (hexString == null || hexString.length() == 0) {
            return null;
        }

        hexString = hexString.replaceAll(" ", "");
        hexString = hexString.toUpperCase();
        if (hexString.length()%2 != 0) {
            hexString = hexString.concat("0");
        }
        char[] hexChars = hexString.toCharArray();
        int bytesLen = (hexString.length()+1) / 2;
        byte[] desBytes = new byte[bytesLen];
        int pos = 0;

        for (int i = 0; i < desBytes.length; i++) {
            pos = i * 2;
            desBytes[i] = (byte) ((charToByte(hexChars[pos]) << 4) | charToByte(hexChars[pos + 1]));
        }

        return desBytes;
    }

    public static byte charToByte(char c) {
        return (byte) "0123456789ABCDEF".indexOf(c);
    }

    public static String Encode(byte[] bytes)
    {
        StringBuilder hexString = new StringBuilder();

        if (bytes.length <= 0 || bytes == null) {
            return null;
        }

        String hv;
        int v = 0;

        for (int i = 0; i < bytes.length; i++) {
            v = bytes[i] & 0xFF;
            hv = Integer.toHexString(v);

            if (hv.length() < 2) {
                hexString.append(0);
            }

            hexString.append(hv);
        }

        return hexString.toString().toUpperCase();
    }

    public static char[] byteToHexChar(byte b)
    {
        char[] ch = new char[2];
        if (b > 0xFF) {
            return null;
        }
        ch[0] = "0123456789ABCDEF".charAt((b & 0xF0)>>4);
        ch[1] = "0123456789ABCDEF".charAt((b & 0x0F));
        return ch;
    }

    public static String Encode(byte[] bytes, int start, int end)
    {
        StringBuilder hexString = new StringBuilder();

        if (bytes.length <= 0 || bytes == null) {
            return null;
        }

        String hv;
        int v = 0;

        for (int i = start; i < end; i++) {
            v = bytes[i] & 0xFF;
            hv = Integer.toHexString(v);

            if (hv.length() < 2) {
                hexString.append(0);
            }

            hexString.append(hv);
        }

        return hexString.toString().toUpperCase();
    }
}
