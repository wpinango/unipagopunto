package com.corpagos.corpunto.utils;

import android.content.Context;
import android.telephony.TelephonyManager;

public class SimDetect {


    public static int getSimStatus(Context context) {
        int value = -1;
        TelephonyManager telMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        int simState = telMgr.getSimState();
        switch (simState) {
            case TelephonyManager.SIM_STATE_ABSENT:
                value = 1;
                break;
            case TelephonyManager.SIM_STATE_NETWORK_LOCKED:
                value = 2;
                break;
            case TelephonyManager.SIM_STATE_PIN_REQUIRED:
                value = 2;
                break;
            case TelephonyManager.SIM_STATE_PUK_REQUIRED:
                value = 2;
                break;
            case TelephonyManager.SIM_STATE_READY:
                value = 2;
                break;
            case TelephonyManager.SIM_STATE_UNKNOWN:
                value = 2;
                break;
        }
        return value;
    }
}
