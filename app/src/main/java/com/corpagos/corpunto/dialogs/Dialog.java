package com.corpagos.corpunto.dialogs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Message;
import androidx.appcompat.app.AlertDialog;

import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.corpagos.corpunto.Global;
import com.corpagos.corpunto.R;
import com.corpagos.corpunto.adapters.DailyClosureListAdapter;
import com.corpagos.corpunto.adapters.SelectedServerAddressAdapter;
import com.corpagos.corpunto.commons.Format;
import com.corpagos.corpunto.enums.PosModelType;
import com.corpagos.corpunto.interfaces.ButtonInterface;
import com.corpagos.corpunto.interfaces.DialogInterface;
import com.corpagos.corpunto.models.DailyClosure;
import com.corpagos.corpunto.models.Transaction;
import com.corpagos.corpunto.singletons.PosModel;
import com.corpagos.corpunto.utils.Keyboard;
import com.corpagos.corpunto.widgets.CustomMiniKeyboard;
import com.cunoraz.gifview.library.GifView;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.corpagos.corpunto.Asynctask.finalHost;
import static com.corpagos.corpunto.Asynctask.host;
import static com.corpagos.corpunto.Asynctask.authHost;
import static com.corpagos.corpunto.Asynctask.resourcesHost;
import static com.corpagos.corpunto.Asynctask.transactionalHost;

public class Dialog {

    private GeneralDialog generalDialog;
    private Context context;
    private SweetAlertDialog sweetAlertDialog;
    private MaterialDialog materialDialog;
    private AlertDialog alertDialog;

    public Dialog(Context context) {
        this.generalDialog = new GeneralDialog(context);
        this.context = context;
    }

    public void removeCallbacksAndMessages(Context context){
        generalDialog.removeCallbacksAndMessages(context);
        generalDialog.removeMessages(generalDialog.showProgressDialog);
    }

    public void autoCloseDialog(int time){
        generalDialog.sendEmptyMessageAtTime(generalDialog.dismissDialog,time);
    }

    public void showProgressDialog(String msg) {
        Message message = new Message();
        message.what = generalDialog.showProgressDialog;
        message.obj = msg;
        generalDialog.sendMessage(message);
    }

    public void showErrorDialog(String error) {
        Message message = new Message();
        message.what = generalDialog.showErrorDialog;
        message.obj = error;
        generalDialog.sendMessage(message);
    }

    public void showInfoDialog(String info) {
        Message message = new Message();
        message.what = generalDialog.showInfoDialog;
        message.obj = info;
        generalDialog.sendMessage(message);
    }

    public void showSuccessDialog(String msg) {
        Message message = new Message();
        message.what = generalDialog.showSuccesDialog;
        message.obj = msg;
        generalDialog.sendMessage(message);
    }

    public void showErrorDialogWithListenerButton(String error,
                                                  SweetAlertDialog.OnSweetClickListener listener) {
        try {
            sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE);
            sweetAlertDialog.setContentText(error);
            sweetAlertDialog.setTitle("Error");
            sweetAlertDialog.setConfirmButton("Aceptar", listener);
            sweetAlertDialog.setCancelable(false);
            sweetAlertDialog.show();
        } catch (Exception e) {
            e.getMessage();
        }
    }

    public void showDialog() {
        try {
            sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE);
            sweetAlertDialog.setContentText(" ");
            sweetAlertDialog.setTitle("Error");
            sweetAlertDialog.setConfirmButton("Imprimir", null);
            sweetAlertDialog.setNeutralButton("Editar", null);
            sweetAlertDialog.setCancelButton("Cancelar", null);
            sweetAlertDialog.setCancelable(true);
            sweetAlertDialog.show();
        } catch (Exception e) {
            e.getMessage();
        }
    }

    public void showErrorDialogWithListenerButtons(String error,
                                                   SweetAlertDialog.OnSweetClickListener positiveListener,
                                                   SweetAlertDialog.OnSweetClickListener cancelListener,
                                                   String positiveText, String cancelText) {
        try {
            sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE);
            sweetAlertDialog.setContentText(error);
            sweetAlertDialog.setTitle("Error");
            sweetAlertDialog.setConfirmButton(positiveText, positiveListener);
            sweetAlertDialog.setCancelButton(cancelText, cancelListener);
            sweetAlertDialog.setCancelable(true);
            sweetAlertDialog.show();
        } catch (Exception e) {
            e.getMessage();
        }
    }

    public void showSuccessDialogWithListenerButton(String successMsg,
                                                    SweetAlertDialog.OnSweetClickListener listener) {
        sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE);
        sweetAlertDialog.setTitle("Correcto");
        sweetAlertDialog.setContentText(successMsg);
        sweetAlertDialog.setConfirmButton("Aceptar", listener);
        sweetAlertDialog.setCancelable(true);
        sweetAlertDialog.show();
    }

    public void showSuccessDialogWithListenerButtons(String successMsg,
                                                    SweetAlertDialog.OnSweetClickListener positiveListener,
                                                     SweetAlertDialog.OnSweetClickListener negativeListener,
                                                     String positiveText, String negativeText) {
        sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE);
        sweetAlertDialog.setTitle("Correcto");
        sweetAlertDialog.setContentText(successMsg);
        sweetAlertDialog.setConfirmButton(positiveText, positiveListener);
        sweetAlertDialog.setCancelButton(negativeText, negativeListener);
        sweetAlertDialog.setCancelable(true);
        sweetAlertDialog.show();
    }

    public void showInfoDialogWithListenerButtonAndTex(String infoMsg,
                                                       SweetAlertDialog.OnSweetClickListener listener,
                                                       String text) {

        sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.NORMAL_TYPE);
        sweetAlertDialog.setTitle("Informacion");
        sweetAlertDialog.setContentText(infoMsg);
        sweetAlertDialog.setConfirmButton(text, listener);
        sweetAlertDialog.setCancelable(true);
        sweetAlertDialog.show();
    }

    public void showInfoDialogWithListenerButtons(String infoMsg,
                                                  MaterialDialog.SingleButtonCallback positiveListener,
                                                  MaterialDialog.SingleButtonCallback negativeListener) {
        try {
            materialDialog = new MaterialDialog.Builder(context)
                    .title("Informacion")
                    .titleGravity(GravityEnum.CENTER)
                    .content(infoMsg)
                    .positiveText("Aceptar").onPositive(positiveListener)
                    .negativeText("Cancelar").onNegative(negativeListener)
                    .show();
        }catch (Exception e) {
            e.getMessage();
        }
    }

    public void showInfoDialogWithListenerButtonsAndText(String infoMsg,
                                                         MaterialDialog.SingleButtonCallback positiveListener,
                                                         MaterialDialog.SingleButtonCallback negativeListener,
                                                         String positiveText, String negativeTex) {
       try {
           materialDialog = new MaterialDialog.Builder(context)
                   .title("Informacion")
                   .titleGravity(GravityEnum.CENTER)
                   .content(infoMsg)
                   .positiveText(positiveText).onPositive(positiveListener)
                   .negativeText(negativeTex).onNegative(negativeListener)
                   .show();
       } catch (Exception e) {
           e.getMessage();
       }
    }

    public void showSomethingWrongDialog(){
        sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE);
        sweetAlertDialog.setTitle("Error");
        sweetAlertDialog.setContentText("Algo salio mal");
        sweetAlertDialog.setConfirmButton("Aceptar", null);
        sweetAlertDialog.show();
    }

    public void dismissDialog(){
        generalDialog.sendEmptyMessage(generalDialog.dismissDialog);
        if (sweetAlertDialog != null) {
            sweetAlertDialog.dismiss();
        }
        if (materialDialog != null) {
            materialDialog.dismiss();
        }
        if (alertDialog != null) {
            alertDialog.dismiss();
        }
    }

    public void showWarningDelectionWithListenerButton(String msg,
                                                       MaterialDialog.SingleButtonCallback
                                                               positiveListener) {
        materialDialog = new MaterialDialog.Builder(context)
                .title("Advertencia")
                .content(msg)
                .titleGravity(GravityEnum.CENTER)
                .positiveText("Aceptar").onPositive(positiveListener)
                .negativeText("Cancelar")
                .show();
    }

    public void showWarningDialog(String msg, SweetAlertDialog.OnSweetClickListener positiveListener) {
        sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        sweetAlertDialog.setTitle("Advertencia");
        sweetAlertDialog.setContentText(msg);
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.setConfirmButton("Aceptar", positiveListener);
        sweetAlertDialog.setCancelButton("Cancelar", null);
        sweetAlertDialog.show();
    }

    public void showInfo3ButtonsDialog(String content, SweetAlertDialog.OnSweetClickListener positiveListener,
                                       SweetAlertDialog.OnSweetClickListener neutralListener,
                                       SweetAlertDialog.OnSweetClickListener cancelListener) {
        sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE);
        sweetAlertDialog.setTitle("Error");
        sweetAlertDialog.setContentText(content);
        sweetAlertDialog.setConfirmButton("Edit", positiveListener);
        sweetAlertDialog.setCancelButton("Can", cancelListener);
        sweetAlertDialog.setNeutralButton("Print", neutralListener);
        sweetAlertDialog.show();
    }

    public void showBalanceDialog(String infoMsg) {
        try {
            materialDialog = new MaterialDialog.Builder(context)
                    .title("Balance Diario")
                    .titleGravity(GravityEnum.CENTER)
                    .content(infoMsg)
                    .positiveText("Cerrar")
                    .show();
        }catch (Exception e) {
            e.getMessage();
        }
    }

    public static void showSuccessfulSaleDialog(Context context, Transaction transaction,
                                                DialogInterface listener, String textButton){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_success_sell, null);
        TextView tvDate = view.findViewById(R.id.tv_date2);
        TextView tvAmount = view.findViewById(R.id.tv_amount2);
        TextView tvReference = view.findViewById(R.id.tv_reference2);
        GifView gifView1 = view.findViewById(R.id.gif2);
        gifView1.setVisibility(View.VISIBLE);
        gifView1.play();
        tvAmount.setText(Format.getCashFormat(Math.abs(transaction.getAmount())));
        tvDate.setText(Format.getLocalDateFormat(context, Format.dateFullPattern, transaction.getCreated_at()));
        tvReference.setText(transaction.getReference());
        builder.setTitle("Transaccion Aprobada");
        builder.setPositiveButton(textButton, (dialogInterface, i) -> {
            listener.positiveClick();
        });
        builder.setNegativeButton("Cerrar", (dialogInterface, i) -> {
            listener.negativeClick();
            dialogInterface.dismiss();
        });
        builder.setView(view);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public static void showSuccessfulPrepaidServicePaymentDialog(Context context, Transaction transaction,
                                                DialogInterface listener, String textButton){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_sucess_payment, null);
        TextView tvDate = view.findViewById(R.id.tv_date3);
        TextView tvServiceLabel = view.findViewById(R.id.textView23);
        tvServiceLabel.setText("Servicio");
        TextView tvTargetNumberLabel = view.findViewById(R.id.textView21);
        tvTargetNumberLabel.setText("Numero");
        TextView tvAmount = view.findViewById(R.id.tv_amount3);
        TextView tvReference = view.findViewById(R.id.tv_reference3);
        TextView tvService = view.findViewById(R.id.tv_dni_payment_dialog);
        TextView tvTargetNumber = view.findViewById(R.id.tv_card_name_payment_dialog);
        GifView gifView1 = view.findViewById(R.id.gif);
        gifView1.setVisibility(View.VISIBLE);
        gifView1.play();
        tvAmount.setText(Format.getCashFormat(transaction.getAmount()));
        tvDate.setText(Format.getLocalDateFormat(context, Format.dateFullPattern, transaction.getCreated_at()));
        tvReference.setText("Ref. " + transaction.getReference());
        tvTargetNumber.setText(transaction.getDate().substring(transaction.getDate().length() - 4));
        tvService.setText(transaction.getConcept());
        builder.setTitle("Recarga exitosa");
        builder.setPositiveButton(textButton, (dialogInterface, i) -> listener.positiveClick());
        builder.setNeutralButton("Cerrar", (dialog, which) -> listener.negativeClick());
        builder.setView(view);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public static void showTransactionHistories(Context context, Transaction transaction,
                                                DialogInterface listener, String buttonAction){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_success_sell, null);
        TextView tvDate = view.findViewById(R.id.tv_date2);
        TextView tvAmount = view.findViewById(R.id.tv_amount2);
        TextView tvReference = view.findViewById(R.id.tv_reference2);
        GifView gifView1 = view.findViewById(R.id.gif2);
        gifView1.setVisibility(View.VISIBLE);
        gifView1.play();
        tvAmount.setText(Format.getCashFormat(transaction.getAmount()));
        tvDate.setText(Format.getLocalDateFormat(context, Format.dateFullPattern, transaction.getCreated_at()));
        tvReference.setText(transaction.getReference());
        builder.setTitle("Su transaccion fue procesada");
        builder.setPositiveButton(buttonAction, (dialogInterface, i) -> {
            listener.positiveClick();
        });
        builder.setNeutralButton("Cerrar", null);
        builder.setView(view);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public static void showPaymentHistories(Context context, Transaction transaction,
                                                DialogInterface listener){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_sucess_payment, null);
        TextView tvDate = view.findViewById(R.id.tv_date3);
        TextView tvAmount = view.findViewById(R.id.tv_amount3);
        TextView tvReference = view.findViewById(R.id.tv_reference3);
        TextView tvDni = view.findViewById(R.id.tv_dni_payment_dialog);
        TextView tvCardName = view.findViewById(R.id.tv_card_name_payment_dialog);
        GifView gifView1 = view.findViewById(R.id.gif);
        gifView1.setVisibility(View.VISIBLE);
        gifView1.play();
        tvAmount.setText(Format.getCashFormat(transaction.getAmount()));
        tvDate.setText(Format.getLocalDateFormat(context, Format.dateFullPattern, transaction.getCreated_at()));
        tvReference.setText("Ref. 1183458391915");
        tvCardName.setText(transaction.getConcept());
        tvDni.setText(transaction.getReference());
        builder.setTitle("Pago exitoso");
        builder.setPositiveButton("Imprimir", (dialogInterface, i) -> listener.positiveClick());
        builder.setNeutralButton("Cerrar", (dialog, which) -> listener.negativeClick());
        builder.setView(view);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public void showUpdateAppDialog(String updateDetails, DialogInterface listener){
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            LayoutInflater inflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.dialog_update_details, null);
            TextView tvDetail = view.findViewById(R.id.tv_update_detail);
            tvDetail.setText(updateDetails);
            builder.setTitle("Actualizacion de aplicacion");
            builder.setMessage("Se ha encontrado una actualizacion, con las siguientes mejoras: ");
            builder.setPositiveButton("Descargar ", (dialogInterface, i) -> {
                listener.positiveClick();
            });
            builder.setView(view);
            builder.setCancelable(false);
            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        } catch (Exception e) {
            e.getMessage();
        }
    }

    public void updateProgressDialog(int update, ProgressDialog progressDialog) {
        progressDialog.setProgress(update);
        progressDialog.show();
    }

    public void showDownloadFileSuccess(MaterialDialog.SingleButtonCallback positiveListener,
                                        MaterialDialog.SingleButtonCallback negativeListener) {
        materialDialog = new MaterialDialog.Builder(context)
                .title("Descarga exitosa")
                .content("Se descargo actualizacion, instale la actualizacion por favor")
                .positiveText("Instalar").onPositive(positiveListener)
                .negativeText("Cancelar").onNegative(negativeListener)
                .show();
    }

    public void showAdminCredentialsDialog(DialogInterface listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_admin_credentials, null);
        EditText etUser = view.findViewById(R.id.et_username_admin);
        EditText etPass = view.findViewById(R.id.et_password_admin);
        builder.setTitle("Introduzca sus credenciales");
        builder.setPositiveButton("Entrar", (dialogInterface, i) -> {
        });
        builder.setNegativeButton("Cerrar", (dialogInterface, i) ->{
        });
        builder.setCancelable(false);
        builder.setView(view);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(v -> {
            if (etPass.getText().toString().equals("") || etUser.getText().toString().equals("")) {
                Toast.makeText(context, "Debe ingresar las credenciales",
                        Toast.LENGTH_LONG).show();
            } else {
                listener.positiveClick();
                alertDialog.dismiss();
            }
        });
        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(v -> {
            listener.negativeClick();
            alertDialog.dismiss();
        });
    }

    public void showDailyClosureDialog(String title, DailyClosure dailyClosure, DialogInterface listener, PosModel posModel) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_daily_closure , null);
        ListView lvTransactions = view.findViewById(R.id.lv_daily_closure);
        DailyClosureListAdapter dailyClosureListAdapter = new DailyClosureListAdapter(context,
                dailyClosure.getTransactions());
        lvTransactions.setAdapter(dailyClosureListAdapter);
        dailyClosureListAdapter.notifyDataSetChanged();
        TextView tvTotal = view.findViewById(R.id.tv_total_amount);
        TextView tvTotalTransactions = view.findViewById(R.id.tv_total_transactions);
        TextView tvLot = view.findViewById(R.id.tv_dialog_closure_lot_number);
        tvLot.setText(String.format("%04d", dailyClosure.getSummary().getLot_number()));
        tvTotalTransactions.setText(String.valueOf(dailyClosure.getTransactions().size()));
        tvTotal.setText(Format.getCashFormat(dailyClosure.getSummary().getTotal_amount()));
        builder.setTitle(title);
        if (!posModel.getPosModelType().equals(PosModelType.WPOS_MINI)) {
            builder.setPositiveButton("Imprimir resumen", (dialogInterface, i) -> {
                listener.positiveClick();
            });
            builder.setNeutralButton("Imprimir detalle", (dialogInterface, i) -> {
                listener.negativeClick();
            });
        } else {
            builder.setPositiveButton("Aceptar", (dialogInterface, i) -> {
            });
        }
        builder.setNegativeButton("Cerrar", null) ;
        builder.setCancelable(false);
        builder.setView(view);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @SuppressLint("NewApi")
    public void showInputDniDialog(Activity activity, Context context, ButtonInterface listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_input_dni , null);
        EditText etDni = view.findViewById(R.id.et_client_dni);
        etDni.setShowSoftInputOnFocus(false);
        CustomMiniKeyboard keyboard  = view.findViewById(R.id.keyboard_mini_dni);
        etDni.setOnFocusChangeListener((view1, b) -> {
            if (b){
                InputConnection ic1 = etDni.onCreateInputConnection(new EditorInfo());
                keyboard.setInputConnection(ic1);
            }
        });
        etDni.requestFocus();
        Keyboard.hideKeyboard(activity);
        builder.setPositiveButton("Aceptar", (dialogInterface, i) -> {
        });
        builder.setNegativeButton("Cancelar", (dialog, which) -> {
        });
        builder.setCancelable(false);
        builder.setView(view);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(v -> {
            if (etDni.getText().toString().equals("")){
                Global.showToast(context, "Cedula del cliente", 2000);
            } else {
                listener.onPositiveButtonClick(etDni.getText().toString());
                alertDialog.dismiss();
            }
        });
        alertDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(v -> {
            listener.onNegativeButtonClick("");
            alertDialog.dismiss();
        });
    }

    @SuppressLint("NewApi")
    public void showInputPosPassDialog(Context context, ButtonInterface listener, String title) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_input_owner_pass , null);
        EditText etPass = view.findViewById(R.id.et_owner_pass);
        TextView tvTitle = view.findViewById(R.id.textView10);
        tvTitle.setText(title);
        CustomMiniKeyboard keyboard  = view.findViewById(R.id.keyboard_mini_pass);
        etPass.setShowSoftInputOnFocus(false);
        etPass.setOnFocusChangeListener((view1, b) -> {
            if (b){
                InputConnection ic1 = etPass.onCreateInputConnection(new EditorInfo());
                keyboard.setInputConnection(ic1);
            }
        });
        builder.setPositiveButton("Aceptar", (dialogInterface, i) -> {
        });
        builder.setNegativeButton("Cancelar", (dialog, which) -> {
        });
        builder.setCancelable(false);
        builder.setView(view);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(v -> {
            if (etPass.getText().toString().equals("")){
                Global.showToast(context, "Introduzca clave del punto", 2000);
            }  else {
                listener.onPositiveButtonClick(etPass.getText().toString());
                alertDialog.dismiss();
            }
        });
        alertDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(v -> {
            listener.onNegativeButtonClick("");
            alertDialog.dismiss();
        });
    }

    public void showInsertCardDialog(ButtonInterface listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_insert_card , null);
        GifView gifView1 = view.findViewById(R.id.gif1);
        gifView1.setVisibility(View.VISIBLE);
        gifView1.play();
        gifView1.setGifResource(R.drawable.card_gif);
        builder.setNegativeButton("Cancelar", (dialog, which) -> {
        });
        builder.setCancelable(false);
        builder.setView(view);
        alertDialog = builder.create();
        alertDialog.show();
        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(v -> {
            listener.onPositiveButtonClick("");
            alertDialog.dismiss();
        });
    }

    public void showUpdatePassDialog(Context context, ButtonInterface listener, String title) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_input_owner_pass , null);
        EditText etPass = view.findViewById(R.id.et_owner_pass);
        TextView tvTitle = view.findViewById(R.id.textView10);
        tvTitle.setText(title);
        builder.setPositiveButton("Aceptar", (dialogInterface, i) -> {
        });
        builder.setNegativeButton("Cancelar", (dialog, which) -> {
        });
        builder.setCancelable(false);
        builder.setView(view);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(v -> {
            if (etPass.getText().toString().equals("")){
                Global.showToast(context, "Introduzca clave del punto", 2000);
            } else {
                listener.onPositiveButtonClick(etPass.getText().toString());
                alertDialog.dismiss();
            }
        });
        alertDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(v -> {
            listener.onNegativeButtonClick("");
            alertDialog.dismiss();
        });
    }

    public static void showServerSelectionDialog(Context context, DialogInterface listener) {
        host.clear();
        host.add("http://46.101.158.43");
        host.add("http://172.16.31.28");
        host.add("http://172.16.31.27");
        host.add("http://200.125.187.60");
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_select_server_address , null);
        TextView tvServerAddress = view.findViewById(R.id.tv_selected_address);
        TextView tvAuth = view.findViewById(R.id.tv_auth_host);
        TextView tvResouses = view.findViewById(R.id.tv_resourses_host);
        TextView tvtransactiona = view.findViewById(R.id.tv_transactional_host);
        tvServerAddress.setText(finalHost);
        tvAuth.setText(finalHost + ":3000");
        tvResouses.setText(finalHost + ":3001");
        tvtransactiona.setText(finalHost + ":3002");
        ListView lvServerAddress = view.findViewById(R.id.lv_server_address);
        SelectedServerAddressAdapter selectedServerAddressAdapter =
                new SelectedServerAddressAdapter(context, host);
        lvServerAddress.setAdapter(selectedServerAddressAdapter);
        lvServerAddress.setOnItemClickListener((parent, view1, position, id) -> {
            finalHost = host.get(position);
            selectedServerAddressAdapter.notifyDataSetChanged();
            System.out.println("valores : " + finalHost);
        });
        builder.setPositiveButton("Aceptar", (dialogInterface, i) -> {
        });
        builder.setNegativeButton("Cancelar", (dialog, which) -> {
        });
        builder.setCancelable(false);
        builder.setView(view);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(v -> {
            alertDialog.dismiss();
            authHost = finalHost + ":3000";
            resourcesHost = finalHost + ":3001";
            transactionalHost = finalHost + ":3002";
            listener.positiveClick();
        });
        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(v -> {
            finalHost = authHost;
            alertDialog.dismiss();
            listener.negativeClick();
        });
    }
}
