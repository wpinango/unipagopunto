package com.corpagos.corpunto;

import android.content.Context;
import android.os.Handler;
import android.widget.Toast;

public class Global {

    public static boolean activityVisible = false;
    public static String amount;
    public static String deviceSerial = "deviceSerial";
    public static String keyNfcCount = "nfcCount";
    public static String keyChipCount = "chipCount";
    public static String keyPrintCount = "printCount";
    public static String keyPrintLinesCount = "printLinesCount";
    public static String keyTimeCount = "timeCount";
    public static int nfcCount;
    public static int chipCount;
    public static int printCount;
    public static int printLinesCount;
    public static long timeCount;
    public static String credit = "credit";
    public static String debit = "debit";
    public static String nfc = "nfc";
    public static String chip = "chip";
    public static final String ERR_CONNECTION_FAILED = "ERR_CONNECTION_FAILED";
    public static final String ERR_INVALID_PASSWORD = "ERR_INVALID_PASSWORD";
    public static final String ERR_INSUFICIENT_FOUNDS = "ERR_INSUFICIENT_FOUNDS";
    public static final String ERR_INVALID_DNI = "ERR_INVALID_DNI";
    public static final String ERR_UNKNOWN_DEVICE_SERIAL = "ERR_UNKNOWN_DEVICE_SERIAL";
    public static final String software = "plus";

    public static String messageResponse(String message) {
        switch (message) {
            case "ERR_INVALID_OR_EXPIRED_TOKEN":
                return "Error en la validacion del punto, por favor comuniquese con un operador.";
            case ERR_CONNECTION_FAILED:
                return "Error de conexion";
            case ERR_INSUFICIENT_FOUNDS:
                return "Error saldo infuciente";
            case ERR_INVALID_PASSWORD:
                return "Error clave invalida";
            case ERR_INVALID_DNI:
                return "Error cedula invalida";
            case ERR_UNKNOWN_DEVICE_SERIAL:
                return "Error en validacion de serial pos";
            default:
                return "Algo salio mal";
        }
    }

    public static void showToast(Context context, String msg, int timeMillis){
        final Toast toast = Toast.makeText(context, msg, Toast.LENGTH_SHORT);
        if (context != null) {
            toast.show();
        }

        Handler handler = new Handler();
        handler.postDelayed(() -> toast.cancel(), timeMillis);
    }
}
