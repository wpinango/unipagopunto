package com.corpagos.corpunto.models;

import android.content.Context;
import android.util.Base64;

import com.corpagos.corpunto.encryption.AESCipher;
import com.corpagos.corpunto.utils.SharedPreferencesHandler;
import com.google.gson.Gson;

public class Keys {
    private String iv;
    private String key;
    private String verification = "null";

    public String getVerification() {
        return verification;
    }

    public void setVerification(String verification) {
        this.verification = verification;
    }

    public byte[] getIv() {
        return Base64.decode(iv,Base64.DEFAULT);
    }

    public void setIv(String iv) {
        this.iv = iv;
    }

    public byte[] getKey() {
        return Base64.decode(key,Base64.DEFAULT);
    }

    public void setKey(String key) {
        this.key = key;
    }

    public static Keys generateNewRandomKeys(Context context) {
        Keys keys = new Keys();
        keys.setIv(AESCipher.generateSecureRandomKey(16).replace("\n",""));
        keys.setKey(AESCipher.generateSecureRandomKey(32).replace("\n",""));
        System.out.println("valores llaves: + " + new Gson().toJson(keys));
        SharedPreferencesHandler.saveData(context, SharedPreferencesHandler.KEY_AES_KEY,
                new Gson().toJson(keys), SharedPreferencesHandler.FILE_AES_KEY);
        return keys;
    }

    public static Keys getKeys(Context context) {
        return new Gson().fromJson(SharedPreferencesHandler.getSavedData(context,
                SharedPreferencesHandler.KEY_AES_KEY, SharedPreferencesHandler.FILE_AES_KEY), Keys.class);
    }

    public static boolean isKeys(Context context) {
        return !SharedPreferencesHandler.getSavedData(context, SharedPreferencesHandler.KEY_AES_KEY,
                SharedPreferencesHandler.FILE_AES_KEY).equals(SharedPreferencesHandler.NOT_FOUND);
    }

}
