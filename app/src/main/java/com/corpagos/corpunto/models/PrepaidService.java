package com.corpagos.corpunto.models;

import com.corpagos.corpunto.enums.PrepaidServiceType;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PrepaidService {

    @SerializedName("id")
    @Expose
    private int id;
    private String commission;

    @SerializedName("name")
    @Expose
    private String name;
    private String logo;
    private String targetNumber;
    private float amount;

    @SerializedName("min_amount")
    @Expose
    private int minAmount;

    @SerializedName("max_amount")
    @Expose
    private int maxAmount;

    @SerializedName("step_amount")
    @Expose
    private int stepAmount;

    @SerializedName("profit")
    @Expose
    private int profit;
    private PrepaidServiceType prepaidServiceType = PrepaidServiceType.PHONE;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCommission() {
        return commission;
    }

    public void setCommission(String commission) {
        this.commission = commission;
    }

    public PrepaidServiceType getPrepaidServiceType() {
        return prepaidServiceType;
    }

    public void setPrepaidServiceType(PrepaidServiceType prepaidServiceType) {
        this.prepaidServiceType = prepaidServiceType;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public String getTargetNumber() {
        return targetNumber;
    }

    public void setTargetNumber(String targetNumber) {
        this.targetNumber = targetNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }


    public int getMinAmount() {
        return minAmount;
    }

    public void setMinAmount(int minAmount) {
        this.minAmount = minAmount;
    }

    public int getMaxAmount() {
        return maxAmount;
    }

    public void setMaxAmountl(int maxAmount) {
        this.maxAmount = maxAmount;
    }

    public int getStepAmount() {
        return stepAmount;
    }

    public void setStepAmount(int stepAmount) {
        this.stepAmount = stepAmount;
    }

    public void setMaxAmount(int maxAmount) {
        this.maxAmount = maxAmount;
    }

    public int getProfit() {
        return profit;
    }

    public void setProfit(int profit) {
        this.profit = profit;
    }
}

