package com.corpagos.corpunto.models;

public class TransactionType {

    public static String CLOSURE = "CLOSURE";
    public static String SECTION = "SECTION";
    public static String SALE = "SALE";
    public static String PAYMENT = "PAYMENT";
    public static String PREPAID_SERVICE = "PREPAID_SERVICE";
    public static String ROLLBACK = "ROLLBACK";
    public static String PAYMENT_PROVIDERS = "PAYMENT_PROVIDERS";
}
