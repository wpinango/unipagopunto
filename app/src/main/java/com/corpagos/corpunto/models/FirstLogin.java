package com.corpagos.corpunto.models;

public class FirstLogin extends ClientCredentials {

    private boolean isLogin;


    public boolean isLogin() {
        return isLogin;
    }

    public void setLogin(boolean login) {
        isLogin = login;
    }
}
