package com.corpagos.corpunto.models;

import com.corpagos.corpunto.enums.CardAccountType;
import com.corpagos.corpunto.enums.CardTypeEnum;

public class Card {
    private CardAccountType cardAccountType;
    private CardTypeEnum cardTypeEnum;

    public CardAccountType getCardAccountType() {
        return cardAccountType;
    }

    public void setCardAccountType(CardAccountType cardAccountType) {
        this.cardAccountType = cardAccountType;
    }

    public CardTypeEnum getCardTypeEnum() {
        return cardTypeEnum;
    }

    public void setCardTypeEnum(CardTypeEnum cardTypeEnum) {
        this.cardTypeEnum = cardTypeEnum;
    }
}
