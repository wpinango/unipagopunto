package com.corpagos.corpunto.models;

public class TransactionDB {

    public static String transactionTable = "transaction_content";
    public static String TABLE = "tabla";

    public static final String KEY_ID = "id";
    public static final String KEY_REFERENCE = "reference";
    public static final String KEY_DATE = "date";
    public static final String KEY_USER_ID = "user_id";
    public static final String KEY_AMOUNT = "amount";
    public static final String KEY_CONCEPT = "concept";
    public static final String KEY_CREATED = "created_at";
    public static final String KEY_BALANCE = "balance";
    public static final String KEY_CARD_NUMBER = "card_number";
    public static final String KEY_CARD_TYPE = "card_type";
    public static final String KEY_LOT = "lot";
    public static final String KEY_REVERSE = "reverse";
    public static final String KEY_STATUS = "status";
    public static final String KEY_VERSION = "app_version";
    public static final String KEY_TRANSACTION_TYPE = "transaction_type";
    public static final String KEY_TRACE = "trace";

}
