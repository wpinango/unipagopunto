package com.corpagos.corpunto.models;

import com.corpagos.corpunto.emv.PosCounterType;

public class CountReader {

    private String title;
    private long count;
    private PosCounterType posCounterType;

    public CountReader(){

    }

    public CountReader(String title, int count) {
        this.count = count;
        this.title = title;
    }

    public PosCounterType getPosCounterType() {
        return posCounterType;
    }

    public void setPosCounterType(PosCounterType posCounterType) {
        this.posCounterType = posCounterType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }
}
