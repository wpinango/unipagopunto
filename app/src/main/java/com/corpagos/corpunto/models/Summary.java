package com.corpagos.corpunto.models;

public class Summary {
    private float total_amount;
    private String close_date;
    private int lot_number;

    public int getLot_number() {
        return lot_number;
    }

    public void setLot_number(int lot_number) {
        this.lot_number = lot_number;
    }

    public float getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(float total_amount) {
        this.total_amount = total_amount;
    }

    public String getClose_date() {
        return close_date;
    }

    public void setClose_date(String close_date) {
        this.close_date = close_date;
    }
}