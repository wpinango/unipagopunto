package com.corpagos.corpunto.models;

import java.util.ArrayList;

public class ClientCredentials {
    private String client_id;
    private String client_secret;
    private String serial;
    private ArrayList<Password> avaivable_passwords;

    public ArrayList<Password> getAvailable_password() {
        return avaivable_passwords;
    }

    public void setAvailable_password(ArrayList<Password> available_password) {
        this.avaivable_passwords = available_password;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getClient_id() {
        return client_id;
    }

    public void setClient_id(String client_id) {
        this.client_id = client_id;
    }

    public String getClient_secret() {
        return client_secret;
    }

    public void setClient_secret(String client_secret) {
        this.client_secret = client_secret;
    }
}
