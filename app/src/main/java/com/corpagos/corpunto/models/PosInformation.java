package com.corpagos.corpunto.models;

public class PosInformation {

    private String posModel;
    private String posSerial;
    private String posFamily;


    public String getPosModel() {
        return posModel;
    }

    public void setPosModel(String posModel) {
        this.posModel = posModel;
    }

    public String getPosSerial() {
        return posSerial;
    }

    public void setPosSerial(String posSerial) {
        this.posSerial = posSerial;
    }

    public String getPosFamily() {
        return posFamily;
    }

    public void setPosFamily(String posFamily) {
        this.posFamily = posFamily;
    }
}
