package com.corpagos.corpunto.models;

public class UpdateApp {
    private String version;
    private String path;
    private String changelog;
    private String filename;

    public String getChangelog() {
        return changelog;
    }

    public void setChangelog(String changelog) {
        this.changelog = changelog;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getChangeLog() {
        return changelog;
    }

    public void setChangeLog(String changeLog) {
        this.changelog = changeLog;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
