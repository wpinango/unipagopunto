package com.corpagos.corpunto.models;

public class Ticket {
    private Transaction transaction;
    private OwnerData ownerData;
    private CardData cardData;
    private String title;

    public Ticket(Transaction transaction, OwnerData ownerData, String title, CardData cardData) {
        this.transaction = transaction;
        this.ownerData = ownerData;
        this.cardData = cardData;
        this.title = title;
    }

    public OwnerData getOwnerData() {
        return ownerData;
    }

    public void setOwnerData(OwnerData ownerData) {
        this.ownerData = ownerData;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public CardData getCardData() {
        return cardData;
    }

    public void setCardData(CardData cardData) {
        this.cardData = cardData;
    }
}
