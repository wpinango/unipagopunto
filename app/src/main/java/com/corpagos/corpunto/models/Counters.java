package com.corpagos.corpunto.models;

public class Counters {
    private int nfcCount;
    private int printCount;
    private int chipCount;
    private int printLinesCount;
    private long timeCount;
    private int printedMeters;

    public int getPrintedMeters() {
        return printedMeters;
    }

    public void setPrintedMeters(int printedMeters) {
        this.printedMeters = printedMeters;
    }

    public long getTimeCount() {
        return timeCount;
    }

    public void setTimeCount(long timeCount) {
        this.timeCount = timeCount;
    }

    public int getPrintLinesCount() {
        return printLinesCount;
    }

    public void setPrintLinesCount(int printLinesCount) {
        this.printLinesCount = printLinesCount;
    }

    public int getNfcCount() {
        return nfcCount;
    }

    public void setNfcCount(int nfcCount) {
        this.nfcCount = nfcCount;
    }

    public int getPrintCount() {
        return printCount;
    }

    public void setPrintCount(int printCount) {
        this.printCount = printCount;
    }

    public int getChipCount() {
        return chipCount;
    }

    public void setChipCount(int chipCount) {
        this.chipCount = chipCount;
    }
}
