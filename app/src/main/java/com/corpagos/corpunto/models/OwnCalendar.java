package com.corpagos.corpunto.models;

import java.util.ArrayList;

public class OwnCalendar {

    private String dayName;
    private String dayNumber;
    private String month;
    private String monthNumber;
    private boolean select;

    public String getMonthNumber() {
        return monthNumber;
    }

    public void setMonthNumber(String monthNumber) {
        this.monthNumber = monthNumber;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public boolean isSelect() {
        return select;
    }

    public void setSelect(boolean select) {
        this.select = select;
    }

    public String getDayName() {
        return dayName;
    }

    public void setDayName(String dayName) {
        this.dayName = dayName;
    }

    public String getDayNumber() {
        return dayNumber;
    }

    public void setDayNumber(String dayNumber) {
        this.dayNumber = dayNumber;
    }

    public static void deselectAll(ArrayList<OwnCalendar> ownCalendars) {
        for (OwnCalendar o : ownCalendars) {
            o.setSelect(false);
        }
    }
}
