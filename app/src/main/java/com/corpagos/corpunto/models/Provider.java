package com.corpagos.corpunto.models;

import java.util.ArrayList;

public class Provider {
    private int id;
    private String name;
    private String tin;
    private int user_id;
    private ArrayList<ProviderAccount> providers_accounts;

    public String getTin() {
        return tin;
    }

    public void setTin(String tin) {
        this.tin = tin;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public ArrayList<ProviderAccount> getAccounts() {
        return providers_accounts;
    }

    public void setAcounts(ArrayList<ProviderAccount> accounts) {
        this.providers_accounts = accounts;
    }
}
