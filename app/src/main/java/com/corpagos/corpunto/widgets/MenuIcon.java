package com.corpagos.corpunto.widgets;

import androidx.appcompat.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;

import java.util.ArrayList;

public class MenuIcon {

    public  static View getToolbarNavigationIcon(Toolbar toolbar){
        boolean hadContentDescription = TextUtils.isEmpty(toolbar.getNavigationContentDescription());
        CharSequence contentDescription = !hadContentDescription ?
                toolbar.getNavigationContentDescription() : "navigationIcon";
        toolbar.setNavigationContentDescription(contentDescription);
        ArrayList<View> potentialViews = new ArrayList<View>();
        toolbar.findViewsWithText(potentialViews,contentDescription,
                View.FIND_VIEWS_WITH_CONTENT_DESCRIPTION);
        View navIcon = null;
        if(potentialViews.size() > 0){
            navIcon = potentialViews.get(0);
        }
        if(hadContentDescription)
            toolbar.setNavigationContentDescription(null);
        return navIcon;
    }
}
