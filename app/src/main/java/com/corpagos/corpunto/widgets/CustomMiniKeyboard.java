package com.corpagos.corpunto.widgets;

import android.content.Context;
import android.media.AudioManager;
import android.os.Vibrator;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.SoundEffectConstants;
import android.view.View;
import android.view.inputmethod.InputConnection;
import android.widget.Button;
import android.widget.ImageButton;

import com.corpagos.corpunto.R;
import com.corpagos.corpunto.interfaces.KeyListener;

public class CustomMiniKeyboard extends ConstraintLayout implements View.OnClickListener {

    private Button btnOne;
    private Button btnTwo;
    private Button btnThree;
    private Button btnFour;
    private Button btnFive;
    private Button btnSix;
    private Button btnSeven;
    private Button btnEight;
    private Button btnNine;
    private Button btnZero;
    private ImageButton btnDel;
    private Button btnDone;
    private Button btnCancel;
    //private Button btnBack;
    //private Button btnForward;
    private SparseArray<String> keyValues = new SparseArray<>();
    private InputConnection inputConnection;
    private KeyListener listener;

    public CustomMiniKeyboard(Context context) {
        super(context);
        init(context);
    }

    public CustomMiniKeyboard(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public CustomMiniKeyboard(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @Override
    public void onClick(View view) {
        try {
            playClick();
            if (inputConnection == null)
                return;
            if (view.getId() == R.id.btn_delete) {
                CharSequence selectedText = inputConnection.getSelectedText(0);
                if (TextUtils.isEmpty(selectedText)) {
                    inputConnection.deleteSurroundingText(1, 0);
                } else {
                    inputConnection.commitText("", 1);
                }
            } else if (view.getId() == R.id.btn_cancel) {
                listener.onKeyPressed(KeyListener.specialKey.CANCEL);
            } else if (view.getId() == R.id.btn_done) {
                listener.onKeyPressed(KeyListener.specialKey.DONE);
            } /*else if (view.getId() == R.id.btn_back) {
                listener.onKeyPressed(KeyListener.specialKey.BACK);
            } else if (view.getId() == R.id.btn_foward) {
                listener.onKeyPressed(KeyListener.specialKey.FORWARD);
            } */else {
                String value = keyValues.get(view.getId());
                inputConnection.commitText(value, 0);

            }
        } catch (Exception e) {
            e.getMessage();
        }
    }

    public void setInputConnection(InputConnection ic) {
        this.inputConnection = ic;
    }

    public void setListener(KeyListener listener){
        this.listener = listener;
    }


    private void init(Context context) {
        ConstraintLayout.inflate(context,R.layout.keyboard_mini,this);
        btnOne = findViewById(R.id.btn_one);
        btnOne.setOnClickListener(this);
        btnTwo = findViewById(R.id.btn_two);
        btnTwo.setOnClickListener(this);
        btnThree = findViewById(R.id.btn_three);
        btnThree.setOnClickListener(this);
        btnFour = findViewById(R.id.btn_four);
        btnFour.setOnClickListener(this);
        btnFive = findViewById(R.id.btn_five);
        btnFive.setOnClickListener(this);
        btnSix = findViewById(R.id.btn_six);
        btnSix.setOnClickListener(this);
        btnSeven = findViewById(R.id.btn_seven);
        btnSeven.setOnClickListener(this);
        btnEight = findViewById(R.id.btn_eight);
        btnEight.setOnClickListener(this);
        btnNine = findViewById(R.id.btn_nine);
        btnNine.setOnClickListener(this);
        btnZero = findViewById(R.id.btn_zero);
        btnZero.setOnClickListener(this);
        btnDel = findViewById(R.id.btn_delete);
        btnDel.setOnClickListener(this);
        btnCancel = findViewById(R.id.btn_cancel);
        btnCancel.setOnClickListener(this);
        btnDone = findViewById(R.id.btn_done);
        btnDone.setOnClickListener(this);
        //btnBack = findViewById(R.id.btn_back);
        //btnBack.setOnClickListener(this);
        //btnForward = findViewById(R.id.btn_foward);
        //btnForward.setOnClickListener(this);
        keyValues.put(R.id.btn_one, "1");
        keyValues.put(R.id.btn_two, "2");
        keyValues.put(R.id.btn_three, "3");
        keyValues.put(R.id.btn_four, "4");
        keyValues.put(R.id.btn_five, "5");
        keyValues.put(R.id.btn_six, "6");
        keyValues.put(R.id.btn_seven, "7");
        keyValues.put(R.id.btn_eight, "8");
        keyValues.put(R.id.btn_nine, "9");
        keyValues.put(R.id.btn_zero, "0");
        //keyValues.put(R.id.btn_back,"\b");
        keyValues.put(R.id.btn_done, "\n\r");
    }

    private void playClick(){
        AudioManager am = (AudioManager)getContext().getSystemService(Context.AUDIO_SERVICE);
        am.playSoundEffect(SoundEffectConstants.CLICK);
        playSoundEffect(SoundEffectConstants.CLICK);
        Vibrator vb = (Vibrator)getContext().getSystemService(Context.VIBRATOR_SERVICE);
        vb.vibrate(50);
    }
}

