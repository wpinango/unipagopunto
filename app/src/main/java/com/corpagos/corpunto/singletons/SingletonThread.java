package com.corpagos.corpunto.singletons;

import com.corpagos.corpunto.interfaces.ThreadInterface;


public class SingletonThread {

    private static SingletonThread instance;
    private Thread nfcThread;
    private boolean threadStatus = true;


    private SingletonThread(ThreadInterface listener) {
        nfcThread = new Thread(() -> {
            while (threadStatus) {
                try {
                    listener.task();
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    return;
                }
            }
        });
    }

    public static SingletonThread getInstance(ThreadInterface listener) {
        if (instance == null) {
            instance = new SingletonThread(listener);
        }
        return instance;
    }

    public  void startThread() {
        nfcThread.start();
    }

    public  void setThreadStatus(boolean value) {
        threadStatus = value;
    }

    public boolean isThreadAlive() {
        return nfcThread.isAlive();
    }
}
