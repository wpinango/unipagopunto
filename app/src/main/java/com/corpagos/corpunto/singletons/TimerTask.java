package com.corpagos.corpunto.singletons;

import android.content.Context;

import com.corpagos.corpunto.Global;

import static com.corpagos.corpunto.Global.timeCount;
import static com.corpagos.corpunto.utils.SharedPreferencesHandler.saveUseTime;

public class TimerTask {

    private static TimerTask instance;
    private Thread timerThread;
    private static boolean threadStatus = true;


    private TimerTask(Context context) {
        timerThread = new Thread(() -> {
            while (threadStatus) {
                try {
                    timeCount++;
                    System.out.println("valores timer : " + timeCount);
                    saveUseTime(context, Global.keyTimeCount, timeCount);
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    return;
                }
            }
        });
    }

    public static TimerTask getInstance(Context context) {
        if (instance == null) {
            instance = new TimerTask(context);
        }
        return instance;
    }

    public void startThread() {
        timerThread.start();
    }

    public void setThreadStatus(boolean value) {
        threadStatus = value;
    }

    public boolean isThreadAlive() {
        return timerThread.isAlive();
    }
}
