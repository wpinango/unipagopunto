package com.corpagos.corpunto.singletons;

import android.annotation.SuppressLint;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.corpagos.corpunto.Asynctask;
import com.corpagos.corpunto.activities.NotificationActivity;
import com.corpagos.corpunto.app.AppCredentials;
import com.corpagos.corpunto.databases.ClosureDBHelper;
import com.corpagos.corpunto.databases.TransactionDBHelper;
import com.corpagos.corpunto.interfaces.AsynctaskListener;
import com.corpagos.corpunto.models.DailyClosure;
import com.corpagos.corpunto.models.Notification;
import com.corpagos.corpunto.models.Response;
import com.corpagos.corpunto.models.Token;
import com.corpagos.corpunto.models.Transaction;
import com.corpagos.corpunto.models.TransactionType;
import com.corpagos.corpunto.services.JobLocationService;
import com.corpagos.corpunto.services.MqttHelper;
import com.corpagos.corpunto.utils.SharedPreferencesHandler;
import com.corpagos.corpunto.utils.Time;
import com.google.gson.Gson;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.util.HashMap;
import java.util.Map;

import static com.corpagos.corpunto.Global.software;
import static com.corpagos.corpunto.activities.MainLauncherActivity.version;
import static com.corpagos.corpunto.services.MyFirebaseMessagingService.ACTION_INTENT;


public class NotificationHandler implements AsynctaskListener {

    private static NotificationHandler instance;
    private MqttHelper mqttHelper;
    private Context context;

    public static NotificationHandler getInstance(Context context) {
        if (instance == null) {
            instance = new NotificationHandler(context);
        }
        return instance;
    }

    private NotificationHandler(Context context) {
        startMqtt(context);
        this.context = context;
    }

    private void sendNotificationToPrincipalActivity(String value) {
        Intent intent = new Intent(ACTION_INTENT);
        intent.putExtra("UI_KEY", value);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    private void startMqtt(Context context) {
        mqttHelper = new MqttHelper(context);
        mqttHelper.setCallback(new MqttCallbackExtended() {
            @Override
            public void connectComplete(boolean b, String s) {

            }

            @Override
            public void connectionLost(Throwable throwable) {

            }

            @SuppressLint("MissingPermission")
            @Override
            public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {
                System.out.println("valores notification : " + mqttMessage.toString() + " " + topic);
                try {
                    TelephonyManager telephonyManager = (TelephonyManager)
                            context.getSystemService(Context.TELEPHONY_SERVICE);
                    Notification notification = new Gson().fromJson(mqttMessage.toString(),
                            Notification.class);
                    if (topic.equals(software)) {
                        if (notification.getData().getType().equals("Update")) {
                            NotificationActivity.isUpdateNotification = true;
                            NotificationActivity.isNotification = true;
                            notification.getData().setTime((Time.getTimestamp()));
                            SharedPreferencesHandler.saveData(context, SharedPreferencesHandler.KEY_UPDATE,
                                    new Gson().toJson(notification.getData()),
                                    SharedPreferencesHandler.KEY_UPDATE_FILE);
                            sendNotificationToPrincipalActivity(new Gson().toJson(notification.getData()));
                        }
                    } else if (topic.equals(telephonyManager.getDeviceId())) {
                        if (notification.getData().getType().equals("PosClosing")) {
                            if (AppCredentials.isLogin(context)) {
                                System.out.println("valores " + notification.getData().getData());
                                Token tokenObject = AppCredentials.getToken(context);
                                Map<String, String> headers = new HashMap<>();
                                headers.put(Asynctask.authorization, tokenObject.getToken_type()
                                        + " " + tokenObject.getAccess_token());
                                new Asynctask.GetMethodAsynctask(Asynctask.transactionalHost,
                                        Asynctask.URL_GET_LAST_CLOSURE, headers,
                                        NotificationHandler.this).execute();
                            }
                        } else if (notification.getData().getType().equals("GeolocationRequest")) {
                            JobScheduler jobScheduler = (JobScheduler) context
                                    .getSystemService(Context.JOB_SCHEDULER_SERVICE);
                            jobScheduler.schedule(new JobInfo.Builder(1,
                                    new ComponentName(context, JobLocationService.class))
                                    .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                                    .setMinimumLatency(5000)
                                    .setOverrideDeadline(5000)
                                    .build());
                        }
                    }
                } catch (Exception e) {
                    e.getMessage();
                    System.out.println("valores : " + e.getMessage());
                }
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

            }
        });
    }

    public void disconnectMqttClient() {
        try {
            mqttHelper.mqttAndroidClient.disconnect();
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    public void reconnectMqttClient() {

    }

    public void sendMessageMqtt(String topic, String message) {
        MqttMessage mqttMessage = new MqttMessage();
        //mqttMessage.; //TODO queda por hacer message to MqttMessage
        try {
            mqttHelper.mqttAndroidClient.publish(topic, mqttMessage);
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onAsynctaskFinished(String endPoint, String response, String headers) {
        if (!response.equals("")) {
            Response res = new Gson().fromJson(response, Response.class);
            if (!res.isError() && res.getMessage().equals(Asynctask.OK_RESPONSE)) {
                if (endPoint.equals(Asynctask.URL_GET_LAST_CLOSURE)) {
                    DailyClosure dailyClosure = new Gson().fromJson(new Gson().toJson(res.getData()),
                            DailyClosure.class);
                    Transaction transaction = new Transaction();
                    transaction.setTransactionType(TransactionType.CLOSURE);
                    transaction.setAmount(dailyClosure.getSummary().getTotal_amount());
                    transaction.setApp_version(version);
                    transaction.setCreated_at(dailyClosure.getSummary().getClose_date());
                    transaction.setDate(Time.getCurrentDate());
                    transaction.setSelected(false);
                    transaction.setLot(dailyClosure.getSummary().getLot_number());
                    transaction.setReference("1041551637202");
                    transaction.setReverse(1);
                    transaction.setStatus("ACCEPTED");
                    transaction.setCard_type("NOT_APPLY");
                    TransactionDBHelper dbHelper = new TransactionDBHelper(context);
                    dbHelper.updateClosureStatus();
                    dbHelper.insertTransaction(transaction);
                    ClosureDBHelper closureDBHelper = new ClosureDBHelper(context);
                    closureDBHelper.insertClosure(dailyClosure);
                    updateClosureView("value");
                }
            }
        }
    }

    private void updateClosureView(String value) {
        Intent intent = new Intent("transaction.closure.UI_UPDATE");
        intent.putExtra("UI_KEY2", value);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }
}
