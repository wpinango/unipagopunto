package com.corpagos.corpunto.singletons;

import android.content.Context;

import com.corpagos.corpunto.enums.PaymentOptionEnum;

public class PaymentOption {
    private static PaymentOption instance;
    private PaymentOptionEnum paymentOptionEnum;

    private PaymentOption(Context context) {
        setDefaultPaymentOption();
    }

    public static PaymentOption getInstance(Context context) {
        if (instance == null) {
            instance = new PaymentOption(context);
        }
        return instance;
    }

    public PaymentOptionEnum getPaymentOptionEnum() {
        return paymentOptionEnum;
    }

    public void setPaymentOptionEnum(PaymentOptionEnum paymentOptionEnum) {
        this.paymentOptionEnum = paymentOptionEnum;
    }

    public void setDefaultPaymentOption() {
        paymentOptionEnum = PaymentOptionEnum.CARD;
    }

    public void setPaymentOption(PaymentOptionEnum paymentOptionEnum) {
        this.paymentOptionEnum = paymentOptionEnum;
    }
}
