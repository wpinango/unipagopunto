package com.corpagos.corpunto.singletons;

import android.content.Context;

import com.corpagos.corpunto.enums.RoleType;

public class Role {

    private static Role instance;
    private RoleType roleType;

    private Role(Context context) {
        roleType = RoleType.USER;
    }

    public static Role getInstance(Context context) {
        if (instance == null) {
            instance = new Role(context);
        }
        return instance;
    }


    public void setRoleType(RoleType roleType) {
        this.roleType = roleType;
    }

    public RoleType getRoleType() {
        return this.roleType;
    }
}
