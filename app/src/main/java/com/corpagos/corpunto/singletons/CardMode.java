package com.corpagos.corpunto.singletons;

import android.content.Context;

import com.corpagos.corpunto.enums.CardAccountType;
import com.corpagos.corpunto.enums.CardTypeEnum;
import com.corpagos.corpunto.models.Card;

public class CardMode {

    private static CardMode instance;
    private Card card;

    private CardMode(Context context) {
        card = new Card();
    }

    public static CardMode getInstance(Context context) {
        if (instance == null) {
            instance = new CardMode(context);
        }
        return instance;
    }

    public void setPosMode(Card card) {
        this.card = card;
    }

    public Card getCard() {
        return this.card;
    }

    public void setCardAccountType(CardAccountType cardAccountType) {
        this.card.setCardAccountType(cardAccountType);
    }

    public void setCardType(CardTypeEnum cardTypeEnum) {
        this.card.setCardTypeEnum(cardTypeEnum);
    }

    public void setCardModeDefault() {
        this.card.setCardTypeEnum(CardTypeEnum.UNDEFINED);
        this.card.setCardAccountType(CardAccountType.UNDEFINED);
    }

}
