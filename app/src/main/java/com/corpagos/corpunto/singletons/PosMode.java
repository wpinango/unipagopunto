package com.corpagos.corpunto.singletons;


import android.content.Context;

import com.corpagos.corpunto.enums.PosModeType;

public class PosMode {

    private static PosMode instance;
    private PosModeType posModeType;

    private PosMode(Context context) {
        posModeType = PosModeType.SELL;
    }

    /*private synchronized static void createInstance(Context context){
        if (instance == null) {
            instance = new PosMode(context);
        }
    }*/

    public static PosMode getInstance(Context context) {
        if (instance == null) {
            instance = new PosMode(context);
        }
        return instance;
    }

    public void setPosMode(PosModeType posModeType) {
        this.posModeType = posModeType;
    }

    public PosModeType getPosMode() {
        return this.posModeType;
    }

    public void setDeFaultMode() {
        this.posModeType = PosModeType.SELL;
    }

}
