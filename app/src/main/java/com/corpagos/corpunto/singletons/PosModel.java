package com.corpagos.corpunto.singletons;

import android.content.Context;

import com.corpagos.corpunto.enums.PosModelType;
import com.corpagos.corpunto.models.PosInformation;

public class PosModel {
    private static PosModel instance;
    private PosModelType posModelType;
    private PosInformation posInformation;

    private PosModel(Context context) {

    }

    public static PosModel getInstance(Context context) {
        if (instance == null) {
            instance = new PosModel(context);
        }
        return instance;
    }

    public PosModelType getPosModelType() {
        return posModelType;
    }

    public void setPosModelType(PosModelType posModelType) {
        this.posModelType = posModelType;
    }

    public PosInformation getPosInformation() {
        return posInformation;
    }

    public void setPosInformation(PosInformation posInformation) {
        this.posInformation = posInformation;
    }
}
