package com.corpagos.corpunto.pos;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import android.text.Layout;
import android.util.Log;

import com.corpagos.corpunto.Global;
import com.corpagos.corpunto.dialogs.Dialog;
import com.corpagos.corpunto.emv.EMV;
import com.corpagos.corpunto.emv.EMVManager;
import com.corpagos.corpunto.emv.TradeInfo;
import com.corpagos.corpunto.enums.PosModelType;
import com.corpagos.corpunto.interfaces.CardInterface;
import com.corpagos.corpunto.interfaces.OnPinPadListener;
import com.corpagos.corpunto.interfaces.ReadCardListener;
import com.corpagos.corpunto.logs.CustomizedExceptionHandler;
import com.corpagos.corpunto.models.CardData;
import com.corpagos.corpunto.models.PosInformation;
import com.corpagos.corpunto.models.Transaction;
import com.corpagos.corpunto.singletons.PosModel;
import com.corpagos.corpunto.utils.ByteUtil;
import com.corpagos.corpunto.utils.ConsoleMessageManager;
import com.corpagos.corpunto.utils.FileUtils;
import com.corpagos.corpunto.utils.MoneyUtil;
import com.google.gson.Gson;
import com.imagpay.Settings;
import com.imagpay.SwipeEvent;
import com.imagpay.SwipeListener;
import com.imagpay.emv.EMVHandler;
import com.imagpay.emv.EMVListener;
import com.imagpay.emv.EMVResponse;
import com.imagpay.enums.CardDetected;
import com.imagpay.enums.EmvStatus;
import com.imagpay.enums.PrintStatus;
import com.imagpay.mpos.MposHandler;
import com.zcs.sdk.DriverManager;
import com.zcs.sdk.Led;
import com.zcs.sdk.LedLightModeEnum;
import com.zcs.sdk.SdkResult;
import com.zcs.sdk.Sys;
import com.zcs.sdk.card.CardInfoEntity;
import com.zcs.sdk.card.CardReaderManager;
import com.zcs.sdk.card.CardReaderTypeEnum;
import com.zcs.sdk.card.CardSlotNoEnum;
import com.zcs.sdk.card.ICCard;
import com.zcs.sdk.card.MagCard;
import com.zcs.sdk.card.RfCard;
import com.zcs.sdk.emv.EmvData;
import com.zcs.sdk.emv.EmvHandler;
import com.zcs.sdk.emv.EmvResult;
import com.zcs.sdk.emv.EmvTermParam;
import com.zcs.sdk.emv.EmvTransParam;
import com.zcs.sdk.emv.OnEmvListener;
import com.zcs.sdk.listener.OnSearchCardListener;
import com.zcs.sdk.pin.pinpad.PinPadManager;
import com.zcs.sdk.print.PrnStrFormat;
import com.zcs.sdk.print.PrnTextStyle;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import sdk4.wangpos.libemvbinder.EmvCore;
import wangpos.sdk4.emv.ICallbackListener;
import wangpos.sdk4.libbasebinder.BankCard;
import wangpos.sdk4.libbasebinder.Core;
import wangpos.sdk4.libbasebinder.Printer;

import static com.corpagos.corpunto.Global.printCount;
import static com.corpagos.corpunto.Global.printLinesCount;
import static com.corpagos.corpunto.activities.MyApp.sDriverManager;
import static com.corpagos.corpunto.utils.SharedPreferencesHandler.saveCount;
import static com.zcs.sdk.util.StringUtils.*;
import static org.apache.commons.lang3.StringUtils.isEmpty;

public class PosDevice implements SwipeListener, EMVListener, CardInterface {

    private PosModel posModel;
    private Context context;
    private EMV emv;
    private MposHandler handler;
    private Settings settings;
    private Core core;
    private EmvCore emvCore;
    private BankCard mBankCard;
    private String TAG = "valores";
    private Transaction transaction;
    private boolean isOffLine = false;
    private String strTxt = "";
    private ReadCardListener listener;
    private Activity parent;
    private Thread cardThread;
    private boolean threadStatus;
    private boolean isTransaction = false, flag;
    private Printer mPrinter;
    private com.zcs.sdk.Printer printer;
    private boolean bloop = false, isCard;
    private boolean bthreadrunning = false;
    private CardData cardData;
    private Sys mBaseSysDevice;
    private DriverManager mDriverManager = sDriverManager;
    private EmvHandler emvHandler;
    private int iRet;
    private byte[] mPinBlock = new byte[12 + 1];
    private ICCard mICCard;
    private MagCard mMAGCard;
    private RfCard mRFCard;
    private CardReaderTypeEnum realCardType;
    private CardReaderManager mCardReadManager;
    private PinPadManager mPinPadManager;
    private Led led = mDriverManager.getLedDriver();
    int[] tags = {
            0x9F26,
            0x9F27,
            0x9F10,
            0x9F37,
            0x9F36,
            0x95,
            0x9A,
            0x9C,
            0x9F02,
            0x5F2A,
            0x82,
            0x9F1A,
            0x9F03,
            0x9F33,
            0x9F34,
            0x9F35,
            0x9F1E,
            0x84,
            0x9F09,
            0x9F41,
            0x9F63,
            0x5F24
    };

    public void setCard(boolean isCard) {
        this.isCard = isCard;
    }

    private final String miniModel = "WPOS-MINI", wpos3Model = "WPOS-3", z90Model = "SP7731CEA",
            newZ90 = "Z90";

    public void setTransaction(boolean isTransaction) {
        this.isTransaction = isTransaction;
    }

    public boolean isTransaction() {
        return isTransaction;
    }

    public boolean isCard() {
        return isCard;
    }

    public PosDevice(Context context) {
        this.context = context;
        transaction = new Transaction();
        Thread.setDefaultUncaughtExceptionHandler(new CustomizedExceptionHandler());
        PosInformation posInformation = new PosInformation();
        posModel = PosModel.getInstance(context);
        switch (android.os.Build.MODEL) {
            case wpos3Model:
                posInformation.setPosFamily("WPOS");
                posInformation.setPosSerial(Build.SERIAL);
                posInformation.setPosModel(Build.DISPLAY);
                posModel.setPosInformation(posInformation);
                posModel.setPosModelType(PosModelType.WPOS_3);
                break;
            case miniModel:
                posInformation.setPosFamily("WPOS");
                posInformation.setPosSerial(Build.SERIAL);
                posInformation.setPosModel(Build.DISPLAY);
                posModel.setPosInformation(posInformation);
                posModel.setPosModelType(PosModelType.WPOS_MINI);
                break;
            case z90Model:
                posInformation.setPosFamily("SMART_POS");
                posInformation.setPosModel("Z90");
                posModel.setPosInformation(posInformation);
                posModel.setPosModelType(PosModelType.Z90);
                handler = MposHandler.getInstance(context, com.imagpay.enums.PosModel.Z90);
                settings = Settings.getInstance(handler);
                handler.setShowLog(true);
                handler.setShowAPDU(true);
                settings.mPosPowerOn();
                try {
                    if (!handler.isConnected()) {
                        ConsoleMessageManager.consoleConnectionStatusMsg(handler.connect());
                    } else {
                        handler.close();
                        ConsoleMessageManager.consoleConnectionStatusMsg(handler.connect());
                    }
                } catch (Exception e) {
                    e.getMessage();
                }
                if (settings.setReadSN() == null) {
                    new Thread(() -> {
                        do {
                            posInformation.setPosSerial(settings.setReadSN());
                            try {
                                Thread.sleep(100);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        } while (posInformation.getPosSerial() == null);
                    }).start();
                }
                try {
                    Thread.sleep(800);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                handler.setNfcThread(false);
                handler.onDestroy();// Close TTL
                settings.mPosPowerOff();
                settings.onDestroy();
                break;
            case newZ90:
                mBaseSysDevice = mDriverManager.getBaseSysDevice();
                posInformation.setPosFamily("SMART_POS");
                posInformation.setPosModel("Z90");
                posModel.setPosInformation(posInformation);
                posModel.setPosModelType(PosModelType.Z90);
                new Thread(() -> {
                    String[] mTerminalSN = new String[1];
                    int resPid = mBaseSysDevice.getPid(mTerminalSN);
                    if (resPid == SdkResult.SDK_OK) {
                        posInformation.setPosSerial(mTerminalSN[0]);
                    }
                }).start();
                break;
            default:
                Dialog dialog = new Dialog(context);
                dialog.showInfoDialog("Dispositivo no especificado");
                break;
        }
    }

    public void cancelSearchCard() {
        switch (android.os.Build.MODEL) {
            case newZ90:
                mCardReadManager.cancelSearchCard();
                break;
        }
    }

    public PosDevice(Context context, ReadCardListener listener, Activity activity, boolean readCard) {
        this.context = context;
        this.listener = listener;
        this.parent = activity;
        this.flag = !readCard;
        transaction = new Transaction();
        Thread.setDefaultUncaughtExceptionHandler(new CustomizedExceptionHandler());
        PosInformation posInformation = new PosInformation();
        posModel = PosModel.getInstance(context);
        switch (android.os.Build.MODEL) {
            case wpos3Model:
                posInformation.setPosFamily("WPOS");
                posInformation.setPosSerial(Build.SERIAL);
                posInformation.setPosModel(Build.DISPLAY);
                posModel.setPosInformation(posInformation);
                posModel.setPosModelType(PosModelType.WPOS_3);
                break;
            case miniModel:
                posInformation.setPosFamily("WPOS");
                posInformation.setPosSerial(Build.SERIAL);
                posInformation.setPosModel(Build.DISPLAY);
                posModel.setPosInformation(posInformation);
                posModel.setPosModelType(PosModelType.WPOS_MINI);
                break;
            case z90Model:
                handler = MposHandler.getInstance(context, com.imagpay.enums.PosModel.Z90);
                settings = Settings.getInstance(handler);
                handler.setShowLog(true);
                handler.setShowAPDU(true);
                settings.mPosPowerOn();
                try {
                    if (!handler.isConnected()) {
                        ConsoleMessageManager.consoleConnectionStatusMsg(handler.connect());
                    } else {
                        handler.close();
                        ConsoleMessageManager.consoleConnectionStatusMsg(handler.connect());
                    }
                } catch (Exception e) {
                    e.getMessage();
                }
                if (settings.setReadSN() == null) {
                    new Thread(() -> {
                        do {
                            posInformation.setPosSerial(settings.setReadSN());
                            try {
                                Thread.sleep(100);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        } while (posInformation.getPosModel() == null);
                    }).start();
                }
                handler.setNfcThread(false);
                handler.onDestroy();// Close TTL
                settings.mPosPowerOff();
                settings.onDestroy();
                posInformation.setPosFamily("SMART_POS");
                posInformation.setPosModel("Z90");
                posModel.setPosInformation(posInformation);
                posModel.setPosModelType(PosModelType.Z90);
                break;
            case newZ90:
                new Thread(() -> {
                    String[] mTerminalSN = new String[1];
                    int resPid = mBaseSysDevice.getPid(mTerminalSN);
                    if (resPid == SdkResult.SDK_OK) {
                        posInformation.setPosSerial(mTerminalSN[0]);
                    }
                }).start();
                mBaseSysDevice = mDriverManager.getBaseSysDevice();
                posInformation.setPosFamily("SMART_POS");
                posInformation.setPosModel("Z90");
                posModel.setPosInformation(posInformation);
                posModel.setPosModelType(PosModelType.Z90);
                mCardReadManager = mDriverManager.getCardReadManager();
                mICCard = mCardReadManager.getICCard();
                mMAGCard = mCardReadManager.getMAGCard();
                mRFCard = mCardReadManager.getRFCard();
                mPinPadManager = mDriverManager.getPadManager();
                emvHandler = EmvHandler.getInstance();
                led = mDriverManager.getLedDriver();
                //if (readCard) {
                //    searchCard(CardReaderTypeEnum.IC_CARD);
                //}
                break;
        }
    }

    public boolean isDetect() {
        boolean isDetect = false;
        switch (android.os.Build.MODEL) {
            case wpos3Model:
                break;
            case miniModel:
                break;
            case z90Model:
                if (settings.icDetect().equals("00")) {
                    isDetect = true;
                }
                break;
            case newZ90:
                if (mCardReadManager.getICCard().icCardReset(CardSlotNoEnum.SDK_ICC_USERCARD) == 0) {
                    isDetect = true;
                }
                break;
        }
        return isDetect;
    }

    public void readCardAgain() {
        switch (android.os.Build.MODEL) {
            case wpos3Model:
                break;
            case miniModel:
                break;
            case z90Model:
                break;
            case newZ90:
                if (mCardReadManager.getICCard().icCardReset(CardSlotNoEnum.SDK_ICC_USERCARD) == 0) {
                    isCard = false;
                }
                break;
        }
    }

    private Handler handler1 = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 0:
                    System.out.println("valores 125 : " + strTxt);
                    break;
                case 1:
                    System.out.println("valores 21 : " + TradeInfo.getInstance().getId());
                    break;
                case 2:
                    System.out.println("valores 34 : " + "get card information success,start trading ,pls connect your services ...");
                    break;
            }
        }
    };


    public void initReadingPeripherals() {
        switch (android.os.Build.MODEL) {
            case wpos3Model:
                new Thread(() -> {
                    emvCore = new EmvCore(context);
                    core = new Core(context);
                    mBankCard = new BankCard(context);
                    initCardDetectionThread();
                    setCardDetectionThread(true);
                }).start();
                break;
            case miniModel:
                new Thread(() -> {
                    emvCore = new EmvCore(context);
                    core = new Core(context);
                    mBankCard = new BankCard(context);
                    initCardDetectionThread();
                    setCardDetectionThread(true);
                }).start();
                break;
            case z90Model:
                handler = MposHandler.getInstance(context, com.imagpay.enums.PosModel.Z90);
                settings = Settings.getInstance(handler);
                handler.setShowLog(true);
                handler.setShowAPDU(true);
                settings.mPosPowerOn();
                emv = new EMV();
                try {
                    if (!handler.isConnected()) {
                        ConsoleMessageManager.consoleConnectionStatusMsg(handler.connect());
                    } else {
                        handler.close();
                        ConsoleMessageManager.consoleConnectionStatusMsg(handler.connect());
                    }
                } catch (Exception e) {
                    e.getMessage();
                }
                handler.addSwipeListener(this);
                handler.addEMVListener(this);
                //nfc();
                break;
            case newZ90:
                searchCard(CardReaderTypeEnum.IC_CARD);
                /*new Thread(() -> {
                    try {
                        Thread.sleep(3000);
                        initCardDetectionThread();
                        setCardDetectionThread(true);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }).start();*/
                break;
        }
    }

    public void turnOffReadingPeripherals() {
        switch (android.os.Build.MODEL) {
            case wpos3Model:
                try {
                    cardThread = null;
                    if (mBankCard != null) {
                        mBankCard.breakOffCommand();
                    }
                    setCardDetectionThread(false);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
                break;
            case miniModel:
                try {
                    cardThread = null;
                    if (mBankCard != null) {
                        mBankCard.breakOffCommand();
                    }
                    setCardDetectionThread(false);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
                break;
            case z90Model:
                handler.setNfcThread(false);
                handler.onDestroy();
                settings.mPosPowerOff();
                settings.onDestroy();
                break;
            case newZ90:
                setCardDetectionThread(false);
                controlGreenLed(false);
                break;
        }
    }

    public void controlGreenLed(boolean status) {
        led.setLed(LedLightModeEnum.GREEN, status);
    }

    @Override
    public void onCardReading(String json) {
        EMVHandler.AppBean appBean = new Gson().fromJson(json, EMVHandler.AppBean.class);
        listener.onCardRead(appBean.getAID().toLowerCase());
    }

    @Override
    public void onDisconnected(SwipeEvent paramSwipeEvent) {

    }

    @Override
    public void onConnected(SwipeEvent paramSwipeEvent) {

    }

    @Override
    public void onStarted(SwipeEvent paramSwipeEvent) {

    }

    @Override
    public void onStopped(SwipeEvent paramSwipeEvent) {

    }

    @Override
    public void onReadData(SwipeEvent paramSwipeEvent) {

    }

    @Override
    public void onParseData(SwipeEvent paramSwipeEvent) {

    }

    @Override
    public void onPermission(SwipeEvent paramSwipeEvent) {

    }

    @Override
    public void onCardDetect(CardDetected cardDetected) {
        if (cardDetected == CardDetected.INSERTED) {
            isCard = true;
            parent.runOnUiThread(() -> listener.onViewEnabled());
            iCCardTest();
        }
        if (cardDetected == CardDetected.REMOVED) {
            chipError();
        }
    }

    @Override
    public void onPrintStatus(PrintStatus paramPrintStatus) {
        listener.onPrintStatus(paramPrintStatus);
    }

    @Override
    public void onEmvStatus(EmvStatus paramEmvStatus) {

    }

    @Override
    public int onSelectApp(List<String> paramList) {
        return 0;
    }

    @Override
    public boolean onReadData() {
        return false;
    }

    @Override
    public String onReadPin(int paramInt1, int paramInt2) {
        return null;
    }

    @Override
    public EMVResponse onSubmitData() {
        return null;
    }

    @Override
    public void onConfirmData() {

    }

    @Override
    public void onReversalData() {

    }

    private void initCardDetectionThread() {
        cardThread = null;
        cardThread = new Thread(() -> {
            while (threadStatus) {
                try {
                switch (posModel.getPosModelType()) {
                    /*case Z90:
                        int icCardStatus = mCardReadManager.getICCard().icCardReset(CardSlotNoEnum.SDK_ICC_USERCARD);
                        System.out.println("tarjeta : " + icCardStatus + " " + isCard);
                        if (icCardStatus == 0 && !isCard) {
                            setCardDetectionThread(false);
                            searchCard(CardReaderTypeEnum.IC_CARD);
                            break;
                        } else if (icCardStatus == -1201 && isCard) {
                            cardRemoved();
                            break;
                        }
                        break;*/
                        case WPOS_3:
                        case WPOS_MINI:
                            if (mBankCard.iccDetect() == 1 && !isCard) {
                                doTrade(100L);
                            } else if (mBankCard.iccDetect() == 0 && isCard) {
                                cardRemoved();
                            }
                            break;
                }
                //try {
                    Thread.sleep(600);
                /*} catch (InterruptedException e) {
                    e.printStackTrace();
                }*/
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    return;
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void cardRemoved() {
        isCard = false;
        if (isTransaction) {
            listener.onError("Transaccion Cancelada\n\t\t\n\t\t\t\t\t\t\npor remover tarjera");
            isTransaction = false;
        }
        listener.onCardDetect(CardDetected.REMOVED, null, null);
        controlGreenLed(false);
    }

    private void chipError() {
        isCard = false;
        if (isTransaction) {
            listener.onChipError();
            isTransaction = false;
        }
        listener.onCardDetect(CardDetected.REMOVED, null, null);
    }

    private void setCardDetectionThread(boolean status) {
        threadStatus = status;
        if (status && !cardThread.isAlive()) {
            cardThread.start();
        } else {
            if (cardThread != null) {
                threadStatus = false;
                cardThread.interrupt();
                cardThread = null;
            }
        }
    }

    private void doTrade(Long amount) {
        strTxt = "waiting read card...";
        handler1.sendEmptyMessage(0);


        TradeInfo.getInstance().setTradeType(TradeInfo.Type_Sale);
        TradeInfo.getInstance().setMerchantNo("123456789012");
        TradeInfo.getInstance().setTerminalNo("12345678");
        TradeInfo.getInstance().setSerialNo(000001);
        TradeInfo.getInstance().setAmount(amount);
        TradeInfo.getInstance().setOnLine(false);//默认是联机交易

        try {
            mBankCard.breakOffCommand();//结束上一笔交易的命令(Order to close the previous transaction)
            byte[] outData = new byte[512];
            int[] outDataLen = new int[1];
            int result = -1;
            do {
                result = mBankCard.readCard(BankCard.CARD_TYPE_NORMAL,
                        BankCard.CARD_MODE_PICC | BankCard.CARD_MODE_ICC | BankCard.CARD_MODE_MAG,
                        0x60, outData, outDataLen, "app1");
            } while (result != 0);
            if (result == 0) {
                Log.d("outData", ByteUtil.bytes2HexString(outData));
                switch (outData[0]) {
                    case 0x00://磁条卡成功(read card success,type MAG)
                        int len1 = outData[1];
                        int len2 = outData[2];
                        int len3 = outData[3];
                        Log.i(TAG, "run: read card len1==" + len1 + "len2==" + len2 + "len3==" + len3);
                        byte[] data1 = Arrays.copyOfRange(outData, 4, len1 + 4);
                        Log.i(TAG, "run: card data1==" + ByteUtil.bytes2HexString(data1));
                        byte[] data2 = Arrays.copyOfRange(outData, 4 + len1, len2 + len1 + 4);
                        Log.i(TAG, "run: card data2==" + ByteUtil.bytes2HexString(data2));
                        byte[] data3 = Arrays.copyOfRange(outData, 4 + len1 + len2, len3 + len2 + 4 + len1);
                        Log.i(TAG, "run: card data3==" + ByteUtil.bytes2HexString(data3));
                        if (len2 > 0) {
                            TradeInfo.getInstance().setMagneticCardData2(new String(data2));
                            String serviceCode = ByteUtil.bytes2HexString(data2).split("D")[1];
                            if (isEmpty(TradeInfo.getInstance().getId())) {
                                TradeInfo.getInstance().setId(new String(data2).split("=")[0]);
                                handler1.sendEmptyMessage(1);
                            }
                            Log.i(TAG, "run: serviceCode" + serviceCode + "--" + new
                                    String(Arrays.copyOfRange(ByteUtil.hexString2Bytes(serviceCode), 4, 6)));
                            String expireDate = new String(Arrays.copyOfRange(ByteUtil.hexString2Bytes(serviceCode), 0, 4));
                            Log.i(TAG, "run: expireDate==" + expireDate.replace("=", "D"));
                            TradeInfo.getInstance().setValidityPeriod(expireDate);
                            serviceCode = new String(Arrays.copyOfRange(ByteUtil.hexString2Bytes(serviceCode), 4, 6));
                            if (serviceCode.startsWith("2") || serviceCode.startsWith("6")) {
                                //IC卡不支持降级操作
                                strTxt = "The IC card does not support degraded operation";
                                handler1.sendEmptyMessage(0);
                                return;
                            }
                        }
                        if (len3 > 0) {
                            TradeInfo.getInstance().setMagneticCardData3(new String(data3));
                        }
                        if (len2 > 0 || len3 > 0) {
                            readCardInfo("02");
                        } else {
                            strTxt = "The card operate fail";
                            handler1.sendEmptyMessage(0);
                        }
                        break;
                    case 0x01:
                        //读卡失败(read card failed)
                        break;
                    case 0x02:
                        //刷磁条卡成功，加密处理失败(read card success,but encryption processing failed)
                        break;
                    case 0x03:
                        //刷卡超时(read card timeout)
                        break;
                    case 0x04:
                        //取消读卡(cancel read card)
                        break;
                    case 0x05:
                        //IC成功(read card success,type ICC)
                        parent.runOnUiThread(() -> listener.onViewEnabled());
                        core.ledFlash(0, 0, 1, 0, 20, 20, 3000);
                        readCardInfo("05");
                        break;
                    case 0x07:
                        //PICC成功(read card success,type PICC)
                        readCardInfo("07");
                        break;
                }
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private void readCardInfo(String s) {
        isOffLine = false;
        strTxt = "read card success ..." + s;
        handler1.sendEmptyMessage(0);
        TradeInfo.getInstance().setPosInputType(s);
        EMVManager.setEMVManager(context, handler1, emvCore);
        switch (s) {
            case "02":
                activeShowPinPad("02");
                break;
            case "05":
                try {
                    strTxt = "EMV-Process waiting…";
                    handler1.sendEmptyMessage(0);
                    int result = EMVManager.PBOC_Simple(iCallBackListener);
                    if (result != TradeInfo.SUCCESS) {
                        strTxt = "EMV-Process fail==" + result;
                        handler1.sendEmptyMessage(0);
                        listener.onError("Error de lectura");
                    } else {
                        int transType = TradeInfo.getInstance().getTradeType();
                        Log.v(TAG, "PBOC_Simple。transType==" + transType);
                        if (transType == TradeInfo.Type_QueryBalance
                                || transType == TradeInfo.Type_Sale
                                || transType == TradeInfo.Type_Auth
                                || transType == TradeInfo.Type_CoilingSale) {//EMV内核弹密
                            // TODO: 2017/10/11 阻塞方法，返回交易结果
                            int transResult = EMVManager.EMV_TransProcess(iCallBackListener);
                            Log.d(TAG, "checkResult==" + transResult);
                            if (transResult == 0) {
                                strTxt = "finish";
                                handler1.sendEmptyMessage(0);
                                transaction = new Transaction();
                                transaction.setCardExpiration(TradeInfo.getInstance().validityPeriod);
                                transaction.setCardNumber(TradeInfo.getInstance().getId());
                                transaction.setDeviceSerial(Build.SERIAL);
                                transaction.setCardHolder("Test");
                                CardData cardData = new CardData();
                                cardData.setAID(ByteUtil.bytes2HexString(TradeInfo.getInstance().getAid())
                                        .substring(0, 14));
                                System.out.println("valores : " + new Gson().toJson(TradeInfo.getInstance()));
                                cardData.setAC(ByteUtil.bytes2HexString(TradeInfo.getInstance().getAppCrypt()));
                                cardData.setNA(ByteUtil.bytes2HexString(TradeInfo.getInstance().getAucUnPredNum()));
                                cardData.setTSI(ByteUtil.bytes2HexString(TradeInfo.getInstance().getTSI()));
                                cardData.setTVR(ByteUtil.bytes2HexString(TradeInfo.getInstance().getTVR()));
                                cardData.setTrack2(TradeInfo.getInstance().magneticCardData2);
                                System.out.println("valores L: " + new Gson().toJson(cardData));
                                if (cardData.getAC().equals("0000000000000000")) {
                                    listener.onCardDetect(CardDetected.REMOVED, null, null);
                                } else {
                                    listener.onCardRead(ByteUtil.bytes2HexString(TradeInfo.getInstance().getAid())
                                            .substring(0, 14).toLowerCase());
                                    listener.onCardDetect(CardDetected.INSERTED, transaction, cardData);
                                    isTransaction = true;
                                }

                            } else {
                                if (transResult != -8) {
                                    strTxt = "EMV-Process fail";
                                    handler1.sendEmptyMessage(0);
                                    listener.onError("Error de lectura");
                                    listener.onCardDetect(CardDetected.REMOVED, null, null);
                                }
                            }
                        } else {
                            activeShowPinPad("07");
                        }
                    }
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
                break;
            case "07":
                try {
                    strTxt = "EMV-Process waiting…";
                    handler1.sendEmptyMessage(0);
                    int result = EMVManager.QPBOC_PreProcess(iCallBackListener);
                    if (result != TradeInfo.SUCCESS) {
                        Log.d(TAG, "QPBOC_PreProcess fail，result==" + result);
                    } else {
                        result = EMVManager.PBOC_Simple(iCallBackListener);
                        if (result == TradeInfo.SUCCESS) {
                            if (TradeInfo.getInstance().getKernel() == 1) {//payWave process
                                int transResult = EMVManager.EMV_TransProcess(iCallBackListener);
                                Log.d(TAG, "checkResult==" + transResult);
                                if (transResult == 0) {
                                    strTxt = "finish";
                                    handler1.sendEmptyMessage(0);
                                } else {
                                    if (transResult != -8) {
                                        strTxt = "EMV-Process fail";
                                        handler1.sendEmptyMessage(0);
                                        listener.onError("Error de lectura");
                                    }
                                }
                            } else {//qPBOC process
                                activeShowPinPad("07");
                            }
                        } else {
                            strTxt = "PBOC fail result==" + result;
                            handler1.sendEmptyMessage(0);
                            Log.d(TAG, "PBOC_Simple fail，result==" + result);
                        }
                    }
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    private void activeShowPinPad(final String tradeType) {
        parent.runOnUiThread(() -> {
            String cardNo = TradeInfo.getInstance().getId();
            KeyPadDialog.getInstance(core).showDialog((Activity) context, cardNo, new OnPinPadListener() {
                @Override
                public void onUpDate() {
                }

                @Override
                public void onCancel() {
                    strTxt = "User canceled";
                    handler1.sendEmptyMessage(0);
                }

                @Override
                public void onByPass() {
                    strTxt = "ByPass";
                    handler1.sendEmptyMessage(0);
                    if ("07".equals(tradeType)) {
                        try {
                            int result = EMVManager.EMV_TransProcess(iCallBackListener);
                            Log.d(TAG, "result==" + result);
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    } else {//MAG
                        handler1.sendEmptyMessage(2);
                    }
                }

                @Override
                public void onSuccess(String pin) {
                    Log.d(TAG, "Pin==" + pin);
                    if ("07".equals(tradeType)) {
                        try {
                            int result = EMVManager.EMV_TransProcess(iCallBackListener);
                            Log.d(TAG, "result==" + result);
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    } else {//MAG
                        handler1.sendEmptyMessage(2);
                    }
                }

                @Override
                public void onError(int errorCode, String errorMsg) {
                    Log.e(TAG, "errorCode==" + errorCode + "\nerrorMsg==" + errorMsg);
                }
            });
        });
    }

    private CountDownLatch countDownLatch = null;
    private ICallbackListener iCallBackListener = new ICallbackListener.Stub() {
        @Override
        public int emvCoreCallback(final int command, final byte[] data, final byte[] result, final int[] resultlen) throws RemoteException {
            countDownLatch = new CountDownLatch(1);
            Log.d(TAG, "emvCoreCallback。command==" + command);
            switch (command) {
                case 2818://Core.CALLBACK_PIN
                    Log.i("iCallbackListener", "Core.CALLBACK_PIN");
                    parent.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            String pin = "000A940F2DE827346B";//0412AC89ABCDEF67
                            countDownLatch.countDown();       //042506F6BBEFEFA8
                            Log.d(TAG, "Pin==" + pin);
                            System.out.println("valores : " + pin);
                            if (pin != null && "offLine".equals(pin)) {
                                isOffLine = true;
                                strTxt = "offLine success";
                                handler1.sendEmptyMessage(0);
                            }
                        }
                    });
                    break;
                case 2821:
                    Log.i("iCallbackListener", "Core.CALLBACK_ONLINE");
                    if (isOffLine) {
                        countDownLatch.countDown();
                    } else {
                        strTxt = "getCardInformation …";
                        handler1.sendEmptyMessage(0);


                        byte[] DataTag84 = new byte[20];
                        int[] DataTag84Len = new int[1];
                        emvCore.getTLV(0x84, DataTag84, DataTag84Len);


                        byte[] outTLVMessage = new byte[4096];
                        int[] dataLen = new int[1];
                        Arrays.fill(outTLVMessage, (byte) 0x00);
                        int result1 = emvCore.getCoreTLVMessage(outTLVMessage, dataLen);


                        // TODO: 2018\4\27 0027 shall link to Bank/Acquire Server
                        int ret = EMVManager.EMV_OnlineProc(result, resultlen, countDownLatch, handler1);

                        byte resData[] = ByteUtil.hexString2Bytes("0030300631363133303600");
                        System.arraycopy(resData, 0, result, 0, resData.length);
                        resultlen[0] = resData.length;
                        Log.i("iCallbackListener", "Core.CALLBACK_ONLINE, ret = " + ret);


                        // TODO: 2018\4\27 0027 after the msg exchange and take
                        // TODO: 2018\4\27 0027 TAG 8A(or ISO8583 field 39)  TAG 91 TAG71 72
                        // TODO: 2018\4\27 0027 then fill in the result arrary in the data format:
                        /* 00          —— Result for executing  0: Online accepted
                        3030           ——Authorization response code(ARC)
                        06             ——Length for Authorization ID Response(ISO8583 Field 38) if these field is 0x 00, the next field will not present
                        313631333036   —— Authorization ID Response(ISO8583 Field 38)
                        0A             ——Length for Authorization Data  if these field is 0x 00, the next field will not present
                        73653f8bca734d390014 ——Authorization Data
                        00             ——Length for Script. if these field is 0x 00, the next field will not present
                        ....
                        you can check the data format in 《WPOS SDK Development Reference Part II – EMV》  4.4Callback for Online Process.
                        */
//                        byte resData[] = ByteUtil.hexString2Bytes("003030063136313330360a73653f8bca734d39001400");
//                        System.arraycopy(resData,0, result,0,resData.length);
//                        Log.i("iCallbackListener", "Core.CALLBACK_ONLINE, ret = " + ret);


                    }

//                    emvCore.EmvLib_Init()


                    //交易完成后调用此方法释放等待，回传sp联机结果
                    handler1.sendEmptyMessage(2);
                    countDownLatch.countDown();
                    //交易完成后调用此方法释放等待，回传sp联机结果
//                    countDownLatch.countDown();
                    break;
                case 2823://Core.CALLBACK_PINRESULT
                    strTxt = "OffLine pin check success";
                    handler1.sendEmptyMessage(0);
                    countDownLatch.countDown();
                    break;
                case 2817://Core.CALLBACK_NOTIFY
//                    Log.i("iCallbackListener", "Core.CALLBACK_NOTIFY");
//                    byte[] appId = new byte[data.length-3];
//                    System.arraycopy(data,3,appId,0,appId.length);
//                    Log.i("iCallbackListener", "app select operation "+ByteUtil.bytes2HexString(appId));
//                    //app select operation
//                    //default select first
//                    result[0] = 1;
//                    resultlen[0] = 1;
//                    countDownLatch.countDown();
                    //app select operation
                    Log.i("iCallbackListener", "Core.CALLBACK_NOTIFY");
                    Log.e("emvCoreCallback threed", "start");
                    ((Activity) context).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
//                Looper.prepare();
                            String resultDate = ByteUtil.bytes2HexString(data);

                            String timeOut = resultDate.substring(0, 2);
                            String appNum = resultDate.substring(4, 6);
                            String appList = resultDate.substring(6);
                            String[] appData = appList.split("000A");


                            Log.e("CALLBACK_NOTIFY", "appdate" + Arrays.toString(appData));

                            int i = 0;
                            for (i = 0; i < appData.length; i++) {
                                appData[i] = ByteUtil.fromUtf8(ByteUtil.hexString2Bytes(appData[i]));
                            }

                            result[0] = (byte) 1;
                            resultlen[0] = 1;
                            countDownLatch.countDown();

//                            appData[0] = ByteUtil.fromUtf8(ByteUtil.hexString2Bytes(appData[0]));
//                            appData[1] = ByteUtil.fromUtf8(ByteUtil.hexString2Bytes(appData[1]));

                           /* new AlertDialog.Builder(context).setSingleChoiceItems(appData, 0, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    result[0] = (byte) which;
                                    resultlen[0] = 1;
                                    Log.e("which", "which" + which);
//                                    countDownLatch.countDown();
                                    countDownLatch.countDown();
                                    dialog.dismiss();
                                }
                            }).create().show();*/
                        }
                    });
                    break;
                case 2819://Core.CALLBACK_AMOUNT
                    long amount = TradeInfo.getInstance().getAmount();
                    String a = MoneyUtil.fen2yuan(amount);
                    Log.d(TAG, "amount==" + amount);
                    result[0] = 0;
                    Log.d(TAG, "int2Bytes==" + (int) MoneyUtil.yuan2fen(Double.parseDouble(a)));
                    byte[] tmp = ByteUtil.int2Bytes((int) MoneyUtil.yuan2fen(Double.parseDouble(a)));
                    System.arraycopy(tmp, 0, result, 1, 4);
                    resultlen[0] = 9;
                    countDownLatch.countDown();
                    break;
            }
            try {
                countDownLatch.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return 0;
        }
    };

    private synchronized void iCCardTest() {
        if (this.flag) {
            //sendMessage("Running......");
        } else {
            this.flag = true;
            new Thread(() -> {
                cardData = new CardData();
                emv = new EMV();
                emv.emv(handler, settings, transaction, this, cardData);
                System.out.println("valores track2  : " + handler.getIcField55());
                parent.runOnUiThread(() -> {
                    if (transaction.getCardHolder() == null || transaction.getCardNumber() == null ||
                            transaction.getCardExpiration() == null) {
                        listener.onCardDetect(CardDetected.REMOVED, null, null);
                    } else {
                        listener.onCardDetect(CardDetected.INSERTED, transaction, cardData);
                    }
                    this.flag = false;
                });
            }).start();
        }
    }

    private void emv(CardReaderTypeEnum cardType) {
        // 1. copy aid and capk to '/sdcard/emv/' as the default aid and capk
        try {
            if (!new File(EmvTermParam.emvParamFilePath).exists()) {
                FileUtils.doCopy(context, "emv", EmvTermParam.emvParamFilePath);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        // 2. set params
        final EmvTransParam emvTransParam = new EmvTransParam();
        if (cardType == CardReaderTypeEnum.IC_CARD) {
            emvTransParam.setTransKernalType(EmvData.KERNAL_EMV_PBOC);
        } else if (cardType == CardReaderTypeEnum.RF_CARD) {
            emvTransParam.setTransKernalType(EmvData.KERNAL_CONTACTLESS_ENTRY_POINT);
        }

        final EmvTermParam emvTermParam = new EmvTermParam();
        // 3. add aid or capk
        //loadVisaAIDs(emvHandler);
        //emvHandler.addApp();
        //emvHandler.addCapk()
        emvHandler.kernelInit(emvTermParam);

        // 4. transaction
        byte[] pucIsEcTrans = new byte[1];
        byte[] pucBalance = new byte[6];
        byte[] pucTransResult = new byte[1];

        OnEmvListener onEmvListener = new OnEmvListener() {
            @Override
            public int onSelApp(String[] appLabelList) {
                Log.d("Debug", "onSelApp");
                return iRet;
            }

            @Override
            public int onConfirmCardNo(String cardNo) {
                parent.runOnUiThread(() -> listener.onViewEnabled());
                Log.d("Debug", "onConfirmCardNo");
                String[] track2 = new String[1];
                final String[] pan = new String[1];
                emvHandler.getTrack2AndPAN(track2, pan);
                int index = 0;
                if (track2[0].contains("D")) {
                    index = track2[0].indexOf("D") + 1;
                } else if (track2[0].contains("=")) {
                    index = track2[0].indexOf("=") + 1;
                }
                final String exp = track2[0].substring(index, index + 4);
                //showLog("cardNum:" + pan[0]);
                //showLog("exp:" + exp);\
                return 0;
            }

            @Override
            public int onInputPIN(byte pinType) {
                // 1. open the secret pin pad to get pin block
                // 2. send the pinBlock to emv kernel
                if (emvTransParam.getTransKernalType() == EmvData.KERNAL_CONTACTLESS_ENTRY_POINT) {
                    String[] track2 = new String[1];
                    final String[] pan = new String[1];
                    emvHandler.getTrack2AndPAN(track2, pan);
                    int index = 0;
                    if (track2[0].contains("D")) {
                        index = track2[0].indexOf("D") + 1;
                    } else if (track2[0].contains("=")) {
                        index = track2[0].indexOf("=") + 1;
                    }
                    final String exp = track2[0].substring(index, index + 4);
                    //showLog("card:" + pan[0]);
                    //showLog("exp:" + exp);
                }
                Log.d("Debug", "onInputPIN");
                int iRet = 0;
                iRet = 0;//inputPIN(pinType);
                Log.d("Debug", "iRet=" + iRet);
                if (iRet == EmvResult.EMV_OK) {
                    emvHandler.setPinBlock(mPinBlock);
                }
                return iRet;
            }

            @Override
            public int onCertVerify(int certType, String certNo) {
                Log.d("Debug", "onCertVerify");
                return 0;
            }

            @Override
            public int onlineProc() {
                // 1. assemble the authorisation request data and send to bank by using get 'emvHandler.getTlvData()'
                // 2. separateOnlineResp to emv kernel
                // 3. return the callback ret
                Log.d("Debug", "onOnlineProc");
                byte[] authRespCode = new byte[3];
                byte[] issuerResp = new byte[512];
                int[] issuerRespLen = new int[1];
                int iSendRet = emvHandler.separateOnlineResp(authRespCode, issuerResp, issuerRespLen[0]);
                Log.d("Debug", "separateOnlineResp iSendRet=" + iSendRet);
                return 0;
            }

            @Override
            public byte[] onExchangeApdu(byte[] send) {
                Log.d("Debug", "onExchangeApdu");
                if (realCardType == CardReaderTypeEnum.IC_CARD) {
                    return mICCard.icExchangeAPDU(CardSlotNoEnum.SDK_ICC_USERCARD, send);
                } else if (realCardType == CardReaderTypeEnum.RF_CARD) {
                    return mRFCard.rfExchangeAPDU(send);
                }
                return null;
            }
        };
        //showLog("Emv Trans start...");
        // for the emv result, plz refer to emv doc.
        int ret = emvHandler.emvTrans(emvTransParam, onEmvListener, pucIsEcTrans, pucBalance, pucTransResult);
        //showLog("Emv trans end, ret = " + ret);
        String str = "Decline";
        if (pucTransResult[0] == EmvData.APPROVE_M) {
            str = "Approve";
        } else if (pucTransResult[0] == EmvData.ONLINE_M) {
            str = "Online";
        } else if (pucTransResult[0] == EmvData.DECLINE_M) {
            str = "Decline";
        }
        //showLog("Emv trans result = " + pucTransResult[0] + ", " + str);
        if (ret == 0) {
            getEmvData();
        } else if (ret == -4) {
            listener.onError("No se puede leer tarjeta");
        }
    }

    private void searchCard(final CardReaderTypeEnum cardType) {
        OnSearchCardListener listener = new OnSearchCardListener() {
            @Override
            public void onError(int resultCode) {
                mCardReadManager.closeCard();
                //closeDialog();
            }

            @Override
            public void onCardInfo(CardInfoEntity cardInfoEntity) {
                realCardType = cardInfoEntity.getCardExistslot();
                switch (realCardType) {
                    case RF_CARD:
                        byte resetData[] = new byte[EmvData.BUF_LEN];
                        int datalength[] = new int[1];
                        iRet = mRFCard.rfReset(resetData, datalength);
                        if (iRet != 0) {
                            Log.d("Debug", "rf reset error!");
                            return;
                        }
                        break;
                    case MAG_CARD:
                        Log.d("Debug", "MAG_CARD");
                        //getMagData();
                        break;
                    case IC_CARD:
                        Log.d("Debug", "ICC_CARD");
                        iRet = mICCard.icCardReset(CardSlotNoEnum.SDK_ICC_USERCARD);
                        if (iRet != 0) {
                            Log.d("Debug", "ic reset error!");
                            return;
                        }
                        break;
                    default:
                        break;
                }
                if (iRet == 0 && realCardType != CardReaderTypeEnum.MAG_CARD) {
                    emv(realCardType);
                }
            }

            @Override
            public void onNoCard(CardReaderTypeEnum arg0, boolean arg1) {
                System.out.println("valores : no card");
            }
        };
        mCardReadManager.searchCard(cardType, 0, listener);
    }

    private void getEmvData() {
        byte[] field55 = emvHandler.packageTlvList(tags);
        Log.d("Filed55: ", convertBytesToHex(field55));
        int[] ACTags = {0x9F26};
        String AC = convertBytesToHex(emvHandler.packageTlvList(ACTags)).toUpperCase().split("9F26")[1];
        int[] TVRTags = {0x95};
        String TVR = convertBytesToHex(emvHandler.packageTlvList(TVRTags)).toUpperCase().split("9505")[1];
        int[] NATags = {0x9F37};
        String NA = convertBytesToHex(emvHandler.packageTlvList(NATags)).toUpperCase().split("9F37")[1];
        String cardHolder = convertHexToASCII(convertBytesToHex(emvHandler.getTlvData(0x5F20)));
        String AID = (convertBytesToHex(emvHandler.getTlvData(0x4F)));
        String TSI = (convertBytesToHex(emvHandler.getTlvData(0x9B)));
        String expirationDate = convertBytesToHex(emvHandler.getTlvData(0x5F24));
        String cardNumber = convertBytesToHex(emvHandler.getTlvData(0x5A));

        //new Thread(() -> {
        //led.setLed(LedLightModeEnum.GREEN, true);
        //}).start();
        controlGreenLed(true);
        isCard = true;
        transaction = new Transaction();
        transaction.setCardExpiration(expirationDate);
        transaction.setCardNumber(cardNumber);
        transaction.setDeviceSerial(posModel.getPosInformation().getPosSerial());
        transaction.setCardHolder(cardHolder);
        CardData cardData = new CardData();
        cardData.setAID(AID);
        cardData.setAC(AC);
        cardData.setNA(NA);
        cardData.setTSI(TSI);
        cardData.setTVR(TVR);

        System.out.println("valores L: " + new Gson().toJson(cardData));
        mCardReadManager.closeCard();
        if (cardData.getAC().equals("0000000000000000")) {
            listener.onCardDetect(CardDetected.REMOVED, null, null);
            isCard = false;
        } else {
            listener.onCardRead(AID);
            listener.onCardDetect(CardDetected.INSERTED, transaction, cardData);
            isTransaction = true;
        }
        /*if (cardThread == null) {
            initCardDetectionThread();
            setCardDetectionThread(true);
        } else {
            if (!cardThread.isAlive()) {
                initCardDetectionThread();
                setCardDetectionThread(true);
            }
        }*/
    }

    public void printTicket(ArrayList<String> items, ArrayList<String> headers, ArrayList<String>
            footers) {
        switch (android.os.Build.MODEL) {
            case wpos3Model:
                new PrintThread(items, headers, footers).start();
                break;
            case z90Model:
                printTicketSmartPos(items, headers, footers);
                break;
            case newZ90:
                printZ90(items, headers, footers);
                break;
        }
    }

    private class PrintThread extends Thread {

        private ArrayList<String> items;
        private ArrayList<String> headers;
        private ArrayList<String> footers;

        PrintThread(ArrayList<String> items, ArrayList<String> headers, ArrayList<String> footers) {
            this.items = items;
            this.headers = headers;
            this.footers = footers;
        }

        @Override
        public void run() {
            mPrinter = new Printer(context);
            bthreadrunning = true;
            int datalen = 0;
            int result = 0;
            byte[] senddata = null;
            do {
                try {
                    result = mPrinter.printInit();
                    //clear print cache
                    mPrinter.clearPrintDataCache();
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
                try {
                    printCount++;
                    saveCount(context, Global.keyPrintCount, printCount);
                    ConsoleMessageManager.consolePrintingMsg();
                    for (String line : headers) {
                        result = mPrinter.printString(line, 18, Printer.Align.CENTER, true, false);
                    }
                    printLinesCount += items.size() + 4;
                    saveCount(context, Global.keyPrintLinesCount, printLinesCount);
                    for (String line : items) {
                        result = mPrinter.printString(line, 18, Printer.Align.LEFT, false, false);
                    }
                    if (footers != null) {
                        for (String line : footers) {
                            result = mPrinter.printString(line, 18, Printer.Align.LEFT, true, false);
                        }
                    }
                    result = mPrinter.printPaper(10);
                    mPrinter.printPaper_trade(5, result);
                    result = mPrinter.printFinish();
                } catch (Exception e) {
                    System.out.println("valores : error : " + e.getMessage());
                    e.printStackTrace();
                }

            } while (bloop);
            bthreadrunning = false;
            listener.onPrintStatus(PrintStatus.EXIT);
        }
    }

    private void printTicketSmartPos(ArrayList<String> items, ArrayList<String> headers, ArrayList<String>
            footers) {
        new Thread(() -> {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            boolean isPrint = settings.mPosEnterPrint();
            if (isPrint) {
                printCount++;
                saveCount(context, Global.keyPrintCount, printCount);
                ConsoleMessageManager.consolePrintingMsg();
                settings.mPosPrintTextSize(Settings.MPOS_PRINT_TEXT_DOUBLE_WIDTH);
                settings.mPosPrintAlign(Settings.MPOS_PRINT_ALIGN_CENTER);
                for (String line : headers) {
                    settings.mPosPrnStr(line);
                }
                settings.mPosPrintTextSize(Settings.MPOS_PRINT_TEXT_NORMAL);
                settings.mPosPrintAlign(Settings.MPOS_PRINT_ALIGN_LEFT);
                printLinesCount += items.size() + 4;
                saveCount(context, Global.keyPrintLinesCount, printLinesCount);
                for (String line : items) {
                    settings.mPosPrnStr(line);
                }
                settings.mPosPrintTextSize(Settings.MPOS_PRINT_TEXT_DOUBLE_WIDTH);
                if (footers != null) {
                    for (String line : footers) {
                        settings.mPosPrnStr(line);
                    }
                }
                settings.mPosPrintTextSize(Settings.MPOS_PRINT_TEXT_DOUBLE_SIZE);
                settings.mPosPrintLn();
                settings.mPosPrintLn();
            } else {
                listener.onError("Error en la impresion");
            }
        }).start();

    }

    private void printZ90(ArrayList<String> items, ArrayList<String> headers, ArrayList<String>
            footers) {
        new Thread(() -> {
            printer = mDriverManager.getPrinter();
            int printStatus = printer.getPrinterStatus();
            if (printStatus == SdkResult.SDK_PRN_STATUS_PAPEROUT) {
                listener.onError("Sin papel");
            } else {
                PrnStrFormat format = new PrnStrFormat();
                format.setTextSize(19);
                format.setAli(Layout.Alignment.ALIGN_CENTER);
                format.setStyle(PrnTextStyle.BOLD);
                for (String line : headers) {
                    printer.setPrintAppendString(line, format);
                }
                format.setTextSize(18);
                format.setStyle(PrnTextStyle.NORMAL);
                format.setAli(Layout.Alignment.ALIGN_NORMAL);
                for (String line : items) {
                    printer.setPrintAppendString(line, format);
                }
                format.setTextSize(19);
                format.setAli(Layout.Alignment.ALIGN_NORMAL);
                format.setStyle(PrnTextStyle.BOLD);
                if (footers != null) {
                    for (String line : footers) {
                        printer.setPrintAppendString(line, format);
                    }
                }
                printer.setPrintAppendString(" ", format);
                printStatus = printer.setPrintStart();
                printCount++;
                saveCount(context, Global.keyPrintCount, printCount);
                printLinesCount += items.size() + 4;
                saveCount(context, Global.keyPrintLinesCount, printLinesCount);
                if (printStatus == SdkResult.SDK_PRN_STATUS_PAPEROUT) {
                    listener.onError("Sin papel");
                }
                listener.onPrintStatus(PrintStatus.EXIT);
            }
        }).start();
    }

}
