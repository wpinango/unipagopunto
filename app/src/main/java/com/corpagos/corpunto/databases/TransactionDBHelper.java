package com.corpagos.corpunto.databases;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


import com.corpagos.corpunto.models.TransactionDB;
import com.corpagos.corpunto.models.Transaction;
import com.google.gson.Gson;

import java.util.ArrayList;

public class TransactionDBHelper extends SQLiteOpenHelper {

    public static final String DBName = "transaction.db";
    private static final int DBVersion = 12;

    public TransactionDBHelper(Context context) {
        super(context, DBName, null, DBVersion);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TransactionDB.transactionTable + " " +
                "(id INTEGER PRIMARY KEY AUTOINCREMENT, reference TEXT, date TEXT, user_id INTEGER," +
                " amount REAL, concept TEXT, created_at TEXT, balance REAL, card_number TEXT," +
                " coordenates TEXT, lot INTEGER, reverse INTEGER, app_version TEXT," +
                " card_type TEXT, status TEXT, transaction_type TEXT, trace INTEGER);" );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TransactionDB.transactionTable);
        onCreate(db);
    }

    public void insertTransaction(Transaction transaction) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("amount", transaction.getAmount());
        contentValues.put("balance", transaction.getBalance());
        contentValues.put("concept", transaction.getConcept());
        contentValues.put("created_at", transaction.getCreated_at());
        contentValues.put("date", transaction.getDate());
        contentValues.put("reference", transaction.getReference());
        contentValues.put("user_id", transaction.getUser_id());
        contentValues.put("card_number", transaction.getCard_number());
        contentValues.put("coordenates", transaction.getCoordenates());
        contentValues.put("lot", transaction.getLot());
        contentValues.put("reverse", transaction.getReverse());
        contentValues.put("app_version", transaction.getApp_version());
        contentValues.put("card_type", transaction.getCard_type());
        contentValues.put("status", transaction.getStatus());
        contentValues.put("transaction_type", transaction.getTransactionType());
        contentValues.put("trace", transaction.getTrace());
        db.insert(TransactionDB.transactionTable, null, contentValues);
        db.close();
    }

    public ArrayList<Transaction> getTransactionList() {
        ArrayList<Transaction> transactions = new ArrayList<>();
        Cursor cursor = getCursor(TransactionDB.transactionTable);
        for(cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            Transaction transaction = new Transaction();
            transaction.setAmount(cursor.getFloat(cursor.getColumnIndex(TransactionDB.KEY_AMOUNT)));
            transaction.setBalance(cursor.getFloat(cursor.getColumnIndex(TransactionDB.KEY_BALANCE)));
            transaction.setDate(cursor.getString(cursor.getColumnIndex(TransactionDB.KEY_DATE)));
            transaction.setReference(cursor.getString(cursor.getColumnIndex(TransactionDB.KEY_REFERENCE)));
            transaction.setConcept(cursor.getString(cursor.getColumnIndex(TransactionDB.KEY_CONCEPT)));
            transaction.setCreated_at(cursor.getString(cursor.getColumnIndex(TransactionDB.KEY_CREATED)));
            transaction.setUser_id(cursor.getInt(cursor.getColumnIndex(TransactionDB.KEY_USER_ID)));
            transaction.setId(cursor.getInt(cursor.getColumnIndex(TransactionDB.KEY_ID)));
            transaction.setCard_number(cursor.getString(cursor.getColumnIndex(TransactionDB.KEY_CARD_NUMBER)));
            transaction.setCard_type(cursor.getString(cursor.getColumnIndex(TransactionDB.KEY_CARD_TYPE)));
            transaction.setApp_version(cursor.getString(cursor.getColumnIndex(TransactionDB.KEY_VERSION)));
            transaction.setReverse(cursor.getInt(cursor.getColumnIndex(TransactionDB.KEY_REVERSE)));
            transaction.setStatus(cursor.getString(cursor.getColumnIndex(TransactionDB.KEY_STATUS)));
            transaction.setLot(cursor.getInt(cursor.getColumnIndex(TransactionDB.KEY_LOT)));
            transaction.setTransactionType(cursor.getString(cursor.getColumnIndex(TransactionDB.KEY_TRANSACTION_TYPE)));
            transaction.setTrace(cursor.getLong(cursor.getColumnIndex(TransactionDB.KEY_TRACE)));
            transactions.add(transaction);
        }
        return transactions;
    }

    private Cursor getCursor(String table) {
        SQLiteDatabase db = this.getReadableDatabase();
        return db.rawQuery("SELECT * FROM " + table, null);
    }

    public Transaction getTransactionSuccessByReference(String reference) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor =  db.rawQuery( "SELECT * FROM "+TransactionDB.transactionTable + "" +
                " WHERE reference=?", new String[] {reference});
        Transaction transaction = new Transaction();
        if (cursor.moveToFirst()) {
            transaction.setAmount(cursor.getFloat(cursor.getColumnIndex(TransactionDB.KEY_AMOUNT)));
            transaction.setBalance(cursor.getFloat(cursor.getColumnIndex(TransactionDB.KEY_BALANCE)));
            transaction.setDate(cursor.getString(cursor.getColumnIndex(TransactionDB.KEY_DATE)));
            transaction.setReference(cursor.getString(cursor.getColumnIndex(TransactionDB.KEY_REFERENCE)));
            transaction.setConcept(cursor.getString(cursor.getColumnIndex(TransactionDB.KEY_CONCEPT)));
            transaction.setCreated_at(cursor.getString(cursor.getColumnIndex(TransactionDB.KEY_CREATED)));
            transaction.setUser_id(cursor.getInt(cursor.getColumnIndex(TransactionDB.KEY_USER_ID)));
            transaction.setId(cursor.getInt(cursor.getColumnIndex(TransactionDB.KEY_ID)));
            transaction.setCard_number(cursor.getString(cursor.getColumnIndex(TransactionDB.KEY_CARD_NUMBER)));
            transaction.setCard_type(cursor.getString(cursor.getColumnIndex(TransactionDB.KEY_CARD_TYPE)));
            transaction.setApp_version(cursor.getString(cursor.getColumnIndex(TransactionDB.KEY_VERSION)));
            transaction.setReverse(cursor.getInt(cursor.getColumnIndex(TransactionDB.KEY_REVERSE)));
            transaction.setStatus(cursor.getString(cursor.getColumnIndex(TransactionDB.KEY_STATUS)));
            transaction.setLot(cursor.getInt(cursor.getColumnIndex(TransactionDB.KEY_LOT)));
            transaction.setTransactionType(cursor.getString(cursor.getColumnIndex(TransactionDB.KEY_TRANSACTION_TYPE)));
            db.close();
        }
        return transaction;
    }

    public ArrayList<Transaction> getTransactionsByLotNumber(int lot) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TransactionDB.transactionTable +
                " WHERE lot=?", new String[]{String.valueOf(lot)});
        ArrayList<Transaction> transactions = new ArrayList<>();
        for(cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            Transaction transaction = new Transaction();
            transaction.setAmount(cursor.getFloat(cursor.getColumnIndex(TransactionDB.KEY_AMOUNT)));
            transaction.setBalance(cursor.getFloat(cursor.getColumnIndex(TransactionDB.KEY_BALANCE)));
            transaction.setDate(cursor.getString(cursor.getColumnIndex(TransactionDB.KEY_DATE)));
            transaction.setReference(cursor.getString(cursor.getColumnIndex(TransactionDB.KEY_REFERENCE)));
            transaction.setConcept(cursor.getString(cursor.getColumnIndex(TransactionDB.KEY_CONCEPT)));
            transaction.setCreated_at(cursor.getString(cursor.getColumnIndex(TransactionDB.KEY_CREATED)));
            transaction.setUser_id(cursor.getInt(cursor.getColumnIndex(TransactionDB.KEY_USER_ID)));
            transaction.setId(cursor.getInt(cursor.getColumnIndex(TransactionDB.KEY_ID)));
            transaction.setCard_number(cursor.getString(cursor.getColumnIndex(TransactionDB.KEY_CARD_NUMBER)));
            transaction.setCard_type(cursor.getString(cursor.getColumnIndex(TransactionDB.KEY_CARD_TYPE)));
            transaction.setApp_version(cursor.getString(cursor.getColumnIndex(TransactionDB.KEY_VERSION)));
            transaction.setReverse(cursor.getInt(cursor.getColumnIndex(TransactionDB.KEY_REVERSE)));
            transaction.setStatus(cursor.getString(cursor.getColumnIndex(TransactionDB.KEY_STATUS)));
            transaction.setLot(cursor.getInt(cursor.getColumnIndex(TransactionDB.KEY_LOT)));
            transaction.setTransactionType(cursor.getString(cursor.getColumnIndex(TransactionDB.KEY_TRANSACTION_TYPE)));
            transaction.setTrace(cursor.getLong(cursor.getColumnIndex(TransactionDB.KEY_TRACE)));
            transactions.add(transaction);
        }
        return transactions;
    }

    public void updateTransaction(Transaction transaction) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        System.out.println("base in : " + new Gson().toJson(transaction));
        contentValues.put("amount", transaction.getAmount());
        contentValues.put("balance", transaction.getBalance());
        contentValues.put("concept", transaction.getConcept());
        contentValues.put("created_at", transaction.getCreated_at());
        contentValues.put("date", transaction.getDate());
        contentValues.put("reference", transaction.getReference());
        contentValues.put("user_id", transaction.getUser_id());
        contentValues.put("card_number", transaction.getCard_number());
        contentValues.put("coordenates", transaction.getCoordenates());
        contentValues.put("lot", transaction.getLot());
        contentValues.put("reverse", transaction.getReverse());
        contentValues.put("app_version", transaction.getApp_version());
        contentValues.put("card_type", transaction.getCard_type());
        contentValues.put("status", transaction.getStatus());
        contentValues.put("transaction_type", transaction.getTransactionType());
        db.update(TransactionDB.transactionTable, contentValues," created_at = ? ",
                new String[]{transaction.getCreated_at()});
        db.close();
    }

    public void deleteTransaction(String reference) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TransactionDB.transactionTable, " reference = ? ", new String[]{reference});
    }

    public void deleteAll() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TransactionDB.transactionTable, null, null);
        db.close();
    }

    public void updateClosureStatus() {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("reverse", 1);
        db.update(TransactionDB.transactionTable, contentValues,null, null);
    }

    public void updateReverseStatus(Transaction transaction) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("reverse", 1);
        db.update(TransactionDB.transactionTable, contentValues," reference = ? ",
                new String[]{transaction.getReference()});
    }
}
