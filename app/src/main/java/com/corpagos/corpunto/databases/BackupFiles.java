package com.corpagos.corpunto.databases;

import android.content.Context;
import android.os.Environment;

import com.corpagos.corpunto.Global;
import com.corpagos.corpunto.models.CardData;
import com.corpagos.corpunto.models.Counters;
import com.corpagos.corpunto.models.FirstLogin;
import com.corpagos.corpunto.models.Notification;
import com.corpagos.corpunto.models.OwnerData;
import com.corpagos.corpunto.models.Ticket;
import com.corpagos.corpunto.models.Token;
import com.corpagos.corpunto.models.Transaction;
import com.corpagos.corpunto.utils.SharedPreferencesHandler;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;

import static com.corpagos.corpunto.Global.timeCount;
import static com.corpagos.corpunto.utils.SharedPreferencesHandler.getCount;

public class BackupFiles {

    public static String notificationBackupFileName = "notification.json";
    public static String transactionsBackupFileName = "transaction.json";
    public static String counterBackupFileName = "counter.json";
    public static String configBackupFileName = "config.json";
    public static String versionBackupFileName = "version.json";
    public static String ownerBackupFileName = "owner.json";
    public static String loginBackupFileName = "login.json";
    private static File globalPath =  Environment.getExternalStoragePublicDirectory(
            Environment.DIRECTORY_DOCUMENTS);

    public static  boolean isFilesExist(Context context, String fileName) {
        String path = globalPath + "/" + fileName;
        File file = new File(path);
        return file.exists();
    }

    public static boolean createFiles(Context context, String fileName, String fileContent) {
        try {
            String path = globalPath + "/" + fileName;
            globalPath.mkdirs();
            FileOutputStream fos = new FileOutputStream(path);
            if (fileContent != null) {
                fos.write(fileContent.getBytes());
            }
            fos.close();
            return true;
        } catch (FileNotFoundException fileNotFound) {
            Global.showToast(context, "File create failed ", 1000);
            return false;
        } catch (IOException ioException) {
            Global.showToast(context, "File create failed ", 1000);
            return false;
        }
    }

    public static String readFileContent(Context context, String fileName) {
        try {
            String path = globalPath + "/" + fileName;
            globalPath.mkdirs();
            FileInputStream fis = new FileInputStream(path);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader bufferedReader = new BufferedReader(isr);
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line);
            }
            return sb.toString();
        } catch (FileNotFoundException fileNotFound) {
            return "";
        } catch (IOException ioException) {
            return "";
        }
    }

    public static void writeFiles(Context context, String fileName, String fileContent) {
        try {
            String path = globalPath + "/" + fileName;
            FileOutputStream outputStreamWriter = new FileOutputStream(path);
            globalPath.mkdirs();
            if (fileContent != null) {
                outputStreamWriter.write(fileContent.getBytes());
            }
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Global.showToast(context, "File write failed ", 1000);
        }
    }

    public static void addNotificationContentToFile(Context context,String fileName,
                                                   Notification.NotificationData notificationData) {
        if (isFilesExist(context,fileName)) {
            Type custom = new TypeToken<ArrayList<Notification.NotificationData>>() {
            }.getType();
            ArrayList<Notification.NotificationData> notificationList = new ArrayList<>();
            if (!BackupFiles.readFileContent(context, BackupFiles.notificationBackupFileName).equals("")) {
                notificationList = new Gson().fromJson(BackupFiles.
                        readFileContent(context, BackupFiles.notificationBackupFileName), custom);
            }
            notificationList.add(notificationData);
            BackupFiles.writeFiles(context,BackupFiles. notificationBackupFileName,
                    new Gson().toJson(notificationList));
        } else {
            ArrayList<Notification.NotificationData>notificationList = new ArrayList<>();
            notificationList.add(notificationData);
            createFiles(context,fileName, new Gson().toJson(notificationList));
        }
    }

    public static void backupTransactions(Context context) {
        TransactionDBHelper dbHelper = new TransactionDBHelper(context);
        ArrayList<Transaction> transactions = new ArrayList<>(dbHelper.getTransactionList());
        if (BackupFiles.isFilesExist(context, BackupFiles.transactionsBackupFileName)) {
            BackupFiles.writeFiles(context, BackupFiles.transactionsBackupFileName,
                    new Gson().toJson(transactions));
        } else {
            BackupFiles.createFiles(context,BackupFiles.transactionsBackupFileName, "");
            BackupFiles.writeFiles(context, BackupFiles.transactionsBackupFileName,
                    new Gson().toJson(transactions));
        }
    }

    public static void backupCounters(Context context){
        Counters countReaders = new Counters();
        countReaders.setNfcCount(getCount(context, Global.keyNfcCount));
        countReaders.setChipCount(getCount(context, Global.keyChipCount));
        countReaders.setPrintCount(getCount(context, Global.keyPrintCount));
        countReaders.setPrintLinesCount(getCount(context, Global.keyPrintLinesCount));
        countReaders.setTimeCount(timeCount);
        if (BackupFiles.isFilesExist(context, BackupFiles.counterBackupFileName)) {
            BackupFiles.writeFiles(context, BackupFiles.counterBackupFileName,
                    new Gson().toJson(countReaders));
        } else {
            BackupFiles.createFiles(context,BackupFiles.counterBackupFileName, "");
            BackupFiles.writeFiles(context, BackupFiles.counterBackupFileName,
                    new Gson().toJson(countReaders));
        }
    }

    public static void backupConfig(Context context) {
        if (!SharedPreferencesHandler.getSavedData(context, SharedPreferencesHandler.KEY_TOKEN,
                SharedPreferencesHandler.FILE_TOKEN).equals(SharedPreferencesHandler.NOT_FOUND) &&
                !SharedPreferencesHandler.getSavedData(context, SharedPreferencesHandler.KEY_TOKEN,
                        SharedPreferencesHandler.FILE_TOKEN).equals("")) {
            Token token = new Gson().fromJson(SharedPreferencesHandler.getSavedData(
                    context, SharedPreferencesHandler.KEY_TOKEN, SharedPreferencesHandler.FILE_TOKEN),
                    Token.class);
            if (BackupFiles.isFilesExist(context, BackupFiles.configBackupFileName)) {
                BackupFiles.writeFiles(context, BackupFiles.configBackupFileName,
                        new Gson().toJson(token));
            } else {
                BackupFiles.createFiles(context,BackupFiles.configBackupFileName,
                        new Gson().toJson(token));
            }

        }
    }

    public static void resetFirstLogin(Context context) {
        FirstLogin firstLogin = new FirstLogin();
        firstLogin.setClient_secret("");
        firstLogin.setClient_id("");
        firstLogin.setSerial("");
        firstLogin.setLogin(false);
        BackupFiles.backupData(context, BackupFiles.loginBackupFileName, new Gson().toJson(firstLogin));
    }

    public static void backupData(Context context, String filename, String json) {
        if (BackupFiles.isFilesExist(context, filename)) {
            BackupFiles.writeFiles(context, filename, json);
        } else {
            BackupFiles.createFiles(context, filename, json);
        }
    }

    public static String getBackupData(Context context, String filename) {
        if (BackupFiles.isFilesExist(context, filename)) {
            return BackupFiles.readFileContent(context,filename);
        } else {
            return SharedPreferencesHandler.NOT_FOUND;
        }
    }

    public static void backupVersion(Context context, String version) {
        if (BackupFiles.isFilesExist(context, BackupFiles.versionBackupFileName)) {
            BackupFiles.createFiles(context, BackupFiles.versionBackupFileName, version);
        }
    }

    public static void populateTicketObject(Context context, Transaction transaction, OwnerData ownerData,
                                            CardData cardData, String title) {
        Ticket ticket = new Ticket(transaction, ownerData, title, cardData);
        SharedPreferencesHandler.saveData(context, SharedPreferencesHandler.KEY_TRANSACTION,
                new Gson().toJson(ticket), SharedPreferencesHandler.KEY_TRANSACTION_FILE);
    }

    public static void backupFirstLogin(Context context, String login) {
        if (BackupFiles.isFilesExist(context, BackupFiles.loginBackupFileName)) {
            BackupFiles.createFiles(context, BackupFiles.loginBackupFileName, login);
        }


        /*File file = new File(context.getFilesDir(), fileName);
        FileOutputStream fileOutputStream = null;
        System.out.println("valores : " + file.getAbsolutePath() + " " + file.getPath());
        try {
            fileOutputStream = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
        BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);
        try {
            bufferedWriter.write(body);
        } catch (IOException e) {
            System.out.println("valores : " + e.getMessage());
        }
        Toast.makeText(context, "File saved successfully!",
                Toast.LENGTH_SHORT).show();*/
    }
}
