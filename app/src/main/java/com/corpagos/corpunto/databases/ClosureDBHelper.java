package com.corpagos.corpunto.databases;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.corpagos.corpunto.models.DailyClosure;
import com.corpagos.corpunto.models.Summary;
import com.corpagos.corpunto.models.Transaction;
import com.google.gson.Gson;

import java.util.ArrayList;


public class ClosureDBHelper extends SQLiteOpenHelper {

    private static final String dBName = "closure.db";
    private final String tableName = "closure_data";
    private static final int dBVersion = 3;
    private static String strSeparator = ",";
    private Context context;

    public ClosureDBHelper(Context context) {
        super(context, dBName, null, dBVersion);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE " + tableName + " (id INTEGER PRIMARY KEY AUTOINCREMENT," +
                " amount REAL, close_date TEXT, reference TEXT, lot_number INTEGER);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + tableName);
        onCreate(sqLiteDatabase);
    }

    public ArrayList<DailyClosure> getDailyClosure() {
        ArrayList<DailyClosure> closures = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + tableName, null);
        for(cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            DailyClosure dailyClosure = new DailyClosure();
            Summary summary = new Summary();
            summary.setTotal_amount(cursor.getFloat(cursor.getColumnIndex("amount")));
            summary.setClose_date(cursor.getString(cursor.getColumnIndex("close_date")));
            summary.setLot_number(cursor.getInt(cursor.getColumnIndex("lot_number")));
            dailyClosure.setSummary(summary);
            closures.add(dailyClosure);
        }
        cursor.close();
        return closures;
    }

    public DailyClosure getDailyClosureByDate(String date) {
        TransactionDBHelper dbHelper = new TransactionDBHelper(context);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor =  db.rawQuery( "SELECT * FROM "+ tableName + " WHERE close_date = ?",
                new String[]{date});
        DailyClosure dailyClosure = new DailyClosure();
        Summary summary = new Summary();
        if (cursor.moveToFirst()) {
            summary.setTotal_amount(cursor.getFloat(cursor.getColumnIndex("amount")));
            summary.setClose_date(cursor.getString(cursor.getColumnIndex("close_date")));
            summary.setLot_number(cursor.getInt(cursor.getColumnIndex("lot_number")));
            String[] references = convertStringToArray(cursor.getString(cursor.getColumnIndex(
                    "reference")));
            dailyClosure.setSummary(summary);
            ArrayList<Transaction> transactions = new ArrayList<>();
            for (String reference : references) {
                transactions.add(dbHelper.getTransactionSuccessByReference(reference));
            }
           /* for (Transaction t : transactionSuccesses) {
                Transaction transaction = new Transaction();
                transaction.setAmount(String.valueOf(t.getAmount()));
                transaction.setLot(String.valueOf(t.getLot()));
                transaction.setCard_number(t.getCard_number());
                transaction.setReference(t.getReference());
                transaction.setCardAccountType(t.getCard_type());
                transactions.add(transaction);
            }*/
            dailyClosure.setTransactions(transactions);
            db.close();
            cursor.close();
        }
        return dailyClosure;
    }

    public void insertClosure(DailyClosure dailyClosure) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("close_date", dailyClosure.getSummary().getClose_date());
        contentValues.put("reference", convertArrayToString(getReferences(dailyClosure
                .getTransactions())));
        contentValues.put("amount", dailyClosure.getSummary().getTotal_amount());
        contentValues.put("lot_number", dailyClosure.getSummary().getLot_number());
        db.insert(tableName, null, contentValues);
        db.close();
    }

    private String[] getReferences(ArrayList<Transaction> transactions) {
        String[] references = new String[transactions.size()];
        for (int i = 0; i <transactions.size(); i++) {
            references[i] = transactions.get(i).getReference();
        }
        return references;
    }


    private String convertArrayToString(String[] array){
        String str = "";
        for (int i = 0;i<array.length; i++) {
            str = str+array[i];
            if(i<array.length-1){
                str = str+strSeparator;
            }
        }
        return str;
    }

    private String[] convertStringToArray(String str){
        String[] arr = str.split(strSeparator);
        return arr;
    }
}
