package com.corpagos.corpunto.print;

import android.content.Context;

import com.corpagos.corpunto.commons.Format;
import com.corpagos.corpunto.models.CardData;
import com.corpagos.corpunto.models.DailyClosure;
import com.corpagos.corpunto.models.OwnerData;
import com.corpagos.corpunto.models.Transaction;
import com.corpagos.corpunto.utils.Time;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class PrintTicket {
    
    
    public static ArrayList<String> getHeaders(OwnerData ownerData, String title) {
        ArrayList<String> items = new ArrayList<>();
        items.add(ownerData.getBank());
        items.add(title);
        return items;
    }

    public static ArrayList<String> getErrorTicketBody(Transaction transaction, OwnerData ownerData,
                                                       String serialPos, CardData cardData) {
        ArrayList<String> items = new ArrayList<>();
        items.add(ownerData.getName());
        items.add(ownerData.getAddress());
        items.add("RIF: " +ownerData.getTis() + " AFIL: 846584000103052");
        items.add("TER: " + serialPos + " LOTE: " + String.format("%04d", transaction.getLot()));
        items.add(Format.getCardNumberPrintFormat(
                transaction.getCardNumber()) + " FECHA:" + Time.getCurrentFullDate());
        //items.add("REF: " + transactionSuccess.getReference());
        items.add("AID: " + cardData.getAID() + " NA: " + cardData.getNA());
        items.add("AC: " + cardData.getAC());
        items.add("TVR: " + cardData.getTVR() + " TSI: " + cardData.getTSI());
        return items;

    }

    public static ArrayList<String> getTicketClosureResume(DailyClosure dailyClosure,
                                                           OwnerData ownerData, String serialPos) {
        ArrayList<String> items = new ArrayList<>();
        items.add(ownerData.getName());
        items.add(ownerData.getAddress());
        items.add("RIF: " +ownerData.getTis());
        items.add("AFIL: " + "846584000103052  " + " TER: " + serialPos);
        items.add(Time.getCurrentFullDate());
        items.add("MONTO TOTAL "  + Format.getCashFormat(dailyClosure.getSummary().getTotal_amount()));
        return items;
    }

    public static ArrayList<String> getTicketClosureDetails(Context context, DailyClosure dailyClosure,
                                                            OwnerData ownerData, String serialPos) {
        ArrayList<String> items = new ArrayList<>();
        items.add(ownerData.getName());
        items.add(ownerData.getAddress());
        items.add("RIF: " +ownerData.getTis());
        items.add("AFIL: " + "846584000103052  " + " TER: " + serialPos);
        items.add(Time.getCurrentFullDate());
        items.add(" ");
        for (Transaction transaction : dailyClosure.getTransactions()) {
            items.add(Format.getLastNumberFromCard(transaction.getCard_number()) + " " +
                    Format.getLocalDateFormat(context, Format.datePattern, transaction.getCreated_at()) + " " +
                    //new SimpleDateFormat("dd/MM/yyyy").
                    //        format(Format.dateFormat(transaction.getCreated_at())) + " " +
                    transaction.getReference() + " " +
                    transaction.getAmount());
        }
        items.add(" ");
        return items;
    }

    public static ArrayList<String> getHistoricalTicketBody(Context context, String serialPos,
                                                            Transaction transaction,
                                                            OwnerData ownerData) {
        ArrayList<String> items = new ArrayList<>();
        items.add(ownerData.getName());
        items.add(ownerData.getAddress());
        items.add("RIF: " +ownerData.getTis() + " AFIL: 846584000103052");
        items.add("TER: " + serialPos + " LOTE: " + String.format("%04d", transaction.getLot()));
        items.add(Format.getCardNumberPrintFormat(
                transaction.getCard_number()) + " FECHA:" +
                Format.getLocalDateFormat(context, Format.dateFullPattern, transaction.getCreated_at()));
                //new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").
                //        format(Format.dateFormat(transaction.getCreated_at())));
        items.add("REF: " + transaction.getReference());
        return items;
    }

    public static ArrayList<String> getTicketSuccessBody(Transaction transaction, OwnerData ownerData,
                                                         CardData cardData) {
        ArrayList<String> items = new ArrayList<>();
        items.add(ownerData.getName());
        items.add(ownerData.getAddress());
        items.add("RIF: " +ownerData.getTis() + " AFIL: 846584000103052");
        items.add("TER: " + transaction.getDeviceSerial() + " LOTE: " + String.format("%04d", transaction.getLot()));
        items.add(Format.getCardNumberPrintFormat(transaction.getCard_number()) +
                " FECHA:" + transaction.getDate());
        items.add("APROB: 05781 " + " REF: " + transaction.getReference() + " TRACE: " + String.format("%06d", transaction.getTrace()));
        items.add("AID: " + cardData.getAID() + " NA: " + cardData.getNA());
        items.add("AC: " + cardData.getAC());
        items.add("TVR: " + cardData.getTVR() + " TSI: " + cardData.getTSI());
        return items;
    }


    public static ArrayList<String> getRollBackFooter(String amount) {
        ArrayList<String> items = new ArrayList<>();
        items.add("MONTO: " + amount);

        return items;
    }

    public static ArrayList<String> getErrorFooter(String error) {
        ArrayList<String> items = new ArrayList<>();
        items.add(error);
        return items;
    }

    public static ArrayList<String> getCreditFooter(String amount, boolean isCopy ) {
        ArrayList<String> items = new ArrayList<>();
        if (isCopy) {
            items.add("DUPLICADO");
        }
        items.add("MONTO: " + amount);
        items.add(" ");
        items.add("FIRMA:________________");
        items.add(" ");
        items.add(" ");
        items.add(" ");
        return items;
    }

    public static ArrayList<String> getDebitFooter(String amount, boolean isCopy) {
        ArrayList<String> items = new ArrayList<>();
        if (isCopy) {
            items.add("DUPLICADO");
        }
        items.add("MONTO: " + amount);
        items.add(" ");
        items.add("NO SE REQUIERE FIRMA");
        items.add(" ");
        items.add(" ");
        items.add(" ");
        return items;
    }
}
