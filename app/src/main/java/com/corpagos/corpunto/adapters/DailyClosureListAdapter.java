package com.corpagos.corpunto.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.corpagos.corpunto.R;
import com.corpagos.corpunto.commons.Format;
import com.corpagos.corpunto.models.Transaction;
import com.corpagos.corpunto.widgets.RTextView;

import java.util.ArrayList;

public class DailyClosureListAdapter extends BaseAdapter {

    private LayoutInflater layoutInflater;
    private ArrayList<Transaction> transactions;

    public DailyClosureListAdapter(Context context, ArrayList<Transaction> transactions) {
        this.layoutInflater = LayoutInflater.from(context);
        this.transactions = transactions;
    }

    @Override
    public int getCount() {
        return transactions.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.item_list_daily_closure, null);
        }
        Transaction transaction = transactions.get(position);
        RTextView tvCardNumber = convertView.findViewById(R.id.tv_item_list_daily_card);
        RTextView tvAmount = convertView.findViewById(R.id.tv_item_list_daily_amount);
        RTextView tvReference = convertView.findViewById(R.id.tv_item_list_daily_reference);
        try {
            tvAmount.setText(Format.getCashFormat(transaction.getAmount()));
            tvCardNumber.setText(Format.getLastNumberFromCard(transaction.getCard_number()));
            tvReference.setText(transaction.getReference());
        } catch (Exception e) {
            System.out.println("valores : " + e.getMessage());
        }
        return convertView;
    }
}
