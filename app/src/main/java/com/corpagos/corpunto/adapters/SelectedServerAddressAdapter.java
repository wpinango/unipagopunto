package com.corpagos.corpunto.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;

import com.corpagos.corpunto.R;

import java.util.ArrayList;

import static com.corpagos.corpunto.Asynctask.finalHost;

public class SelectedServerAddressAdapter extends BaseAdapter {

    private LayoutInflater layoutInflater;
    private ArrayList<String> hosts;

    public SelectedServerAddressAdapter(Context context, ArrayList<String> hosts) {
        this.layoutInflater = LayoutInflater.from(context);
        this.hosts = hosts;
    }

    @Override
    public int getCount() {
        return hosts.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.item_list_server_address, null);
        }
        RadioButton rbServerAddress = convertView.findViewById(R.id.rb_selected_address);
        TextView tvServerAddress = convertView.findViewById(R.id.textView46);
        tvServerAddress.setText(hosts.get(position));
        rbServerAddress.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!finalHost.contains(hosts.get(position))) {
                    rbServerAddress.setChecked(false);
                }
            }
        });
        if (finalHost.contains(hosts.get(position))) {
            rbServerAddress.setChecked(true);
        } else {
            rbServerAddress.setChecked(false);
        }
        return convertView;
    }
}

