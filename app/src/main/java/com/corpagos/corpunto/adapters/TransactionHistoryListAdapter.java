package com.corpagos.corpunto.adapters;

import android.annotation.SuppressLint;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.corpagos.corpunto.R;
import com.corpagos.corpunto.commons.Format;
import com.corpagos.corpunto.emv.CardType;
import com.corpagos.corpunto.interfaces.OnClick;
import com.corpagos.corpunto.models.Transaction;
import com.corpagos.corpunto.models.TransactionType;
import com.corpagos.corpunto.widgets.RTextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class TransactionHistoryListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
    implements Filterable {

    private ItemFilter filter = new ItemFilter();
    private List<Transaction> originalData;
    private List<Transaction> filteredData;
    private final int TYPE_HEADER = 0;
    private final int TYPE_ITEM = 1;
    private OnClick listener;
    private Context context;

    public TransactionHistoryListAdapter(Context context, ArrayList<Transaction> operations, OnClick listener) {
        this.originalData = operations;
        this.filteredData = operations;
        this.listener = listener;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if(viewType==TYPE_HEADER) {
            return new VHHeader(LayoutInflater.from(parent.getContext()).inflate(R.layout
                            .header_list_transaction, parent, false));
        }
        else {
            return new VHItem(LayoutInflater.from(parent.getContext()).inflate(R.layout
                            .item_list_transaction, parent, false));
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Transaction transaction = filteredData.get(position);
        if(holder instanceof VHHeader) {
            ((VHHeader)holder).tvAmount.setText("T:" + Format.getCashFormat(transaction.getAmount()));
            ((VHHeader) holder).tvtTitle.setText(transaction.getTransactionType().equals(
                    TransactionType.CLOSURE) ? "Cierre" : "Corte");
            ((VHHeader)holder).tvLot.setText("L:" + String.format("%04d", transaction.getLot()));
            if (transaction.isSelected()) {
                holder.itemView.setBackgroundResource(R.color.colorSelectBlueTransparen);
            } else {
                if (transaction.getTransactionType().equals(TransactionType.CLOSURE)) {
                    holder.itemView.setBackgroundResource(R.color.light_red);
                } else if (transaction.getTransactionType().equals(TransactionType.SECTION)) {
                    holder.itemView.setBackgroundResource(R.color.colorGreen);
                }
            }
        }
        else if(holder instanceof VHItem) {
            ((VHItem)holder).tvAmount.setText(Format.getCashFormat(transaction.getAmount()));
            if (transaction.getAmount() > 0.0) {
                ((VHItem)holder).tvCardNumber.setText(CardType.getCardName(transaction.getConcept())
                         + " - Venta " + transaction.getCard_number());//Format.getLastNumberFromCard(transaction.getCard_number()));
            } else if (transaction.getAmount() < 0.0) {
                ((VHItem)holder).tvCardNumber.setText(CardType.getCardName(transaction.getConcept()) +
                        " - Reverso " + Format.getLastNumberFromCard(transaction.getCard_number()));
            }
            ((VHItem)holder).tvReference.setText("Ref. " + transaction.getReference());
            ((VHItem)holder).tvDay.setText(String.valueOf(transaction.getIndex()));
            ((VHItem)holder).tvMonth.setText(Format.getLocalDateFormat(context, Format.hourPattern,
                    transaction.getCreated_at()).toUpperCase());
            //((VHItem)holder).tvMonth.setText(new SimpleDateFormat("HH:mm:ss").
            //        format(Format.dateFormat(transaction.getCreated_at())).toUpperCase());
            if (transaction.isSelected()) {
                holder.itemView.setBackgroundResource(R.color.colorSelectBlueTransparen);
            } else {
                holder.itemView.setBackgroundResource(R.color.white);
            }
        }
    }

    public List<Transaction> getFilteredData() {
        return filteredData;
    }

    @Override
    public int getItemCount() {
        return filteredData.size();
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    public void update() {
        filteredData = originalData;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            String filterString = constraint.toString().toLowerCase();
            FilterResults results = new FilterResults();
            final List<Transaction> list = originalData;
            int count = list.size();
            final ArrayList<Transaction> nlist = new ArrayList<>(count);
            Transaction filterableObject;
            for (int i = 0; i < count; i++) {
                filterableObject = list.get(i);
                if (filterableObject.getReference().contains(filterString)) {
                    nlist.add(filterableObject);
                }
            }
            results.values = nlist;
            results.count = nlist.size();
            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredData = (ArrayList<Transaction>) results.values;
            notifyDataSetChanged();
        }
    }

    class VHHeader extends RecyclerView.ViewHolder{
        TextView tvtTitle;
        TextView tvLot;
        RTextView tvAmount;

        VHHeader(View itemView) {
            super(itemView);
            tvAmount = itemView.findViewById(R.id.tv_amount_header);
            tvtTitle = itemView.findViewById(R.id.tv_title_header);
            tvLot = itemView.findViewById(R.id.tv_lot_header);
            itemView.setOnClickListener(view -> listener.onClick(getAdapterPosition()));
        }
    }
    class VHItem extends RecyclerView.ViewHolder{
        RTextView tvCardNumber;
        RTextView tvAmount;
        RTextView tvReference;
        TextView tvDay;
        TextView tvMonth;

        VHItem(View itemView) {
            super(itemView);
            tvCardNumber = itemView.findViewById(R.id.item_list_card_number_transaction2);
            tvReference = itemView.findViewById(R.id.tv_item_list_reference_transaction2);
            tvAmount = itemView.findViewById(R.id.tv_item_list_amount_transaction2);
            tvMonth = itemView.findViewById(R.id.tv_item_list_time);
            tvDay = itemView.findViewById(R.id.tv_item_list_day_transaction2);
            itemView.setOnClickListener(view -> listener.onClick(getAdapterPosition()));
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position)) {
            return TYPE_HEADER;
        }
        return TYPE_ITEM;
    }

    private boolean isPositionHeader(int position) {
        return filteredData.get(position).getTransactionType().equals(TransactionType.CLOSURE) ||
                filteredData.get(position).getTransactionType().equals(TransactionType.SECTION);
    }
}
