package com.corpagos.corpunto.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.corpagos.corpunto.R;
import com.corpagos.corpunto.commons.Format;
import com.corpagos.corpunto.interfaces.OnClick;
import com.corpagos.corpunto.models.Transaction;
import com.corpagos.corpunto.widgets.RTextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ProviderPaymentListAdapter extends RecyclerView.Adapter<ProviderPaymentListAdapter.ViewHolder>
        implements Filterable {

    private ProviderPaymentListAdapter.ItemFilter filter = new ProviderPaymentListAdapter.ItemFilter();
    private List<Transaction> originalData;
    private List<Transaction>filteredData;
    private OnClick listener;
    private Context context;

    public ProviderPaymentListAdapter(Context context, ArrayList<Transaction> operations, OnClick listener) {
        this.originalData = operations;
        this.filteredData = operations;
        this.listener = listener;
        this.context = context;
    }

    @NonNull
    @Override
    public ProviderPaymentListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_transaction,
                parent,false);
        return new ProviderPaymentListAdapter.ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ProviderPaymentListAdapter.ViewHolder holder, int position) {
        Transaction transaction = filteredData.get(position);
        holder.tvAmount.setText(Format.getCashFormat(transaction.getAmount()));
        holder.tvCardNumber.setText("Tar - Pago " + Format.getLastNumberFromCard(
                transaction.getCard_number()));
        holder.tvReference.setText("Ref. 1183458391915"); //+ transaction.getReference());
        holder.tvDay.setText(String.valueOf(filteredData.size() - position));
        //holder.tvMonth.setText(new SimpleDateFormat("hh:mm:ss aaa").format(
        //        Format.dateFormat(transaction.getCreated_at())).toUpperCase());
        holder.tvMonth.setText(Format.getLocalDateFormat(context, Format.hourPattern,
                transaction.getCreated_at()).toUpperCase());
        if (transaction.isSelected()) {
            holder.itemView.setBackgroundResource(R.color.colorSelectBlueTransparen);
        } else {
            holder.itemView.setBackgroundResource(R.color.white);
        }
    }

    public List<Transaction> getFilteredData() {
        return filteredData;
    }

    @Override
    public int getItemCount() {
        return filteredData.size();
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    public void update() {
        filteredData = originalData;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        RTextView tvCardNumber;
        RTextView tvAmount;
        RTextView tvReference;
        TextView tvDay;
        TextView tvMonth;

        ViewHolder(View itemView) {
            super(itemView);
            tvCardNumber = itemView.findViewById(R.id.item_list_card_number_transaction2);
            tvAmount = itemView.findViewById(R.id.tv_item_list_amount_transaction2);
            tvReference = itemView.findViewById(R.id.tv_item_list_reference_transaction2);
            tvDay = itemView.findViewById(R.id.tv_item_list_day_transaction2);
            tvMonth = itemView.findViewById(R.id.tv_item_list_time);
            itemView.setOnClickListener(view -> {
                listener.onClick(getAdapterPosition());
            });
        }

    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            final List<Transaction> list = originalData;

            int count = list.size();
            final ArrayList<Transaction> nlist = new ArrayList<>(count);

            Transaction filterableObject;

            for (int i = 0; i < count; i++) {
                filterableObject = list.get(i);
                if (filterableObject.getReference().contains(filterString)) {
                    nlist.add(filterableObject);
                }
            }

            results.values = nlist;
            results.count = nlist.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredData = (ArrayList<Transaction>) results.values;
            notifyDataSetChanged();
        }
    }
}
