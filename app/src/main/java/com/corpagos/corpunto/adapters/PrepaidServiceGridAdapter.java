package com.corpagos.corpunto.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.corpagos.corpunto.R;
import com.corpagos.corpunto.models.PrepaidService;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class PrepaidServiceGridAdapter extends BaseAdapter {

    private ArrayList<PrepaidService> services;
    private LayoutInflater layoutInflater;
    private Context context;

    public PrepaidServiceGridAdapter(Context context, ArrayList<PrepaidService> services) {
        this.layoutInflater = LayoutInflater.from(context);
        this.services = services;
        this.context = context;
    }

    @Override
    public int getCount() {
        return services.size();
    }

    @Override
    public Object getItem(int i) {
        return services.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = layoutInflater.inflate(R.layout.item_grid_prepaid_services, null);
        }
        PrepaidService prepaidService = services.get(i);
        try {
            TextView tvName = view.findViewById(R.id.textView51);
            tvName.setText(prepaidService.getName().toUpperCase());
            ImageView imgService = view.findViewById(R.id.btn_prepaid_service);
            Picasso.get().load(prepaidService.getLogo()).into(imgService);
        } catch (Exception e) {
            e.getMessage();
        }
        return view;
    }
}
