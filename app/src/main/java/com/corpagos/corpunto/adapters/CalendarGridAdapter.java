package com.corpagos.corpunto.adapters;

import android.content.Context;
import android.graphics.Color;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.corpagos.corpunto.R;
import com.corpagos.corpunto.enums.PosModelType;
import com.corpagos.corpunto.interfaces.ClickInterface;
import com.corpagos.corpunto.models.OwnCalendar;
import com.corpagos.corpunto.singletons.PosModel;
import com.corpagos.corpunto.widgets.RTextView;

import java.util.ArrayList;

public class CalendarGridAdapter extends RecyclerView.Adapter<CalendarGridAdapter.ViewHolder>{

    private ArrayList<OwnCalendar> ownCalendars;
    private ClickInterface listener;
    private Context context;

    public CalendarGridAdapter(Context context, ArrayList<OwnCalendar> ownCalendars, ClickInterface listener) {
        this.ownCalendars = ownCalendars;
        this.listener = listener;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        PosModel posModel = PosModel.getInstance(context);
        if (posModel.getPosModelType().equals(PosModelType.WPOS_MINI)) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_grid_calendar_mini,
                    parent,false);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_grid_calendar,
                    parent,false);
        }
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        OwnCalendar ownCalendar = ownCalendars.get(position);
        holder.tvDayName.setText(ownCalendar.getDayName());
        holder.tvDayNumber.setText(ownCalendar.getDayNumber());
        holder.tvMonth.setText(ownCalendar.getMonth().toUpperCase());
        if (position == 9) {
            holder.tvDayNumber.setTextColor(Color.parseColor("#E83E3F"));
            holder.tvDayName.setTextColor(Color.parseColor("#E83E3F"));
            holder.tvMonth.setTextColor(Color.parseColor("#E83E3F"));
        } else {
            holder.tvDayNumber.setTextColor(Color.parseColor("#1D89E4"));
            holder.tvDayName.setTextColor(Color.parseColor("#1D89E4"));
            holder.tvMonth.setTextColor(Color.parseColor("#1D89E4"));
        }
        if (ownCalendar.isSelect()){
            holder.itemView.setBackgroundResource(R.color.colorSelectBlueTransparen);
        } else {
            holder.itemView.setBackgroundResource(R.color.white);
        }
    }

    @Override
    public int getItemCount() {
        return ownCalendars.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        RTextView tvDayName, tvMonth;
        TextView tvDayNumber;

        ViewHolder(View itemView) {
            super(itemView);
            tvDayNumber = itemView.findViewById(R.id.tv_item_grid_number_calendar);
            tvDayName = itemView.findViewById(R.id.tv_item_grid_day_calendar);
            tvMonth = itemView.findViewById(R.id.tv_item_grid_month_calendar);
            itemView.setOnClickListener(view -> listener.onclick(getAdapterPosition()));
        }

    }
}
