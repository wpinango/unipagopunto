package com.corpagos.corpunto.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.corpagos.corpunto.R;
import com.corpagos.corpunto.emv.PosCounterType;
import com.corpagos.corpunto.enums.RoleType;
import com.corpagos.corpunto.interfaces.OnClick;
import com.corpagos.corpunto.models.CountReader;

import java.util.ArrayList;

import static com.corpagos.corpunto.Global.printLinesCount;
import static com.corpagos.corpunto.Global.timeCount;
import static com.corpagos.corpunto.commons.Format.getHoursWithDecimalsFormatFromSeconds;

public class CountCardReaderListAdapter extends BaseAdapter {

    private ArrayList<CountReader> readers;
    private LayoutInflater layoutInflater;
    private RoleType roleType;
    private OnClick listener;

    public CountCardReaderListAdapter(Context context, ArrayList<CountReader> readers,
                                      RoleType roleType, OnClick listener) {
        this.readers = readers;
        this.layoutInflater = LayoutInflater.from(context);
        this.roleType = roleType;
        this.listener = listener;
    }

    @Override
    public int getCount() {
        return readers.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        CountReader countReader = readers.get(i);
        if (view == null) {
            view = layoutInflater.inflate(R.layout.item_list_readers, null);
        }
        TextView tvTitle = view.findViewById(R.id.tv_title_reader);
        TextView tvCount = view.findViewById(R.id.tv_count_reader);
        ImageButton btnDelete = view.findViewById(R.id.btn_delete_count);
        if (roleType.equals(RoleType.USER)) {
            btnDelete.setEnabled(false);
            btnDelete.setVisibility(View.INVISIBLE);
        } else if (roleType.equals(RoleType.ADMIN)) {
            btnDelete.setOnClickListener(v -> listener.onClick(i));
        }
        try {
            tvTitle.setText(countReader.getTitle());
            if (countReader.getPosCounterType().equals(PosCounterType.HOURS_USE)) {
                tvCount.setText(getHoursWithDecimalsFormatFromSeconds(timeCount));
            } else if (countReader.getPosCounterType().equals(PosCounterType.PRINTED_METERS)) {
                float printMts = printLinesCount * 0.003f;
                tvCount.setText(String.format("%.2f", printMts));
            } else {
                tvCount.setText(String.valueOf(countReader.getCount()));
            }
        } catch (Exception e) {
            e.getMessage();
        }
        return view;
    }
}
