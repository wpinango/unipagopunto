package com.corpagos.corpunto.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.corpagos.corpunto.R;
import com.corpagos.corpunto.models.ProviderAccount;
import com.google.gson.Gson;

import java.util.ArrayList;

import androidx.annotation.NonNull;

public class AccountListAdapter extends ArrayAdapter<ProviderAccount> {

    private ArrayList<ProviderAccount> accounts;
    private LayoutInflater layoutInflater;

    public AccountListAdapter(@NonNull Context context, int resource, int textViewId,
                              ArrayList<ProviderAccount> accounts) {
        super(context, resource, textViewId, accounts);
        this.accounts = accounts;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return accounts.size();
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.item_list_provider_account, null);
        }
        System.out.println("valores adapter : " + new Gson().toJson(accounts.get(position)));
        TextView tvAccount = convertView.findViewById(R.id.tv_provider_account);
        tvAccount.setText("*" + accounts.get(position).getAccount());
        return convertView;
    }
}
