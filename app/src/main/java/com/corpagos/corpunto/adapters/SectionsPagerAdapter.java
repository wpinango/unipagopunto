package com.corpagos.corpunto.adapters;

import com.corpagos.corpunto.fragments.historicals.PaymentTransactionFragment;
import com.corpagos.corpunto.fragments.historicals.PrepaidTransactionFragment;
import com.corpagos.corpunto.fragments.historicals.SellTransactionFragment;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class SectionsPagerAdapter extends FragmentPagerAdapter {

    private PaymentTransactionFragment paymentTransactionFragment = new PaymentTransactionFragment();
    private PrepaidTransactionFragment prepaidTransactionFragment = new PrepaidTransactionFragment();
    private SellTransactionFragment sellTransactionFragment = new SellTransactionFragment();

    public SectionsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return sellTransactionFragment;
            case 1:
                return prepaidTransactionFragment;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Ventas";
            case 1:
                return "Recargas";
        }
        return null;
    }

    public TransactionHistoryListAdapter getSellsAdapter() {
        return sellTransactionFragment.getTransactionHistoryListAdapter();
    }

    public ProviderPaymentListAdapter getPaymentsAdapter() {
        return paymentTransactionFragment.getProviderPaymentListAdapter();
    }

    public HistoricalPrepaidServiceAdapter getServicesPaymentAdapter() {
        return prepaidTransactionFragment.getHistoricalPrepaidServiceAdapter();
    }

    public void getTransactionStorage() {
        sellTransactionFragment.getTransactionStorage();
    }
}
