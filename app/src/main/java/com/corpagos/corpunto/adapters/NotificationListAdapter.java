package com.corpagos.corpunto.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.corpagos.corpunto.R;
import com.corpagos.corpunto.models.Notification;
import com.corpagos.corpunto.models.UpdateApp;
import com.corpagos.corpunto.utils.Time;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Date;

public class NotificationListAdapter extends BaseAdapter {

    private LayoutInflater layoutInflater;
    private ArrayList<Notification.NotificationData> notificationData;

    public NotificationListAdapter(Context context, ArrayList<Notification.NotificationData> notificationData) {
        this.notificationData = notificationData;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return notificationData.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.item_list_notification, null);
        }
        try {
            Notification.NotificationData notificationData = this.notificationData.get(position);
            TextView tvTitle = convertView.findViewById(R.id.tv_item_list_notification_title);
            TextView tvContent = convertView.findViewById(R.id.tv_item_list_notification_content);
            TextView tvTime = convertView.findViewById(R.id.tv_item_list_notification_time);
            UpdateApp updateApp = new Gson().fromJson(notificationData.getData().toString(),
                    UpdateApp.class);
            Date date = new Date();
            date.setTime(Time.getTimestamp() - notificationData.getTime());
            tvTime.setText(com.corpagos.corpunto.commons.Format.parseDate(notificationData.getTime()));
            tvContent.setText(notificationData.getMessage() + " " + updateApp.getVersion());
            tvTitle.setText(notificationData.getType());

            if (notificationData.getType().equals("Update")) {

            }


        } catch (Exception e) {
            e.getMessage();
        }
        return convertView;
    }
}
