package com.corpagos.corpunto.requests;

import android.content.Context;

import com.corpagos.corpunto.Asynctask;
import com.corpagos.corpunto.interfaces.AsynctaskListener;
import com.corpagos.corpunto.models.PrepaidService;
import com.corpagos.corpunto.models.Provider;
import com.corpagos.corpunto.models.Token;
import com.corpagos.corpunto.models.Transaction;
import com.corpagos.corpunto.app.AppCredentials;
import com.corpagos.corpunto.utils.SharedPreferencesHandler;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

import static com.corpagos.corpunto.activities.MainLauncherActivity.version;

public class Request {

    public static void requestDoPayment(Context context, Transaction transaction, String mode,
                                        String method, AsynctaskListener listener) {
        Token token = new Token();
        if (!SharedPreferencesHandler.getSavedData(context, SharedPreferencesHandler.KEY_TOKEN,
                SharedPreferencesHandler.FILE_TOKEN).equals(SharedPreferencesHandler.NOT_FOUND) &&
                !SharedPreferencesHandler.getSavedData(context, SharedPreferencesHandler.KEY_TOKEN,
                        SharedPreferencesHandler.FILE_TOKEN).equals("")) {
            token = new Gson().fromJson(SharedPreferencesHandler.getSavedData(
                    context, SharedPreferencesHandler.KEY_TOKEN, SharedPreferencesHandler.FILE_TOKEN),
                    Token.class);
        } else {
            token.setExpires_in(0);
            token.setAccess_token("");
            token.setToken_type("");
            return;
        }
        Map<String, Object> map = new HashMap<>();
        map.put("card_number", transaction.getCardNumber());
        map.put("cardholder_dni", transaction.getDni());
        map.put("pos_serial", transaction.getDeviceSerial());
        map.put("concept", "Punto de pago");
        map.put("coords", transaction.getLocation());
        map.put("card_account_type", transaction.getCardAccountType());
        map.put("card_password", transaction.getCardPassword());
        map.put("amount", String.valueOf(transaction.getAmount()).replace(",","."));
        map.put("app_version", version);

        Map<String, String> headers = new HashMap<>();
        System.out.println("valores : " + map);
        headers.put(Asynctask.authorization, token.getToken_type() + " " + token.getAccess_token());

        new Asynctask.PostFormMethodAsynctask(Asynctask.transactionalHost,
                Asynctask.getEncryptedAESRequest(context, map), Asynctask.URL_DO_TRANSACTION +
                mode + "/" + method, headers, listener).execute();
    }

    public static void requestDoServicePayment(Context context, PrepaidService prepaidService,
                                               Transaction transaction, String mode, String method,
                                               AsynctaskListener listener) {
        Token token = AppCredentials.getToken(context);
        Map<String, Object> map = new HashMap<>();
        map.put("card_number", transaction.getCardNumber());
        map.put("cardholder_dni", transaction.getDni());
        map.put("pos_serial", transaction.getDeviceSerial());
        map.put("concept", "Punto de pago");
        map.put("coords", transaction.getLocation());
        map.put("card_account_type", transaction.getCardAccountType());
        map.put("card_password", transaction.getCardPassword());
        map.put("amount", String.valueOf(transaction.getAmount()).replace(",","."));
        map.put("app_version", version);
        map.put("service_contract", prepaidService.getTargetNumber());
        map.put("service_id", prepaidService.getId());
        map.put("pin_code", "1234");
        Map<String, String> headers = new HashMap<>();
        headers.put(Asynctask.authorization, token.getToken_type() + " " + token.getAccess_token());

        System.out.println("valores : " + map + " " + Asynctask.URL_SERVICE_PAYMENT +
                mode + "/" + method);
        headers.put(Asynctask.authorization, token.getToken_type() + " " + token.getAccess_token());

        new Asynctask.PostFormMethodAsynctask(Asynctask.transactionalHost,
                Asynctask.getEncryptedAESRequest(context, map), Asynctask.URL_SERVICE_PAYMENT +
                mode + "/" + method, headers, listener).execute();
    }

    public static void requestDoProviderPayment(Context context, Transaction transaction,
                                                AsynctaskListener listener, Provider provider) {
        Token token = AppCredentials.getToken(context);
        Map<String, Object> map = new HashMap<>();
        map.put("card_number", transaction.getCardNumber());
        map.put("cardholder_dni", transaction.getDni());
        map.put("pos_serial", transaction.getDeviceSerial());
        map.put("concept", "Punto de pago");
        map.put("coords", transaction.getLocation());
        map.put("card_account_type", transaction.getCardAccountType());
        map.put("card_password", transaction.getCardPassword());
        map.put("amount", String.valueOf(transaction.getAmount()).replace(",","."));
        map.put("app_version", version);
        map.put("provider_id", provider.getId());
        map.put("pin_code", transaction.getCardPassword());
        System.out.println("valores : " + map);
        Map<String, String> headers = new HashMap<>();
        headers.put(Asynctask.authorization, token.getToken_type() + " " + token.getAccess_token());

        System.out.println("valores : " + map);
        headers.put(Asynctask.authorization, token.getToken_type() + " " + token.getAccess_token());

        new Asynctask.PostFormMethodAsynctask(Asynctask.transactionalHost,
                Asynctask.getEncryptedAESRequest(context, map), Asynctask.URL_DO_PROVIDER_PAYMENT,
                headers, listener).execute();
    }


    public static void requestLoginToken(String user, String pass, AsynctaskListener listener) {
        Map<String, Object> body = new HashMap<>();
        body.put("username", user);
        body.put("password", pass);
        body.put("grant_type", "password");
        body.put("client_id", "dev");
        body.put("client_secret", "null");
        System.out.println("valores : " + body);
        new Asynctask.PostFormMethodAsynctask(Asynctask.authHost, body, Asynctask.URL_AUTH,
                new HashMap<>(), listener).execute();
    }

    public static void requestFirstLogin(Token token, String posSerial, AsynctaskListener listener) {
        Map<String, String> headers = new HashMap<>();
        headers.put(Asynctask.authorization, token.getToken_type() + " " + token.getAccess_token());
        new Asynctask.GetMethodAsynctask(Asynctask.authHost, Asynctask.URL_FIRST_LOGIN +
                posSerial, headers, listener).execute();
    }

    public static void getRequestWithToken(Context context, String host, String endpoint,
                                           Map<String, String> headers, AsynctaskListener listener) {
        Token token = AppCredentials.getToken(context);
        headers.put(Asynctask.authorization, token.getToken_type() + " " + token.getAccess_token());
        new Asynctask.GetMethodAsynctask(host, endpoint, headers, listener).execute();
    }

    public static void postRequestToken(Context context, String host, String endpoint, Map<String, Object> body,
                                        Map<String, String> headers, AsynctaskListener listener) {
        Token token = AppCredentials.getToken(context);
        headers.put(Asynctask.authorization, token.getToken_type() + " " + token.getAccess_token());
        new Asynctask.PostFormMethodAsynctask(host, body, endpoint, headers, listener).execute();
    }
}
