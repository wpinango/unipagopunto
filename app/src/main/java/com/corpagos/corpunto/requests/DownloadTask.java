package com.corpagos.corpunto.requests;

import android.os.AsyncTask;
import android.os.Environment;
import com.corpagos.corpunto.interfaces.DownloadProgress;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import static com.corpagos.corpunto.activities.UpdateAppActivity.fileName;

public class DownloadTask extends AsyncTask<String,String,String> {

    private String urlD;
    private DownloadProgress downloadProgress;

    public DownloadTask(String url, DownloadProgress downloadProgress){
        this.urlD = url;
        this.downloadProgress = downloadProgress;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    /**
     * Downloading file in background thread
     * */
    @Override
    protected String doInBackground(String... f_url) {
        int count;
        try {
            URL url = new URL(urlD);
            URLConnection connection = url.openConnection();
            connection.connect();
            int lengthOfFile = connection.getContentLength();
            InputStream input = new BufferedInputStream(url.openStream(),
                    8192);
            File path = Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DOWNLOADS);
            OutputStream output = new FileOutputStream(
                    path + "/" + fileName);
            path.mkdirs();
            byte data[] = new byte[lengthOfFile];
            long total = 0;
            while ((count = input.read(data)) != -1) {
                total += count;
                publishProgress("" + (int) ((total * 100) / lengthOfFile));
                output.write(data, 0, count);
            }
            output.flush();
            output.close();
            input.close();
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            return "";
        }
        return urlD;
    }

    /**
     * Updating progress bar
     * */
    protected void onProgressUpdate(String... progress) {
        System.out.println("algo " + progress[0]);
        downloadProgress.onDownloadUpdate(DownloadProgress.progressType.ONPROGRESS,Integer.parseInt(progress[0]));
    }

    /**
     * After completing background task Dismiss the progress dialog
     * **/
    @Override
    protected void onPostExecute(String file_url) {
        if (!file_url.equals("")) {
            downloadProgress.onDownloadUpdate(DownloadProgress.progressType.FINISH, 105);
        } else {
            downloadProgress.onDownloadUpdate(DownloadProgress.progressType.ERROR, 0);
        }
    }
}