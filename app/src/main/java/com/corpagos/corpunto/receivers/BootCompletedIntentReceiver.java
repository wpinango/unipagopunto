package com.corpagos.corpunto.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.corpagos.corpunto.services.BootService;

public class BootCompletedIntentReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {
            Intent pushIntent = new Intent(context, BootService.class);
            context.startService(pushIntent);
        }
    }
}
