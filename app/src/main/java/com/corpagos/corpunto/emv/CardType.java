package com.corpagos.corpunto.emv;

public class CardType {
    private final static String masterCard = "a0000000041010";
    private final static String visa = "a0000000031010";
    private final static String maestro = "a0000000043060";
    private final static String americanExpress = "AmericanExpress";
    private final static String diners = "A0000001524010";
    private final static String electronDebit = "a0000000032010";
    private final static String electronCredit = "a0000000032020";
    //private final static String debitMasterCard = "DEBIT MASTERCARD";
    private final static String debitMasterCard = "a0000000042203";
    public final static int credit = 1;
    public final static int debit = 2;
    public final static int undefined = 0;

    public static int checkCardType(String cardType) {
        switch (cardType) {
            case masterCard:
                return credit;
            case visa:
                return credit;
            case maestro:
                return debit;
            case electronDebit:
                return credit;
            case debitMasterCard:
                return debit;
            default:
                return undefined;
        }
    }

    public static String getCardName(String aid) {
        if (aid != null) {
            switch (aid.toLowerCase()) {
                case masterCard:
                    return "MasterCard";
                case visa:
                    return "Visa";
                case maestro:
                    return "Maestro";
                case electronDebit:
                    return "Electron";
                default:
                    return "Visa";
            }
        } else {
            return "Visa";
        }
    }

}
