package com.corpagos.corpunto.emv;

public enum PosCounterType {
    CHIP, NFC, TOTAL_READINGS, PRINTING_LINES, HOURS_USE, TICKET_PRINTINGS, PRINTED_METERS,
}
