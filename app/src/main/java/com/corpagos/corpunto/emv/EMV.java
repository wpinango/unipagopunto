package com.corpagos.corpunto.emv;

import android.annotation.SuppressLint;
import android.util.Log;

import com.corpagos.corpunto.interfaces.CardInterface;
import com.corpagos.corpunto.models.CardData;
import com.corpagos.corpunto.models.Transaction;
import com.corpagos.corpunto.utils.ConsoleMessageManager;
import com.corpagos.corpunto.utils.ConsoleTree;
import com.google.gson.Gson;
import com.imagpay.Settings;
import com.imagpay.emv.EMVParam;
import com.imagpay.mpos.MposHandler;

import java.text.SimpleDateFormat;
import java.util.Date;

import timber.log.Timber;

public class EMV {

    @SuppressLint({"SimpleDateFormat"})
    public void emv(MposHandler handler, Settings settings, Transaction transaction, CardInterface
            cardInterface, CardData cardData) {
        //Message message = new Message();
        //message.what = showProgressDialog;
        //message.obj = "Leyendo tarjeta";
        //handleros.sendMessage(message);
        long start = System.currentTimeMillis();
        EMVParam param = new EMVParam();
        param.setSlot((byte) 0);
        param.setMerchName("4368696E61");
        param.setMerchCateCode("0001");
        param.setMerchId("313233343536373839303132333435");
        param.setTermId("3132333435363738");
        param.setTerminalType((byte) 34);
        param.setCapability("E0F8C8");
        param.setExCapability("F00000A001");
        param.setTransCurrExp((byte) 2);
        param.setCountryCode("0862");
        param.setTransCurrCode("0862");
        param.setTransType((byte) 0);
        param.setTermIFDSn("3838383838383838");
        param.setAuthAmnt(8000000);
        param.setOtherAmnt(0);
        Date date = new Date();
        param.setTransDate(new SimpleDateFormat("yyMMdd").format(date));
        param.setTransTime(new SimpleDateFormat("HHmmss").format(date));
        EMVUtils emvUtils = new EMVUtils();
        emvUtils.loadMasterCardAIDs(param);
        emvUtils.loadMasterCardCapks(param);
        emvUtils.loadMasterCardRevocs(param);
        emvUtils.loadVisaAIDs(param);
        emvUtils.loadVisaCapks(param);
        emvUtils.loadVisaRevocs(param);
        emvUtils.loadChinaAIDs(param);
        emvUtils.loadAmericanExpressAIDs(param);
        emvUtils.loadDiscoverAIDs(param);
        emvUtils.loadInteracAIDs(param);
        emvUtils.loadJCBAIDs(param);
        handler.kernelInit(param);
        settings.icReset();
        if (handler.icReset() != null) {
            handler.process(cardInterface);
        }
        handler.icOff();
        long end = System.currentTimeMillis();
        String field55 = handler.getIcField55();
        String icTrack2Data = handler.getICTrack2Data();
        String icSeq = handler.getIcSeq();
        System.out.println("Field55:" + field55);
        Timber.plant(new ConsoleTree());
        System.out.println("ICTrack2Data:" + icTrack2Data);
        String data = handler.getIcPan();
        if (data != null) {
            System.out.println("CarNo:" + data);
        } else {
            System.out.println("valores : CarNo:");
        }
        data = handler.getTLVDataByTag(24352);
        if (data != null) {
            StringBuffer sb = new StringBuffer();
            for (String s : data.replaceAll("..", "$0 ").trim().split(" ")) {
                sb.append((char) Integer.parseInt(s, 16));
            }
            System.out.println("valores CarHolder:" + sb.toString());
            transaction.setCardHolder(sb.toString().trim());
        } else {
            System.out.println("valores CarHolder:");
        }
        //9F3704D63BCB1F950500000000009A031902069C01009F02060000080000005F2A020840820239009F1A0208409F03060000000000009F3303E0F8C89F3501229F1E0838383838383838388407A00000000410109F0902008C9F41020004
        //9F3704A3349C82950500000000009A031902069C01009F02060000080000005F2A020840820218009F1A0208409F03060000000000009F3303E0F8C89F3501229F1E0838383838383838388407A00000000430609F0902008C9F41020002
        System.out.println("IcSeq:" + icSeq);
        Log.i("valores", "IcSeq:" + icSeq);
        String effDate = handler.getIcEffDate();
        System.out.println("EffDate:" + emvUtils.formatDate(effDate));
        Log.i("valores", "EffDate:" + emvUtils.formatDate(effDate));
        String expDate = handler.getIcExpDate();
        System.out.println("ExpDate:" + emvUtils.formatDate(expDate));
        Log.i("valores", "ExpDate:" + emvUtils.formatDate(expDate));
        Log.i("valores", "number:" + field55);//handler.getTLVDataByTag(40742));
        Log.i("valores", "number:" + icTrack2Data);//handler.getTLVDataByTag(149));
        //cardData.setAC(handler.getTLVDataByTag(40742).toUpperCase());
        ConsoleMessageManager.consoleGetDataCardMsg();
        System.out.println("IC card read time:" + (end - start) + "ms");
        //System.out.println("valores: " + new Gson().toJson());
        //handleros.sendEmptyMessage(dismissDialog);
        transaction.setCardExpiration(emvUtils.formatDate(expDate));
        transaction.setCardNumber(handler.getIcPan());
        transaction.setDeviceSerial(settings.setReadSN());
        try {
            if (cardData != null) {
                cardData.setAID(handler.getTLVDataByTag(40710).toUpperCase());
                cardData.setTVR(handler.getTLVDataByTag(149).toUpperCase());
                cardData.setTSI(handler.getTLVDataByTag(155).toUpperCase());
                cardData.setNA(handler.getTLVDataByTag(40759).toUpperCase());
                //cardData.setAC(handler.getTLVDataByTag(40742));
                cardData.setTrack2(icTrack2Data);

                System.out.println("valores card : " + new Gson().toJson(cardData));
            }
        } catch (Exception e) {
            e.getMessage();
            System.out.println("valores : card error " + e.toString());
        }
        //System.out.println("valores : " + new Gson().toJson(transaction));
        //requestUserIdentity();
        //Log.d(" valores ejemplo", );
        //Log.d("valores example ", new Gson().toJson(settings));
    }

    /*@SuppressLint({"SimpleDateFormat"})
    private void emv() {
        Message message = new Message();
        message.what = showProgressDialog;
        message.obj = "Leyendo tarjeta";
        handleros.sendMessage(message);
        long start = System.currentTimeMillis();
        EMVParam param = new EMVParam();
        param.setSlot((byte) 0);
        param.setMerchName("4368696E61");
        param.setMerchCateCode("0001");
        param.setMerchId("313233343536373839303132333435");
        param.setTermId("3132333435363738");
        param.setTerminalType((byte) 34);
        param.setCapability("E0F8C8");
        param.setExCapability("F00000A001");
        param.setTransCurrExp((byte) 2);
        param.setCountryCode("0840");
        param.setTransCurrCode("0840");
        param.setTransType((byte) 0);
        param.setTermIFDSn("3838383838383838");
        param.setAuthAmnt(8000000);
        param.setOtherAmnt(0);
        Date date = new Date();
        param.setTransDate(new SimpleDateFormat("yyMMdd").format(date));
        param.setTransTime(new SimpleDateFormat("HHmmss").format(date));
        EMVUtils emvUtils = new EMVUtils();
        emvUtils.loadMasterCardAIDs(param);
        emvUtils.loadMasterCardCapks(param);
        emvUtils.loadMasterCardRevocs(param);
        emvUtils.loadVisaAIDs(param);
        emvUtils.loadVisaCapks(param);
        emvUtils.loadVisaRevocs(param);
        emvUtils.loadChinaAIDs(param);
        emvUtils.loadAmericanExpressAIDs(param);
        emvUtils.loadDiscoverAIDs(param);
        emvUtils.loadInteracAIDs(param);
        emvUtils.loadJCBAIDs(param);
        this.handler.kernelInit(param);
        this.settings.icReset();
        if (this.handler.icReset() != null) {
            this.handler.process();
        }
        this.handler.icOff();
        long end = System.currentTimeMillis();
        String field55 = this.handler.getIcField55();
        String icTrack2Data = this.handler.getICTrack2Data();
        String icSeq = this.handler.getIcSeq();
        System.out.println("Field55:" + field55);
        Timber.plant(new ConsoleTree());
        System.out.println("ICTrack2Data:" + icTrack2Data);
        String data = this.handler.getIcPan();
        if (data != null) {
            System.out.println("CarNo:" + data);
        } else {
            System.out.println("valores : CarNo:");
        }
        data = this.handler.getTLVDataByTag(24352);
        if (data != null) {
            StringBuffer sb = new StringBuffer();
            for (String s : data.replaceAll("..", "$0 ").trim().split(" ")) {
                sb.append((char) Integer.parseInt(s, 16));
            }
            System.out.println("valores CarHolder:" + sb.toString());
            transaction.setCardHolder(sb.toString().trim());
        } else {
            System.out.println("valores CarHolder:");
        }
        System.out.println("IcSeq:" + icSeq);
        Log.i("valores", "IcSeq:" + icSeq);
        String effDate = this.handler.getIcEffDate();
        System.out.println("EffDate:" + emvUtils.formatDate(effDate));
        Log.i("valores", "EffDate:" + emvUtils.formatDate(effDate));
        String expDate = this.handler.getIcExpDate();
        System.out.println("ExpDate:" + emvUtils.formatDate(expDate));
        Log.i("valores", "ExpDate:" + emvUtils.formatDate(expDate));
        ConsoleMessageManager.consoleGetDataCardMsg();
        System.out.println("IC card read time:" + (end - start) + "ms");
        //System.out.println("valores: " + new Gson().toJson());
        handleros.sendEmptyMessage(dismissDialog);
        transaction.setCardExpiration(emvUtils.formatDate(expDate));
        transaction.setCardNumber(handler.getIcPan());
        transaction.setDeviceSerial(settings.setReadSN());
        requestUserIdentity();

    }*/




    /*EmvCoreJNI dds = new EmvCoreJNI();
        final String path = dds.exists();
        dds.addTransMethod(new TransAPDU() {
            @Override
            public byte[] onTransmitApdu(byte[] arg0) {
                String dataWithAPDU = settings.getDataWithAPDUForStr(
                        Settings.SLOT_NFC, StringUtils.convertBytesToHex(arg0));
                if (dataWithAPDU == null) {
                    return null;
                }
                byte[] arg1 = new byte[dataWithAPDU.substring(4).length() / 2];
                System.arraycopy(Base16EnDecoder.Decode(dataWithAPDU.substring(4)), 0,
                        arg1, 0, dataWithAPDU.substring(4).length() / 2);
                return arg1;
            }

            @Override
            public String onGetDataPath() {
                return path;
            }
        });
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (reset != null) {
            Toast.makeText(this,"card near field",Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this,"no card near field",Toast.LENGTH_SHORT).show();
            return;
        }
        dds.EmvQTransParamInit(ISOUtil.zeropad("100", 12));
        byte[] bTransResult = new byte[1];
        byte[] bCVMType = new byte[1];
        byte[] bBalance = new byte[6];
        int resp = dds.EmvQTrans(bBalance, bTransResult, bCVMType);
        dialog.dismissDialog();
        if (resp == 0) {
            Toast.makeText(this, "quick pass read successful...........",
                    Toast.LENGTH_SHORT).show();
        } else {
            //Toast.makeText(this, "quick pass read failed.....<" + resp + ">",
            //        Toast.LENGTH_SHORT).show();
            isNfc = true;
            transaction.setCardHolder("CABRERA/JESUS");
            transaction.setCardExpiration("08/22");
            transaction.setCardNumber("5529735050007873");
            transaction.setDeviceSerial(settings.setReadSN());
            //confirmOperation();
            nfcCount++;
            saveCount(this, Global.keyNfcCount, nfcCount);
            cardDataReadSuccesses();
            return;
        }

        if (bTransResult[0] == (byte) EmvCoreJNI.ONLINE_M) {
            Toast.makeText(this, "Please take the card, online consumption.",
                    Toast.LENGTH_SHORT).show();
            int tagListt[] = { 0x57 };
            String tag57 = dds.getIcField(tagListt);
            if (tag57 != null && tag57.length() > 4) {
                String track2 = tag57.substring(4);
                if (track2.indexOf("D") > 0) {
                    int index = track2.indexOf("D");
                    String _pan = track2.substring(0, index);
                    int index2 = track2.indexOf("D") + 1;
                    String _exp = track2.substring(index2, index2 + 4);
                    //sendMessage("cardmun:" + _pan);
                    //sendMessage("_exp:" + _exp);
                }
            }
        } else if (bTransResult[0] == (byte) EmvCoreJNI.APPROVE_M) {
            Toast.makeText(
                    this, "TransactionController approval\n" + "available balance:"
                            + StringUtil.TwoWei(ISOUtil.hexString(bBalance)),
                    Toast.LENGTH_SHORT).show();
            int tagListt[] = { 0x57 };
            String tag57 = dds.getIcField(tagListt);
            if (tag57 != null && tag57.length() > 4) {
                String track2 = tag57.substring(4);
                if (track2.indexOf("D") > 0) {
                    int index = track2.indexOf("D");
                    String _pan = track2.substring(0, index);
                    int index2 = track2.indexOf("D") + 1;
                    String _exp = track2.substring(index2, index2 + 4);
                    //sendMessage("cardmun:" + _pan);
                    //sendMessage("_exp:" + _exp);
                }
            }
        } else if (bTransResult[0] == (byte) EmvCoreJNI.DECLINE_M) {
            Toast.makeText(this, "The transaction is rejected",
                    Toast.LENGTH_SHORT).show();
        }*/

}
