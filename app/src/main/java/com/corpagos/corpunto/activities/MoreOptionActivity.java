package com.corpagos.corpunto.activities;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.corpagos.corpunto.R;
import com.corpagos.corpunto.commons.AnimationTransition;
import com.corpagos.corpunto.enums.PosModelType;
import com.corpagos.corpunto.singletons.PosModel;

import static com.corpagos.corpunto.activities.MainLauncherActivity.version;

public class MoreOptionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_options);
        findViewById(R.id.button10).setOnClickListener(v -> {
            Intent intent = new Intent(this, AccountRegisterActivity.class);
            startActivity(intent);
            AnimationTransition.setInActivityTransition(this);
        });
        findViewById(R.id.button11).setOnClickListener(v -> {
            Intent intent = new Intent(MoreOptionActivity.this, WebViewActivity.class);
            startActivity(intent);
            AnimationTransition.setInActivityTransition(this);
        });
        TextView tvHeader = findViewById(R.id.textView29);
        PosModel posModel = PosModel.getInstance(this);
        if (posModel.getPosModelType().equals(PosModelType.WPOS_MINI)) {
            tvHeader.setText("Mini V" + version);
        } else if (posModel.getPosModelType().equals(PosModelType.WPOS_3) ||
                posModel.getPosModelType().equals(PosModelType.Z90)) {
            tvHeader.setText("Plus V" + version);
        }
        try {
            SeekBar volumeSeekBar = findViewById(R.id.seekBar2);
            AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
            volumeSeekBar.setMax(audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC));
            volumeSeekBar.setProgress(audioManager.getStreamVolume(AudioManager.STREAM_MUSIC));
            volumeSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onStopTrackingTouch(SeekBar arg0) {
                }

                @Override
                public void onStartTrackingTouch(SeekBar arg0) {
                }

                @Override
                public void onProgressChanged(SeekBar arg0, int progress, boolean arg2) {
                    audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, progress, 0);
                }
            });
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        AnimationTransition.setOutActivityTransition(this);
    }
}
