package com.corpagos.corpunto.activities;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.corpagos.corpunto.R;
import com.corpagos.corpunto.dialogs.Dialog;

import java.util.ArrayList;
import java.util.Arrays;

public class AccountRegisterActivity extends BaseActivity {

    private ArrayList<String> accountsType = new ArrayList<>();
    private ArrayList<String> idType = new ArrayList<>();
    private EditText etAccountNumber, etDni, etName;
    private Spinner spAccountType, spIdType;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_account_register;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initViews();
    }

    private void initViews() {
        etAccountNumber = findViewById(R.id.et_account_number_register);
        etDni = findViewById(R.id.et_dni_register);
        etName = findViewById(R.id.et_name_register);
        spAccountType = findViewById(R.id.sp_account_type_register);
        spIdType = findViewById(R.id.sp_id_type);
        accountsType = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.account_type)));
        ArrayAdapter<String> accountAdapter = new ArrayAdapter<>(this, R.layout.spinner_item,
                accountsType);
        accountAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        spAccountType.setAdapter(accountAdapter);
        idType = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.id_types)));
        ArrayAdapter<String> idAdapter = new ArrayAdapter<>(this, R.layout.spinner_item,
                idType);
        idAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        spIdType.setAdapter(idAdapter);
        findViewById(R.id.btn_register).setOnClickListener(v -> {
            if (validate()) {
                Dialog dialog = new Dialog(this);
                dialog.showSuccessDialog("Se registro correctamente");
                etName.setText("");
                etDni.setText("");
                etAccountNumber.setText("");
                spIdType.setSelection(0);
                spAccountType.setSelection(0);
                etName.requestFocus();
            }
        });
    }

    private boolean validate(){
        boolean isValid = true;
        if (etAccountNumber.getText().toString().isEmpty() || etAccountNumber.getText().length() < 16) {
            etAccountNumber.setError("Debe ingresar un numero de cuenta valido");
            isValid = false;
        } else {
            etAccountNumber.setError(null);
        }
        if (etDni.getText().toString().isEmpty() || etDni.getText().length() < 6) {
            etDni.setError("Debe ingresar un numero valido");
            isValid = false;
        } else {
            etDni.setError(null);
        }
        if (etName.getText().toString().isEmpty()) {
            etName.setError("Debe ingresar un nombre valido");
            isValid = false;
        } else {
            etName.setError(null);
        }
        return isValid;
    }


}
