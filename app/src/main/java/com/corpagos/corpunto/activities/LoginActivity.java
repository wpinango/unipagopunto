package com.corpagos.corpunto.activities;


import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.corpagos.corpunto.Asynctask;
import com.corpagos.corpunto.Global;
import com.corpagos.corpunto.R;
import com.corpagos.corpunto.app.AppCredentials;
import com.corpagos.corpunto.app.AppFirstLogin;
import com.corpagos.corpunto.commons.AnimationTransition;
import com.corpagos.corpunto.databases.BackupFiles;
import com.corpagos.corpunto.dialogs.Dialog;
import com.corpagos.corpunto.enums.PosModelType;
import com.corpagos.corpunto.interfaces.AsynctaskListener;
import com.corpagos.corpunto.locations.LocationService;
import com.corpagos.corpunto.models.ClientCredentials;
import com.corpagos.corpunto.models.FirstLogin;
import com.corpagos.corpunto.models.OwnerData;
import com.corpagos.corpunto.models.Response;
import com.corpagos.corpunto.models.Token;
import com.corpagos.corpunto.requests.Request;
import com.corpagos.corpunto.singletons.PosModel;
import com.corpagos.corpunto.utils.SharedPreferencesHandler;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

import androidx.appcompat.app.AppCompatActivity;

import static com.corpagos.corpunto.activities.MainLauncherActivity.version;
import static com.corpagos.corpunto.utils.ScrollToView.scrollToView;

public class LoginActivity extends AppCompatActivity implements AsynctaskListener {

    private EditText etUserName, etPass;
    private Dialog dialog;
    private String posSerial;
    private ClientCredentials clientCredentials;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        if (!getIntent().getStringExtra("posSerial").equals("")) {
            posSerial = getIntent().getStringExtra("posSerial");
        }
        initViews();
        stopService(new Intent(this, LocationService.class));
    }

    private void initViews() {
        dialog = new Dialog(this);
        etUserName = findViewById(R.id.et_username);
        etPass = findViewById(R.id.et_password);
        BootstrapButton btnLogin = findViewById(R.id.btn_login);
        btnLogin.setOnClickListener(v -> {
            if (validate()) {
                dialog.showProgressDialog("Ingresando");
                Request.requestLoginToken(etUserName.getText().toString(), etPass.getText().toString(),
                        this);
            }
        });
        etPass.setOnFocusChangeListener((v, hasFocus) -> scrollToView(findViewById(R.id.scrollView2),
                etPass));
        TextView tvHeader = findViewById(R.id.textView37);
        PosModel posModel = PosModel.getInstance(this);
        if (posModel.getPosModelType().equals(PosModelType.WPOS_MINI)) {
            tvHeader.setText("Mini V" + version);
        } else if (posModel.getPosModelType().equals(PosModelType.WPOS_3) ||
                posModel.getPosModelType().equals(PosModelType.Z90)) {
            tvHeader.setText("Plus V" + version);
        }
    }

    private boolean validate() {
        boolean isValid = true;
        if (etUserName.getText().toString().isEmpty()) {
            isValid = false;
            etUserName.setError("Introduzca un usuario");
        } else {
            etUserName.setError(null);
        }
        if (etPass.getText().toString().isEmpty()) {
            isValid = false;
            etPass.setError("Introduzca una contraseña");
        } else {
            etPass.setError(null);
        }
        return isValid;
    }

    @Override
    public void onAsynctaskFinished(String endPoint, String response, String headers) {
        System.out.println("Valores : login " + response + " " + endPoint);
        dialog.dismissDialog();
        if (!response.equals("")) {
            Response res = new Gson().fromJson(response, Response.class);
            if (!res.isError()) {
                if (res.getMessage().equals(Asynctask.OK_RESPONSE)) {
                    if (endPoint.contains(Asynctask.URL_AUTH)) {
                        if (!AppFirstLogin.isLogin(this)) {
                            Token token = new Gson().fromJson(new Gson().toJson(res.getData()), Token.class);
                            SharedPreferencesHandler.saveData(this,
                                    SharedPreferencesHandler.KEY_USER_TOKEN, new Gson().toJson(token),
                                    SharedPreferencesHandler.KEY_USER_TOKEN_FILE);
                            if (token.getAccess_token() != null) {
                                dialog.showProgressDialog("Ingresando...");
                                Request.requestFirstLogin(token, posSerial, this);
                            }
                        } else {
                            SharedPreferencesHandler.saveData(this, SharedPreferencesHandler.KEY_TOKEN,
                                    new Gson().toJson(res.getData()), SharedPreferencesHandler.FILE_TOKEN);
                            OwnerData ownerData = new OwnerData();
                            ownerData.setBank("Banesco");
                            ownerData.setPass("1234");
                            ownerData.setAddress("Maturin");
                            ownerData.setName("Farmacia SAAS");
                            ownerData.setTis("J-0402496401");
                            ownerData.setAffiliation("846584000103052");
                            ownerData.setLot(1);
                            BackupFiles.backupData(this, BackupFiles.ownerBackupFileName,
                                    new Gson().toJson(ownerData));
                            dialog.showSuccessDialogWithListenerButton(
                                    "El dispositivo se ha registrado satisfactoriamente",
                                    sweetAlertDialog -> {
                                        if (!clientCredentials.getAvailable_password().isEmpty()) {
                                            dialog.dismissDialog();
                                            Intent intent = new Intent(this, PosCodeActivity.class);
                                            intent.putExtra("passwords", new Gson().toJson(
                                                    clientCredentials));
                                            startActivity(intent);
                                            AnimationTransition.setInActivityTransition(this);
                                            finish();
                                        } else if (clientCredentials.getAvailable_password().isEmpty()) {
                                            finish();
                                        }

                                    });
                        }
                    } else if (endPoint.contains(Asynctask.URL_FIRST_LOGIN)) {
                        System.out.println("valores : res " + res.getData());
                        clientCredentials = new Gson().fromJson(new Gson().toJson(
                                res.getData()),ClientCredentials.class);
                        clientCredentials.setSerial(posSerial);
                        FirstLogin firstLogin = new FirstLogin();
                        firstLogin.setClient_id(clientCredentials.getClient_id());
                        firstLogin.setClient_secret(clientCredentials.getClient_secret());
                        firstLogin.setLogin(true);
                        firstLogin.setSerial(posSerial);
                        BackupFiles.backupData(this, BackupFiles.loginBackupFileName,
                                new Gson().toJson(firstLogin));
                        requestLogin(clientCredentials);
                    }
                }
            } else {
                switch (res.getMessage()) {
                    case "ERR_INVALID_OR_EXPIRED_TOKEN":
                        dialog.showErrorDialogWithListenerButton(
                                "Problema de autorizacion, ingrese sus datos nuevamente " +
                                        "las credenciales",
                                sweetAlertDialog -> {
                                    dialog.dismissDialog();
                                    AppCredentials.deleteToken(this);
                                });
                        break;
                    case "Unsupported grant type: `grant_type` is invalid":
                        dialog.showErrorDialog("Error de verificacion");
                        break;
                    case "ERR_INVALID_CREDENTIALS":
                        dialog.showErrorDialog("Error en las credenciales");
                        break;
                    case "ERR_INACTIVE_USER":
                        dialog.showErrorDialog("Usuario inactivo");
                        break;
                    case "ERR_UNVERIFIED_USER_EMAIL":
                        dialog.showErrorDialog("Error email no validado");
                        break;
                    case "ERR_UNVERIFIED_USER_PHONE":
                        dialog.showErrorDialog("Error numero no validado");
                        break;
                    case "ERR_LOGIN_BLOCKED":
                        dialog.showErrorDialog("Usuario bloqueado");
                        break;
                    case "ERR_VALIDATION_FAILED":
                        dialog.showErrorDialog("Error verificacion fallida");
                        break;
                    case "ERR_ALREADY_LOGGED_IN":
                        dialog.showErrorDialog("Cliente ya registrado");
                        break;
                    case "ERR_UNKNOWN_CLIENT":
                        dialog.showErrorDialog("Cliente no registrado");
                        break;
                    case "ERR_ALREADY_LINKED_DEVICE":
                        dialog.showErrorDialog("Dispositivo ya registrado");
                        break;
                    default:
                        dialog.showErrorDialog(Global.messageResponse(res.getMessage()));
                        break;
                }
            }
        } else {
            dialog.showErrorDialog("Algo salio mal");
        }
    }

    private void requestLogin(ClientCredentials clientCredentials) {
        dialog.showProgressDialog("Iniciando sesion ... ");
        Map<String, Object> data = new HashMap<>();
        data.put("username", "null");
        data.put("password", "null");
        data.put("grant_type", "client_credentials");
        data.put("client_id", clientCredentials.getClient_id());
        data.put("client_secret", clientCredentials.getClient_secret());
        Map<String, String> headers = new HashMap<>();
        String token = SharedPreferencesHandler.getSavedData(this,
                SharedPreferencesHandler.KEY_FB_TOKEN, SharedPreferencesHandler.KEY_FB_TOKEN_FILE);
        if (token.equals(SharedPreferencesHandler.NOT_FOUND)) {
            token = "";
        }
        new Asynctask.PostFormMethodAsynctask(Asynctask.authHost, data, Asynctask.URL_AUTH,
                headers, this).execute();
    }
}
