package com.corpagos.corpunto.activities;

import android.app.Application;
import android.content.Context;

import com.zcs.sdk.DriverManager;
import com.zcs.sdk.card.CardInfoEntity;

public class MyApp extends Application {
    public static DriverManager sDriverManager;
    public  static CardInfoEntity cardInfoEntity;
    public static Context context;
    @Override
    public void onCreate() {
        super.onCreate();
        sDriverManager = DriverManager.getInstance();
        cardInfoEntity = new CardInfoEntity();
        context = getApplicationContext();
    }
}
