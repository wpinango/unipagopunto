package com.corpagos.corpunto.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;

import com.corpagos.corpunto.R;
import com.corpagos.corpunto.adapters.SectionsPagerAdapter;

import com.corpagos.corpunto.utils.Keyboard;
import com.google.android.material.tabs.TabLayout;

import androidx.appcompat.widget.SearchView;
import androidx.core.view.MenuItemCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.viewpager.widget.ViewPager;

public class TransactionHistoryActivity1 extends BaseActivity {

    public static final String ACTION_INTENT = "transaction.closure.UI_UPDATE";
    private SearchView searchView;
    private SectionsPagerAdapter sectionsPagerAdapter;
    private ViewPager viewPager;
    public static int rankInit = 0, rankEnd = 7;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_transaction_history1;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        setToolbarTitle("Registros");
        try {
            sectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
            viewPager = findViewById(R.id.vp_transactions);
            viewPager.setAdapter(sectionsPagerAdapter);
            TabLayout tabLayout = findViewById(R.id.tl_transactions);
            tabLayout.setupWithViewPager(viewPager);
            IntentFilter filter = new IntentFilter(ACTION_INTENT);
            LocalBroadcastManager.getInstance(this).registerReceiver(receiver, filter);
        } catch (Exception e) {
            System.out.println("valores : " + e.getMessage());
        }
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (ACTION_INTENT.equals(intent.getAction())) {
                sectionsPagerAdapter.getTransactionStorage();
            }
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            Keyboard.hideKeyboard(this);
            unregisterReceiver(receiver);
        } catch (Exception e) {
            e.getMessage();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_item_transaction, menu);
        MenuItem searchItem = menu.findItem(R.id.menu_item_transaction_search);
        searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setInputType(InputType.TYPE_CLASS_NUMBER);
        searchView.setQueryHint("Referencia");
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchView.setQuery("", false);
                searchView.setIconified(true);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                switch (viewPager.getCurrentItem()) {
                    case 0:
                        sectionsPagerAdapter.getSellsAdapter().getFilter().filter(newText);
                        break;
                    case 1:
                        sectionsPagerAdapter.getServicesPaymentAdapter().getFilter().filter(newText);
                        break;
                }
                return true;
            }
        });
        searchView.setOnCloseListener(() -> {
            switch (viewPager.getCurrentItem()) {
                case 0:
                    sectionsPagerAdapter.getSellsAdapter().update();
                    sectionsPagerAdapter.getSellsAdapter().notifyDataSetChanged();
                    break;
                case 1:
                    sectionsPagerAdapter.getServicesPaymentAdapter().update();
                    sectionsPagerAdapter.getServicesPaymentAdapter().notifyDataSetChanged();
                    break;
            }
            return false;
        });
        return true;
    }
}
