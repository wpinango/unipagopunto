package com.corpagos.corpunto.activities;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.view.MenuItem;

import com.corpagos.corpunto.Asynctask;
import com.corpagos.corpunto.Global;
import com.corpagos.corpunto.commons.AnimationTransition;
import com.corpagos.corpunto.databases.BackupFiles;
import com.corpagos.corpunto.dialogs.Dialog;
import com.corpagos.corpunto.interfaces.AsynctaskListener;
import com.corpagos.corpunto.logs.CustomizedExceptionHandler;
import com.corpagos.corpunto.models.ClientCredentials;
import com.corpagos.corpunto.models.OwnerData;
import com.corpagos.corpunto.models.Response;
import com.corpagos.corpunto.utils.SharedPreferencesHandler;
import com.google.gson.Gson;
import com.google.zxing.Result;

import java.util.HashMap;
import java.util.Map;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class QRCodeReaderActivity extends AppCompatActivity implements AsynctaskListener,
        ZXingScannerView.ResultHandler {

    private ZXingScannerView scannerView;
    private Dialog dialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        qrScanner();
        dialog = new Dialog(this);
        Thread.setDefaultUncaughtExceptionHandler(new CustomizedExceptionHandler());
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        AnimationTransition.setOutActivityTransition(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            this.finish();
            AnimationTransition.setOutActivityTransition(this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStop() {
        super.onStop();
        scannerView.stopCamera();
    }

    @Override
    public void onAsynctaskFinished(String endPoint, String response, String headers) {
        dialog.dismissDialog();
        System.out.println("valores : " + response);
        if (!response.equals("") && !response.equals(Asynctask.TIMEOUT_MESSAGE) &&
                !response.equals(Asynctask.FAILED_TO_CONNECT)) {
            Response res = new Gson().fromJson(response, Response.class);
            if (!res.isError() && res.getMessage().equals(Asynctask.OK_RESPONSE)) {

                SharedPreferencesHandler.saveData(this, SharedPreferencesHandler.KEY_TOKEN,
                        new Gson().toJson(res.getData()), SharedPreferencesHandler.FILE_TOKEN);
                OwnerData ownerData = new OwnerData();
                ownerData.setBank("Banesco");
                ownerData.setPass("1234");
                ownerData.setAddress("Maturin");
                ownerData.setName("Farmacia SAAS");
                ownerData.setTis("J-0402496401");
                ownerData.setAffiliation("846584000103052");
                ownerData.setLot(1);
                BackupFiles.backupData(this, BackupFiles.ownerBackupFileName,
                        new Gson().toJson(ownerData));
                dialog.showSuccessDialogWithListenerButton(
                        "El dispositivo se ha registrado satisfactoriamente",
                        sweetAlertDialog -> QRCodeReaderActivity.this.finish());
            } else {
                switch (res.getMessage()) {
                    case "ERR_INVALID_OR_EXPIRED_TOKEN":
                        dialog.showErrorDialogWithListenerButton(
                                "Problema de autorizacion, ingrese sus datos nuevamente " +
                                        "las credenciales",
                                sweetAlertDialog -> {
                                    dialog.dismissDialog();
                                    SharedPreferencesHandler.saveData(this,
                                            SharedPreferencesHandler.KEY_TOKEN,
                                            SharedPreferencesHandler.NOT_FOUND,
                                            SharedPreferencesHandler.FILE_TOKEN);
                                    BackupFiles.writeFiles(this, BackupFiles.configBackupFileName,
                                            "");
                                });
                        break;
                    case "Unsupported grant type: `grant_type` is invalid":
                        dialog.showErrorDialog("Error de verificacion");
                        break;
                    case "ERR_INVALID_CREDENTIALS":
                        dialog.showErrorDialog("Error en las credenciales");
                        break;
                    case "ERR_INACTIVE_USER":
                        dialog.showErrorDialog("Usuario inactivo");
                        break;
                    case "ERR_UNVERIFIED_USER_EMAIL":
                        dialog.showErrorDialog("Error email no validado");
                        break;
                    case "ERR_UNVERIFIED_USER_PHONE":
                        dialog.showErrorDialog("Error numero no validado");
                        break;
                    case "ERR_LOGIN_BLOCKED":
                        dialog.showErrorDialog("Usuario bloqueado");
                        break;
                    case "ERR_VALIDATION_FAILED":
                        dialog.showErrorDialog("Error verificacion fallida");
                        break;
                    case "ERR_ALREADY_LOGGED_IN":
                        dialog.showErrorDialog("Cliente ya registrado");
                        break;
                    case "ERR_UNKNOWN_CLIENT":
                        dialog.showErrorDialog("Cliente no registrado");
                        BackupFiles.resetFirstLogin(this);
                        break;
                    default:
                        dialog.showErrorDialog(Global.messageResponse(res.getMessage()));
                        break;
                }
                resetScanner();
            }
        } else if (response.equals(Asynctask.TIMEOUT_MESSAGE)) {
            dialog.showErrorDialog("Error de conexion");
            resetScanner();
        } else if (response.equals(Asynctask.FAILED_TO_CONNECT)) {
            dialog.showErrorDialog("Error de conexion");
            resetScanner();
        }
    }

    @Override
    public void handleResult(Result result) {
        Global.showToast(this, result.toString(), 4000);
        try {
            ClientCredentials clientCredentials = new Gson().fromJson(result.toString(),
                    ClientCredentials.class);
            if (clientCredentials.getSerial().equals(MainLauncherActivity.posSerial)) {
                requestLogin(new Gson().fromJson(result.toString(), ClientCredentials.class));
            } else {
                dialog.showErrorDialogWithListenerButton("Dispositivo no corresponde",
                        sweetAlertDialog -> dialog.dismissDialog());
                resetScanner();
            }
        } catch (Exception e) {
            dialog.showErrorDialog("Algo salio mal");
        }
    }

    private void resetScanner() {
        scannerView.stopCamera();
        scannerView.setResultHandler(this);
        scannerView.startCamera();
    }

    private void requestLogin(ClientCredentials clientCredentials) {
        dialog.showProgressDialog("Iniciando sesion ... ");
        Map<String, Object> data = new HashMap<>();
        data.put("username", "null");
        data.put("password", "null");
        data.put("grant_type","client_credentials");
        data.put("client_id", clientCredentials.getClient_id()); //dev_pos
        data.put("client_secret", clientCredentials.getClient_secret());
        Map<String, String> headers = new HashMap<>();
        String token = SharedPreferencesHandler.getSavedData(this,
                SharedPreferencesHandler.KEY_FB_TOKEN, SharedPreferencesHandler.KEY_FB_TOKEN_FILE);
        if (token.equals(SharedPreferencesHandler.NOT_FOUND)) {
            token = "";
        }
        System.out.println("valores : " + data + " " + new Gson().toJson(token) + " " + Asynctask.URL_AUTH);
        new Asynctask.PostFormMethodAsynctask(Asynctask.authHost, data, Asynctask.URL_AUTH +
                "?FirebaseToken=" + token, headers, this).execute();
    }

    public void qrScanner() {
        scannerView = new ZXingScannerView(this);
        setContentView(scannerView);
        scannerView.setResultHandler(this);
        scannerView.startCamera();
    }
}

