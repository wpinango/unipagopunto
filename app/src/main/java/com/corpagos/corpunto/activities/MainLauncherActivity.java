package com.corpagos.corpunto.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.corpagos.corpunto.Asynctask;
import com.corpagos.corpunto.Global;
import com.corpagos.corpunto.R;
import com.corpagos.corpunto.app.AppCredentials;
import com.corpagos.corpunto.app.AppFirstLogin;
import com.corpagos.corpunto.app.AppVersion;
import com.corpagos.corpunto.commons.AnimationTransition;
import com.corpagos.corpunto.databases.BackupFiles;
import com.corpagos.corpunto.databases.TransactionDBHelper;
import com.corpagos.corpunto.dialogs.Dialog;
import com.corpagos.corpunto.enums.PosModelType;
import com.corpagos.corpunto.enums.RoleType;
import com.corpagos.corpunto.interfaces.AsynctaskListener;
import com.corpagos.corpunto.interfaces.DialogInterface;
import com.corpagos.corpunto.locations.LocationService;
import com.corpagos.corpunto.models.Counters;
import com.corpagos.corpunto.models.Keys;
import com.corpagos.corpunto.models.Response;
import com.corpagos.corpunto.models.Transaction;
import com.corpagos.corpunto.models.TransactionLocation;
import com.corpagos.corpunto.models.UpdateApp;
import com.corpagos.corpunto.pos.PosDevice;
import com.corpagos.corpunto.requests.Request;
import com.corpagos.corpunto.singletons.NotificationHandler;
import com.corpagos.corpunto.singletons.PosMode;
import com.corpagos.corpunto.singletons.PosModel;
import com.corpagos.corpunto.singletons.Role;
import com.corpagos.corpunto.singletons.TimerTask;
import com.corpagos.corpunto.utils.SharedPreferencesHandler;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import static com.corpagos.corpunto.Global.chipCount;
import static com.corpagos.corpunto.Global.nfcCount;
import static com.corpagos.corpunto.Global.printCount;
import static com.corpagos.corpunto.Global.printLinesCount;
import static com.corpagos.corpunto.Global.timeCount;
import static com.corpagos.corpunto.activities.MainActivity.deserializeUpdateModel;
import static com.corpagos.corpunto.activities.NotificationActivity.isNotification;
import static com.corpagos.corpunto.activities.NotificationActivity.isUpdateNotification;
import static com.corpagos.corpunto.app.AppVersion.getAppVersion;
import static com.corpagos.corpunto.locations.LocationService.BROADCAST_ACTION;
import static com.corpagos.corpunto.services.MyFirebaseMessagingService.ACTION_INTENT;
import static com.corpagos.corpunto.utils.SharedPreferencesHandler.getCount;
import static com.corpagos.corpunto.utils.SharedPreferencesHandler.getUseTime;
import static com.corpagos.corpunto.utils.SharedPreferencesHandler.saveCount;
import static com.corpagos.corpunto.utils.SharedPreferencesHandler.saveUseTime;

public class MainLauncherActivity extends AppCompatActivity implements AsynctaskListener {

    private TransactionDBHelper dbHelper;
    private TransactionLocation transactionLocation;
    public static String version;
    public static String posSerial;
    private int adminCount = 10;
    private TextView tvTime;
    private Dialog dialog;
    private PosDevice posDevice;
    private PosModel posModel;
    private Intent locationIntent;
    private TextView tvBadgeNotification;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        posDevice = new PosDevice(this);
        posModel = PosModel.getInstance(this);
        if (posModel.getPosModelType().equals(PosModelType.WPOS_MINI)) {
            setContentView(R.layout.activity_main_launcher_mini1);
        } else {
            setContentView(R.layout.activity_main_launcher1);
        }
        initViews();
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    protected void onResume() {
        super.onResume();
        PosMode posMode = PosMode.getInstance(this);
        posMode.setDeFaultMode();
        System.out.println("valores : " + version + " " + getAppVersion(this));
        if (!AppVersion.checkCurrentInstalledVersion(version, getAppVersion(this))) {
            dialog.showErrorDialogWithListenerButton("Ocurrio un error al momento de la " +
                    "actualizacion, por favor descargue nuevamente", sweetAlertDialog -> {
                openUpdateActivity(this);
                dialog.dismissDialog();
            });
        }
        checkFirstLogin();
        checkNotifications();
        checkAutomaticDateConfig();
        IntentFilter filterNotifications = new IntentFilter(ACTION_INTENT);
        IntentFilter filterLocation = new IntentFilter(BROADCAST_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(receiverNotification,
                filterNotifications);
        LocalBroadcastManager.getInstance(this).registerReceiver(locationReceiver,
                filterLocation);
        if (isNotification) {
            checkNotificationType();
        }
        checkAESKeys();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (receiverNotification != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(receiverNotification);
        }
        if (locationReceiver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(locationReceiver);
        }
        stopService(new Intent(this, LocationService.class));
    }

    private void initViews() {
        NotificationHandler notificationHandler = NotificationHandler.getInstance(this);
        dbHelper = new TransactionDBHelper(this);
        locationIntent = new Intent(this, LocationService.class);
        tvBadgeNotification = findViewById(R.id.badge_notification);
        getCurrentLocation();
        progressBar = findViewById(R.id.progressBar2);
        progressBar.setVisibility(View.INVISIBLE);
        dialog = new Dialog(this);
        tvTime = findViewById(R.id.tv_time);
        TextView tvHeader = findViewById(R.id.textView33);
        tvHeader.setOnClickListener(view -> {
            adminCount--;
            if (adminCount <= 4 && adminCount >= 1) {
                Global.showToast(MainLauncherActivity.this, "Esta a " + adminCount +
                        " pasos de abrir el modulo de administrador", 400);
            }
            if (adminCount == 0) {
                dialog.showAdminCredentialsDialog(new DialogInterface() {
                    @Override
                    public void positiveClick() {
                        Role role = Role.getInstance(MainLauncherActivity.this);
                        role.setRoleType(RoleType.ADMIN);
                        Intent intent = new Intent(MainLauncherActivity.this,
                                AdminActivity.class);
                        startActivity(intent);
                        AnimationTransition.setInActivityTransition(MainLauncherActivity.this);
                    }

                    @Override
                    public void negativeClick() {
                        dialog.dismissDialog();
                    }
                });
                adminCount = 10;
            }
        });
        findViewById(R.id.button6).setOnClickListener(v -> {
            Intent intent = new Intent(MainLauncherActivity.this, SettingsActivity.class);
            startActivity(intent);
            AnimationTransition.setInActivityTransition(this);
        });
        findViewById(R.id.button13).setOnClickListener(v -> {
            Intent intent = new Intent(MainLauncherActivity.this,
                    PrepaidServiceActivity.class);
            startActivity(intent);
            AnimationTransition.setInActivityTransition(this);
        });
        findViewById(R.id.button).setOnClickListener(v -> {
            Intent intent = new Intent(MainLauncherActivity.this, MainActivity.class);
            intent.putExtra("location", transactionLocation != null ?
                    new Gson().toJson(transactionLocation) : "");
            startActivity(intent);
            AnimationTransition.setInActivityTransition(this);
        });
        findViewById(R.id.button4).setOnClickListener(v -> {
            //dialog.showProgressDialog("Cargando....");
            /*progressBar.setVisibility(View.VISIBLE);
            Handler handler = new Handler();
            handler.postDelayed(() -> {
                Intent intent = new Intent(MainLauncherActivity.this,
                        TransactionHistoryActivity1.class);
                startActivity(intent);
                AnimationTransition.setInActivityTransition(this);
                progressBar.setVisibility(View.INVISIBLE);
                //dialog.dismissDialog();
            }, 500);*/
            Intent intent = new Intent(MainLauncherActivity.this,
                    TransactionHistoryActivity1.class);
            startActivity(intent);
            AnimationTransition.setInActivityTransition(this);
        });
        checkVersionBackup();
        checkTransactionBackup();
        checkCounterBackup();
        TimerTask timerTask = TimerTask.getInstance(this);
        if (!timerTask.isThreadAlive()) {
            if (timeCount == 0) {
                timeCount = getUseTime(this, Global.keyTimeCount);
            }
            timerTask.startThread();
        }
        showTime();
        if (posModel.getPosModelType().equals(PosModelType.WPOS_MINI)) {
            tvHeader.setText("Mini V" + version);
        } else if (posModel.getPosModelType().equals(PosModelType.WPOS_3) ||
                posModel.getPosModelType().equals(PosModelType.Z90)) {
            tvHeader.setText("Plus V" + version);
        }
        System.out.println("valores : " + BackupFiles.getBackupData(this, BackupFiles.loginBackupFileName));

    }

    private void getCurrentLocation() {
        startService(locationIntent);
    }

    private void checkFirstLogin() {
        if (posSerial == null) {
            posSerial = posModel.getPosInformation().getPosSerial();
        }
        if (!AppFirstLogin.isLogin(this)) {
            Intent intent = new Intent(this, LoginActivity.class);
            intent.putExtra("posSerial", posSerial);
            startActivity(intent);
            AnimationTransition.setInActivityTransition(this);
        } else {
            checkTokenBackup();
        }
    }

    private void checkTransactionBackup() {
        ArrayList<Transaction> transactionHistoriesTemp = dbHelper.getTransactionList();
        if (transactionHistoriesTemp.isEmpty()) {
            Type custom = new TypeToken<ArrayList<Transaction>>() {
            }.getType();
            try {
                if (BackupFiles.isFilesExist(this, BackupFiles.transactionsBackupFileName)) {
                    transactionHistoriesTemp = new Gson().fromJson(BackupFiles.readFileContent(
                            this, BackupFiles.transactionsBackupFileName), custom);
                    for (Transaction t : transactionHistoriesTemp) {
                        dbHelper.insertTransaction(t);
                    }
                }
            } catch (Exception e) {
                e.getMessage();
            }
        }
    }

    private void checkVersionBackup() {
        if (BackupFiles.isFilesExist(this, BackupFiles.versionBackupFileName)) {
            if (!BackupFiles.readFileContent(this, BackupFiles.versionBackupFileName)
                    .equals("")) {
                version = BackupFiles.readFileContent(this,
                        BackupFiles.versionBackupFileName);
            }
        } else {
            BackupFiles.createFiles(this, BackupFiles.versionBackupFileName, "1.0.0");
            version = BackupFiles.readFileContent(this, BackupFiles.versionBackupFileName);
        }
    }

    private void checkTokenBackup() {
        dialog.dismissDialog();
        if (SharedPreferencesHandler.getSavedData(this, SharedPreferencesHandler.KEY_TOKEN,
                SharedPreferencesHandler.FILE_TOKEN).equals(SharedPreferencesHandler.NOT_FOUND)) {
            if (BackupFiles.isFilesExist(this, BackupFiles.configBackupFileName)) {
                if (!BackupFiles.readFileContent(this, BackupFiles.configBackupFileName)
                        .equals("")) {
                    SharedPreferencesHandler.saveData(this, SharedPreferencesHandler.KEY_TOKEN,
                            new Gson().toJson(BackupFiles.readFileContent(this,
                                    BackupFiles.configBackupFileName)),
                            SharedPreferencesHandler.KEY_TOKEN);
                }
            }
            showWithoutCredentialsDialog();
        } else {
            System.out.println("valores : " + SharedPreferencesHandler.getSavedData(this,
                    SharedPreferencesHandler.KEY_TOKEN, SharedPreferencesHandler.FILE_TOKEN));
        }
    }

    private void checkCounterBackup() {
        if (getCount(this, Global.keyNfcCount) == 0 && getCount(this,
                Global.keyChipCount) == 0 && getCount(this, Global.keyPrintCount) == 0) {
            if (BackupFiles.isFilesExist(this, BackupFiles.counterBackupFileName)) {
                Counters countReaders = new Gson().fromJson(BackupFiles.readFileContent(this,
                        BackupFiles.counterBackupFileName), Counters.class);
                nfcCount = countReaders.getNfcCount();
                chipCount = countReaders.getChipCount();
                printCount = countReaders.getPrintCount();
                timeCount = countReaders.getTimeCount();
                printLinesCount = countReaders.getPrintLinesCount();
                saveCount(this, Global.keyPrintCount, printCount);
                saveCount(this, Global.keyChipCount, chipCount);
                saveCount(this, Global.keyNfcCount, nfcCount);
                saveUseTime(this, Global.keyTimeCount, timeCount);
                saveCount(this, Global.keyPrintLinesCount, printLinesCount);
            } else {
                nfcCount = getCount(this, Global.keyNfcCount);
                chipCount = getCount(this, Global.keyChipCount);
                printCount = getCount(this, Global.keyPrintCount);
                timeCount = getUseTime(this, Global.keyTimeCount);
                printLinesCount = getCount(this, Global.keyPrintLinesCount);
            }
        } else {
            nfcCount = getCount(this, Global.keyNfcCount);
            chipCount = getCount(this, Global.keyChipCount);
            printCount = getCount(this, Global.keyPrintCount);
            printLinesCount = getCount(this, Global.keyPrintLinesCount);
            timeCount = getUseTime(this, Global.keyTimeCount);
        }
    }

    public static void openUpdateActivity(Activity fromActivity) {
        isUpdateNotification = false;
        Intent intent1 = new Intent(fromActivity, UpdateAppActivity.class);
        fromActivity.startActivity(intent1);
        AnimationTransition.setInActivityTransition(fromActivity);
    }

    private void showWithoutCredentialsDialog() {
        dialog.showInfoDialogWithListenerButtonsAndText("Aun no ha iniciado sesion," +
                        " por favor escanear credenciales para utilizar el dispositivo POS",
                (materialDialog, which) -> {
                    Intent intent = new Intent(MainLauncherActivity.this,
                            QRCodeReaderActivity.class);
                    startActivity(intent);
                    dialog.dismissDialog();
                }, (materialDialog, which) -> dialog.dismissDialog(),
                "Ingresar", "Cancelar");
    }

    private void showTime() {
        final Handler someHandler = new Handler(getMainLooper());
        someHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Date date = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy - HH:mm:ss a");
                sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
                sdf.setTimeZone(TimeZone.getTimeZone("America/Manaus"));
                tvTime.setText(sdf.format(date).toUpperCase());
                someHandler.postDelayed(this, 1000);
            }
        }, 10);
    }

    private void checkNotifications() {
        if (isNotification) {
            tvBadgeNotification.setVisibility(View.VISIBLE);
        } else {
            tvBadgeNotification.setVisibility(View.INVISIBLE);
        }
    }

    private void checkNotificationType() {
        if (isUpdateNotification) {
            dialog.dismissDialog();
            showUpdateDialog(new Gson().fromJson(SharedPreferencesHandler.getSavedData(
                    this, SharedPreferencesHandler.KEY_UPDATE,
                    SharedPreferencesHandler.KEY_UPDATE_FILE), UpdateApp.class));
        }
    }

    protected BroadcastReceiver receiverNotification = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (ACTION_INTENT.equals(intent.getAction())) {
                String value = intent.getStringExtra("UI_KEY");
                checkNotifications();
                showUpdateDialog(deserializeUpdateModel(value));
            }
        }
    };

    private BroadcastReceiver locationReceiver = new BroadcastReceiver() {
        @SuppressLint("WrongConstant")
        @Override
        public void onReceive(Context context, Intent intent) {
            if (BROADCAST_ACTION.equals(intent.getAction())) {
                transactionLocation = new TransactionLocation();
                transactionLocation.setLatitude(String.valueOf(intent.getDoubleExtra(
                        "Latitude", 0)));
                transactionLocation.setLongitude(String.valueOf(intent.getDoubleExtra(
                        "Longitude", 0)));
                stopService(new Intent(MainLauncherActivity.this, LocationService.class));
            }
        }
    };

    private void showUpdateDialog(UpdateApp updateApp) {
        dialog.dismissDialog();
        dialog.showUpdateAppDialog(updateApp.getChangeLog(), new DialogInterface() {
            @Override
            public void positiveClick() {
                openUpdateActivity(MainLauncherActivity.this);
            }

            @Override
            public void negativeClick() {
                isUpdateNotification = false;
            }
        });
    }

    private void checkAutomaticDateConfig() {
        /*if (!isTimeAutomatic(this)) {
            dialog.showErrorDialogWithListenerButton(
                    "Active la fecha y hora automatica para continuar",
                    sweetAlertDialog -> {
                        startActivity(new Intent(Settings.ACTION_DATE_SETTINGS));
                        AnimationTransition.setInActivityTransition(this);
                    });
        }*/
    }

    public void checkAESKeys() {
        if (!Keys.isKeys(this)) {
            if (AppCredentials.isLogin(this)) {
                Request.getRequestWithToken(this, Asynctask.transactionalHost,
                        Asynctask.URL_GET_RSA_KEY, new HashMap<>(), this);
                dialog.showProgressDialog("Configurando...");
            }
        }
    }

    @Override
    public void onAsynctaskFinished(String endPoint, String response, String headers) {
        dialog.dismissDialog();
        System.out.println("valores key response : " + response);
        if (!response.equals("")) {
            Response res = new Gson().fromJson(response, Response.class);
            if (!res.isError()) {
                if (endPoint.equals(Asynctask.URL_GET_RSA_KEY)) {
                    try {
                        dialog.showProgressDialog("Configurando...");
                        Map<String,String> publicKey = (Map<String, String>) res.getData();
                        Log.d("valores : ", publicKey.get("public_key") + "");
                        if (publicKey.get("public_key") != null) {

                            Request.postRequestToken(this, Asynctask.transactionalHost,
                                    Asynctask.URL_STORE_AES,
                                    Asynctask.getEncryptedRSARequest(this, publicKey.get("public_key")),
                                    new HashMap<>(), this);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (endPoint.equals(Asynctask.URL_STORE_AES)) {
                    dialog.showSuccessDialog("Se configuro el dispositivo correctamente");
                }
            } else {
                switch (res.getMessage()) {
                    case "ERR_POS_AES_KEYS_NOT_CONFIGURED":
                        dialog.showErrorDialogWithListenerButton(
                                "Dispositivo no se encuentra configurado",
                                sweetAlertDialog -> {
                                    dialog.dismissDialog();
                                    SharedPreferencesHandler.saveData(this,
                                            SharedPreferencesHandler.KEY_AES_KEY,
                                            SharedPreferencesHandler.NOT_FOUND,
                                            SharedPreferencesHandler.FILE_AES_KEY
                                    );
                                });
                        break;
                    case "ERR_INVALID_CREDENTIALS":
                        dialog.showErrorDialog("Error en las credenciales");
                        break;
                    case "ERR_INVALID_OR_EXPIRED_TOKEN":
                    case Global.ERR_UNKNOWN_DEVICE_SERIAL:
                        dialog.showErrorDialogWithListenerButton(
                                "Dispositivo no registrado, registrar nuevamente",
                                sweetAlertDialog -> {
                                    dialog.dismissDialog();
                                    SharedPreferencesHandler.saveData(this,
                                            SharedPreferencesHandler.KEY_TOKEN,
                                            SharedPreferencesHandler.NOT_FOUND,
                                            SharedPreferencesHandler.FILE_TOKEN);
                                    BackupFiles.writeFiles(this,
                                            BackupFiles.configBackupFileName, "");
                                });
                        break;
                    default:
                        dialog.showErrorDialog("No se pudo configurar el dispositivo");
                        break;
                }
            }
        } else {
            dialog.showErrorDialog("No se pudo configurar el dispositivo");
        }
    }


}
