package com.corpagos.corpunto.activities;

import android.app.FragmentTransaction;
import android.os.Bundle;

import com.corpagos.corpunto.R;
import com.corpagos.corpunto.fragments.firstlogin.SecurityPosCodeFragment;
import com.corpagos.corpunto.fragments.firstlogin.SpecialPosCodeFragment;
import com.corpagos.corpunto.interfaces.KeyListener;
import com.corpagos.corpunto.models.ClientCredentials;
import com.corpagos.corpunto.models.Password;
import com.corpagos.corpunto.widgets.CustomKeyboard;
import com.google.gson.Gson;

import java.util.ArrayList;

public class PosCodeActivity extends BaseActivity implements KeyListener {

    private FragmentTransaction fragTransaction;
    private CustomKeyboard keyboard;
    private SecurityPosCodeFragment securityPosCodeFragment = new SecurityPosCodeFragment();
    private SpecialPosCodeFragment specialPosCodeFragment = new SpecialPosCodeFragment();
    private ArrayList<Password> passwords;
    private boolean pinCode = false, altPinCode = false;
    private final String SECURITY_TAG = "security", SPECIAL_TAG = "special";

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_pos_code;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        keyboard = findViewById(R.id.kb_pos_code1);
        keyboard.setListener(this);
        if (!getIntent().getStringExtra("passwords").equals("")) {
            ClientCredentials clientCredentials = new Gson().fromJson(getIntent().getStringExtra(
                    "passwords"), ClientCredentials.class);
            passwords = clientCredentials.getAvailable_password();
        }
        for (Password p : passwords) {
            if (p.getName().equals("pin_code")) {
                pinCode = true;
            } else if (p.getName().equals("alt_pin_code")) {
                altPinCode = true;
            }
        }
        showSecurityPosCodeFragment();
    }

    private void showSecurityPosCodeFragment() {
        if (pinCode) {
            securityPosCodeFragment.setKeyboard(keyboard);
            fragTransaction = getFragmentManager().beginTransaction();
            fragTransaction.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
            fragTransaction.replace(R.id.fl_pos_code, securityPosCodeFragment, SECURITY_TAG);
            fragTransaction.commit();
        } else if (altPinCode) {
            showSpecialPosCodeFragment();
        } else {
            finish();
        }
    }

    public void showSpecialPosCodeFragment() {
        if (altPinCode) {
            specialPosCodeFragment.setKeyboard(keyboard);
            fragTransaction = getFragmentManager().beginTransaction();
            fragTransaction.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
            fragTransaction.replace(R.id.fl_pos_code, specialPosCodeFragment, SPECIAL_TAG);
            fragTransaction.commit();
        } else {
            finish();
        }
    }

    public void setToolbarTitle(String title) {
        setTitle(title);
    }

    private boolean isSpecialPosCodeFragmentVisible() {
        return getFragmentManager().findFragmentByTag(SPECIAL_TAG) != null &&
                getFragmentManager().findFragmentByTag(SPECIAL_TAG).isVisible();
    }

    private boolean isSecurityPosCodeFragmentVisible() {
        return getFragmentManager().findFragmentByTag(SECURITY_TAG) != null &&
                getFragmentManager().findFragmentByTag(SECURITY_TAG).isVisible();
    }

    @Override
    public void onKeyPressed(Enum specialKey) {
        if (specialKey == KeyListener.specialKey.CANCEL) {

        } else if (specialKey == KeyListener.specialKey.DONE) {
            if (isSecurityPosCodeFragmentVisible()) {
                switch (securityPosCodeFragment.getItemFocus()) {
                    case R.id.et_pos_code2:
                        securityPosCodeFragment.setEtRepeatPosCodeFocus();
                        break;
                    case R.id.et_repeat_pos_code2:
                        if (securityPosCodeFragment.validate()) {
                            securityPosCodeFragment.requestChangePosCode();
                        }
                        break;
                }
            } else if (isSpecialPosCodeFragmentVisible()) {
                switch (specialPosCodeFragment.getItemFocus()) {
                    case R.id.et_pos_code2:
                        specialPosCodeFragment.setEtRepeatPosCodeFocus();
                        break;
                    case R.id.et_repeat_pos_code2:
                        if (specialPosCodeFragment.validate()) {
                            specialPosCodeFragment.requestChangePosCode();
                        }
                        break;
                }
            }
        }
    }
}
