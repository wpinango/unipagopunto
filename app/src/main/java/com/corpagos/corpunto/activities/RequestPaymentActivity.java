package com.corpagos.corpunto.activities;

import android.os.Bundle;
import android.os.CountDownTimer;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.corpagos.corpunto.R;
import com.corpagos.corpunto.dialogs.Dialog;

public class RequestPaymentActivity extends AppCompatActivity {

    private Dialog dialog;
    private CountDownTimer timer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_payment);
        initViews();
        if (!getIntent().getStringExtra("payment").equals("")) {
            dialog.showProgressDialog("Procesando pago...");
            initTimerOperation();
        }
    }

    private void initViews() {
        Toolbar toolbar = findViewById(R.id.toolbar6);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle("Peticion de pago");
        }
        dialog = new Dialog(this);
    }

    private void initTimerOperation() {
        timer = new CountDownTimer(4000, 1000) {
            @Override
            public void onTick(long l) {

            }

            @Override
            public void onFinish() {
                timer.cancel();
                dialog.dismissDialog();
                dialog.showSuccessDialogWithListenerButton("Pago completado",
                        sweetAlertDialog -> {
                    finish();
                });
            }
        }.start();
    }
}
