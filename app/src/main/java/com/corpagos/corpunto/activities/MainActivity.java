package com.corpagos.corpunto.activities;

import android.annotation.SuppressLint;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.speech.tts.TextToSpeech;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.corpagos.corpunto.Asynctask;
import com.corpagos.corpunto.Global;
import com.corpagos.corpunto.R;
import com.corpagos.corpunto.app.AppCredentials;
import com.corpagos.corpunto.commons.AnimationTransition;
import com.corpagos.corpunto.commons.Format;
import com.corpagos.corpunto.databases.BackupFiles;
import com.corpagos.corpunto.databases.TransactionDBHelper;
import com.corpagos.corpunto.dialogs.Dialog;
import com.corpagos.corpunto.emv.CardType;
import com.corpagos.corpunto.enums.CardAccountType;
import com.corpagos.corpunto.enums.CardTypeEnum;
import com.corpagos.corpunto.enums.PaymentOptionEnum;
import com.corpagos.corpunto.enums.PosModeType;
import com.corpagos.corpunto.enums.PosModelType;
import com.corpagos.corpunto.enums.ReadCardType;
import com.corpagos.corpunto.fragments.transactions.NoPinFragment;
import com.corpagos.corpunto.fragments.transactions.PinFragment;
import com.corpagos.corpunto.interfaces.AsynctaskListener;
import com.corpagos.corpunto.interfaces.DialogInterface;
import com.corpagos.corpunto.interfaces.KeyListener;
import com.corpagos.corpunto.interfaces.MainActivityListener;
import com.corpagos.corpunto.interfaces.ReadCardListener;
import com.corpagos.corpunto.locations.LocationService;
import com.corpagos.corpunto.logs.CustomizedExceptionHandler;
import com.corpagos.corpunto.models.CardData;
import com.corpagos.corpunto.models.Notification;
import com.corpagos.corpunto.models.OwnerData;
import com.corpagos.corpunto.models.PrepaidService;
import com.corpagos.corpunto.models.Response;
import com.corpagos.corpunto.models.Ticket;
import com.corpagos.corpunto.models.Transaction;
import com.corpagos.corpunto.models.TransactionLocation;
import com.corpagos.corpunto.models.TransactionType;
import com.corpagos.corpunto.models.UpdateApp;
import com.corpagos.corpunto.pos.PosDevice;
import com.corpagos.corpunto.print.PrintTicket;
import com.corpagos.corpunto.requests.Request;
import com.corpagos.corpunto.singletons.CardMode;
import com.corpagos.corpunto.singletons.NotificationHandler;
import com.corpagos.corpunto.singletons.PaymentOption;
import com.corpagos.corpunto.singletons.PosMode;
import com.corpagos.corpunto.singletons.PosModel;
import com.corpagos.corpunto.utils.ConsoleMessageManager;
import com.corpagos.corpunto.utils.SharedPreferencesHandler;
import com.corpagos.corpunto.widgets.CustomKeyboard;
import com.google.gson.Gson;
import com.imagpay.enums.CardDetected;
import com.imagpay.enums.PrintStatus;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;
import static com.corpagos.corpunto.Global.chipCount;
import static com.corpagos.corpunto.Global.nfcCount;
import static com.corpagos.corpunto.databases.BackupFiles.populateTicketObject;
import static com.corpagos.corpunto.locations.LocationService.BROADCAST_ACTION;
import static com.corpagos.corpunto.services.MyFirebaseMessagingService.ACTION_INTENT;
import static com.corpagos.corpunto.utils.SharedPreferencesHandler.saveCount;
import static com.corpagos.corpunto.utils.Util.isPackageInstalled;

public class MainActivity extends AppCompatActivity implements KeyListener,
        TextToSpeech.OnInitListener, AsynctaskListener, ReadCardListener {
    private boolean isFirstRequestFailed = false, isFirstPrint = false, isSell = false, isActive;
    private ToggleButton tbChip, tbCode, tbFace, tbNfc, tbQr, tbFinger;
    private NoPinFragment noPinFragment = new NoPinFragment();
    private PinFragment pinFragment = new PinFragment();
    private TransactionLocation transactionLocation;
    private FragmentTransaction fragTransaction;
    private final String CREDIT_TAG = "credit";
    private final String DEBIT_TAG = "debit";
    private TransactionDBHelper dbHelper;
    private PrepaidService prepaidService;
    private MainActivityListener listener;
    private PaymentOption paymentOption;
    private TextToSpeech textToSpeech;
    private CustomKeyboard keyboard;
    private Transaction transaction;
    private CountDownTimer timer;
    private CardData cardData;
    private CardMode cardMode;
    private PosMode posMode;
    private Dialog dialog;
    private PosDevice posDevice;
    private PosModel posModel;
    private Intent locationIntent;
    private TextView tvTimer;

    @Override
    protected void onStart() {
        super.onStart();
        isActive = true;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        posDevice = new PosDevice(this, this, this, true);
        try {
            transactionLocation = new TransactionLocation();
            if (!getIntent().getStringExtra("location").equals("")) {
                transactionLocation = new Gson().fromJson(getIntent().getStringExtra("location"),
                        TransactionLocation.class);
            }
        } catch (Exception e) {
            e.getMessage();
        }
        initViews();
        try {
            if (!getIntent().getStringExtra("prepaidService").equals("")) {
                prepaidService = new Gson().fromJson(getIntent().getStringExtra(
                        "prepaidService"), PrepaidService.class);
                noPinFragment.setPrepaidService(prepaidService);
            }
        } catch (Exception e) {
            e.getMessage();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        enableChipRead();
        checkNotificationType();
        if (posMode.getPosMode().equals(PosModeType.PAYMENT)) {
            cardData = new Gson().fromJson(getIntent().getStringExtra("cardData"), CardData.class);
            transaction = new Gson().fromJson(getIntent().getStringExtra("transaction"), Transaction.class);
            listener.onSuccessReadCard();
        }
        IntentFilter filterNotifications = new IntentFilter(ACTION_INTENT);
        IntentFilter filterLocation = new IntentFilter(BROADCAST_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(receiverNotification,
                filterNotifications);
        LocalBroadcastManager.getInstance(this).registerReceiver(locationReceiver,
                filterLocation);
    }

    @Override
    protected void onPause() {
        super.onPause();
        disableChipRead();
        resetOperation();
        if (receiverNotification != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(receiverNotification);
        }
        if (locationReceiver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(locationReceiver);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        isActive = false;
    }

    private void initViews() {
        posModel = PosModel.getInstance(this);
        NotificationHandler notificationHandler = NotificationHandler.getInstance(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
        dialog = new Dialog(this);
        cardMode = CardMode.getInstance(this);
        posMode = PosMode.getInstance(this);
        paymentOption = PaymentOption.getInstance(this);
        keyboard = findViewById(R.id.kb_main);
        keyboard.setListener(this);
        transaction = new Transaction();
        dbHelper = new TransactionDBHelper(this);
        showCreditFragment();
        locationIntent = new Intent(this, LocationService.class);
        getCurrentLocation();
        textToSpeech = new TextToSpeech(this, this);
        ((RadioGroup) findViewById(R.id.toggleGroup)).setOnCheckedChangeListener(ToggleListener);
        tbChip = findViewById(R.id.tb_chip);
        tbCode = findViewById(R.id.tb_code);
        tbFace = findViewById(R.id.tb_face);
        tbNfc = findViewById(R.id.tb_nfc);
        tbQr = findViewById(R.id.tb_qr);
        tbFinger = findViewById(R.id.tb_finger);
        Thread.setDefaultUncaughtExceptionHandler(new CustomizedExceptionHandler());
    }

    public void onToggle(View view) {
        ((RadioGroup) view.getParent()).check(view.getId());
        switch (view.getId()) {
            case R.id.tb_chip:
                paymentOption.setDefaultPaymentOption();
                if (!posMode.getPosMode().equals(PosModeType.PREPAID_SERVICE)) {
                    resetOperation();
                }
                if (tbChip.isChecked()) {
                    cardMode.setCardType(CardTypeEnum.CHIP);
                    initReadingPeripherals();
                    listener.onPaymentOption(PaymentOptionEnum.CARD);
                } else {
                    enableChipRead();
                    listener.onPaymentOption(PaymentOptionEnum.CARD);
                }
                break;
            case R.id.tb_code:
                if (!posMode.getPosMode().equals(PosModeType.PREPAID_SERVICE)) {
                    resetOperation();
                }
                if (tbCode.isChecked()) {
                    disableChipRead();
                    paymentOption.setPaymentOption(PaymentOptionEnum.CODE);
                } else {
                    enableChipRead();
                    paymentOption.setDefaultPaymentOption();
                }
                break;
            case R.id.tb_face:
                if (!posMode.getPosMode().equals(PosModeType.PREPAID_SERVICE)) {
                    resetOperation();
                }
                if (tbFace.isChecked()) {
                    disableChipRead();
                    paymentOption.setPaymentOption(PaymentOptionEnum.FACE);
                } else {
                    enableChipRead();
                    paymentOption.setDefaultPaymentOption();
                }
                break;
            case R.id.tb_nfc:
                if (!posMode.getPosMode().equals(PosModeType.PREPAID_SERVICE)) {
                    resetOperation();
                }
                if (tbNfc.isChecked()) {
                    disableChipRead();
                    cardMode.setCardType(CardTypeEnum.NFC);
                } else {
                    enableChipRead();
                    paymentOption.setDefaultPaymentOption();
                }
                break;
            case R.id.tb_qr:
                if (!posMode.getPosMode().equals(PosModeType.PREPAID_SERVICE)) {
                    resetOperation();
                }
                if (tbQr.isChecked()) {
                    disableChipRead();
                    paymentOption.setPaymentOption(PaymentOptionEnum.QR);
                } else {
                    enableChipRead();
                    paymentOption.setDefaultPaymentOption();
                }
                break;
            case R.id.tb_finger:
                if (!posMode.getPosMode().equals(PosModeType.PREPAID_SERVICE)) {
                    resetOperation();
                }
                if (tbFinger.isChecked()) {
                    if (isCorpFingerPackageExist()) {
                        disableChipRead();
                        paymentOption.setPaymentOption(PaymentOptionEnum.FINGER);
                        listener.onPaymentOption(PaymentOptionEnum.FINGER);
                    } else {
                        tbFinger.setChecked(false);
                    }
                } else {
                    enableChipRead();
                    paymentOption.setDefaultPaymentOption();
                }
                break;
        }
    }

    private void enableChipRead() {
        tbChip.setChecked(true);
        initReadingPeripherals();
    }

    private void disableChipRead() {
        turnOffReadingPeripherals();
        tbChip.setChecked(false);
    }

    private void showCreditFragment() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        noPinFragment.setKeyboard(keyboard);
        fragTransaction = getFragmentManager().beginTransaction();
        fragTransaction.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
        fragTransaction.replace(R.id.fl_main, noPinFragment, CREDIT_TAG);
        fragTransaction.commit();
        tvTimer = noPinFragment.getTimer();
    }

    public void showDebitFragment() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        pinFragment.setKeyboard(keyboard);
        pinFragment.setTransaction(transaction);
        fragTransaction = getFragmentManager().beginTransaction();
        fragTransaction.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
        fragTransaction.replace(R.id.fl_main, pinFragment, DEBIT_TAG);
        fragTransaction.commit();
        tvTimer = pinFragment.getTimer();
    }

    public void setMainListener(MainActivityListener listener) {
        this.listener = listener;
    }

    private boolean isCreditFragmentVisible() {
        return getFragmentManager().findFragmentByTag(CREDIT_TAG) != null &&
                getFragmentManager().findFragmentByTag(CREDIT_TAG).isVisible();
    }

    private boolean isDebitFragmentVisible() {
        return getFragmentManager().findFragmentByTag(DEBIT_TAG) != null &&
                getFragmentManager().findFragmentByTag(DEBIT_TAG).isVisible();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            if (isDebitFragmentVisible()) {
                noPinFragment.setReturn(true);
                noPinFragment.setRefresh(false);
                showCreditFragment();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (timer != null) {
            timer.cancel();
        }
        if (textToSpeech != null) {
            textToSpeech.stop();
            textToSpeech.shutdown();
        }
        stopService(new Intent(this, LocationService.class));
        posDevice.turnOffReadingPeripherals();
    }

    @Override
    public void onKeyPressed(Enum specialKey) {
        if (specialKey == KeyListener.specialKey.CANCEL) {
            dialog.showInfoDialogWithListenerButtonsAndText(
                    "Esta seguro que desea cancelar la operacion?", (dialog, which) -> {
                        resetOperation();
                        disableButtons();
                    }, (dialog, which) -> {
                    }, "Si", "No");
        } else if (specialKey == KeyListener.specialKey.DONE) {
            keyboard.setEnabled(false);
            if (isCreditFragmentVisible()) {
                switch (noPinFragment.getItemFocus()) {
                    case (R.id.et_amount_principal):
                        if (!noPinFragment.getEtAmount().equals("0,00")) {
                            noPinFragment.setEtDniFocus(true);
                        }
                        break;
                    case (R.id.et_user_dni):
                        if (!noPinFragment.getEtAmount().equals("")) {
                            noPinFragment.setEtDniFocus(false);
                            if (noPinFragment.validate(transaction)) {
                                noPinFragment.populateTransactionObject(transaction);
                            }
                        }
                        break;
                    case (R.id.bootstrapDropDown2):
                        if (noPinFragment.validate(transaction)) {
                            noPinFragment.populateTransactionObject(transaction);
                        }
                        break;
                }
            } else if (isDebitFragmentVisible()) {
                if (pinFragment.isViewFocused()) {
                    switch (pinFragment.getItemFocus()) {
                        case R.id.et_amount_pass:
                            pinFragment.finishAmountEdition();
                            break;
                        case R.id.et_dni_pass:
                            pinFragment.finishDniEdition();
                            break;
                    }
                } else {
                    if (pinFragment.validate()) {
                        pinFragment.populateData(transaction);
                    }
                }
            }
        } else if (specialKey == KeyListener.specialKey.FORWARD) {
            if (isCreditFragmentVisible()) {
                switch (noPinFragment.getItemFocus()) {
                    case (R.id.et_amount_principal):
                        if (!noPinFragment.getEtAmount().equals("0,00")) {
                            noPinFragment.setEtDniFocus(true);
                        }
                        break;
                    case (R.id.et_user_dni):
                        if (cardMode.getCard().getCardAccountType().equals(CardAccountType.DEBIT)) {
                            noPinFragment.expandSpinnerAccount();
                            noPinFragment.setEtDniFocus(false);
                        }
                        break;
                }
            } else if (isDebitFragmentVisible()) {
                switch (pinFragment.getItemFocus()) {
                    case R.id.et_amount_pass:
                        pinFragment.finishAmountEdition();
                        break;
                    case R.id.et_dni_pass:
                        pinFragment.finishDniEdition();
                        break;
                }
            }

        } else if (specialKey == KeyListener.specialKey.BACK) {
            if (isCreditFragmentVisible()) {
                if (noPinFragment.getItemFocus() == R.id.et_user_dni) {
                    noPinFragment.setEtAmountFocus();
                }
            }
        }
    }

    @Override
    public void onInit(int status) {

    }

    @Override
    public void onChipError() {
        runOnUiThread(() -> {
            dialog.dismissDialog();
            dialog.showErrorDialog("Error de chip");
        });
    }

    @Override
    public void onProcess() {

    }

    @Override
    public void onCardRead(String json) {
        switch (CardType.checkCardType(json)) {
            case CardType.credit:
                cardMode.setCardAccountType(CardAccountType.CREDIT);
                break;
            case CardType.debit:
                cardMode.setCardAccountType(CardAccountType.DEBIT);
                runOnUiThread(() -> noPinFragment.setAccountsTypeVisibility(VISIBLE));
                break;
            case CardType.undefined:
                break;
        }
    }

    @Override
    public void onViewEnabled() {
        listener.onSuccessReadCard();
    }

    @Override
    public void onCardDetect(CardDetected cardDetected, Transaction transaction, CardData cardData) {
        dialog.dismissDialog();
        if (cardDetected == CardDetected.INSERTED) {
            this.transaction = transaction;
            this.cardData = cardData;
            cardMode.setCardType(CardTypeEnum.CHIP);
            iCCardTest();
            ConsoleMessageManager.consoleCardInsertedMsg();
        }
        if (cardDetected == CardDetected.REMOVED) {
            listener.onCardDetected(CardDetected.REMOVED);
            ConsoleMessageManager.consoleCardRemovedMsg();
            runOnUiThread(() -> {
                readingCardDataFailed();
                if (isDebitFragmentVisible()) {
                    noPinFragment.setRefresh(true);
                    showCreditFragment();
                }
                setTimerText("");
                if (posDevice.isTransaction()) {
                    dialog.showErrorDialog("Transaccion Cancelada por remover tarjera");
                }
                checkNotificationType();
                cleanAllData();
            });
        }
        if (cardDetected == CardDetected.NFC) {
            cardMode.setCardType(CardTypeEnum.NFC);
            runOnUiThread(this::readingCardDataSuccessful);
            transaction.setCardHolder("CABRERA/JESUS");
            transaction.setCardExpiration("08/22");
            transaction.setCardNumber("5529735050007873");
            transaction.setDeviceSerial(posModel.getPosInformation().getPosSerial());
            nfcCount++;
            saveCount(this, Global.keyNfcCount, nfcCount);
        }
    }

    @Override
    public void onPrintStatus(PrintStatus paramPrintStatus) {
        if (paramPrintStatus.equals(PrintStatus.EXIT)) {
            if (isSell && !SharedPreferencesHandler.getSavedData(this,
                    SharedPreferencesHandler.KEY_TRANSACTION, SharedPreferencesHandler
                            .KEY_TRANSACTION_FILE).equals(SharedPreferencesHandler.NOT_FOUND)) {
                runOnUiThread(() -> {
                    isSell = false;
                    dialog.showInfoDialogWithListenerButtonsAndText("Imprimir copia? ",
                            (dialog, which) -> {
                                Ticket ticket = new Gson().fromJson(SharedPreferencesHandler.getSavedData(
                                        MainActivity.this, SharedPreferencesHandler.KEY_TRANSACTION,
                                        SharedPreferencesHandler.KEY_TRANSACTION_FILE), Ticket.class);
                                if (cardMode.getCard().getCardAccountType().equals(CardAccountType.CREDIT)) {
                                    printTicket(PrintTicket.getTicketSuccessBody(
                                            ticket.getTransaction(), ticket.getOwnerData(),
                                            ticket.getCardData()),
                                            PrintTicket.getHeaders(ticket.getOwnerData(), "COMPRA"),
                                            PrintTicket.getCreditFooter(String.valueOf(
                                                    ticket.getTransaction().getAmount()), true));
                                } else if (cardMode.getCard().getCardAccountType().equals(CardAccountType.DEBIT)) {
                                    printTicket(PrintTicket.getTicketSuccessBody(
                                            ticket.getTransaction(), ticket.getOwnerData(),
                                            ticket.getCardData()),
                                            PrintTicket.getHeaders(ticket.getOwnerData(), "COMPRA"),
                                            PrintTicket.getDebitFooter(String.valueOf(
                                                    ticket.getTransaction().getAmount()), true));
                                }
                                SharedPreferencesHandler.saveData(MainActivity.this,
                                        SharedPreferencesHandler.KEY_TRANSACTION,
                                        SharedPreferencesHandler.NOT_FOUND,
                                        SharedPreferencesHandler.KEY_TRANSACTION_FILE);
                                dialog.dismiss();
                                resetOperation();
                                //posDevice.initCardDetectionThread();
                                //posDevice.setCardDetectionThread(true);
                            }, (dialog, which) -> {
                                resetOperation();
                                //posDevice.initCardDetectionThread();
                                //posDevice.setCardDetectionThread(true);
                            },
                            "Imprimir", "Cancelar");
                    checkNotificationType();
                });
                isFirstPrint = false;
            } else {
                isFirstPrint = true;
            }
        }
    }

    @Override
    public void onError(String error) {
        runOnUiThread(() -> {
            noPinFragment.onFailedReadCard();
            resetOperation();
            dialog.dismissDialog();
            dialog.showErrorDialogWithListenerButton(error, sweetAlertDialog -> {
                dialog.dismissDialog();
                //posDevice.initCardDetectionThread();
                //posDevice.setCardDetectionThread(true);
            });
        });
    }

    private void getCurrentLocation() {
        if (transactionLocation != null) {
            if (transactionLocation.getLatitude().equals("") || transactionLocation.getLongitude().equals("")) {
                startService(locationIntent);
            }
        }
    }

    private BroadcastReceiver locationReceiver = new BroadcastReceiver() {
        @SuppressLint("WrongConstant")
        @Override
        public void onReceive(Context context, Intent intent) {
            if (BROADCAST_ACTION.equals(intent.getAction())) {
                transactionLocation.setLatitude(String.valueOf(intent.getDoubleExtra(
                        "Latitude", 0)));
                transactionLocation.setLongitude(String.valueOf(intent.getDoubleExtra(
                        "Longitude", 0)));
                LocalBroadcastManager.getInstance(MainActivity.this).unregisterReceiver(locationReceiver);
                stopService(new Intent(MainActivity.this, LocationService.class));
            }
        }
    };

    private void initReadingPeripherals() {
        posDevice.initReadingPeripherals();
        //posDevice.initCardDetectionThread();
        //posDevice.setCardDetectionThread(true);
    }

    private void turnOffReadingPeripherals() {
        posDevice.turnOffReadingPeripherals();
    }

    private synchronized void iCCardTest() {
        new Thread(() -> {
            dialog.showProgressDialog("Leyendo tarjeta... ");
            dialog.dismissDialog();
            runOnUiThread(() -> {
                transaction.setLocation("{lat:" + transactionLocation.getLatitude() + ",long:" +
                        transactionLocation.getLongitude() + "}");
                if (transaction.getCardHolder() == null || transaction.getCardNumber() == null ||
                        transaction.getCardExpiration() == null) {
                    dialog.showErrorDialogWithListenerButton(
                            "Inserte nuevamente la tarjeta para continuar",
                            sweetAlertDialog -> {
                                dialog.dismissDialog();
                            });
                } else {
                    chipCount++;
                    saveCount(this, Global.keyChipCount, chipCount);
                    if (posMode.getPosMode().equals(PosModeType.PAYMENT) && cardMode.getCard()
                            .getCardAccountType().equals(CardAccountType.CREDIT)) {
                        runOnUiThread(() -> {
                            readingCardDataFailed();
                            dialog.showErrorDialog("No se puede realizar un pago a una tarjeta de credito");
                        });
                    } else {
                        readingCardDataSuccessful();
                    }
                }
            });
        }).start();
    }

    private void readingCardDataSuccessful() {
        initTimerOperation();
        listener.onReadCard(ReadCardType.SUCCESSFUL);
        /*try {
            if (cardMode.getCard().getCardTypeEnum().equals(CardTypeEnum.NFC)) {
                //menu.getItem(0).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_nfc_green));
            } else if (cardMode.getCard().getCardTypeEnum().equals(CardTypeEnum.CHIP)) {
                //posDevice.controlGreenLed(true);
            }
        } catch (Exception e) {
            e.getMessage();
        }*/
        posDevice.setTransaction(true);
        if (posMode.getPosMode().equals(PosModeType.PREPAID_SERVICE)) {
            listener.onPosMode(PosModeType.PREPAID_SERVICE);
        } else if (posMode.getPosMode().equals(PosModeType.SELL)) {
            listener.onPosMode(PosModeType.SELL);
        } else if (posMode.getPosMode().equals(PosModeType.PAYMENT)) {
            listener.onPosMode(PosModeType.PAYMENT);
        }
        pinFragment.setKeyboard(keyboard);
        noPinFragment.setKeyboard(keyboard);
    }

    private void readingCardDataFailed() {
        posMode.setDeFaultMode();
        cardMode.setCardModeDefault();
        posDevice.controlGreenLed(false);
        listener.onFailedReadCard();
        if (timer != null) {
            timer.cancel();
        }
        //disableButtons(false);
        posDevice.setTransaction(false);
    }

    private void initTimerOperation() {
        timer = new CountDownTimer(60000, 1000) {
            @Override
            public void onTick(long l) {
                if (posDevice.isCard()) {
                    DateFormat formatter = new SimpleDateFormat("ss");
                    setTimerText(formatter.format(new Date(l)));
                } else {
                    timer.cancel();
                }
            }

            @Override
            public void onFinish() {
                if (isActive) {
                    listener.onCleanData();
                    //readingCardDataFailed();
                    resetOperation();
                    posDevice.cancelSearchCard();
                    disableButtons();
                }
            }
        }.start();
    }

    private void setTimerText(String time) {
        if (isCreditFragmentVisible()) {
            noPinFragment.setTime(time);
        } else if (isDebitFragmentVisible()) {
            pinFragment.setTimer(time);
        }
    }

    private void cleanAllData() {
        if (isCreditFragmentVisible()) {
            noPinFragment.setAccountsTypeVisibility(INVISIBLE);
        } else if (isDebitFragmentVisible()) {
            noPinFragment.onCleanData();
            showCreditFragment();
        }
        transaction = new Transaction();
        readingCardDataFailed();
        listener.onCleanData();
    }

    private void checkNotificationType() {
        if (!posDevice.isTransaction() && NotificationActivity.isUpdateNotification) {
            dialog.dismissDialog();
            showUpdateDialog(new Gson().fromJson(SharedPreferencesHandler.getSavedData(
                    this, SharedPreferencesHandler.KEY_UPDATE,
                    SharedPreferencesHandler.KEY_UPDATE_FILE), UpdateApp.class));
        }
    }

    private void showUpdateDialog(UpdateApp updateApp) {
        dialog.dismissDialog();
        dialog.showUpdateAppDialog(updateApp.getChangeLog(), new DialogInterface() {
            @Override
            public void positiveClick() {
                MainLauncherActivity.openUpdateActivity(MainActivity.this);
            }

            @Override
            public void negativeClick() {
                NotificationActivity.isUpdateNotification = false;
            }
        });
    }

    @Override
    public void onAsynctaskFinished(String endPoint, String response, String headers) {
        dialog.dismissDialog();
        keyboard.setEnabled(true);
        System.out.println("valores 2 :" + response + " " + endPoint);
        OwnerData ownerData = new OwnerData();
        if (BackupFiles.isFilesExist(this, BackupFiles.ownerBackupFileName)) {
            ownerData = new Gson().fromJson(BackupFiles.getBackupData(
                    MainActivity.this, BackupFiles.ownerBackupFileName),
                    OwnerData.class);
        }
        if (!response.equals("") && !response.equals(Asynctask.TIMEOUT_MESSAGE) &&
                !response.equals(Asynctask.FAILED_TO_CONNECT)) {
            Response res = new Gson().fromJson(response, Response.class);
            System.out.println("valores : " + Asynctask.getDecryptedAESResponse(
                    this, new Gson().toJson(res.getData())));
            if (!res.isError()) {
                if (endPoint.contains(Asynctask.URL_DO_TRANSACTION)) {
                    disableButtons();
                    posDevice.setTransaction(false);
                    timer.cancel();
                    Transaction transaction = new Gson().fromJson(Asynctask.getDecryptedAESResponse(
                            this, new Gson().toJson(res.getData())), Transaction.class);
                    OwnerData finalOwnerData = ownerData;
                    listener.onCleanData();
                    transaction.setDeviceSerial(posModel.getPosInformation().getPosSerial());
                    transaction.setDate(Format.getLocalDateFormat(this, Format.dateFullPattern,
                            transaction.getCreated_at()).toUpperCase());
                    transaction.setCard_number(Format.getLastNumberFromCard(
                            this.transaction.getCardNumber()));
                    if (endPoint.contains(Asynctask.URL_SERVICE_PAYMENT)) {
                        transaction.setTransactionType(TransactionType.PREPAID_SERVICE);
                        transaction.setReverse(1);
                        transaction.setConcept(prepaidService.getName());
                        transaction.setDate(prepaidService.getTargetNumber());
                    } else {
                        transaction.setTransactionType(TransactionType.SALE);
                        transaction.setConcept(cardData.getAID());
                    }
                    long trace = dbHelper.getTransactionList().size() >= 999999 ? 0 :
                            dbHelper.getTransactionList().size();
                    transaction.setTrace(trace);
                    dbHelper.insertTransaction(transaction);
                    checkAmountDecimals(transaction.getAmount());
                    Dialog.showSuccessfulSaleDialog(this, transaction,
                            new DialogInterface() {
                                @Override
                                public void positiveClick() {
                                    if (!posModel.getPosModelType().equals(PosModelType.WPOS_MINI)) {
                                        //posDevice.setCardDetectionThread(false);
                                        if (cardMode.getCard().getCardAccountType().equals(
                                                CardAccountType.CREDIT)) {
                                            printTicket(PrintTicket.getTicketSuccessBody(
                                                    transaction, finalOwnerData, cardData),
                                                    PrintTicket.getHeaders(finalOwnerData, "COMPRA"),
                                                    PrintTicket.getCreditFooter(String.valueOf(
                                                            transaction.getAmount()), false));
                                        } else if (cardMode.getCard().getCardAccountType().equals(
                                                CardAccountType.DEBIT)) {
                                            printTicket(PrintTicket.getTicketSuccessBody(
                                                    transaction, finalOwnerData, cardData),
                                                    PrintTicket.getHeaders(finalOwnerData, "COMPRA"),
                                                    PrintTicket.getDebitFooter(String.valueOf(
                                                            transaction.getAmount()), false));
                                        }
                                        isSell = true;
                                        populateTicketObject(MainActivity.this,
                                                transaction, finalOwnerData, cardData, "COMPRA");
                                    } else if (posModel.getPosModelType().equals(PosModelType.WPOS_MINI)) {
                                        cleanAllData();
                                        resetOperation();
                                    }
                                }

                                @Override
                                public void negativeClick() {
                                    dialog.dismissDialog();
                                    resetOperation();
                                }
                            }, posModel.getPosModelType().equals(PosModelType.WPOS_MINI) ? "Aceptar"
                                    : "Imprimir Ticket");
                }
            } else {
                switch (res.getMessage()) {
                    case "ERR_INSUFFICIENT_FOUNDS":
                        if (posMode.getPosMode().equals(PosModeType.PREPAID_SERVICE)) {
                            dialog.showErrorDialogWithListenerButtons("Saldo insuficiente",
                                    sweetAlertDialog -> {
                                        Intent intent = new Intent(this,
                                                PrepaidServiceTargetActivity.class);
                                        intent.putExtra("service", new Gson().toJson(prepaidService));
                                        startActivity(intent);
                                        AnimationTransition.setInActivityTransition(this);
                                        finish();
                                    },
                                    sweetAlertDialog -> resetOperation(),
                                    "Volver", "Cancelar");
                        } else {
                            OwnerData finalOwnerData2 = ownerData;
                            dialog.showInfo3ButtonsDialog("Saldo insuficiente, desea editar monto?",
                                    sweetAlertDialog -> {
                                        if (isCreditFragmentVisible()) {
                                            noPinFragment.editAmount();
                                        } else if (isDebitFragmentVisible()) {
                                            pinFragment.editAmount();
                                        }
                                        dialog.dismissDialog();
                                    },
                                    sweetAlertDialog -> {
                                        dialog.dismissDialog();
                                        printTicket(PrintTicket.getErrorTicketBody(transaction, finalOwnerData2,
                                                " ", cardData), PrintTicket.getHeaders(finalOwnerData2,
                                                "COMPRA"), PrintTicket.getErrorFooter("SALDO INSUFICIENTE"));
                                        cleanAllData();
                                    },
                                    sweetAlertDialog -> {
                                        cleanAllData();
                                        resetOperation();
                                        dialog.dismissDialog();
                                    });
                        }
                        break;
                    case Global.ERR_INVALID_PASSWORD:
                        OwnerData finalOwnerData1 = ownerData;
                        dialog.showInfo3ButtonsDialog("Clave incorrecta, desea editar clave?",
                                sweetAlertDialog -> {
                                    pinFragment.cleanPass();
                                    dialog.dismissDialog();
                                }, sweetAlertDialog -> {
                                    dialog.dismissDialog();
                                    printTicket(PrintTicket.getErrorTicketBody(transaction, finalOwnerData1,
                                            posModel.getPosInformation().getPosSerial(), cardData),
                                            PrintTicket.getHeaders(finalOwnerData1,
                                                    "COMPRA"), PrintTicket.getErrorFooter("CLAVE INCORRECTA"));
                                    try {
                                        Thread.sleep(100);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                }, sweetAlertDialog -> {
                                    resetOperation();
                                    dialog.dismissDialog();
                                });
                        break;
                    case "ERR_INVALID_OR_EXPIRED_TOKEN":
                    case Global.ERR_UNKNOWN_DEVICE_SERIAL:
                        cleanAllData();
                        dialog.showErrorDialogWithListenerButton(
                                "Dispositivo no registrado, registrar nuevamente",
                                sweetAlertDialog -> {
                                    dialog.dismissDialog();
                                    AppCredentials.deleteToken(this);
                                    resetOperation();
                                    disableButtons();
                                });
                        break;
                    case "Unsupported grant type: `grant_type` is invalid":
                        dialog.showErrorDialog("Error de verificacion");
                        resetOperation();
                        disableButtons();
                        break;
                    case "ERR_INVALID_CREDENTIALS":
                        dialog.showErrorDialog("Error en las credenciales");
                        resetOperation();
                        disableButtons();
                        break;
                    case "ERR_INACTIVE_USER":
                        dialog.showErrorDialog("Usuario inactivo");
                        resetOperation();
                        disableButtons();
                        break;
                    case "ERR_UNVERIFIED_USER_EMAIL":
                        dialog.showErrorDialog("Error email no validado");
                        resetOperation();
                        disableButtons();
                        break;
                    case "ERR_UNVERIFIED_USER_PHONE":
                        dialog.showErrorDialog("Error numero no validado");
                        resetOperation();
                        disableButtons();
                        break;
                    case "ERR_LOGIN_BLOCKED":
                        dialog.showErrorDialog("Usuario bloqueado");
                        resetOperation();
                        disableButtons();
                        break;
                    case "ERR_VALIDATION_FAILED":
                        dialog.showErrorDialog("Error verificacion fallida");
                        resetOperation();
                        disableButtons();
                        break;
                    case "ERR_ALREADY_LOGGED_IN":
                        dialog.showErrorDialog("Cliente ya registrado");
                        resetOperation();
                        disableButtons();
                        break;
                    case "ERR_CHECKSUM_MISMATCH":
                        dialog.showErrorDialog("Problema en la comunicacion de datos");
                        resetOperation();
                        disableButtons();
                        break;
                    case "ERR_UNKNOWN_CLIENT":
                        dialog.showErrorDialog("Cliente no registrado");
                        BackupFiles.resetFirstLogin(this);
                        resetOperation();
                        disableButtons();
                        break;
                    case "ERR_REQUEST_ENCRYPTION_REQUIRED":
                        dialog.showErrorDialog("Problema de encriptacion, por favor comuniquese " +
                                "con un proveedor");
                        resetOperation();
                        disableButtons();
                        break;
                    case "ERR_REQUEST_DECRYPTION_FAILED":
                        dialog.showErrorDialog("Surgio un problema de encriptacion, por favor " +
                                "comuniquese con el proveedor");
                        resetOperation();
                        disableButtons();
                        break;
                    case "ERR_ENCRYPTED_REQUEST_DECRYPTION_FAILED":
                    case "ERR_POS_AES_KEYS_NOT_CONFIGURED":
                        cleanAllData();
                        dialog.showErrorDialogWithListenerButton(
                                "Dispositivo no se encuentra configurado",
                                sweetAlertDialog -> {
                                    dialog.dismissDialog();
                                    SharedPreferencesHandler.saveData(this,
                                            SharedPreferencesHandler.KEY_AES_KEY,
                                            SharedPreferencesHandler.NOT_FOUND,
                                            SharedPreferencesHandler.FILE_AES_KEY
                                    );
                                    finish();
                                });

                        break;
                    case Global.ERR_INVALID_DNI:
                        OwnerData finalOwnerData3 = ownerData;
                        dialog.showInfo3ButtonsDialog("Cedula incorrecta, desea editar cedula?",
                                sweetAlertDialog -> {
                                    if (isCreditFragmentVisible()) {
                                        noPinFragment.editDni();
                                    } else if (isDebitFragmentVisible()) {
                                        pinFragment.editDni();
                                    }
                                    dialog.dismissDialog();
                                }, sweetAlertDialog -> {
                                    dialog.dismissDialog();
                                    printTicket(PrintTicket.getErrorTicketBody(transaction, finalOwnerData3,
                                            posModel.getPosInformation().getPosSerial(), cardData),
                                            PrintTicket.getHeaders(finalOwnerData3,
                                                    "COMPRA"), PrintTicket.getErrorFooter("CEDULA INCORRECTA"));
                                    cleanAllData();
                                }, sweetAlertDialog -> {
                                    resetOperation();
                                    cleanAllData();
                                    dialog.dismissDialog();
                                });
                        break;
                    default:
                        cleanAllData();
                        dialog.showErrorDialog(Global.messageResponse(res.getMessage()));
                        break;
                }
            }
        } else if (response.equals(Asynctask.TIMEOUT_MESSAGE) && !isFirstRequestFailed) {
            isFirstRequestFailed = true;
            requestPayment("Procesando con otro servidor ...");
        } else if (response.equals(Asynctask.TIMEOUT_MESSAGE) && isFirstRequestFailed) {
            isFirstRequestFailed = false;
            dialog.showErrorDialog("Servidor no responde");
            cleanAllData();
            disableButtons();
        } else if (response.equals(Asynctask.FAILED_TO_CONNECT)) {
            dialog.showErrorDialog("Error de conexion");
        } else {
            cleanAllData();
            disableButtons();
            dialog.showSomethingWrongDialog();
        }
    }

    private void printTicket(ArrayList<String> items, ArrayList<String> headers, ArrayList<String>
            footers) {
        posDevice.printTicket(items, headers, footers);
    }

    public void requestPayment(String message) {
        if (AppCredentials.isLogin(this)) {
            switch (paymentOption.getPaymentOptionEnum()) {
                case FINGER:
                    Intent intent = new Intent("com.corpagos.corpfinger.MainActivity");
                    startActivityForResult(intent, 1);
                    break;
                case CARD:
                    dialog.showProgressDialog(message);
                    Request.requestDoPayment(this, transaction, cardMode.getCard().getCardAccountType()
                                    .equals(CardAccountType.CREDIT) ? Global.credit : Global.debit,
                            cardMode.getCard().getCardTypeEnum().equals(CardTypeEnum.NFC) ?
                                    Global.nfc : Global.chip, this);
                    break;
            }
        } else {
            dialog.showErrorDialogWithListenerButton("Se debe registrar el dispositivo",
                    sweetAlertDialog -> {
                        dialog.dismissDialog();
                        resetOperation();
                    });
        }
    }

    private void checkAmountDecimals(float amount) {
        String regex = "^0*([1-9]{1,2})$";
        String amountWithoutDecimals = String.valueOf(amount).split("\\.")[0];
        String amountDecimals = String.valueOf(amount).split("\\.")[1];
        if (amountDecimals.matches(regex)) {
            amountSpeech(String.valueOf(amount));
        } else {
            amountSpeech(amountWithoutDecimals);
        }
    }

    private void amountSpeech(String amount) {
        textToSpeech.setLanguage(new Locale("spa", "VEN"));
        textToSpeech.speak("Transacción Aprobada por " + amount + " Bolívares",
                TextToSpeech.QUEUE_FLUSH, null);
        textToSpeech.setSpeechRate(0.0f);
        textToSpeech.setPitch(0.0f);
    }

    public void showSuccessPrepaidServicePaymentDialog() {
        if (AppCredentials.isLogin(this)) {
            dialog.showProgressDialog("Cargando...");
            Request.requestDoServicePayment(this, prepaidService, transaction,
                    cardMode.getCard().getCardAccountType().equals(CardAccountType.CREDIT) ?
                            Global.credit : Global.debit,
                    cardMode.getCard().getCardTypeEnum().equals(CardTypeEnum.NFC) ?
                            Global.nfc : Global.chip, this);
        } else {
            dialog.showErrorDialogWithListenerButton("Se debe registrar el dispositivo",
                    sweetAlertDialog -> {
                        dialog.dismissDialog();
                        resetOperation();
                    });
        }
    }

    private void resetOperation() {
        cleanAllData();
        posDevice.cancelSearchCard();
        noPinFragment.setRefresh(true);
        showCreditFragment();
        tvTimer.setText("");
        if (timer != null) {
            timer.cancel();
        }
    }

    protected BroadcastReceiver receiverNotification = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (ACTION_INTENT.equals(intent.getAction())) {
                String value = intent.getStringExtra("UI_KEY");
                if (!posDevice.isTransaction()) {
                    showUpdateDialog(deserializeUpdateModel(value));
                }
            }
        }
    };

    public static UpdateApp deserializeUpdateModel(String json) {
        Notification.NotificationData notificationData = new Gson().fromJson(json,
                Notification.NotificationData.class);
        return new Gson().fromJson(new Gson().toJson(notificationData.getData()), UpdateApp.class);
    }

    static final RadioGroup.OnCheckedChangeListener ToggleListener = (radioGroup, i) -> {
        for (int j = 0; j < radioGroup.getChildCount(); j++) {
            final ToggleButton view = (ToggleButton) radioGroup.getChildAt(j);
            view.setChecked(view.getId() == i);
        }
    };

    private boolean isCorpFingerPackageExist() {
        PackageManager pm = getPackageManager();
        if (isPackageInstalled("com.corpagos.corpfinger", pm)) {
            Global.showToast(this, "Instalado", 3000);
            return true;
        } else {
            Global.showToast(this, "No Instalado", 3000);
            dialog.showInfoDialogWithListenerButtons("Desea configurar el pago con huella?",
                    (dialog, which) -> {
                //TODO descargar e instalar la app de lector de huella
                    }, null);
            return false;
        }
    }

    private void disableButtons() {
        tbChip.setChecked(false);
        tbCode.setChecked(false);
        tbFace.setChecked(false);
        tbFinger.setChecked(false);
        tbNfc.setChecked(false);
        tbQr.setChecked(false);
    }
}
