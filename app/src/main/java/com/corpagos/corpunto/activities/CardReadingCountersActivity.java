package com.corpagos.corpunto.activities;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.corpagos.corpunto.Global;
import com.corpagos.corpunto.R;
import com.corpagos.corpunto.adapters.CountCardReaderListAdapter;
import com.corpagos.corpunto.databases.BackupFiles;
import com.corpagos.corpunto.dialogs.Dialog;
import com.corpagos.corpunto.emv.PosCounterType;
import com.corpagos.corpunto.enums.PosModelType;
import com.corpagos.corpunto.interfaces.OnClick;
import com.corpagos.corpunto.models.CountReader;
import com.corpagos.corpunto.singletons.PosModel;
import com.corpagos.corpunto.singletons.Role;

import java.util.ArrayList;

import static com.corpagos.corpunto.Global.chipCount;
import static com.corpagos.corpunto.Global.nfcCount;
import static com.corpagos.corpunto.Global.printLinesCount;
import static com.corpagos.corpunto.Global.printCount;
import static com.corpagos.corpunto.Global.timeCount;
import static com.corpagos.corpunto.utils.SharedPreferencesHandler.saveCount;
import static com.corpagos.corpunto.utils.SharedPreferencesHandler.saveUseTime;

public class CardReadingCountersActivity extends BaseActivity implements OnClick {

    private ArrayList<CountReader> readers = new ArrayList<>();
    private Dialog dialog;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_count_card_reader;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initViews();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_item_delete, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_delete:
                dialog.showInfoDialogWithListenerButtonsAndText(
                        "Desea reiniciar todos los contadores? ", (dialog, which) -> {

                }, (dialog, which) -> {

                }, "Aceptar", "Cancelar");
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    private void initViews() {
        populateArray();
        dialog = new Dialog(this);
        Role role = Role.getInstance(this);
        CountCardReaderListAdapter countCardReaderListAdapter =
                new CountCardReaderListAdapter(this, readers, role.getRoleType(), this);
        ListView lvCardReaders = findViewById(R.id.lv_card_reader);
        lvCardReaders.setAdapter(countCardReaderListAdapter);
        androidx.appcompat.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setSubtitle("Contador de lecturas");
    }

    private void populateArray() {
        PosModel posModel = PosModel.getInstance(this);
        CountReader countReader = new CountReader();
        countReader.setCount(nfcCount);
        countReader.setTitle("NFC");
        countReader.setPosCounterType(PosCounterType.NFC);
        readers.add(countReader);
        countReader = new CountReader();
        countReader.setCount(chipCount);
        countReader.setTitle("CHIP");
        countReader.setPosCounterType(PosCounterType.CHIP);
        readers.add(countReader);
        countReader = new CountReader();
        countReader.setTitle("Total Lectura");
        countReader.setCount((chipCount + nfcCount));
        countReader.setPosCounterType(PosCounterType.TOTAL_READINGS);
        readers.add(countReader);
        countReader = new CountReader();
        countReader.setTitle("Horas de Uso");
        countReader.setPosCounterType(PosCounterType.HOURS_USE);
        readers.add(countReader);
        if (!posModel.getPosModelType().equals(PosModelType.WPOS_MINI)) {
            countReader = new CountReader();
            countReader.setTitle("Tickets Impresos");
            countReader.setCount(printCount);
            countReader.setPosCounterType(PosCounterType.TICKET_PRINTINGS);
            readers.add(countReader);
            countReader = new CountReader();
            countReader.setTitle("Lineas de Impresion");
            countReader.setCount(printLinesCount);
            countReader.setPosCounterType(PosCounterType.PRINTING_LINES);
            readers.add(countReader);
            countReader = new CountReader();
            countReader.setTitle("Metros de Tickets Impresos");
            countReader.setPosCounterType(PosCounterType.PRINTED_METERS);
            readers.add(countReader);
        }
    }

    @Override
    public void onClick(int position) {
        showResetCountersDialog(position);
    }

    private void showResetCountersDialog(int position) {
        CountReader countReader = readers.get(position);
        dialog.showInfoDialogWithListenerButtonsAndText("Desea reiniciar el contador de " +
                        countReader.getTitle(), (dialog, which) -> {
            switch (countReader.getPosCounterType()) {
                case HOURS_USE:
                    timeCount = 0;
                    saveUseTime(this, Global.keyTimeCount, 0);
                    break;
                case CHIP:
                    saveCount(this, Global.keyChipCount, 0);
                    break;
                case NFC:
                    saveCount(this, Global.keyNfcCount, 0);
                    break;
                case PRINTING_LINES:
                    saveCount(this, Global.keyPrintLinesCount, 0);
                    break;
                case TOTAL_READINGS:
                    saveCount(this, Global.keyChipCount, 0);
                    saveCount(this, Global.keyNfcCount, 0);
                    break;
                case TICKET_PRINTINGS:
                    saveCount(this, Global.keyPrintCount, 0);
                    break;
            }
            BackupFiles.backupCounters(this);
        }, (dialog, which) -> {

        }, "Aceptar" , "Cancelar");
    }
}
