package com.corpagos.corpunto.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.corpagos.corpunto.R;
import com.corpagos.corpunto.commons.AnimationTransition;
import com.corpagos.corpunto.commons.Format;
import com.corpagos.corpunto.dialogs.Dialog;
import com.corpagos.corpunto.enums.PosModeType;
import com.corpagos.corpunto.enums.PrepaidServiceType;
import com.corpagos.corpunto.interfaces.KeyListener;
import com.corpagos.corpunto.models.PrepaidService;
import com.corpagos.corpunto.singletons.PosMode;
import com.corpagos.corpunto.widgets.CustomKeyboard;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;

public class PrepaidServiceTargetActivity extends BaseActivity implements KeyListener {

    private ArrayList<String> amounts = new ArrayList<>();
    private PrepaidService prepaidService;
    private ArrayList<String> codes = new ArrayList<>();
    private CustomKeyboard keyboard;
    private PosMode posMode;
    private EditText etNumber;
    private Dialog dialog;
    private Spinner spCode, spAmount;
    private ArrayAdapter<String> amountAdapter;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_prepaid_service_target;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        posMode = PosMode.getInstance(this);
        prepaidService = new PrepaidService();
        etNumber = findViewById(R.id.et_service_target);
        etNumber.setShowSoftInputOnFocus(false);
        etNumber.setOnFocusChangeListener((v, b) -> {
            if (b) {
                keyboard.setEncryption(false, "");
                InputConnection ic12 = etNumber.onCreateInputConnection(new EditorInfo());
                keyboard.setInputConnection(ic12);
            }
        });
        keyboard = findViewById(R.id.kb_prepaid_service);
        keyboard.setListener(this);
        dialog = new Dialog(this);
        spAmount = findViewById(R.id.sp_service_amount);
        spCode = findViewById(R.id.sp_codes);
        amountAdapter = new ArrayAdapter<>(this, R.layout.spinner_item,
                amounts);
        amountAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        spAmount.setAdapter(amountAdapter);
        if (!getIntent().getStringExtra("service").isEmpty()) {
            prepaidService = new Gson().fromJson(getIntent().getStringExtra("service"),
                    PrepaidService.class);
            populateAmounts();
            System.out.println("Valores : " + new Gson().toJson(prepaidService));
            setToolbarTitle("Recarga " + prepaidService.getName());
            if (prepaidService.getPrepaidServiceType().equals(PrepaidServiceType.PHONE)) {
                switch (prepaidService.getName().split(" ")[0].toLowerCase()) {
                    case "movistar":
                        codes = new ArrayList<>(Arrays.asList(getResources().getStringArray(
                                R.array.movistar_codes)));
                        break;
                    case "movilnet":
                        codes = new ArrayList<>(Arrays.asList(getResources().getStringArray(
                                R.array.movilnet_codes)));
                        break;
                    case "digitel":
                        codes = new ArrayList<>(Arrays.asList(getResources().getStringArray(
                                R.array.digitel_codes)));
                        break;
                }
                ArrayAdapter<String> codeAdapter = new ArrayAdapter<>(this, R.layout.spinner_item, codes);
                codeAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
                spCode.setAdapter(codeAdapter);
            } else if (prepaidService.getPrepaidServiceType().equals(PrepaidServiceType.TV)) {
                spCode.setVisibility(View.INVISIBLE);
                TextView textInputLayout = findViewById(R.id.textView2);
                textInputLayout.setText("Numero de contrato");
                TextView tvC = findViewById(R.id.textView5);
                tvC.setVisibility(View.INVISIBLE);
                ConstraintLayout constraintLayout = findViewById(R.id.cl_service);
                ConstraintSet constraintSet = new ConstraintSet();
                constraintSet.clone(constraintLayout);
                constraintSet.connect(R.id.et_service_target, ConstraintSet.START,
                        R.id.sp_service_amount, ConstraintSet.START, 0);
                constraintSet.applyTo(constraintLayout);
            }
        }
    }

    private void populateAmounts() {
        for (int i = prepaidService.getMinAmount(); i < prepaidService.getMaxAmount();
             i += prepaidService.getStepAmount()) {
            amounts.add(Format.getCashFormatWithoutSymbol(i));
        }
        amountAdapter.notifyDataSetChanged();
    }

    private boolean validate() {
        boolean isValid = false;
        if (!etNumber.getText().toString().isEmpty() && etNumber.getText().length() > 6) {
            isValid = true;
        } else {
            etNumber.setError("Debe ingresar un numero valido");
        }
        return isValid;
    }

    @Override
    public void onKeyPressed(Enum specialKey) {
        if (specialKey == KeyListener.specialKey.CANCEL) {
            dialog.showInfoDialogWithListenerButtonsAndText("Seguro que desea cancelar?",
                    (dialog, which) -> finish(), (dialog, which) -> {
                    }, "Aceptar", "Cancelar");
        } else if (specialKey == KeyListener.specialKey.DONE) {
            confirmPayment();
        }
    }

    private void confirmPayment() {
        if (validate()) {
            posMode.setPosMode(PosModeType.PREPAID_SERVICE);
            float amount = Float.valueOf(spAmount.getSelectedItem().toString()
                    .replace(".", "").replace(",", "."));
            prepaidService.setAmount(amount);
            prepaidService.setTargetNumber(prepaidService.getPrepaidServiceType().equals(
                    PrepaidServiceType.PHONE) ? spCode.getSelectedItem().toString() +
                    etNumber.getText().toString() : etNumber.getText().toString());
            Intent intent = new Intent(this, MainActivity.class);
            intent.putExtra("prepaidService", new Gson().toJson(prepaidService));
            startActivity(intent);
            AnimationTransition.setInActivityTransition(this);
            finish();
        }
    }
}
