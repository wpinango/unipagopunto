package com.corpagos.corpunto.activities;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.provider.Settings;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.view.MenuItem;

import com.corpagos.corpunto.R;
import com.corpagos.corpunto.commons.AnimationTransition;
import com.corpagos.corpunto.databases.BackupFiles;
import com.corpagos.corpunto.dialogs.Dialog;
import com.corpagos.corpunto.enums.RoleType;
import com.corpagos.corpunto.interfaces.ButtonInterface;
import com.corpagos.corpunto.models.OwnerData;
import com.corpagos.corpunto.singletons.Role;
import com.google.gson.Gson;


public class AdminActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Admin");
        getFragmentManager().beginTransaction().replace(android.R.id.content,
                new AdminActivity.MainPreferenceFragment()).commit();
    }

    @Override
    protected void onPause() {
        super.onPause();
        ActivityManager activityManager = (ActivityManager) getApplicationContext()
                .getSystemService(Context.ACTIVITY_SERVICE);

        activityManager.moveTaskToFront(getTaskId(), 0);
    }

    public static class MainPreferenceFragment extends PreferenceFragment {

        private Dialog dialog;

        @Override
        public void onCreate(final Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_admin);
            dialog = new Dialog(getActivity());

            Preference resetConsole = findPreference("count_reset");
            resetConsole.setOnPreferenceClickListener(preference -> {
                Intent intent = new Intent(getActivity(), CardReadingCountersActivity.class);
                startActivity(intent);
                AnimationTransition.setInActivityTransition(getActivity());
                /*dialog.showInfoDialogWithListenerButtonsAndText(
                        "Desea reiniciar contador de lectura?", (dialog, which) -> {
                            saveCount(getActivity(), Global.keyChipCount, 0);
                            saveCount(getActivity(), Global.keyNfcCount, 0);
                            if (BackupFiles.isFilesExist(getActivity(),
                                    BackupFiles.counterBackupFileName)) {
                                Map<String,Integer> countReaders = new HashMap<>();
                                countReaders.put(Global.keyChipCount, 0);
                                countReaders.put(Global.keyNfcCount, 0);
                                countReaders.put(Global.keyPrintCount,0);
                                countReaders.put(Global.keyPrintLinesCount,0);
                                BackupFiles.writeFiles(getActivity(),
                                        BackupFiles.counterBackupFileName,
                                        new Gson().toJson(countReaders));
                            }
                        },
                        (materialDialog, which) -> {
                            dialog.dismissDialog();
                        },
                        "Reiniciar", "Cancelar"
                );*/

                return true;
            });

            findPreference("location").setOnPreferenceClickListener(preference -> {
                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                AnimationTransition.setInActivityTransition(getActivity());
                return true;
            });

            Preference preference = findPreference("console");
            preference.setOnPreferenceClickListener(preference13 -> {
                Intent intent = new Intent(getActivity(), ConsoleActivity.class);
                startActivity(intent);
                AnimationTransition.setInActivityTransition(getActivity());
                return true;
            });

            Preference home = findPreference("home");
            home.setOnPreferenceClickListener(preference12 -> {
                startActivity(new Intent(Settings.ACTION_HOME_SETTINGS));
                AnimationTransition.setInActivityTransition(getActivity());
                return true;
            });

            /*findPreference("owner_pass").setOnPreferenceClickListener(preference1 -> {
                dialog.showUpdatePassDialog(getActivity(), new ButtonInterface() {
                    @Override
                    public void onPositiveButtonClick(String buttonAction) {
                        if (BackupFiles.isFilesExist(getActivity(), BackupFiles.ownerBackupFileName)) {
                            OwnerData ownerData = new Gson().fromJson(BackupFiles.getBackupData(
                                    getActivity(), BackupFiles.ownerBackupFileName), OwnerData.class);
                            ownerData.setPass(buttonAction);
                            BackupFiles.backupData(getActivity(), BackupFiles.ownerBackupFileName,
                                    new Gson().toJson(ownerData));
                        } else {
                            dialog.showErrorDialog("No se ha registrado dispositivo Pos");
                        }
                    }

                    @Override
                    public void onNegativeButtonClick(String buttonAction) {

                    }
                }, "MODIFIQUE LA CLAVE DEL PUNTO");
                return true;
            });*/
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            AnimationTransition.setOutActivityTransition(this);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Role role = Role.getInstance(this);
        role.setRoleType(RoleType.USER);
    }

    private static Preference.OnPreferenceChangeListener sBindPreferenceSummaryToValueListener =
            (preference, newValue) -> {
                String stringValue = newValue.toString();
                return true;
            };

    public static void sendFeedback(Context context) {
        String body = null;
        try {
            body = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
            body = "\n\n-----------------------------\nPlease don't remove this information\n Device OS: Android \n Device OS version: " +
                    Build.VERSION.RELEASE + "\n App Version: " + body + "\n Device Brand: " + Build.BRAND +
                    "\n Device Model: " + Build.MODEL + "\n Device Manufacturer: " + Build.MANUFACTURER;
        } catch (PackageManager.NameNotFoundException e) {
        }
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("message/rfc822");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"contact@androidhive.info"});
        intent.putExtra(Intent.EXTRA_SUBJECT, "Query from android app");
        intent.putExtra(Intent.EXTRA_TEXT, body);
        context.startActivity(Intent.createChooser(intent, context.getString(R.string.choose_email_client)));
    }
}
