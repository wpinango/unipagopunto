package com.corpagos.corpunto.activities;

import android.content.Intent;
import android.os.Bundle;
import android.widget.GridView;

import com.corpagos.corpunto.Asynctask;
import com.corpagos.corpunto.Global;
import com.corpagos.corpunto.R;
import com.corpagos.corpunto.adapters.PrepaidServiceGridAdapter;
import com.corpagos.corpunto.commons.AnimationTransition;
import com.corpagos.corpunto.dialogs.Dialog;
import com.corpagos.corpunto.enums.PrepaidServiceType;
import com.corpagos.corpunto.interfaces.AsynctaskListener;
import com.corpagos.corpunto.models.PrepaidService;
import com.corpagos.corpunto.models.Response;
import com.corpagos.corpunto.models.Token;
import com.corpagos.corpunto.app.AppCredentials;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class PrepaidServiceActivity extends BaseActivity implements AsynctaskListener {

    private ArrayList<PrepaidService> services = new ArrayList<>();
    private Dialog dialog;
    private PrepaidServiceGridAdapter prepaidServiceGridAdapter;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_prepaid_service;
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initViews();
    }

    private void initViews() {
        dialog = new Dialog(this);
        requestServices();
        prepaidServiceGridAdapter = new PrepaidServiceGridAdapter(this, services);
        GridView gridPrepaidService = findViewById(R.id.grid_prepaid_service);
        gridPrepaidService.setAdapter(prepaidServiceGridAdapter);
        gridPrepaidService.setOnItemClickListener((adapterView, view, i, l) -> {
            Intent intent = new Intent(this, PrepaidServiceTargetActivity.class);
            intent.putExtra("service", new Gson().toJson(services.get(i)));
            startActivity(intent);
            AnimationTransition.setInActivityTransition(this);
        });
        setToolbarTitle("Recargas");
    }

    private void populateArrayService() {
        PrepaidService prepaidService = new PrepaidService();
        prepaidService.setLogo("digitel");
        prepaidService.setName("Digitel");
        prepaidService.setPrepaidServiceType(PrepaidServiceType.PHONE);
        services.add(prepaidService);
        prepaidService = new PrepaidService();
        prepaidService.setLogo("directv");
        prepaidService.setName("Directv");
        prepaidService.setPrepaidServiceType(PrepaidServiceType.TV);
        services.add(prepaidService);
        prepaidService = new PrepaidService();
        prepaidService.setLogo("movilnet");
        prepaidService.setName("Movilnet");
        prepaidService.setPrepaidServiceType(PrepaidServiceType.PHONE);
        services.add(prepaidService);
        prepaidService = new PrepaidService();
        prepaidService.setLogo("movistar");
        prepaidService.setName("Movistar");
        prepaidService.setPrepaidServiceType(PrepaidServiceType.PHONE);
        services.add(prepaidService);
    }

    private void requestServices() {
        dialog.showProgressDialog("Cargando...");
        Token tokenObject = AppCredentials.getToken(this);
        Map<String, String> headers = new HashMap<>();
        headers.put(Asynctask.authorization, tokenObject.getToken_type() + " " +
                tokenObject.getAccess_token());
        new Asynctask.GetMethodAsynctask(Asynctask.transactionalHost, Asynctask.URL_GET_SERVICE, headers,
                this).execute();
    }

    @Override
    public void onAsynctaskFinished(String endPoint, String response, String headers) {
        System.out.println("valores :  " + response);
        dialog.dismissDialog();
        if (!response.equals("")) {
            Response res = new Gson().fromJson(response, Response.class);
            if (!res.isError()) {
                if (res.getMessage().equals(Asynctask.OK_RESPONSE)) {
                    Type custom = new TypeToken<ArrayList<PrepaidService>>() {
                    }.getType();
                    ArrayList<PrepaidService> prepaidServices = new Gson().fromJson(new Gson()
                            .toJson(res.getData()), custom);
                    services.addAll(prepaidServices);
                    System.out.println("valores : 100 " + new Gson().toJson(services));
                    prepaidServiceGridAdapter.notifyDataSetChanged();
                }
            } else {
                if (res.getMessage().equals("ERR_INVALID_OR_EXPIRED_TOKEN")) {
                    dialog.showErrorDialogWithListenerButton("Token invalido",
                            sweetAlertDialog -> {
                        finish();
                    });
                }
            }
        } else {
            dialog.showErrorDialog("Error de comunicacion");
        }
    }
}
