package com.corpagos.corpunto.activities;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.MenuItem;

import com.corpagos.corpunto.R;
import com.corpagos.corpunto.fragments.rollback.DniRollbackFragment;
import com.corpagos.corpunto.fragments.rollback.SecurityPinRollbackFragment;
import com.corpagos.corpunto.interfaces.KeyListener;
import com.corpagos.corpunto.models.Transaction;
import com.corpagos.corpunto.widgets.CustomKeyboard;
import com.google.gson.Gson;

public class RollbackActivity extends BaseActivity implements KeyListener {

    private SecurityPinRollbackFragment securityPinRollbackFragment = new SecurityPinRollbackFragment();
    private DniRollbackFragment dniRollbackFragment = new DniRollbackFragment();
    private final String SECURITY_TAG = "security", DNI_TAG = "dni";
    private FragmentTransaction fragTransaction;
    private CustomKeyboard keyboard;
    private Transaction transaction;
    private String dni, pinCode;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_rollback;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarTitle("Reversar");
        if (!getIntent().getStringExtra("transaction").equals("")) {
            transaction = new Gson().fromJson(getIntent().getStringExtra("transaction"),
                    Transaction.class);
        }
        keyboard = findViewById(R.id.kb_rollback);
        keyboard.setListener(this);
        showSecurityPinFragment();
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void closeActivity() {
        if (isDniRollbackFragmentVisible()) {
            showSecurityPinFragment();
        } else if (isSecurityPinFragmentVisible()) {
            finish();
        }
    }


    @Override
    public void onKeyPressed(Enum specialKey) {
        if (specialKey == KeyListener.specialKey.CANCEL) {
            finish();
        } else if (specialKey == KeyListener.specialKey.DONE) {
           if (isSecurityPinFragmentVisible()) {
               if (securityPinRollbackFragment.validate()) {
                   pinCode = securityPinRollbackFragment.getPin();
                   showDniRollback();
               }
           } else if (isDniRollbackFragmentVisible()) {
               if (dniRollbackFragment.validate()) {
                   dni = dniRollbackFragment.getDni();
                   dniRollbackFragment.showRollBackDialog();
               }
           }
        } else if (specialKey == KeyListener.specialKey.FORWARD) {

        } else if (specialKey == KeyListener.specialKey.BACK) {

        }
    }

    public void showSecurityPinFragment() {
        fragTransaction = getFragmentManager().beginTransaction();
        fragTransaction.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
        fragTransaction.replace(R.id.fl_rollback, securityPinRollbackFragment, SECURITY_TAG);
        fragTransaction.commit();
        securityPinRollbackFragment.setKeyboard(keyboard);
    }

    public void showDniRollback() {
        fragTransaction = getFragmentManager().beginTransaction();
        fragTransaction.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
        fragTransaction.replace(R.id.fl_rollback, dniRollbackFragment, DNI_TAG);
        fragTransaction.commit();
        dniRollbackFragment.setKeyboard(keyboard);
        dniRollbackFragment.setTransactionData(transaction.getReference(), pinCode);
    }

    private boolean isDniRollbackFragmentVisible() {
        return getFragmentManager().findFragmentByTag(DNI_TAG) != null &&
                getFragmentManager().findFragmentByTag(DNI_TAG).isVisible();
    }

    private boolean isSecurityPinFragmentVisible() {
        return getFragmentManager().findFragmentByTag(SECURITY_TAG) != null &&
                getFragmentManager().findFragmentByTag(SECURITY_TAG).isVisible();
    }
}
