package com.corpagos.corpunto.activities;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ToggleButton;

import com.corpagos.corpunto.R;
import com.corpagos.corpunto.adapters.PingListAdapter;
import com.stealthcopter.networktools.Ping;
import com.stealthcopter.networktools.ping.PingResult;
import com.stealthcopter.networktools.ping.PingStats;

import java.net.UnknownHostException;
import java.util.ArrayList;

import static com.corpagos.corpunto.commons.ValidateFields.getIpformat;

public class PingActivity extends AppCompatActivity {
    private EditText etPing;
    private ToggleButton btnOp;
    private PingListAdapter pingListAdapter;
    private ArrayList<String> pingResults = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ping);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setSubtitle("ping to address");
        pingListAdapter = new PingListAdapter(this,pingResults);
        ListView lvPing = findViewById(R.id.lv_ping);
        lvPing.setAdapter(pingListAdapter);
        etPing = findViewById(R.id.tv_ping_result);
        btnOp = findViewById(R.id.btn_ping);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        etPing.setFilters(getIpformat());
        btnOp.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b){
                try {
                    Ping.onAddress(etPing.getText().toString()).setTimeOutMillis(1000)
                            .setTimes(5).doPing(new Ping.PingListener() {
                        @Override
                        public void onResult(PingResult pingResult) {
                            if (pingResult.fullString != null) {
                                pingResults.add(pingResult.fullString.split("\\n\\n")[0]);
                            } else {
                                pingResults.add("Destination host is unreachable");
                            }
                        }
                        @Override
                        public void onFinished(PingStats pingStats) {
                            runOnUiThread(() -> {
                                btnOp.setChecked(false);
                                pingListAdapter.notifyDataSetChanged();
                            });
                        }
                    });
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_item_delete, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.menu_delete){
            pingResults.clear();
            pingListAdapter.notifyDataSetChanged();
        }
        return super.onOptionsItemSelected(item);
    }
}
