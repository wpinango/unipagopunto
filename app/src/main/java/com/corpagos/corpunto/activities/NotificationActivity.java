package com.corpagos.corpunto.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.corpagos.corpunto.Global;
import com.corpagos.corpunto.R;
import com.corpagos.corpunto.adapters.NotificationListAdapter;
import com.corpagos.corpunto.commons.AnimationTransition;
import com.corpagos.corpunto.databases.BackupFiles;
import com.corpagos.corpunto.dialogs.Dialog;
import com.corpagos.corpunto.models.Notification;
import com.corpagos.corpunto.models.UpdateApp;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;

import static com.corpagos.corpunto.activities.MainLauncherActivity.version;
import static com.corpagos.corpunto.app.AppVersion.versionCompare;

public class NotificationActivity extends BaseActivity {

    public static boolean isNotification, isUpdateNotification = false;
    private ArrayList<Notification.NotificationData> notificationData = new ArrayList<>();
    private Dialog dialog;
    private  NotificationListAdapter notificationListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setTitle("Notificaciones");
        initViews();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_item_delete, menu);
        return true;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_notification;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_delete) {
            dialog.showWarningDelectionWithListenerButton("Desea eliminar las notificaciones?",
                    (dialog, which) -> {
                        BackupFiles.writeFiles(this, BackupFiles.notificationBackupFileName,
                                "");
                        notificationData.clear();
                        notificationListAdapter.notifyDataSetChanged();
                    });
        }
        return super.onOptionsItemSelected(item);
    }

    private void initViews() {
        ListView lvNotification = findViewById(R.id.lv_notification);
        notificationListAdapter = new NotificationListAdapter(this, notificationData);
        lvNotification.setAdapter(notificationListAdapter);
        if (!getNotificationStorage(this).isEmpty()) {
            notificationData.addAll(getNotificationStorage(this));
        }
        notificationListAdapter.notifyDataSetChanged();
        dialog = new Dialog(this);
        lvNotification.setOnItemClickListener((parent, view, position, id) -> {
            if (notificationData.get(position).getType().equals("Update")) {
                UpdateApp updateApp = new Gson().fromJson(notificationData.get(position)
                                .getData().toString(), UpdateApp.class);
                if (versionCompare(updateApp.getVersion(), version) > 0) {
                    dialog.showInfoDialogWithListenerButtons("Desea descargar la actualizacion?",
                            (dialog, which) -> {
                                Intent intent = new Intent(this, UpdateAppActivity.class);
                                startActivity(intent);
                                AnimationTransition.setInActivityTransition(this);
                            }, (dialog, which) -> {

                            });
                } else {
                    Global.showToast(this, "Posee la version mas actualizada", 3000);
                }
            }
        });
        lvNotification.setOnItemLongClickListener((parent, view, position, id) -> {
            dialog.showWarningDelectionWithListenerButton("Desea eliminar esta notificacion?",
                    (dialog, which) -> {
                        notificationData.remove(position);
                        notificationListAdapter.notifyDataSetChanged();
                    });
            return true;
        });
    }

    public static ArrayList<Notification.NotificationData> getNotificationStorage(Context context) {
        Type custom = new TypeToken<ArrayList<Notification.NotificationData>>() {
        }.getType();
        ArrayList<Notification.NotificationData> notificationData = new ArrayList<>();
        if (BackupFiles.isFilesExist(context, BackupFiles.notificationBackupFileName)) {
            if (!BackupFiles.readFileContent(context,BackupFiles.notificationBackupFileName).equals("")) {
                notificationData = new Gson().fromJson(BackupFiles.readFileContent(context,
                        BackupFiles.notificationBackupFileName), custom);
                Collections.reverse(notificationData);
            }
        }
        return notificationData;
    }
}
