package com.corpagos.corpunto.activities;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import android.view.Menu;
import android.view.MenuItem;

import com.corpagos.corpunto.R;
import com.corpagos.corpunto.commons.AnimationTransition;
import com.jraska.console.Console;

public class ConsoleActivity extends BaseActivity{

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("Consola de eventos");
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_console;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        AnimationTransition.setOutActivityTransition(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_item_delete, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_delete:
                Console.clear();
                break;
            case android.R.id.home:
                this.finish();
                AnimationTransition.setOutActivityTransition(this);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }
}
