package com.corpagos.corpunto.activities;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.MenuItem;

import com.corpagos.corpunto.R;
import com.corpagos.corpunto.fragments.payments.FindProviderFragment;
import com.corpagos.corpunto.fragments.payments.ProviderInformationFragment;
import com.corpagos.corpunto.fragments.payments.ProviderPaymentFragment;
import com.corpagos.corpunto.interfaces.KeyListener;
import com.corpagos.corpunto.models.Provider;
import com.corpagos.corpunto.models.ProviderAccount;
import com.corpagos.corpunto.models.TransactionLocation;
import com.corpagos.corpunto.widgets.CustomKeyboard;
import com.google.gson.Gson;


public class ProviderActivity extends BaseActivity implements KeyListener {

    private FragmentTransaction fragTransaction;
    private CustomKeyboard keyboard;
    private Provider provider;
    private TransactionLocation transactionLocation;
    private FindProviderFragment findProviderFragment = new FindProviderFragment();
    private ProviderInformationFragment providerInformationFragment = new ProviderInformationFragment();
    private ProviderPaymentFragment providerPaymentFragment = new ProviderPaymentFragment();

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_provider;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        keyboard = findViewById(R.id.customKeyboard);
        keyboard.setListener(this);
        showFindProviderInfo();
        findProviderFragment.setKeyboard(keyboard);
        if (!getIntent().getStringExtra("location").isEmpty()) {
            transactionLocation = new Gson().fromJson(getIntent().getStringExtra("location"),
                    TransactionLocation.class);
        }
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void closeActivity() {
        if (getFragmentManager().findFragmentByTag("tis") != null &&
                getFragmentManager().findFragmentByTag("tis").isVisible()) {
            finish();
        } else if (getFragmentManager().findFragmentByTag("payment") != null &&
                getFragmentManager().findFragmentByTag("payment").isVisible()) {
            showProviderInfoFragment(provider);
        } else {
            showFindProviderInfo();
        }
    }

    public void setToolbarTitle(String title) {
        setTitle(title);
    }

    private void showFindProviderInfo() {
        fragTransaction = getFragmentManager().beginTransaction();
        fragTransaction.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
        fragTransaction.replace(R.id.fragment_layout, findProviderFragment, "tis");
        fragTransaction.commit();
    }

    public void showProviderInfoFragment(Provider provider) {
        this.provider = provider;
        providerInformationFragment.setProvider(provider);
        fragTransaction = getFragmentManager().beginTransaction();
        fragTransaction.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
        fragTransaction.replace(R.id.fragment_layout, providerInformationFragment, "information");
        fragTransaction.commit();
    }

    public void showProviderPaymentFragment(Provider provider, ProviderAccount providerAccount) {
        providerPaymentFragment.setProviderInfo(provider, providerAccount);
        providerPaymentFragment.setTransactionLocation(transactionLocation);
        fragTransaction = getFragmentManager().beginTransaction();
        fragTransaction.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
        fragTransaction.replace(R.id.fragment_layout, providerPaymentFragment, "payment");
        fragTransaction.commit();
    }

    @Override
    public void onKeyPressed(Enum specialKey) {
        if (specialKey == KeyListener.specialKey.CANCEL) {
            finish();
        } else if (specialKey == KeyListener.specialKey.DONE) {
            if (getFragmentManager().findFragmentByTag("tis") != null &&
                    getFragmentManager().findFragmentByTag("tis").isVisible()) {
                if (findProviderFragment.validate()) {
                    findProviderFragment.requestProviderInfo();
                }
            } else if (getFragmentManager().findFragmentByTag("information") != null &&
                    getFragmentManager().findFragmentByTag("information").isVisible()) {
                providerInformationFragment.selectAccount();
                providerPaymentFragment.setKeyboard(keyboard);
            } else  if (getFragmentManager().findFragmentByTag("payment") != null &&
                    getFragmentManager().findFragmentByTag("payment").isVisible()) {
                if (providerPaymentFragment.isEtAmountFocus()) {
                    providerPaymentFragment.setEtPassFocus();
                } else {
                    if (providerPaymentFragment.validate()) {
                        providerPaymentFragment.prepareTransactionData();
                    }
                }
            }
        } else if (specialKey == KeyListener.specialKey.FORWARD) {
            if (getFragmentManager().findFragmentByTag("payment") != null &&
                    getFragmentManager().findFragmentByTag("payment").isVisible()) {
                providerPaymentFragment.setEtPassFocus();
            }
        } else if (specialKey == KeyListener.specialKey.BACK) {
            if (getFragmentManager().findFragmentByTag("payment") != null &&
                    getFragmentManager().findFragmentByTag("payment").isVisible()) {
                providerPaymentFragment.setEtAmountFocus();
            }
        }
    }
}
