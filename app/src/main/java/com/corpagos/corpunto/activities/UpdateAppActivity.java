package com.corpagos.corpunto.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;

import androidx.core.content.FileProvider;
import androidx.appcompat.app.AppCompatActivity;

import android.view.MenuItem;
import android.widget.TextView;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.corpagos.corpunto.Asynctask;
import com.corpagos.corpunto.R;
import com.corpagos.corpunto.app.AppCredentials;
import com.corpagos.corpunto.commons.AnimationTransition;
import com.corpagos.corpunto.dialogs.Dialog;
import com.corpagos.corpunto.interfaces.AsynctaskListener;
import com.corpagos.corpunto.interfaces.DownloadProgress;
import com.corpagos.corpunto.interfaces.OnFileExist;
import com.corpagos.corpunto.logs.CustomizedExceptionHandler;
import com.corpagos.corpunto.models.Response;
import com.corpagos.corpunto.models.Token;
import com.corpagos.corpunto.models.UpdateApp;
import com.corpagos.corpunto.requests.DownloadTask;
import com.corpagos.corpunto.app.AppVersion;
import com.google.gson.Gson;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import static com.corpagos.corpunto.Global.software;
import static com.corpagos.corpunto.activities.NotificationActivity.isUpdateNotification;
import static com.corpagos.corpunto.databases.BackupFiles.backupConfig;
import static com.corpagos.corpunto.databases.BackupFiles.backupCounters;
import static com.corpagos.corpunto.databases.BackupFiles.backupTransactions;
import static com.corpagos.corpunto.databases.BackupFiles.backupVersion;
import static com.corpagos.corpunto.services.DeleteFirebaseToken.deleteFBToken;
import static com.corpagos.corpunto.app.AppVersion.getAppVersion;


public class UpdateAppActivity extends AppCompatActivity implements OnFileExist, DownloadProgress,
        AsynctaskListener {

    private Dialog dialog;
    private TextView tvVersion;
    private BootstrapButton btnCheckUpdate;
    private boolean isUpdate = false;
    private ProgressDialog progressDialog;
    public static String fileName = "app.apk";
    private String path, version;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);
        initViews();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        AnimationTransition.setOutActivityTransition(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                AnimationTransition.setOutActivityTransition(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initViews() {
        dialog = new Dialog(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Actualizacion");
        tvVersion = findViewById(R.id.tv_update);
        btnCheckUpdate = findViewById(R.id.btn_check_update);
        btnCheckUpdate.setOnClickListener(view -> {
            if (isUpdate) {
                new DownloadTask(path, this).execute();
            } else {
                requestVersion();
            }
        });
        requestVersion();
        initProgressDialog();
        Thread.setDefaultUncaughtExceptionHandler(new CustomizedExceptionHandler());
    }

    private void initProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Descargando archivo. Espere por favor...");
        progressDialog.setIndeterminate(false);
        progressDialog.setMax(100);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setCancelable(true);
    }

    @Override
    public void onFileExist(boolean isExist) {
        dialog.dismissDialog();
        isUpdate = isExist;
        if (isExist) {
            tvVersion.setText("Se econtro una nueva actualizacion");
            btnCheckUpdate.setText("Descargar actualizacion");
        }
    }

    @Override
    public void onDownloadUpdate(Enum progressType, int progress) {
        System.out.println("valores : descargar " + progress);
        if (progressType.equals(DownloadProgress.progressType.ONPROGRESS)) {
            dialog.updateProgressDialog(progress, progressDialog);
        } else if (progressType.equals(DownloadProgress.progressType.FINISH)) {
            backupTransactions(this);
            backupCounters(this);
            backupConfig(this);
            backupVersion(this, this.version);
            progressDialog.dismiss();
            dialog.showDownloadFileSuccess((materialDialog, which) -> {
                dialog.dismissDialog();
                isUpdateNotification = false;
                deleteFBToken(dialog);
                dialog.showProgressDialog("Procesando ...");
                installApk(Environment.getExternalStoragePublicDirectory( //TODO VALIDAR APK
                        Environment.DIRECTORY_DOWNLOADS + "/" + fileName));
            }, (materialDialog, which) -> dialog.dismissDialog());
        } else if (progressType.equals(DownloadProgress.progressType.ERROR)) {
            dialog.showErrorDialog("No se ha podido descargar la actualizacion");
        }
    }

    private void requestVersion() {
        dialog.showProgressDialog("Consultando si existe una actualizacion ... ");
        Map<String, String> headers = new HashMap<>();
        Token token = AppCredentials.getToken(this);
        headers.put(Asynctask.authorization, token.getToken_type() + " " + token.getAccess_token());
        new Asynctask.GetMethodAsynctask(Asynctask.resourcesHost, Asynctask.URL_GET_VERSION +
                software, headers, this).execute();
    }

    private void installApk(File file) {
        try {
            if (file.exists()) {
                String[] fileNameArray = file.getName().split(Pattern.quote("."));
                if (fileNameArray[fileNameArray.length - 1].equals("apk")) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        Uri downloadedApk = getFileUri(this, file);
                        Intent intent = new Intent(Intent.ACTION_VIEW).setDataAndType(downloadedApk,
                                "application/vnd.android.package-archive");
                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        intent.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
                        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                    } else {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setDataAndType(Uri.fromFile(file),
                                "application/vnd.android.package-archive");
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        intent.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
                        startActivity(intent);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Uri getFileUri(Context context, File file) {
        return FileProvider.getUriForFile(context,
                context.getApplicationContext().getPackageName() +
                        ".HelperClasses.GenericFileProvider", file);
    }

    @Override
    public void onAsynctaskFinished(String endPoint, String response, String headers) {
        dialog.dismissDialog();
        System.out.println("Valores : update " + response);
        if (!response.equals("")) {
            if (endPoint.contains(Asynctask.URL_GET_VERSION)){
                Response res = new Gson().fromJson(response, Response.class);
                if (!res.isError()){
                    Map<String, Map<String, String>> map = (Map<String, Map<String, String>>)
                            res.getData();
                    UpdateApp version = new Gson().fromJson(new Gson().toJson(map.get("version")),
                            UpdateApp.class);
                    if (AppVersion.versionCompare(version.getVersion(),
                            getAppVersion(this)) > 0) {
                        this.isUpdate = true;
                        this.path = version.getPath();
                        this.version = version.getVersion();
                        btnCheckUpdate.setText("Descargar actualizacion");
                        tvVersion.setText("Existe una actualizacion, la version " + version.getVersion());
                    } else {
                        tvVersion.setText("Posee la ultima version V " + getAppVersion(this));
                    }
                } else {
                    if (res.getMessage().equals("ERR_NO_VERSIONS")) {
                        dialog.showInfoDialog("No se ha encontrado actualizacion");
                    } else if (res.getMessage().equals("ERR_UNKNOW_DEVICE")) {
                        dialog.showErrorDialog("Dispositivo desconocido");
                    } else {
                        dialog.showErrorDialog("Algo salio mal");
                    }
                }
            }
        }
    }
}
