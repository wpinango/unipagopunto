package com.corpagos.corpunto.activities;

import android.app.ActivityManager;
import android.content.Context;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.MenuItem;

import com.corpagos.corpunto.Global;
import com.corpagos.corpunto.commons.AnimationTransition;

public abstract class BaseActivity extends AppCompatActivity {

    androidx.appcompat.app.ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResourceId());
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        activityResumed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        activityPaused();
        ActivityManager activityManager = (ActivityManager) getApplicationContext()
                .getSystemService(Context.ACTIVITY_SERVICE);

        activityManager.moveTaskToFront(getTaskId(), 0);
    }

    public void setToolbarTitle(String title) {
        actionBar.setTitle(title);
    }

    protected abstract int getLayoutResourceId();

    public static void activityResumed() {
        Global.activityVisible = true;
    }

    public static void activityPaused() {
        Global.activityVisible = false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        AnimationTransition.setOutActivityTransition(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                AnimationTransition.setOutActivityTransition(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
