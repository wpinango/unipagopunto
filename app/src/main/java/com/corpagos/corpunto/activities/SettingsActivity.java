package com.corpagos.corpunto.activities;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.RingtonePreference;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.MenuItem;

import com.corpagos.corpunto.Asynctask;
import com.corpagos.corpunto.Global;
import com.corpagos.corpunto.R;
import com.corpagos.corpunto.app.AppCredentials;
import com.corpagos.corpunto.app.AppFirstLogin;
import com.corpagos.corpunto.commons.AnimationTransition;
import com.corpagos.corpunto.databases.BackupFiles;
import com.corpagos.corpunto.dialogs.Dialog;
import com.corpagos.corpunto.interfaces.AsynctaskListener;
import com.corpagos.corpunto.interfaces.DialogInterface;
import com.corpagos.corpunto.models.Response;
import com.corpagos.corpunto.singletons.PosMode;
import com.corpagos.corpunto.singletons.PosModel;
import com.corpagos.corpunto.utils.SharedPreferencesHandler;
import com.corpagos.corpunto.wifi.WifiInformation;
import com.google.gson.Gson;

import java.util.HashMap;

import static com.corpagos.corpunto.Asynctask.finalHost;
import static com.corpagos.corpunto.activities.MainLauncherActivity.posSerial;
import static com.corpagos.corpunto.activities.NotificationActivity.isNotification;

public class SettingsActivity extends AppCompatPreferenceActivity {
    private static final String TAG = SettingsActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Configuraciones");
        getFragmentManager().beginTransaction().replace(android.R.id.content,
                new MainPreferenceFragment()).commit();
    }

    @Override
    protected void onPause() {
        super.onPause();
        ActivityManager activityManager = (ActivityManager) getApplicationContext()
                .getSystemService(Context.ACTIVITY_SERVICE);

        activityManager.moveTaskToFront(getTaskId(), 0);
    }

    public static class MainPreferenceFragment extends PreferenceFragment implements AsynctaskListener {
        private WifiInformation wifiInformation;
        private Preference wifi;

        @Override
        public void onCreate(final Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_main);

            findPreference("signin").setOnPreferenceClickListener(preference -> {
                PosModel posModel = PosModel.getInstance(getActivity());
                if (!AppFirstLogin.isLogin(getActivity())) {
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    intent.putExtra("posSerial", posModel.getPosInformation().getPosSerial());
                    startActivity(intent);
                    AnimationTransition.setInActivityTransition(getActivity());
                } else {
                    if (!AppCredentials.isLogin(getActivity())) {
                        Intent intent = new Intent(getActivity(), QRCodeReaderActivity.class);
                        startActivity(intent);
                        AnimationTransition.setInActivityTransition(getActivity());
                    } else {
                        Global.showToast(getActivity(), "Dispositivo registrado", 3000);
                    }
                }
                return true;
            });

            Preference time = findPreference("date");
            time.setOnPreferenceClickListener(preference -> {

                startActivity(new Intent(Settings.ACTION_DATE_SETTINGS));
                AnimationTransition.setInActivityTransition(getActivity());
                return true;
            });

            findPreference("mobile_usage").setOnPreferenceClickListener(preference -> {
                Intent intent = new Intent();
                intent.setComponent(new ComponentName("com.android.settings",
                        "com.android.settings.Settings$DataUsageSummaryActivity"));
                startActivity(intent);
                AnimationTransition.setInActivityTransition(getActivity());
                return true;
            });

            findPreference("wireless").setOnPreferenceClickListener(preference -> {
                startActivity(new Intent(Settings.ACTION_WIRELESS_SETTINGS));
                AnimationTransition.setInActivityTransition(getActivity());
                return true;
            });

            Preference screenBrigth = findPreference("screen");
            screenBrigth.setOnPreferenceClickListener(preference -> {
                startActivity(new Intent(Settings.ACTION_DISPLAY_SETTINGS));
                AnimationTransition.setInActivityTransition(getActivity());
                return true;
            });

            Preference update = findPreference("update");
            if (isNotification) {
                update.setSummary("Se ha encontrado una nueva actualizacion");
                update.setIcon(getResources().getDrawable(R.drawable.ic_info_outline_black_36dp));
            }
            update.setOnPreferenceClickListener(preference -> {
                Intent intent = new Intent(getActivity(), UpdateAppActivity.class);
                startActivity(intent);
                AnimationTransition.setInActivityTransition(getActivity());
                return false;
            });

            wifiInformation = new WifiInformation(getActivity());
            wifi = findPreference("wifi");
            wifi.setOnPreferenceClickListener(preference -> {
                startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                AnimationTransition.setInActivityTransition(getActivity());
                return true;
            });

            Preference count = findPreference("count");
            count.setOnPreferenceClickListener(preference -> {
                Intent intent = new Intent(getActivity(), CardReadingCountersActivity.class);
                startActivity(intent);
                AnimationTransition.setInActivityTransition(getActivity());
                return true;
            });
            Preference myPref = findPreference(getString(R.string.key_send_feedback));
            myPref.setOnPreferenceClickListener(preference -> {
                sendFeedback(getActivity());
                return true;
            });

            Preference access = findPreference("access");
            access.setOnPreferenceClickListener(preference -> {
                startActivity(new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS));
                AnimationTransition.setInActivityTransition(getActivity());
                return true;
            });

            findPreference("about").setOnPreferenceClickListener(preference -> {
                startActivity(new Intent(Settings.ACTION_DEVICE_INFO_SETTINGS));
                AnimationTransition.setInActivityTransition(getActivity());
                return true;
            });

            Preference server = findPreference("server");
            server.setSummary(finalHost);
            server.setOnPreferenceClickListener(preference -> {
                Dialog.showServerSelectionDialog(getActivity(), new DialogInterface() {
                    @Override
                    public void positiveClick() {
                        server.setSummary(finalHost);
                    }

                    @Override
                    public void negativeClick() {

                    }
                });
                return false;
            });

            Preference logout = findPreference("logout");
            logout.setOnPreferenceClickListener(preference -> {
                Dialog dialog = new Dialog(getActivity());
                dialog.showInfoDialogWithListenerButtons("Cerrar Sesion?", (dialog1, which) -> {
                    PosModel posModel = PosModel.getInstance(getActivity());
                    System.out.println("valores : " + Asynctask.URL_LOGOUT
                            + posModel.getPosInformation().getPosSerial());
                    new Asynctask.GetMethodAsynctask(Asynctask.authHost, Asynctask.URL_LOGOUT
                            + posModel.getPosInformation().getPosSerial(), new HashMap<>(), this).execute();
                }, null);
                return false;
            });

            Preference aes = findPreference("aes");
            aes.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    SharedPreferencesHandler.saveData(getActivity(), SharedPreferencesHandler.KEY_AES_KEY,
                            SharedPreferencesHandler.NOT_FOUND,
                            SharedPreferencesHandler.FILE_AES_KEY
                    );
                    return false;
                }
            });
        }

        @Override
        public void onResume() {
            super.onResume();
            if (wifiInformation.isWifiConnected()) {
                wifi.setSummary(wifiInformation.getWifiInformation().getSSID().
                        replace("\"", ""));
            } else {
                wifi.setSummary("");
            }
        }

        @Override
        public void onAsynctaskFinished(String endPoint, String response, String headers) {
            System.out.println("valores : sesion " + response);
            if (!response.equals("")) {
                Response res = new Gson().fromJson(response, Response.class);
                    if (res.getMessage().equals("OK")) {
                        BackupFiles.resetFirstLogin(getActivity());
                    }
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            AnimationTransition.setOutActivityTransition(this);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        AnimationTransition.setOutActivityTransition(this);
    }

    private static void bindPreferenceSummaryToValue(Preference preference) {
        preference.setOnPreferenceChangeListener(sBindPreferenceSummaryToValueListener);

        sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,
                PreferenceManager
                        .getDefaultSharedPreferences(preference.getContext())
                        .getString(preference.getKey(), ""));
    }

    private static Preference.OnPreferenceChangeListener sBindPreferenceSummaryToValueListener =
            (preference, newValue) -> {
                String stringValue = newValue.toString();

                if (preference instanceof ListPreference) {
                    ListPreference listPreference = (ListPreference) preference;
                    int index = listPreference.findIndexOfValue(stringValue);
                    preference.setSummary(
                            index >= 0 ? listPreference.getEntries()[index] : null);
                } else if (preference instanceof RingtonePreference) {
                    if (TextUtils.isEmpty(stringValue)) {
                        preference.setSummary(R.string.pref_ringtone_silent);
                    } else {
                        Ringtone ringtone = RingtoneManager.getRingtone(
                                preference.getContext(), Uri.parse(stringValue));
                        if (ringtone == null) {
                            preference.setSummary(R.string.summary_choose_ringtone);
                        } else {
                            String name = ringtone.getTitle(preference.getContext());
                            preference.setSummary(name);
                        }
                    }

                } else if (preference instanceof EditTextPreference) {
                    if (preference.getKey().equals("key_gallery_name")) {
                        preference.setSummary(stringValue);
                    }
                } else {
                    preference.setSummary(stringValue);
                }
                return true;
            };

    public static void sendFeedback(Context context) {
        String body = null;
        try {
            body = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
            body = "\n\n-----------------------------\nPlease don't remove this information\n Device OS: Android \n Device OS version: " +
                    Build.VERSION.RELEASE + "\n App Version: " + body + "\n Device Brand: " + Build.BRAND +
                    "\n Device Model: " + Build.MODEL + "\n Device Manufacturer: " + Build.MANUFACTURER;
        } catch (PackageManager.NameNotFoundException e) {
        }
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("message/rfc822");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"contact@androidhive.info"});
        intent.putExtra(Intent.EXTRA_SUBJECT, "Query from android app");
        intent.putExtra(Intent.EXTRA_TEXT, body);
        context.startActivity(Intent.createChooser(intent, context.getString(R.string.choose_email_client)));
    }


}