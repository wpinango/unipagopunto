package com.corpagos.corpunto.activities;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.corpagos.corpunto.R;
import com.corpagos.corpunto.commons.AnimationTransition;
import com.corpagos.corpunto.dialogs.Dialog;
import com.corpagos.corpunto.emv.CardType;
import com.corpagos.corpunto.enums.CardAccountType;
import com.corpagos.corpunto.enums.CardTypeEnum;
import com.corpagos.corpunto.enums.PosModeType;
import com.corpagos.corpunto.fragments.payments.FindProviderFragment;
import com.corpagos.corpunto.fragments.payments.FirstFragment;
import com.corpagos.corpunto.fragments.payments.ProviderInformationFragment;
import com.corpagos.corpunto.fragments.payments.ProviderPaymentFragment;
import com.corpagos.corpunto.interfaces.KeyListener;
import com.corpagos.corpunto.interfaces.ReadCardListener;
import com.corpagos.corpunto.models.CardData;
import com.corpagos.corpunto.models.Provider;
import com.corpagos.corpunto.models.ProviderAccount;
import com.corpagos.corpunto.models.Transaction;
import com.corpagos.corpunto.models.TransactionLocation;
import com.corpagos.corpunto.pos.PosDevice;
import com.corpagos.corpunto.singletons.CardMode;
import com.corpagos.corpunto.singletons.PosMode;
import com.corpagos.corpunto.utils.ConsoleMessageManager;
import com.corpagos.corpunto.utils.Keyboard;
import com.corpagos.corpunto.widgets.CustomKeyboard;
import com.google.gson.Gson;
import com.imagpay.enums.CardDetected;
import com.imagpay.enums.PrintStatus;

public class PaymentActivity extends BaseActivity implements KeyListener, ReadCardListener {

    private ProviderInformationFragment providerInformationFragment = new ProviderInformationFragment();
    private ProviderPaymentFragment providerPaymentFragment = new ProviderPaymentFragment();
    private FindProviderFragment findProviderFragment = new FindProviderFragment();
    private FirstFragment firstFragment = new FirstFragment();
    private TransactionLocation transactionLocation;
    private FragmentTransaction fragTransaction;
    private CustomKeyboard keyboard;
    private PosDevice posDevice;
    private Provider provider;
    private Dialog dialog;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_provider;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarTitle("Pagos");
        keyboard = findViewById(R.id.customKeyboard);
        keyboard.setListener(this);
        dialog = new Dialog(this);
        posDevice = new PosDevice(this, this, this, true);
        showFirstFragment();
        findProviderFragment.setKeyboard(keyboard);
        try {
            if (!getIntent().getStringExtra("location").isEmpty()) {
                transactionLocation = new Gson().fromJson(getIntent().getStringExtra("location"),
                        TransactionLocation.class);
            }
        } catch (Exception e) {
            e.getMessage();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void closeActivity() {
        Keyboard.hideKeyboard(this);
        if (getFragmentManager().findFragmentByTag("first") != null &&
                getFragmentManager().findFragmentByTag("first").isVisible()) {
            posDevice.turnOffReadingPeripherals();
            finish();
            AnimationTransition.setOutActivityTransition(this);
        } else if (getFragmentManager().findFragmentByTag("tis") != null &&
                getFragmentManager().findFragmentByTag("tis").isVisible()) {
            showFirstFragment();
        } else if (getFragmentManager().findFragmentByTag("payment") != null &&
                getFragmentManager().findFragmentByTag("payment").isVisible()) {
            showProviderInfoFragment(provider);
        } else {
            showFindProviderInfo();
        }
    }

    public void setToolbarTitle(String title) {
        setTitle(title);
    }

    private void showFirstFragment() {
        posDevice.initReadingPeripherals();
        keyboard.setVisibility(View.INVISIBLE);
        fragTransaction = getFragmentManager().beginTransaction();
        fragTransaction.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
        fragTransaction.replace(R.id.fragment_layout, firstFragment, "first");
        fragTransaction.commit();
    }

    public void showFindProviderInfo() {
        posDevice.turnOffReadingPeripherals();
        keyboard.setVisibility(View.VISIBLE);
        fragTransaction = getFragmentManager().beginTransaction();
        fragTransaction.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
        fragTransaction.replace(R.id.fragment_layout, findProviderFragment, "tis");
        fragTransaction.commit();
    }

    public void showProviderInfoFragment(Provider provider) {
        this.provider = provider;
        providerInformationFragment.setProvider(provider);
        providerInformationFragment.setKeyboard(keyboard);
        fragTransaction = getFragmentManager().beginTransaction();
        fragTransaction.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
        fragTransaction.replace(R.id.fragment_layout, providerInformationFragment, "information");
        fragTransaction.commit();
    }

    public void showProviderPaymentFragment(Provider provider, ProviderAccount providerAccount,
                                            Transaction transaction) {
        providerPaymentFragment.setProviderInfo(provider, providerAccount);
        providerPaymentFragment.setTransactionLocation(transactionLocation);
        providerPaymentFragment.setKeyboard(keyboard);
        providerPaymentFragment.setTransaction(transaction);
        fragTransaction = getFragmentManager().beginTransaction();
        fragTransaction.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
        fragTransaction.replace(R.id.fragment_layout, providerPaymentFragment, "payment");
        fragTransaction.commit();
    }

    @Override
    public void onKeyPressed(Enum specialKey) {
        if (specialKey == KeyListener.specialKey.CANCEL) {
            finish();
        } else if (specialKey == KeyListener.specialKey.DONE) {
            if (getFragmentManager().findFragmentByTag("tis") != null &&
                    getFragmentManager().findFragmentByTag("tis").isVisible()) {
                if (findProviderFragment.validate()) {
                    findProviderFragment.requestProviderInfo();
                }
            } else if (getFragmentManager().findFragmentByTag("information") != null &&
                    getFragmentManager().findFragmentByTag("information").isVisible()) {
                if (providerInformationFragment.validate()) {
                    providerInformationFragment.selectAccount();
                }
            } else  if (getFragmentManager().findFragmentByTag("payment") != null &&
                    getFragmentManager().findFragmentByTag("payment").isVisible()) {
                if (providerPaymentFragment.isEtAmountFocus()) {
                    providerPaymentFragment.setEtPassFocus();
                } else {
                    if (providerPaymentFragment.validate()) {
                        providerPaymentFragment.prepareTransactionData();
                    }
                }
            }
        } else if (specialKey == KeyListener.specialKey.FORWARD) {
            if (getFragmentManager().findFragmentByTag("payment") != null &&
                    getFragmentManager().findFragmentByTag("payment").isVisible()) {
                providerPaymentFragment.setEtPassFocus();
            }
        } else if (specialKey == KeyListener.specialKey.BACK) {
            if (getFragmentManager().findFragmentByTag("payment") != null &&
                    getFragmentManager().findFragmentByTag("payment").isVisible()) {
                providerPaymentFragment.setEtAmountFocus();
            }
        }
    }

    @Override
    public void onCardRead(String json) {
        CardMode cardMode = CardMode.getInstance(this);
        switch (CardType.checkCardType(json)) {
            case CardType.credit:
                cardMode.setCardAccountType(CardAccountType.CREDIT);
                runOnUiThread(() -> dialog.showErrorDialogWithListenerButton(
                        "No se puede hacer pagos a tarjeta de credito", sweetAlertDialog ->
                                finish()
                ));
                break;
            case CardType.debit:
                if (getFragmentManager().findFragmentByTag("first") != null &&
                        getFragmentManager().findFragmentByTag("first").isVisible()) {
                    posDevice.turnOffReadingPeripherals();
                    cardMode.setCardAccountType(CardAccountType.DEBIT);
                    PosMode posMode = PosMode.getInstance(this);
                    posMode.setPosMode(PosModeType.PAYMENT);
                    Intent intent = new Intent(this, MainActivity.class);
                    startActivity(intent);
                    finish();
                    AnimationTransition.setInActivityTransition(this);
                }
                break;
            case CardType.undefined:
                break;
        }
    }

    @Override
    public void onCardDetect(CardDetected cardDetected, Transaction transaction, CardData cardData) {
        if (cardDetected == CardDetected.INSERTED) {
            ConsoleMessageManager.consoleCardInsertedMsg();
            CardMode cardMode = CardMode.getInstance(this);
            cardMode.setCardType(CardTypeEnum.CHIP);
        }
        if (cardDetected == CardDetected.REMOVED) {

        }

    }

    @Override
    public void onPrintStatus(PrintStatus paramPrintStatus) {

    }

    @Override
    public void onChipError() {

    }

    @Override
    public void onProcess() {

    }

    @Override
    public void onError(String error) {

    }

    @Override
    public void onViewEnabled() {

    }
}
