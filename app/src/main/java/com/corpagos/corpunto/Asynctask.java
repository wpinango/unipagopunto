package com.corpagos.corpunto;

import android.content.Context;
import android.os.AsyncTask;

import com.corpagos.corpunto.encryption.AESCipher;
import com.corpagos.corpunto.encryption.RSACipher;
import com.corpagos.corpunto.interfaces.AsynctaskListener;
import com.corpagos.corpunto.models.Encryption;
import com.corpagos.corpunto.models.Keys;
import com.corpagos.corpunto.utils.Util;
import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Asynctask {
    private static int timeout = 30000;

    //public static String authHost =  "http://172.16.31.28:3000";//"http://172.16.31.28:3000";
    //public static String resourcesHost =  "http://172.16.31.28:3001";//"http://172.16.31.28:3001";
    //public static String transactionalHost = "http://172.16.31.28:3002";//"http://172.16.31.28:3002";
    /*public static String authHost =  "http://46.101.158.43:3000";
    public static String resourcesHost =  "http://46.101.158.43:3001";
    public static String transactionalHost = "http://46.101.158.43:3002";*/
    public static String authHost =  "http://200.74.233.35:3000";
    public static String resourcesHost =  "http://200.74.233.36:3000";
    public static String transactionalHost = "http://200.74.233.37:3000";

    public static final ArrayList<String> host = new ArrayList<>();
    public static String finalHost = authHost;
    //----------------------------------------------------------------------------------------------
    public static final String authorization = "Authorization";
    //----------------------------------------------------------------------------------------------
    public static final String URL_REGISTER_LOCATION = "/device_geolocation/register_location";
    public static final String URL_GET_LAST_OPERATIONS = "/transaction/last_day_operations";
    public static final String URL_DO_PROVIDER_PAYMENT = "/transaction/provider_payment/";
    public static final String URL_GET_LAST_CLOSURE = "/transaction/get_last_closing";
    public static final String URL_SERVICE_PAYMENT = "/transaction/service_payment/";
    public static final String URL_GET_RSA_KEY = "/transaction/generate_rsa_key";
    public static final String URL_TRANSACTION_REVERSE = "/transaction/reverse";
    public static final String URL_CHANGE_PASSWORD = "/device/change_password/";
    public static final String URL_GET_CLOSING = "/transaction/get_closing";
    public static final String URL_GET_VERSION = "/app_dist/check_version/";
    public static final String URL_STORE_AES = "/transaction/store_aes_key";
    public static final String URL_GET_SERVICE = "/transaction/services";
    public static final String URL_FIRST_LOGIN = "/device/first_login/";
    public static final String URL_DO_TRANSACTION = "/transaction/";
    public static final String URL_GET_FIND_PROVIDER = "/provider/";
    public static final String URL_LOGOUT = "/device/logout/"; //TODO eliminar !!!!
    public static final String URL_AUTH = "/oauth/token";
    //----------------------------------------------------------------------------------------------
    public static final String USER_NOT_FOUND = "USER-NOT-FOUND";
    public static final String OK_RESPONSE = "OK";
    public static final String TIMEOUT_MESSAGE = "java.net.SocketTimeoutException";
    public static final String FAILED_TO_CONNECT = "failed to connect to";
    //----------------------------------------------------------------------------------------------

    public static class GetMethodAsynctask extends AsyncTask<String, String, String> {
        private String endPoint, requestHeaders, response, host;
        private HttpRequest request;
        private Map<String,String> headers;
        private AsynctaskListener listener;

        public GetMethodAsynctask(String host,String endPoint, Map<String, String> headers,
                                  AsynctaskListener listener) {
            this.endPoint = endPoint;
            this.headers = headers;
            this.listener = listener;
            this.host = host;
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                String requestAddress = host + endPoint;
                System.out.println("valores url  : " + requestAddress);
                request = HttpRequest.get(requestAddress)
                        .accept("application/json")
                        .headers(headers)
                        .connectTimeout(timeout)
                        .readTimeout(timeout);
                response = request.body();
                requestHeaders = new Gson().toJson(request.headers());
            } catch (Exception e) {
                e.getMessage();
                response = "";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                listener.onAsynctaskFinished(endPoint, s,requestHeaders);
            } catch (Exception e) {
                e.getMessage();
            }
        }
    }

    public static class PostMethodAsynctask extends AsyncTask<String, String, String> {
        private String endPoint, response, requestHeaders, host;
        private HttpRequest request;
        private Map<String, String> headers;
        private String body;
        private AsynctaskListener listener;

        public PostMethodAsynctask(String host, String body, String endPoint, Map<String,String> headers,
                                   AsynctaskListener listener) {
            this.endPoint = endPoint;
            this.headers = headers;
            this.listener = listener;
            this.body = body;
            this.host = host;
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                request = HttpRequest.post(host + endPoint)
                        .accept("application/json")
                        .headers(headers)
                        .contentType("application/json")
                        .connectTimeout(timeout)
                        .readTimeout(timeout)
                        .send(body);
                response = request.body();
                System.out.println("valores ip : " + host);
                System.out.println("valores : 1 " + response );
                requestHeaders = new Gson().toJson(request.headers());
            }  catch (Exception e) {
                e.getMessage();
                response = "";
                System.out.println("valores : " + e.getMessage());
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                System.out.println("valores : response : " + s);
                listener.onAsynctaskFinished(endPoint, s, requestHeaders);
            } catch (Exception e) {
                System.out.println("valores error : " + e);
            }
        }
    }

    public static class PostFormMethodAsynctask extends AsyncTask<String, String, String> {
        private String endPoint, response, requestHeaders, host;
        private HttpRequest request;
        private Map<String, String> headers;
        private Map<String, Object> body;
        private AsynctaskListener listener;

        public PostFormMethodAsynctask(String host, Map<String, Object> body,String endPoint,
                                       Map<String,String> headers, AsynctaskListener listener) {
            this.endPoint = endPoint;
            this.headers = headers;
            this.listener = listener;
            this.body = body;
            this.host = host;
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                String requestAddress = host + endPoint;
                request = HttpRequest.post(requestAddress.trim())
                        .accept("application/json")
                        .headers(headers)
                        .contentType("application/x-www-form-urlencoded")
                        .connectTimeout(timeout)
                        .readTimeout(timeout)
                        .form(body);
                response = request.body();
                System.out.println("valores : 12 " + response);
                requestHeaders = new Gson().toJson(request.headers());
            } catch (HttpRequest.HttpRequestException e) {
                System.out.println("valores : error http " + e.getMessage());
                e.printStackTrace();
                if (e.getMessage().equals(TIMEOUT_MESSAGE)) {
                    response = TIMEOUT_MESSAGE;
                } else if (e.getMessage().contains(FAILED_TO_CONNECT)) {
                    response = FAILED_TO_CONNECT;
                } else {
                    response = "";
                }
            } catch (Exception e) {
                e.getMessage();
                response = "";
                System.out.println("valores : error " + e.getMessage());
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                listener.onAsynctaskFinished(endPoint, s, requestHeaders);
            } catch (Exception e) {
                e.getMessage();
            }
        }
    }

    public static String getDecryptedAESResponse(Context context, String message) {
        Encryption encryption = new Gson().fromJson(message, Encryption.class);
        Keys keys = Keys.getKeys(context);
        String r = null;
        try {
            r = AESCipher.decryptDirect(encryption.getEncrypted(), keys.getKey(), keys.getIv());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return r;
    }

    public static Map<String, Object> getEncryptedAESRequest(Context context, Map<?, ?> map) {
        Keys keys = Keys.getKeys(context);
        String json = new Gson().toJson(map);
        Map<String, Object> body = new HashMap<>();
        String enc = null;
        try {
            enc = AESCipher.encryptDirect(json, keys.getKey(), keys.getIv());
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("valores error request : " + e.toString());
        }
        body.put("encrypted", enc.replace("\n",""));
        body.put("checksum", Util.md5(json));
        return body;
    }

    public static Map<String, Object> getEncryptedRSARequest(Context context, String rsaKey) throws Exception {
        Keys keys = Keys.generateNewRandomKeys(context);
        String keysToJson = new Gson().toJson(keys).trim();
        String keysEncrypted = RSACipher.encrypt(keysToJson,
                RSACipher.stringToPublicKey(rsaKey.trim())).replace("\n", "");
        Encryption encryption = new Encryption();
        encryption.setChecksum(Util.md5(keysToJson));
        encryption.setEncrypted(keysEncrypted);
        Map<String, Object> body = new HashMap<>();
        body.put("encrypted", keysEncrypted.trim());
        body.put("checksum", Util.md5(keysToJson));
        return body;
    }
}
