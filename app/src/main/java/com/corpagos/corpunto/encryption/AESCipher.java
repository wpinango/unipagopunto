package com.corpagos.corpunto.encryption;

import android.util.Base64;
import android.util.Log;

import com.google.gson.Gson;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class AESCipher {

    public static String encryptDirect(String plainText, byte[] key, byte[] iv) throws Exception{
        SecretKeySpec secretKey = new SecretKeySpec((key), "AES");
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
        cipher.init(Cipher.ENCRYPT_MODE, secretKey, new IvParameterSpec(iv));
        return new String(Base64.encode(cipher.doFinal(plainText.getBytes(StandardCharsets.UTF_8)),
                Base64.DEFAULT));
    }

    public static String decryptDirect(String message , byte[] key, byte[] iv) throws Exception{
        SecretKey secretKey = new SecretKeySpec((key), "AES");
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
        cipher.init(Cipher.DECRYPT_MODE, secretKey, new IvParameterSpec(iv));
        return new String(cipher.doFinal(Base64.decode(message.getBytes(StandardCharsets.UTF_8),
                Base64.DEFAULT)));
    }

    private static byte[] getKeyBytes(byte[] key) {
        byte[] keyBytes = new byte[256 / 8];
        Arrays.fill(keyBytes, (byte) 0x0);
        int length = key.length < keyBytes.length ? key.length : keyBytes.length;
        System.arraycopy(key, 0, keyBytes, 0, length);
        return keyBytes;
    }

    public static String generateSecureRandomKey(int length) {
        SecureRandom random = new SecureRandom();
        byte[] key = new byte[length];
        random.nextBytes(key);
        return Base64.encodeToString(key, Base64.DEFAULT);
    }
}
