package com.corpagos.corpunto.services;

import android.annotation.SuppressLint;
import android.os.AsyncTask;

import com.corpagos.corpunto.dialogs.Dialog;
import com.google.firebase.iid.FirebaseInstanceId;

import java.io.IOException;

public class DeleteFirebaseToken {

    @SuppressLint("StaticFieldLeak")
    public static void deleteFBToken(Dialog dialog) {
        new AsyncTask<Void,Void,Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try
                {
                    FirebaseInstanceId.getInstance().deleteInstanceId();
                } catch (IOException e)
                {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                dialog.dismissDialog();
                System.out.println("pase ");
                //installApk(Environment.getExternalStoragePublicDirectory(
                //        Environment.DIRECTORY_DOWNLOADS + "/" + fileName));
            }
        }.execute();
    }

}
