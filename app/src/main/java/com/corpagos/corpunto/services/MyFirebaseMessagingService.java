package com.corpagos.corpunto.services;

import android.content.Intent;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.util.Log;


import com.corpagos.corpunto.Asynctask;
import com.corpagos.corpunto.activities.NotificationActivity;
import com.corpagos.corpunto.databases.BackupFiles;
import com.corpagos.corpunto.databases.ClosureDBHelper;
import com.corpagos.corpunto.databases.TransactionDBHelper;
import com.corpagos.corpunto.interfaces.AsynctaskListener;
import com.corpagos.corpunto.models.DailyClosure;
import com.corpagos.corpunto.models.Notification;
import com.corpagos.corpunto.models.Response;
import com.corpagos.corpunto.models.Token;
import com.corpagos.corpunto.models.Transaction;
import com.corpagos.corpunto.models.TransactionType;
import com.corpagos.corpunto.app.AppCredentials;
import com.corpagos.corpunto.utils.SharedPreferencesHandler;
import com.corpagos.corpunto.utils.Time;
import com.firebase.jobdispatcher.Constraint;
import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.firebase.jobdispatcher.Lifetime;
import com.firebase.jobdispatcher.Trigger;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

import static com.corpagos.corpunto.activities.MainLauncherActivity.version;


public class MyFirebaseMessagingService extends FirebaseMessagingService implements AsynctaskListener {

    private static final String TAG = "MyFirebaseMsgService";
    public static final String ACTION_INTENT = "com.bhinary.unipagopunto.notification.action.UI_UPDATE";

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages
        // are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data
        // messages are the type
        // traditionally used with GCM. Notification messages are only received here in
        // onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated
        // notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages
        // containing both notification
        // and data payloads are treated as notification messages. The Firebase console always
        // sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "Firebase From: " + remoteMessage.getFrom());
        System.out.println("message " + remoteMessage.toString());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Firebase Message data payload: " + remoteMessage.getData());

            NotificationActivity.isNotification = true;

            if (remoteMessage.getData().containsKey("type")) {
                if (remoteMessage.getData().get("type").equals("Update")) {
                    NotificationActivity.isUpdateNotification = true;
                    Notification.NotificationData notificationData = new Gson().fromJson(new Gson().toJson(
                            remoteMessage.getData()), Notification.NotificationData.class);
                    System.out.println("firebase valoress " + notificationData.getData());
                    sendNotificationToPrincipalActivity(new Gson().toJson(notificationData));
                    SharedPreferencesHandler.saveData(this, SharedPreferencesHandler.KEY_UPDATE,
                            new Gson().toJson(notificationData),
                            SharedPreferencesHandler.KEY_UPDATE_FILE);
                    notificationData.setTime(Time.getTimestamp());
                    BackupFiles.addNotificationContentToFile(this,
                            BackupFiles.notificationBackupFileName, notificationData);
                } else if (remoteMessage.getData().get("type").equals("PosClosing")) {
                    if (AppCredentials.isLogin(getApplicationContext())) {
                        Token tokenObject = AppCredentials.getToken(getApplicationContext());
                        Map<String, String> headers = new HashMap<>();
                        headers.put(Asynctask.authorization, tokenObject.getToken_type() + " " +
                                tokenObject.getAccess_token());
                        new Asynctask.GetMethodAsynctask(Asynctask.transactionalHost,
                                Asynctask.URL_GET_LAST_CLOSURE, headers, this).execute();
                    }
                } else if (remoteMessage.getData().get("type").equals("GeolocationRequest")) {
                    scheduleJob();
                }
            }


            /*if (*//* Check if data needs to be processed by long running job *//* true) {
                // For long-running tasks (10 seconds or more) use Firebase Job Dispatcher.
                scheduleJob();
            } else {
                // Handle message within 10 seconds
                handleNow();
            }*/

        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Firebase Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }

    private void updateClosureView(String value) {
        Intent intent = new Intent("transaction.closure.UI_UPDATE");
        intent.putExtra("UI_KEY2",value);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    @Override
    public void onNewToken(String token) {
        Log.d(TAG, "Firebase Refreshed token: " + token);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        System.out.println("Firebase token" + token);
        sendRegistrationToServer(token);
    }
    // [END on_new_token]

    /**
     * Schedule a job using FirebaseJobDispatcher.
     */
    private void scheduleJob() {
        /*FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(this));
        Job myJob = dispatcher.newJobBuilder()
                .setService(JobLocationService.class)
                .setLifetime(Lifetime.FOREVER)
                .setTrigger(Trigger.executionWindow(0, 60))
                .setTag("JobLocation")
                .setReplaceCurrent(true)
                .setConstraints(Constraint.ON_ANY_NETWORK)
                .build();
        dispatcher.schedule(myJob);*/
    }

    /**
     * Handle time allotted to BroadcastReceivers.
     */
    private void handleNow() {
        Log.d(TAG, "Firebase Short lived task is done.");
    }

    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        /*if (AppCredentials.isLogin(getApplicationContext())) {
            Token tokenObject = AppCredentials.getToken(getApplicationContext());
            Map<String, String> headers = new HashMap<>();
            headers.put(Asynctask.authorization, tokenObject.getToken_type() + " " +
                    tokenObject.getAccess_token());
            new Asynctask.PostMethodAsynctask(Asynctask.resourcesHost, "",
                    Asynctask.URL_FIREBASE_TOKEN_REGISTER + token, headers, this).execute();
        } else {
            SharedPreferencesHandler.saveData(getApplicationContext(), SharedPreferencesHandler
                    .KEY_FB_TOKEN, token, SharedPreferencesHandler.KEY_FB_TOKEN_FILE);
        }*/
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private void sendNotification(String messageBody) {
        /*Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0  Request code , intent,
                PendingIntent.FLAG_ONE_SHOT);

        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.drawable.ic_stat_ic_notification)
                        .setContentTitle("Neopagos")
                        .setContentText(messageBody)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(0  ID of notification , notificationBuilder.build());*/
    }

    protected void sendNotificationToPrincipalActivity(String value) {
        Intent intent = new Intent(ACTION_INTENT);
        intent.putExtra("UI_KEY", value);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    @Override
    public void onAsynctaskFinished(String endPoint, String response, String headers) {
        if (!response.equals("")) {
            Response res = new Gson().fromJson(response, Response.class);
            if (!res.isError() && res.getMessage().equals(Asynctask.OK_RESPONSE)) {
                if (endPoint.equals(Asynctask.URL_GET_LAST_CLOSURE)) {
                    DailyClosure dailyClosure = new Gson().fromJson(new Gson().toJson(res.getData()),
                            DailyClosure.class);
                    Transaction transaction = new Transaction();
                    transaction.setTransactionType(TransactionType.CLOSURE);
                    transaction.setAmount(dailyClosure.getSummary().getTotal_amount());
                    transaction.setApp_version(version);
                    transaction.setCreated_at(dailyClosure.getSummary().getClose_date());
                    transaction.setDate(Time.getCurrentDate());
                    transaction.setSelected(false);
                    transaction.setLot(dailyClosure.getSummary().getLot_number());
                    transaction.setReference("1041551637202");
                    transaction.setReverse(1);
                    transaction.setStatus("ACCEPTED");
                    transaction.setCard_type("NOT_APPLY");
                    TransactionDBHelper dbHelper = new TransactionDBHelper(this);
                    dbHelper.updateClosureStatus();
                    dbHelper.insertTransaction(transaction);
                    ClosureDBHelper closureDBHelper = new ClosureDBHelper(this);
                    closureDBHelper.insertClosure(dailyClosure);
                    updateClosureView("value");
                }
            }
        }
    }
}
