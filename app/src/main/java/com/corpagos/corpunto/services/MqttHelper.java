package com.corpagos.corpunto.services;

import android.annotation.SuppressLint;
import android.content.Context;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.corpagos.corpunto.singletons.PosModel;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.DisconnectedBufferOptions;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import static com.corpagos.corpunto.Global.software;


public class MqttHelper {

    public MqttAndroidClient mqttAndroidClient;
    private final String serverUri = "tcp://200.74.233.36:1883";
    //public static String softwareTopic;
    private final String username = "admin";
    private final String password = "mqtt";
    private String imei;

    @SuppressLint("MissingPermission")
    public MqttHelper(Context context){
        PosModel posModel = PosModel.getInstance(context);
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        imei = telephonyManager.getDeviceId();
        /*switch (posModel.getPosModelType()) {
            case WPOS_MINI:
                softwareTopic = software;
                break;
            case WPOS_3:
                softwareTopic = software;
                break;
            case Z90:
                softwareTopic = software;
                break;
        }*/
        mqttAndroidClient = new MqttAndroidClient(context, serverUri, MqttClient.generateClientId());
        mqttAndroidClient.setCallback(new MqttCallbackExtended() {
            @Override
            public void connectComplete(boolean b, String s) {
                Log.w("valores mqtt", s);
            }

            @Override
            public void connectionLost(Throwable throwable) {

            }

            @Override
            public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {
                Log.w("valores Mqtt", mqttMessage.toString());
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

            }
        });
        connect();
    }

    public void setCallback(MqttCallbackExtended callback) {
        mqttAndroidClient.setCallback(callback);
    }

    private void connect(){
        MqttConnectOptions mqttConnectOptions = new MqttConnectOptions();
        mqttConnectOptions.setAutomaticReconnect(true);
        mqttConnectOptions.setCleanSession(false);
        mqttConnectOptions.setUserName(username);
        mqttConnectOptions.setPassword(password.toCharArray());
        try {
            mqttAndroidClient.connect(mqttConnectOptions, null, new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    DisconnectedBufferOptions disconnectedBufferOptions = new DisconnectedBufferOptions();
                    disconnectedBufferOptions.setBufferEnabled(true);
                    disconnectedBufferOptions.setBufferSize(100);
                    disconnectedBufferOptions.setPersistBuffer(false);
                    disconnectedBufferOptions.setDeleteOldestMessages(false);
                    mqttAndroidClient.setBufferOpts(disconnectedBufferOptions);
                    subscribeToTopic();
                    System.out.println("valores : onConnectec " + mqttAndroidClient.isConnected());
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    Log.w("valores Mqtt", "Failed to connect to: " + serverUri + " " + exception.toString());
                }
            });
        } catch (MqttException ex){
            ex.printStackTrace();
            System.out.println("valores error mqtt: " + ex.getMessage());
        }
    }

    private void subscribeToTopic() {
        try {
            mqttAndroidClient.subscribe(imei, 0, null, new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    Log.w("valores Mqtt","Subscribed!");
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    Log.w("valores Mqtt", "Subscribed fail!");
                }
            });
        } catch (MqttException ex) {
            //System.err.println("Exceptionst subscribing");
            ex.printStackTrace();
            System.out.println("valores error mqtt: " + ex.getMessage());
        }
        /*try {
            mqttAndroidClient.subscribe(softwareTopic, 0, null, new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    Log.w("valores Mqtt","Subscribed!");
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    Log.w("valores Mqtt", "Subscribed fail!");
                }
            });
        } catch (MqttException ex) {
            //System.err.println("Exceptionst subscribing");
            ex.printStackTrace();
            System.out.println("valores error mqtt: " + ex.getMessage());
        }*/
    }
}
