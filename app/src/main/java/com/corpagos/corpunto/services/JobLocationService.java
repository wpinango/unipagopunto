package com.corpagos.corpunto.services;


import android.annotation.SuppressLint;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.corpagos.corpunto.Asynctask;
import com.corpagos.corpunto.interfaces.AsynctaskListener;
import com.corpagos.corpunto.locations.LocationService;
import com.corpagos.corpunto.models.Token;
import com.corpagos.corpunto.utils.SharedPreferencesHandler;

import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

import static com.corpagos.corpunto.locations.LocationService.BROADCAST_ACTION;

public class JobLocationService extends JobService implements AsynctaskListener {

    private JobParameters job;


    @Override
    public boolean onStartJob(@NonNull JobParameters job) {
        System.out.println("valores location start job");
        this.job = job;
        IntentFilter filterLocation = new IntentFilter(BROADCAST_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(locationReceiver,
                filterLocation);
        Intent locationIntent = new Intent(this, LocationService.class);
        startService(locationIntent);
        return false;
    }

    @Override
    public boolean onStopJob(@NonNull JobParameters job) {
        System.out.println("valores location stop job");
        return false;
    }

    @Override
    public void onAsynctaskFinished(String endPoint, String response, String headers) {
        System.out.println("valores location respuesta : " + response);
        if (!response.equals("")) {
            if (job != null) {
                jobFinished(job, false);
                LocalBroadcastManager.getInstance(this).unregisterReceiver(locationReceiver);
            }
        }
    }

    private BroadcastReceiver locationReceiver = new BroadcastReceiver() {
        @SuppressLint("WrongConstant")
        @Override
        public void onReceive(Context context, Intent intent) {
            if (BROADCAST_ACTION.equals(intent.getAction())) {
                Map<String, Object> body = new HashMap<>();
                body.put("latitude", intent.getDoubleExtra("Latitude",0));
                body.put("longitude", intent.getDoubleExtra("Longitude",0));
                Token token = new Token();
                if (!SharedPreferencesHandler.getSavedData(getApplicationContext(),
                        SharedPreferencesHandler.KEY_TOKEN, SharedPreferencesHandler.FILE_TOKEN)
                        .equals(SharedPreferencesHandler.NOT_FOUND) &&
                        !SharedPreferencesHandler.getSavedData(getApplicationContext(),
                                SharedPreferencesHandler.KEY_TOKEN, SharedPreferencesHandler.FILE_TOKEN)
                                .equals("")) {
                    token = new Gson().fromJson(SharedPreferencesHandler.getSavedData(
                            getApplicationContext(), SharedPreferencesHandler.KEY_TOKEN,
                            SharedPreferencesHandler.FILE_TOKEN), Token.class);
                } else {
                    token.setExpires_in(0);
                    token.setAccess_token("");
                    token.setToken_type("");
                }
                Map<String, String> headers = new HashMap<>();
                headers.put(Asynctask.authorization, token.getToken_type() + " " + token.getAccess_token());
                new Asynctask.PostMethodAsynctask(Asynctask.resourcesHost, new Gson().toJson(body),
                        Asynctask.URL_REGISTER_LOCATION, headers, JobLocationService.this).execute();
                stopService(new Intent(JobLocationService.this, LocationService.class));
            }
        }
    };
}
