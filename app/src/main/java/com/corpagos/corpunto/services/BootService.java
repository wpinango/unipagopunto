package com.corpagos.corpunto.services;

import android.app.ActivityManager;
import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.os.IBinder;

import androidx.annotation.Nullable;

import java.util.List;

public class BootService extends Service {
    private Thread workerThread = null;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if (workerThread == null || !workerThread.isAlive()) {
            workerThread = new Thread(() -> {
                isForeground(getApplicationContext().getPackageName());
            });
            workerThread.start();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Intent restartService = new Intent("BootService");
        sendBroadcast(restartService);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        /*if (workerThread == null || !workerThread.isAlive()) {
            workerThread = new Thread(() -> {
               isForeground(getApplicationContext().getPackageName());
            });
            workerThread.start();
        }*/
        return START_STICKY;
    }


    public boolean isForeground(String myPackage) {
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> runningTaskInfo = manager.getRunningTasks(1);
        ComponentName componentInfo = runningTaskInfo.get(0).topActivity;
        System.out.println("servicio : " + componentInfo.getPackageName() + " " + myPackage);
        return componentInfo.getPackageName().equals(myPackage);
    }
}
