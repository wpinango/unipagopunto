package com.corpagos.corpunto.interfaces;

public interface OnClick {
    void onClick(int position);
}
