package com.corpagos.corpunto.interfaces;

public interface CardInterface {
    void onCardReading(String json);
}
