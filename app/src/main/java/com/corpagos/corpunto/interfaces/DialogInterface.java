package com.corpagos.corpunto.interfaces;

public interface DialogInterface {
    void positiveClick();
    void negativeClick();
}
