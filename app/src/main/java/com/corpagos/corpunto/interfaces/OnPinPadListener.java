package com.corpagos.corpunto.interfaces;

public interface OnPinPadListener {
    void onUpDate();
    void onCancel();
    void onByPass();
    void onSuccess(String pin);
    void onError(int errorCode,String errorMsg);
}
