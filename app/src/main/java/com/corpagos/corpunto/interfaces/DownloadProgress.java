package com.corpagos.corpunto.interfaces;

public interface DownloadProgress {
    int downloadFinish = 200000;
    enum progressType {
        FINISH, ONPROGRESS,ERROR
    }
    void onDownloadUpdate(Enum progressType, int progress);
}
