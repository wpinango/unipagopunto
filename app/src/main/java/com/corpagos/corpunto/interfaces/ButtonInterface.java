package com.corpagos.corpunto.interfaces;

public interface ButtonInterface {
    void onPositiveButtonClick(String buttonAction);
    void onNegativeButtonClick(String buttonAction);

}
