package com.corpagos.corpunto.interfaces;

public interface AsynctaskListener {
    enum requestType {
        REQUESTIDENTITY,
        PROCESSPAYMENT,
        GETTRANSACTIONLIST
    }
    void onAsynctaskFinished(String endPoint ,String response, String headers);
}
