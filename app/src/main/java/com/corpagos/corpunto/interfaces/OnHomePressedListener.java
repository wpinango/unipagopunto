package com.corpagos.corpunto.interfaces;

public interface OnHomePressedListener {
    void onHomePressed();
    void onHomeLongPressed();
}
