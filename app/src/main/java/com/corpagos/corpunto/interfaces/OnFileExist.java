package com.corpagos.corpunto.interfaces;

public interface OnFileExist {
    void onFileExist(boolean isExist);
}
