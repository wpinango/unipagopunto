package com.corpagos.corpunto.interfaces;

import com.corpagos.corpunto.models.CardData;
import com.corpagos.corpunto.models.Transaction;
import com.imagpay.enums.CardDetected;
import com.imagpay.enums.PrintStatus;

public interface ReadCardListener {
    void onCardRead(String json);
    void onCardDetect(CardDetected cardDetected, Transaction transaction, CardData cardData);
    void onPrintStatus(PrintStatus paramPrintStatus);
    void onChipError();
    void onProcess();
    void onError(String error);
    void onViewEnabled();
}
