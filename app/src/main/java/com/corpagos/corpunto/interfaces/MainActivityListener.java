package com.corpagos.corpunto.interfaces;

import com.corpagos.corpunto.enums.PaymentOptionEnum;
import com.corpagos.corpunto.enums.PosModeType;
import com.corpagos.corpunto.enums.ReadCardType;
import com.imagpay.enums.CardDetected;

public interface MainActivityListener {
    void onCleanData();
    void onCardDetected(CardDetected cardDetected);
    void onReadCard(ReadCardType readCardType);
    void onSuccessReadCard();
    void onFailedReadCard();
    void onPosMode(PosModeType posModeType);
    void onPaymentOption(PaymentOptionEnum paymentOptionEnum);
}
