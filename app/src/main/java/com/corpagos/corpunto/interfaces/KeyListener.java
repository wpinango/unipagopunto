package com.corpagos.corpunto.interfaces;

public interface KeyListener {
    enum specialKey {
        MENU,
        DONE,
        CANCEL,
        DELETE,
        BACK,
        FORWARD
    }
    void onKeyPressed(Enum specialKey);
}
